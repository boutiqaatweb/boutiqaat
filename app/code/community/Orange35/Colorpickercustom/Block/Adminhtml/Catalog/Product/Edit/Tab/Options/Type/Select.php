<?php
class Orange35_Colorpickercustom_Block_Adminhtml_Catalog_Product_Edit_Tab_Options_Type_Select extends
    Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Options_Type_Select
{
    private $_colorData;
    private $_uploader;
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        if (Mage::getStoreConfig('colorpicker_section/custom_group/product_module_enable')) {
            $this->setTemplate('orange35/colorpickercustom/catalog/product/edit/options/type/select.phtml');
        }
        $this->setCanEditPrice(true);
        $this->setCanReadPrice(true);
    }

    public function getUploader()
    {
        if (empty($this->_uploader)) {
            $this->_uploader = Mage::getModel('Mage_Adminhtml_Block_Media_Uploader');
        }
        return $this->_uploader;
    }

    public function getDataMaxSize()
    {
        return $this->getUploader()->getDataMaxSize();
    }

    public function getDataMaxSizeInBytes()
    {
        return $this->getUploader()->getDataMaxSizeInBytes();
    }

    public function getDeleteUrl()
    {
        return Mage::helper('orange35_colorpickercustom/image')->getDeleteUrl();
    }
    public function getConfigObj()
    {
        /**
         * @var $sesParam Mage_Adminhtml_Model_Url
         */
        $sesParam = Mage::getModel('adminhtml/url')->addSessionParam();
        $url = $sesParam->getUrl(
            Mage::helper('orange35_colorpickercustom/image')->getUploadPath()

        );
        $this->getConfig()->setUrl($url);
        $this->getConfig()->setParams(array('form_key' => $this->getFormKey()));
        $this->getConfig()->setFileField('Filedata');
        $this->getConfig()->setFilters(
            array(
                'images' => array(
                    'label' => Mage::helper('adminhtml')->__('Images (.gif, .jpg, .png)'),
                    'files' => array('*.gif', '*.jpg', '*.jpeg', '*.png')
                )
            )
        );
        $this->getConfig()->setWidth('32');
        $this->getConfig()->setHideUploadButton(true);
        return $this->getConfig()->getData();
    }

    public function getConfig()
    {
        if(is_null($this->_config)) {
            $this->_config = new Varien_Object();
        }

        return $this->_config;
    }
}