<?php
class Orange35_Colorpickercustom_Helper_Image extends Mage_Core_Helper_Abstract
{
    const MEDIA_PATH = 'catalog/custom_options';
    const MIN_HEIGHT = 10;
    const MAX_HEIGHT = 1800;
    const MIN_WIDTH  = 10;
    const MAX_WIDTH  = 3800;


    private $_heightMini = 16;
    private $_widthMini  = 16;
    private $_heightAdminPreview = 75;
    private $_widthAdminPreview  = 75;

    protected $_imageSize = array(
        'minheight' => self::MIN_HEIGHT,
        'minwidth'  => self::MIN_WIDTH,
        'maxheight' => self::MAX_HEIGHT,
        'maxwidth'  => self::MAX_WIDTH,
    );

    protected $_allowedExtensions = array('jpg', 'gif', 'png');

    private $_attributeImageDeletePath = 'adminhtml/colorpickercustom_media/delete/index';
    private $_attributeImageUploadPath = 'adminhtml/colorpickercustom_media/upload/index';


    public function getAllowedImageExtensions()
    {
        return $this->_allowedExtensions;
    }


    public function getBaseUrl()
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . self::MEDIA_PATH;
    }

    public static function getBaseDir()
    {
        return Mage::getBaseDir('media') . DS . str_replace("/", DS, self::MEDIA_PATH);
    }

    public function getDeleteUrl()
    {
        /**
         * @var $sesParam Mage_Adminhtml_Model_Url
         */
        $sesParam = Mage::getModel('adminhtml/url')->addSessionParam();
        $url = $sesParam->getUrl(
            $this->_attributeImageDeletePath

        );
        return $url;
    }

    public function getUploadPath()
    {
        return $this->_attributeImageUploadPath;
    }

    public function upload()
    {
        $path = $this->getBaseDir();
        $url = $this->getBaseUrl()."/";
        try {
            $uploader = new Mage_Core_Model_File_Uploader("file");
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(false);
            $uploader->setAllowCreateFolders(true);
            $uploader->setAllowedExtensions(array('gif', 'jpg', 'png')); //server-side validation of extension
            $result = $uploader->save($path);

            /**
             * Workaround for prototype 1.7 methods "isJSON", "evalJSON" on Windows OS
             */
            $result['tmp_name'] = str_replace(DS, "/", $result['tmp_name']);
            $result['path'] = str_replace(DS, "/", $result['path']);
            $result['url'] = $url . $result['file'];
            $result['html_id'] = Mage::app()->getRequest()->getPost('html_id');

            if (isset($result['file'])) {
                //$result = array_merge_recursive($this->delete($optionId), $result);
                //$this->_saveFileNameToDb($optionId, $attributeId, $result['file']);
                //$this->resizePack($result['file'], $attributeId);
            }
        } catch(Exception $e) {
            $result = array(
                "error"     => $e->getMessage(),
                "errorCode" => $e->getCode(),
                "status"    => "error"
            );
        }
        return $result;
    }

    public function deleteAttributeDirectory($attributeId)
    {
        if (!empty($attributeId) && $this->removePack($attributeId, null, true)) {
            return true;
        }
        return false;
    }

    /**
     * @param $optionId
     * @return array
     */
    public function delete($image)
    {

        $imageName = $image;
        if (empty($imageName)) {
            return array();
        }

        $path = self::getBaseDir() . DS;
        $imagePath = rtrim($path, DS) . DS . ltrim($imageName, DS);
        if ($this->removePack($imageName)) {
            /*$optionModel->setData('image', '');
            $optionModel->save();*/
            $result = array("status" => "success");
        } else {
            $result = array('status' => 'error', 'error' => 'can not remove image ' . $imagePath);
        }
        return $result;
    }

    private function removePack($imageName = null, $removeDirectory = false)
    {
        $sizes = array(
            //TODO видаляти фронтові кеші
            //$this->_getSizePrefix($this->_getProductSwatchWidth(), $this->_getProductSwatchHeight()),
            //$this->_getSizePrefix($this->_getProductListSwatchWidth(), $this->_getProductListSwatchHeight())
            $this->_getSizePrefix($this->_widthAdminPreview, $this->_heightAdminPreview)
            //$this->_getSizePrefix($this->_getProductListSwatchWidth(), $this->_getProductListSwatchHeight())
        );
        foreach ($sizes as $size) {
            if (!empty($imageName)) {
                $this->remove($this->_getCacheDir($size) . DS . $imageName);
            }
        }
        return $this->remove($this->getBaseDir() .  DS . $imageName);
    }

    public function getSwatchImageUrl($imageName)
    {
        return $this->_getImageUrl(
            $imageName,
            $this->_getProductSwatchWidth(),
            $this->_getProductSwatchHeight()
        );
    }

/*    public function getListSwatchImageUrl($imageName, $attributeId)
    {
        return $this->_getImageUrl(
            $imageName,
            $attributeId,
            $this->_getProductListSwatchWidth(),
            $this->_getProductListSwatchHeight()
        );
    }*/

    public function getPopupSwatchImageUrl($imageName)
    {
        return $this->_getImageUrl(
            $imageName,
            $this->_getProductPopupSwatchWidth(),
            $this->_getProductPopupSwatchHeight()
        );
    }

    public function getAdminPreviewImageUrl($imageName)
    {
        return $this->_getImageUrl($imageName, $this->_widthAdminPreview, $this->_heightAdminPreview);
    }

    /**
     * Removes folder with cached images
     *
     * @return boolean
     */
    public function getOptionsImages()
    {
        $images = array();
        $files = scandir($this->getBaseDir());
        foreach ($files as $file) {
            if ('.' !== $file && '..' !== $file && 'cache' !== $file) {
                $images[] =  $file;

            }
        }
        return $images;
    }

    public function flushImagesCache()
    {   //todo: викликати це чудо з конфігу
        $usedImages = array();
        $images = $this->getOptionsImages();
        $collection = Mage::getResourceModel('catalog/product_option_value_collection')->addFieldToFilter('image', array("in" => $images));
        foreach($collection as $option){
            $usedImages[] = $option->getImage();
        }
        foreach(array_diff($images, $usedImages) as $image){
            $this->delete($image);
        }
        $cacheDir  = $this->getBaseDir() . DS . 'cache' . DS ;
        $io = new Varien_Io_File();
        if ($io->fileExists($cacheDir, false) ) {
            $io->rmdir($cacheDir, true);
        }

        return true;
    }

    /**
     * @param $imageName
     * @param $attributeId
     * @param $width
     * @param $height
     * @return bool
     */
    protected function resizeCropThumb($imageName, $width, $height)
    {
        $width = (int) $width;
        $height = (int) $height;
        $size = $this->_getSizePrefix($width, $height);
        $cacheDir = $this->_getCacheDir($size);
        $io = new Varien_Io_File();
        $io->checkAndCreateFolder($cacheDir);
        $io->open(array('path' => $cacheDir));
        if ($io->fileExists($imageName)) {
            return true;
        }
        try {
            $image = new Varien_Image($this->getBaseDir() . DS . $imageName);
            $image->constrainOnly(false);
            $image->keepFrame(true);
            $image->backgroundColor(array(255,255,255));
            $image->keepAspectRatio(true);
            $image->resize($width, $height);
            $image->save($cacheDir . DS . $imageName);
            return true;
        } catch (Exception $e) {
            Mage::logException($e);
            return false;
        }
    }

    /**
     * Remove image by filePath
     *
     * @param string $path
     * @return bool
     */
    private function remove($path, $onlyFile = true)
    {
        $io = new Varien_Io_File();
            $io->open(array('path' => $this->getBaseDir()));
        if ($io->fileExists($path, $onlyFile)) {
            return $io->rm($path);
        }
        return false;
    }

    /**
     * @param $width
     * @param $height
     * @return string
     */
    protected function _getSizePrefix($width, $height)
    {
        return 'cache/' . $width . 'x' . $height;
    }

    /**
     * @return int
     */
    protected function _getProductSwatchHeight()
    {
        return (int) Mage::getStoreConfig('colorpicker_section/custom_group/product_swatch_height');
    }

    /**
     * @return int
     */
    protected function _getProductSwatchWidth()
    {
        return (int) Mage::getStoreConfig('colorpicker_section/custom_group/product_swatch_width');
    }



    /**
     * @return int
     */
    protected function _getProductPopupSwatchHeight()
    {
        return (int) Mage::getStoreConfig('colorpicker_section/custom_group/product_tooltip_height');
    }

    /**
     * @return int
     */
    protected function _getProductPopupSwatchWidth()
    {
        return (int) Mage::getStoreConfig('colorpicker_section/custom_group/product_tooltip_width');
    }

    protected function _getCacheDir($size)
    {
        return $this->getBaseDir() . DS . $size;
    }

    protected function _getImageUrl($imageName, $width, $height)
    {
        $this->resizeCropThumb($imageName, $width, $height);
        return
            $this->getBaseUrl() . '/' . $this->_getSizePrefix($width, $height) . '/' . $imageName;
    }
}
