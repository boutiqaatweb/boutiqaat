<?php

class Orange35_Colorpickercustom_Model_Observer
{
    const COLORPICKER_SECTION = 'colorpicker_section';

    public function insertBlock($observer)
    {
        if (!Mage::getStoreConfig('colorpicker_section/custom_group/product_module_enable')) {
            return $this;
        }
        /** @var $_block Mage_Core_Block_Abstract */
        /*Get block instance*/
        $_block = $observer->getBlock();
        /*get Block type*/
        $_type = $_block->getType();
        /*Check block type*/
        if ($_type == 'catalog/product_view_options_type_select') {
            /*Clone block instance*/
            $_child = clone $_block;
            /*set another type for block*/
            $_child->setType('test/block');
            /*set child for block*/
            $_block->setChild('child', $_child);
            /*set our template*/
            $_block->setTemplate('orange35_colorpickercustom/custom_options_js.phtml');
        }
        return $this;
    }

    public function addCustomLayoutConfigHandle(Varien_Event_Observer $observer)
    {
        /** @var $layout  Mage_Core_Model_Layout */
        $event = $observer->getEvent();
        $controllerAction = $event->getAction();
        $layout = $event->getLayout();
        if ($controllerAction && $layout && $controllerAction instanceof Mage_Adminhtml_System_ConfigController) {
            if ($controllerAction->getRequest()->getParam('section') == self::COLORPICKER_SECTION) {
                $layout->getUpdate()->addHandle('adminhtml_system_config_edit_orange35_colorpickercustom');
            }
        }
        return $this;
    }

/*    public function onDeleteAttribute(Varien_Event_Observer $observer)
    {
        $attribute = $observer->getData('attribute');
        $attributeId = $attribute->getData('attribute_id');
        $items = $this->getColorsOptions($attributeId);
        foreach ($items as $item) {
            Mage::helper('orange35_colorpicker/image')->delete($item['option_id']);
        }
        Mage::helper('orange35_colorpicker/image')->deleteAttributeDirectory($attributeId);
        return $this;
    }*/

    /**
     * Retrieve the product model
     *
     * @return Mage_Catalog_Model_Product $product
     */
    public function getProduct()
    {
        return Mage::registry('product');
    }

    /**
     * Shortcut to getRequest
     *
     */
    protected function _getRequest()
    {
        return Mage::app()->getRequest();
    }

}