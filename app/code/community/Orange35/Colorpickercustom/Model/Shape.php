<?php
class Orange35_Colorpickercustom_Model_Shape
{
    public function toOptionArray()
    {
        return array(
            array('value'=>"square", 'label'=>Mage::helper('orange35_colorpickercustom')->__('Square')),
            array('value'=>"rounded", 'label'=>Mage::helper('orange35_colorpickercustom')->__('Rounded')),
            array('value'=>"circle", 'label'=>Mage::helper('orange35_colorpickercustom')->__('Circle')),
        );
    }
}