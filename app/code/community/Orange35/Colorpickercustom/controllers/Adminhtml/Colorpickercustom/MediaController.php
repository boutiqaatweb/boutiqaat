<?php
class Orange35_Colorpickercustom_Adminhtml_Colorpickercustom_MediaController extends Mage_Adminhtml_Controller_Action
{

    public function uploadAction()
    {
        if (!empty($_FILES))
        {
            $result = Mage::helper('orange35_colorpickercustom/image')->upload();
            //$result['url'] = $this->getHelperImage()->getAdminPreviewImageUrl($result['name'], $attributeId);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    public function flushAction()
    {
        $this->getHelperImage()->flushImagesCache();
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array("status" => "success")));
    }

    public function deleteAction()
    {
        $image = $this->getRequest()->getParam('image');
        if (empty($image)) {
            $this->getResponse()->setBody(
                Mage::helper('core')->jsonEncode(array('status' => 'error', 'error' => 'no Image option'))
            );
            return false;
        }
        try {
            $result = $this->getHelperImage()->delete($image);
        } catch(Exception $e) {
            $result = array(
                "error"     => $e->getMessage(),
                "errorCode" => $e->getCode(),
                "status"    => "error"
            );
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        return false;
    }

    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/catalog/products');
    }

    /**
     * @return Orange35_Colorpicker_Helper_Image
     */
    protected function getHelperImage()
    {
        return Mage::helper('orange35_colorpickercustom/image');
    }
}