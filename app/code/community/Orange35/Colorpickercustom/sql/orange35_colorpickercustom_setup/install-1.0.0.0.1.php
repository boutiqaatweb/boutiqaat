<?php

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 * @var $adapter Varien_Db_Adapter_Pdo_Mysql
 */
$installer = $this;
$installer->startSetup();
$adapter = $installer->getConnection();
$adapter->addColumn($installer->getTable('catalog/product_option'), 'is_colorpicker', 'INTEGER NULL');

$adapter->addColumn($installer->getTable('catalog/product_option_type_value'), 'color', 'VARCHAR(128) NULL');
$adapter->addColumn($installer->getTable('catalog/product_option_type_value'), 'image', 'VARCHAR(128) NULL');

$installer->endSetup();
