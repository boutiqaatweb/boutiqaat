<?php

class Orange35_ImageSwitcher_Block_Adminhtml_Catalog_Product_Edit_Tab_CustomMatches
    extends Mage_Adminhtml_Block_Widget
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    const PREVIEW_IMAGE_SIZE        = 120;

    protected $_backgroundColor = array(255, 255, 255);

    protected $_productImages;

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('orange35/imageSwitcher/catalog/product/edit/custom-matches.phtml');
    }

    public function getTabLabel()
    {
        return $this->__('Custom Options Image Combinations');
    }

    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    /**
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        return Mage::registry('current_product');
    }

    /**
     * @param $product Mage_Catalog_Model_Product
     * @return Orange35_ImageSwitcher_Model_Resource_Matches_Collection
     */
    public function getMatchesCollection($product)
    {
        $collection = Mage::getModel('orange35_imageSwitcher/matches')->getCollection()
            ->addFieldToFilter('product_id', array('eq' => $product->getId()));
        foreach ($collection as $match) {
            /** @var Orange35_ImageSwitcher_Model_Matches $matches */
            $match->setData('matches', json_decode($match->getData('matches')));
        }
        return $collection;
    }

    /**
     * @return array
     */
    public function prepareData4JS()
    {
        $data = array();
        $product = $this->getProduct();
        /** @var Orange35_ImageSwitcher_Helper_Matches $matchesHelper */
        $matchesHelper = Mage::helper('orange35_imageSwitcher/matches');
        $matches = $matchesHelper->findMatchesByProductId($product);
        $preparedMatches = array();
        foreach ($matches as $match) {
            /** @var Orange35_ImageSwitcher_Model_Matches $match */
            $matchData = $match->getData();
            $preparedMatches[] = array(
                'productId' => $matchData['product_id'],
                'imageId'   => $matchData['image_id'],
                'imagesId'  => explode(",", $matchData['images_id']),
                'values'    => json_decode($matchData['matches'], true),
            );
        }
        $preparedMatches = $matchesHelper->filterMatches($preparedMatches, $product);
        $data['productId']          = $product->getId();
        $data['optionsInfo']        = $matchesHelper->getOptionsInfo($product);
        $data['optionsOrder']       = array_keys($data['optionsInfo']);
        $data['optionsLabels']      = $this->_getOptionsLabels();
        $data['imagesCount']        = count($this->getProductImages());
        $data['matches']            = $preparedMatches;
        $data['notMatchesNotice']   = $this->getEmptyMatchesListNotice();
        $data['images']             = $this->getProductImages4JS();
        $data['localization']       = array(
            'Image'                      => $this->__('Image'),
            'Actions'                    => $this->__('Actions'),
            'Delete this Combination'    => $this->__('Delete this Combination'),
            'Edit this Combination'      => $this->__('Edit this Combination'),
            'Delete'                     => $this->__('Delete'),
            'Edit'                       => $this->__('Edit'),
            "You're about to change an image for an existing combination, would you like to proceed anyway?"
                                         => $this->__("You're about to change an image for an existing combination, would you like to proceed anyway?"),
        );
        return $data;
    }

    public function getEmptyMatchesListNotice()
    {
        return $this->__('Empty list');
    }

    protected function _getOptionsLabels()
    {
        $options = array();
        $values = array();
        foreach ($this->getProduct()->getOptions() as $option) {
            /** @var $option Mage_Catalog_Model_Product_Option */
            $options[$option->getId()] = $option->getData('title');
            foreach ($option->getValues() as $value) {
                /** @var $value Mage_Catalog_Model_Product_Option_Value */
                $values[$value->getId()] = $value->getData('title');
            }
        }
        return compact('options', 'values');
    }

    /**
     * @return array
     */
    public function getProductImages()
    {
        if (null == $this->_productImages) {
            $product = $this->getProduct();
            $galleryImages = $product->getData('media_gallery');
            $result = array();
            foreach ($galleryImages['images'] as $image) {
                $cacheFile = $this->_getCacheFile(self::PREVIEW_IMAGE_SIZE, $image['file']);
                if (!file_exists($cacheFile)) {
                    $file = Mage::getBaseDir('media') . '/catalog/product' . $image['file'];
                    if (file_exists($file)) {
                        $ri = new Varien_Image($file);
                        $ri->backgroundColor($this->_backgroundColor);
                        //->keepAspectRatio(true)->keepFrame(false)->constrainOnly(true)
                        $ri->keepAspectRatio(true);
                        $ri->keepFrame(false);
                        $ri->resize(self::PREVIEW_IMAGE_SIZE, self::PREVIEW_IMAGE_SIZE);
                        $ri->save($cacheFile);
                    } else {
                        $image = false;
                    }
                }
                if ($image) {
                    $result[] = new Varien_Object(array(
                        'url'       => $this->_getCacheUrl(self::PREVIEW_IMAGE_SIZE, $image['file']),
                        'id'        => $image['value_id'],
                    ));
                }
            }
            $this->_productImages = $result;
        }
        return $this->_productImages;
    }

    public function getProductImages4JS()
    {
        $images = array();
        foreach ($this->GetProductImages() as $image) {
            /** @var $image Varien_Object */
            $images[$image->getId()] = $image->getData('url');
        }
        return $images;
    }

    protected function _getCacheFile($size, $file)
    {
        return Mage::getBaseDir('media') . '/catalog/product/cache/' . $size . 'x' . $size . $file;
    }

    protected function _getCacheUrl($size, $file)
    {
        return Mage::getBaseUrl('media') . '/catalog/product/cache/' . $size . 'x' . $size . $file;
    }
}