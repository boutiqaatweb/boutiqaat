<?php

class Orange35_ImageSwitcher_Block_Catalog_Product_View_Switch extends Mage_Core_Block_Template
{
    /**
     * @return string
     */
    public function getConfig()
    {
        $coreHelper = Mage::helper('core');
        $matchesHelper = Mage::helper("orange35_imageSwitcher/matches");
        $configTabPath = 'colorpicker_section/developer_tools/';
        return $coreHelper->jsonEncode(
            array(
                'customMatches'        => $matchesHelper->getMatchesByProductId(
                    Mage::registry('current_product')->getId()
                ),
                'imageSelector'        => Mage::getStoreConfig($configTabPath . 'image_selector'),
                'galleryImageTemplate' => preg_replace(
                    "~\n|\r~",
                    '',
                    Mage::getStoreConfig($configTabPath . 'gallery_image_template')
                ),
                'gallerySelector'      => Mage::getStoreConfig($configTabPath . 'gallery_selector'),
                'galleryImageSelector' => Mage::getStoreConfig($configTabPath . 'gallery_image_selector'),
                'zoomImageSelector'    => Mage::getStoreConfig($configTabPath . 'zoom_image_selector')
            )
        );
    }
}
 