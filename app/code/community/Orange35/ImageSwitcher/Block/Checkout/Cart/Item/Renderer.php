<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Nigilit
 * Date: 9/10/14
 * Time: 12:28 PM
 * To change this template use File | Settings | File Templates.
 */
class Orange35_ImageSwitcher_Block_Checkout_Cart_Item_Renderer extends Mage_Checkout_Block_Cart_Item_Renderer
{
    public function getProductThumbnail()
    {
        $values = array();
        foreach ($this->getProductOptions() as $option) {
            if ($option["option_type"] == "checkbox" || $option["option_type"] == "multiple") {
                $values[$option["option_id"]] =
                    explode(",", $this->getItem()->getOptionByCode('option_' . $option["option_id"])->getValue());
            } elseif ($option["option_type"] == "radio" || $option["option_type"] == "drop_down") {
                $values[$option["option_id"]] =
                    (int) $this->getItem()->getOptionByCode('option_' . $option["option_id"])->getValue();
            }
        }
        $matches =
            Mage::helper("orange35_imageSwitcher/matches")->getCheckoutMatchesByProductId($this->getProduct()->getId());
        $product = $this->getItem()->getProduct();
        ksort($values);
        $image = Mage::helper('catalog/image');
        foreach ($matches as $match) {//todo: чомусь йде по стрінгу???
            if ($match["values"] == json_encode($values)) {
                return $image->init($product, 'image', $match["image"]);
            }
        }
        foreach ($matches as $match) {
            $temp_values = $values;
            $match_values = json_decode($match["values"]);
            $all_combination = true;
            foreach ($match_values as $option_id => $value) {
                if ($value == "%all%") {//todo: винести в константу тут і скрізь, також передати в js на юзерці
                    $temp_values[(int) $option_id] = $value;
                } else {
                    $all_combination = false;
                }
            }
            if (!$all_combination) {
                ksort($temp_values);
                if ($match["values"] == json_encode($temp_values)) {
                    return $image->init($product, 'image', $match["image"]);
                }
            } else {
                $allCombinationImage = $match["image"];
            }
        }
        if (isset($allCombinationImage) && sizeof($values)) {
            return $image->init($product, 'image', $allCombinationImage);
        }
        if (!is_null($this->_productThumbnail)) {
            return $this->_productThumbnail;
        }
        return $this->helper('catalog/image')->init($this->getProduct(), 'thumbnail');
    }
}