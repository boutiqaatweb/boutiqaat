<?php

class Orange35_ImageSwitcher_Helper_Matches extends Mage_Core_Helper_Abstract
{
    /**
     * @param $productId Varien_Object | int
     * @return Orange35_ImageSwitcher_Model_Matches | false
     */
    public function findMatchesByProductId($productId)
    {
        if ($productId instanceof Varien_Object) {
            $productId = $productId->getId();
        }
        $productId = (int) $productId;
        $collection = Mage::getModel('orange35_imageSwitcher/matches')->getCollection()
            ->addFieldToFilter('product_id', array('eq' => $productId));
        return $collection;
    }

    /**
     * @param $productId Mage_Catalog_Model_Product | int
     * @return string
     */

    public function getCheckoutMatchesByProductId($productId){
        $collection = $this->findMatchesByProductId($productId);
        $product = Mage::getModel("catalog/product")->load($productId);
        $productGallery = $product->getData('media_gallery');
        $gallery = array();
        foreach ($productGallery['images'] as $image) {
            $gallery[$image['value_id']] = array(
                'file'      => $image['file']
            );
        }
        $data = array();
        foreach ($collection as $match) {
            $imagesId = explode(",", $match->getData('images_id'));
            $data[] = array(
                'values'   => $match->getData('matches'),
                'image'    => $gallery[$imagesId[0]]['file'],
            );
        }
        return $data;
    }

    public function getMatchesByProductId($productId)
    {
        if ($productId instanceof Mage_Catalog_Model_Product) {
            $product = $productId;
            $productId = $product->getId();
        } else {
            $product = Mage::getModel('catalog/product')->load($productId);
            if (!$product->getId()) {
                return array();
            }
        }
        $collection = $this->findMatchesByProductId($productId);
        $productGallery = $product->getData('media_gallery');
        $gallery = array();
        foreach ($productGallery['images'] as $image) {
            $gallery[$image['value_id']] = array(
                'file'      => $image['file']
            );
        }
        $zoomImageSelector = Mage::getStoreConfig('colorpicker_section/developer_tools/zoom_image_selector');
        $zoomImageWidth = (int) Mage::getStoreConfig('colorpicker_section/developer_tools/zoom_image_width');
        $zoomImageHeight = (int) Mage::getStoreConfig('colorpicker_section/developer_tools/zoom_image_height');
        $thumbImageWidth = (int) Mage::getStoreConfig('colorpicker_section/developer_tools/thumbnail_image_width');
        $matchesArray = array();
        foreach ($collection as $match) {
            /** @var $match Orange35_ImageSwitcher_Model_Matches */
            $imagesId = explode(",", $match->getData('images_id'));
            $data = array(
                'values'    => json_decode($match->getData('matches'), true),
                'images'    => array(),
            );
            foreach($imagesId as $image_id){
                if (isset($gallery[$image_id])) {
                    if (!isset($gallery[$image_id]['url'])) {
                        $gallery[$image_id]['url'] = $this->_getImageUrl($product, $gallery[$image_id]['file']);
                    }
                    $image = array();
                    if (false !== $gallery[$image_id]['url']) {
                        $image['imageUrl'] = $gallery[$image_id]['url'];

                        if ($zoomImageSelector || $zoomImageWidth) {
                            $image['zoomImageUrl'] = $this->_getImageUrl(
                                $product, $gallery[$image_id]['file'], $zoomImageWidth, $zoomImageHeight
                            );
                        }
                        if($thumbImageWidth){
                            $image['thumbImageUrl'] = $this->_getImageUrl(
                                $product, $gallery[$image_id]['file'], $thumbImageWidth, null, true
                            );
                        }
                    }
                    $data["images"][] = $image;
                }
            }
            $matchesArray[] = $data;
        }
        return $matchesArray;
    }

    /**
     * Retrieve url to resized image
     * @param Mage_Catalog_Model_Product
     * @param string $file
     * @param bool | int $width
     * @param bool | int $height
     * @return string
     */
    protected function _getImageUrl($product, $file, $width = false, $height = false, $thumb = false)
    {
        if (false === $width) {
            $width = (int) Mage::getStoreConfig('colorpicker_section/developer_tools/image_width') ?
                (int) Mage::getStoreConfig('colorpicker_section/developer_tools/image_width')
                : null;
            $height = false;
        }
        if (false === $height) {
            $height = (int) Mage::getStoreConfig('colorpicker_section/developer_tools/image_height') ?
                (int) Mage::getStoreConfig('colorpicker_section/developer_tools/image_height')
                : null;
        }
        $height = $height ?$height: null;
        $width = $width ?$width: null;
        /** @var Mage_Catalog_Helper_Image $image */
        $image = Mage::helper('catalog/image');
        $image->init($product, 'image', $file);
        if(!$thumb){
            return (string) $image -> keepAspectRatio(true)
                -> keepFrame(false)
                -> constrainOnly(true)
                -> resize($width, $height);
        } else {
            $image = Mage::helper('catalog/image');
            $image->init($product, 'thumbnail', $file);
            return (string) $image -> keepAspectRatio(true) -> keepFrame(true) -> constrainOnly(true) -> resize($width);
        }
    }

    /**
     * @param array $dirtyMatches
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    public function filterMatches($dirtyMatches, $product)
    {
        $matches = array();
        $existsImages = array();
        $galleryImages = $product->getData('media_gallery');
        foreach ($galleryImages['images'] as $image) {
            $existsImages[] = $image['value_id'];
        }
        $optionsInfo = $this->getOptionsInfo($product);
        foreach ($dirtyMatches as $match) {
            if (!is_array($match)) {
                continue;
            }
            if (!isset($match['productId']) || $product->getId() !== $match['productId']) {
                continue;
            }
            if (!isset($match['imageId']) || !in_array($match['imageId'], $existsImages)) {
                continue;
            }
            if (!isset($match['values']) || !is_array($match['values'])) {
                continue;
            }
            foreach ($match['values'] as $optionId => $values) {
                if (!isset($optionsInfo[$optionId])) {
                    continue 2;
                }
                $info = $optionsInfo[$optionId];
                if ('drop_down' == $info['type'] || 'radio' == $info['type']) {
                    if($values != "%all%"){
                        if (intval($values) != $values || !in_array($values, $info['values'])) {
                            continue 2;
                        }
                    }
                } else if ('multiple' == $info['type'] || 'checkbox' == $info['type']) {
                    if($values != "%all%"){
                        if (!is_array($values)) {
                            continue 2;
                        }
                        foreach ($values as $value) {
                            if (!in_array($value, $info['values'])) {
                                continue 3;
                            }
                        }
                    }
                } else {
                    continue;
                }
            }
            $matches[] = $match;
        }
        return $matches;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    public function getOptionsInfo($product)
    {
        $info = array();
        foreach ($product->getOptions() as $option) {
            /** @var $option Mage_Catalog_Model_Product_Option */
            if (in_array($option->getType(), array('drop_down', 'radio', 'checkbox', 'multiple'))) {
                $info[$option->getId()] = array(
                    'type'      => $option->getType(),
                    'values'    => array_keys($option->getValues()),
                );
            }
        }
        return $info;
    }
}