<?php

class Orange35_ImageSwitcher_Model_Matches extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('orange35_imageSwitcher/matches');
    }
}