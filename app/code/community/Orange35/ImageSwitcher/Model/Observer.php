<?php

class Orange35_ImageSwitcher_Model_Observer
{
    public function onDeleteProduct(Varien_Event_Observer $observer)
    {
        if ($product = $observer->getData('product')) {
            /** @var Orange35_ImageSwitcher_Helper_Matches $matchesHelper */
            $matchesHelper = Mage::helper('orange35_imageSwitcher/matches');
            $matches = $matchesHelper->findMatchesByProductId($product->getId());
            foreach ($matches as $match) {
                /** @var $match Orange35_ImageSwitcher_Model_Matches */
                $match->delete();
            }
        }
        return $this;
    }

    public function onSaveProduct(Varien_Event_Observer $observer)
    {
        if ($product = $observer->getData('product')) {
            /** @var Orange35_ImageSwitcher_Helper_Matches $matchesHelper */
            $matchesHelper = Mage::helper('orange35_imageSwitcher/matches');
            $productId = $product->getId();
            if ($existsMatches = $matchesHelper->findMatchesByProductId($productId)
            ) {
                foreach ($existsMatches as $existsMatch) {
                    /** @var Orange35_ImageSwitcher_Model_Matches $existsMatch */
                    $existsMatch->delete();
                }
            }
            $dirtyMatches = $this->_getRequest()->getParam('customOptionsMatches', array());
            if (!is_array($dirtyMatches)) {
                return $this;
            }
            foreach ($dirtyMatches as &$match) {
                $match = json_decode($match, true);
            }
            $matches = $matchesHelper->filterMatches($dirtyMatches, $product);
            foreach ($matches as $match) {
                $matchObj = Mage::getModel('orange35_imageSwitcher/matches');
                $matchObj->setData(array(
                    'product_id'    => $match['productId'],
                    'matches'       => json_encode($match['values']),
                    'image_id'      => $match['imageId'],
                    'images_id'      => implode(',', $match['imagesId']),
                ));
                $matchObj->save();
            }
        }
        return $this;
    }

    protected function _getRequest()
    {
        return Mage::app()->getRequest();
    }

}