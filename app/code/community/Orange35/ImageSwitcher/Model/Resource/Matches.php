<?php

class Orange35_ImageSwitcher_Model_Resource_Matches extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('orange35_imageSwitcher/matches', 'match_id');
    }
}