<?php

class Orange35_ImageSwitcher_Model_Resource_Matches_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('orange35_imageSwitcher/matches');
    }
}