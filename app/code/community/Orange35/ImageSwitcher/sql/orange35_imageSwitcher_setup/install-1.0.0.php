<?php

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 * @var $adapter Varien_Db_Adapter_Pdo_Mysql
 */
$installer = $this;
$installer->startSetup();
$adapter = $installer->getConnection();

/**
 * Matches Image Table
 */
$tableNameResource = 'orange35_imageSwitcher/matches';
$optionsMatchesImages = $installer->getTable($tableNameResource);
if (!$adapter->isTableExists($optionsMatchesImages)) {
    $table = $adapter->newTable($optionsMatchesImages)
        ->addColumn('match_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'auto_increment' => true,
            'nullable' => false,
            'primary'  => true,
        ), 'Match ID')
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            'unsigned' => true,
        ), 'Product Entity ID')
        ->addColumn('matches', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
        ), 'Matches JSON Description')
        ->addColumn('image_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false
        ), 'Product Gallery Image ID')
        ->addIndex(
            $installer->getIdxName($tableNameResource, array('match_id')), array('match_id'))

        ->addIndex(
            $installer->getIdxName($tableNameResource, array('product_id')), array('product_id'))

        ->addForeignKey(
            $installer->getFkName($tableNameResource, 'product_id', 'catalog/product', 'entity_id'),
            'product_id',
            $installer->getTable('catalog/product'),
            'entity_id')

        ->setComment('Image Switcher Custom Options Image Combinations');

    $adapter->createTable($table);
}

$installer->endSetup();