<?php
$installer = $this;
$installer->startSetup();
$adapter = $installer->getConnection();
$adapter->addColumn($installer->getTable('orange35_imageSwitcher/matches'),
    'images_id',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'unsigned' => falresizese,
        'nullable' => false,
        'comment' => 'Product Gallery Images ID'
    )
);

$adapter->query("UPDATE ".(string)$installer->getTable('orange35_imageSwitcher/matches')." set images_id = image_id");

$installer->endSetup();
