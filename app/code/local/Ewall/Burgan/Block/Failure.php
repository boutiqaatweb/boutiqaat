<?php
class Ewall_Burgan_Block_Failure extends Mage_Core_Block_Template
{  
    public function getRealOrderId()
    {
        return Mage::getSingleton('checkout/session')->getLastRealOrderId();
    }

    public function getErrorMessage ()
    {
        $error = Mage::getSingleton('checkout/session')->getErrorMessage();
        return $error;
    }

    public function getContinueShoppingUrl()
    {
        return Mage::getUrl('checkout/cart');
    }

    public function getOrderDetails()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $result = Mage::getModel('burgan/burgan')->load($orderId, 'order_id');
        return  $result;
    }
    
}