<?php
class Ewall_Burgan_Block_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('burgan/form.phtml');
    }

    protected function _getConfig()
    {
        return Mage::getSingleton('burgan/config');
    }
}