<?php
class Ewall_Burgan_Block_Info extends Mage_Payment_Block_Info
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('burgan/info.phtml');
    }

    public function getMethodCode()
    {
        return $this->getInfo()->getMethodInstance()->getCode();
    }

    public function toPdf()
    {
        $this->setTemplate('burgan/pdf/info.phtml');
        return $this->toHtml();
    }

    public function getBurganData()
    {
        return $this->getInfo()->getMethodInstance()->getBurganInfo();
    }
	
}