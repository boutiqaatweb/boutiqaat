<?php
class Ewall_Burgan_Model_Cc extends Mage_Payment_Model_Method_Abstract
{

	protected $_code = 'burgan_cc';

    protected $_paymentMethod			= 'cc';
    protected $_defaultLocale			= 'en';

    protected $_testUrl	= "https://secure.burgan.dk/payment/transaction.ew";
    protected $_liveUrl	= "https://secure.burgan.dk/payment/transaction.ew";

    protected $_formBlockType = 'burgan/form';
    protected $_infoBlockType = 'burgan/info';

    protected $_order;

    /**
     * Get order model
     *
     * @return Mage_Sales_Model_Orderburgan
     */
    public function getOrder()
    {
		if (!$this->_order) {
			$this->_order = $this->getInfoInstance()->getOrder();
		}
		return $this->_order;
    }

    public function getOrderPlaceRedirectUrl()
    {
          return Mage::getUrl('burgan/processing/redirect');
    }

    public function getPaymentMethodType()
    {
        return $this->_paymentMethod;
    }

    public function getUrl()
    {
    	if ($this->getConfigData('transaction_mode') == 'live')
    		return $this->_liveUrl;
    	return $this->_testUrl;
    }
   
	//Get the payment information.
	 public function getBurganInfo()
    {

		if($this->getOrder()) {
		$order_id = $this->getOrder()->getIncrementId(); 
		}
		
		$result = Mage::getModel('burgan/burgan')->load($order_id, 'order_id');
        return  $result;
    }

    protected function _debug($debugData)
    {
        if (method_exists($this, 'getDebugFlag')) {
            return parent::_debug($debugData);
        }

        if ($this->getConfigData('debug')) {
            Mage::log($debugData, null, 'payment_' . $this->getCode() . '.log', true);
        }
    }
}