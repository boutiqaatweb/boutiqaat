<?php
class Ewall_Burgan_Model_Config extends Mage_Payment_Model_Config
{

    public function getTransactionModes()
    {
        $modes = array();
        foreach (Mage::getConfig()->getNode('global/payment/transaction/modes')->asArray() as $data) {
        	$modes[$data['code']] = $data['name'];
        }
        return $modes;
    }
    
}