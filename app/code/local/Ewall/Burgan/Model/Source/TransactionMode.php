<?php
class Ewall_Burgan_Model_Source_TransactionMode
{
    public function toOptionArray()
    {
        $options =  array();
        foreach (Mage::getSingleton('burgan/config')->getTransactionModes() as $code => $name) {
            $options[] = array(
            	   'value' => $code,
            	   'label' => $name
            );
        }

        return $options;
    }
}