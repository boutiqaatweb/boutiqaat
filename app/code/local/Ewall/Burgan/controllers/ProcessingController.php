<?php
class Ewall_Burgan_ProcessingController extends Mage_Core_Controller_Front_Action
{

    protected $_order = NULL;
    protected $_paymentInst = NULL;

    /**
     * Get singleton of Checkout Session Model
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * when customer selects Burgan payment method
     */
    public function redirectAction()
    {
        try {
            $session = $this->_getCheckout();

            $order = Mage::getModel('sales/order');
            $order->loadByIncrementId($session->getLastRealOrderId());
            if (!$order->getId()) {
                Mage::throwException('No order for processing found');
            }
            if ($order->getState() == Mage_Sales_Model_Order::STATE_NEW) {
                 $order->setState(Mage_Sales_Model_Order::STATE_NEW, true, Mage::helper('burgan')->__('Customer was redirected to BURGAN.'));
                 $order->save();
            }

            if ($session->getQuoteId() && $session->getLastSuccessQuoteId()) {
                $session->setBurganQuoteId($session->getQuoteId());
                $session->setBurganSuccessQuoteId($session->getLastSuccessQuoteId());
                $session->setBurganRealOrderId($session->getLastRealOrderId());
                $session->getQuote()->setIsActive(false)->save();
                $session->clear();
            }

            $this->loadLayout();
            $this->renderLayout();
            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            $this->_debug('Burgan error: ' . $e->getMessage());
            Mage::logException($e);
        }
        $this->_redirect('checkout/cart');
    }

  
    /**
     * BURGAN return action for success
     */
    public function responseAction()
    {
        try {
            // Retreive values from URL
            $burgan_info = $this->getRequest()->getParams();
            $order_id = $burgan_info['UDF1'];
            $session = $this->_getCheckout();
            $session->unsBurganRealOrderId();
            $session->setQuoteId($session->getBurganQuoteId(true));
            $session->setLastSuccessQuoteId($session->getBurganSuccessQuoteId(true));

             // Show message if security is breached
            if ($order_id  == "") {
                $session->addError(Mage::helper('burgan')->__('The payment has been declined. Kindly contact the site administrator.'));
                $this->_redirect('checkout/cart');
                return;
            }
            
            //Write the response to the table
            $result = Mage::getModel('burgan/burgan')->load($order_id, 'order_id');
            $result->setData('paymentid',$burgan_info['PaymentID']);
            $result->setData('result',$burgan_info['Result']);
            $result->setData('postdate',$burgan_info['PostDate']);
            $result->setData('tranid',$burgan_info['TranID']);
            $result->setData('auth',$burgan_info['Auth']);
            $result->setData('ref',$burgan_info['Ref']);
            $result->setData('trackid',$burgan_info['TrackID']);
            $result->setData('udf1',$burgan_info['UDF1']);
            $result->save();

			$order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
            if($order->canInvoice()) {
                //START Handle Invoice
                $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                $invoice->register();
 
                $invoice->getOrder()->setCustomerNoteNotify(true);          
                $invoice->getOrder()->setIsInProcess(true);
                $order->addStatusHistoryComment('The payment has been registered successfully.', false);
 
                $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());
 
                $transactionSave->save();
                $order->sendNewOrderEmail();
                //END Handle Invoice
             }

             $this->_redirect('burgan/processing/success');
             return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            $this->_debug('BURGAN error: ' . $e->getMessage());
            Mage::logException($e);
        }
        $this->_redirect('checkout/cart');
    }

    //BURGAN return action failure
    public function cancelAction()
    {
        // Retreive values from URL
        $burgan_info = $this->getRequest()->getParams();
        $order_id = $burgan_info['UDF1'];
        // set quote to active
        $session = $this->_getCheckout();
        if ($quoteId = $session->getBurganQuoteId()) {
            $quote = Mage::getModel('sales/quote')->load($quoteId);
            if ($quote->getId()) {
                $quote->setIsActive(true)->save();
                $session->setQuoteId($quoteId);
            }
        }

        // Show message if security is breached
        if ($order_id  == "") {
            $session->addError(Mage::helper('burgan')->__('The payment has been declined. Kindly contact the site administrator.'));
            $this->_redirect('checkout/cart');
            return;
        }
		   
        //Write the response to the table
        $result = Mage::getModel('burgan/burgan')->load($order_id, 'order_id');
        $result->setData('paymentid',$burgan_info['PaymentID']);
        $result->setData('result',$burgan_info['Result']);
        $result->setData('postdate',$burgan_info['PostDate']);
        $result->setData('tranid',$burgan_info['TranID']);
        $result->setData('auth',$burgan_info['Auth']);
        $result->setData('ref',$burgan_info['Ref']);
        $result->setData('trackid',$burgan_info['TrackID']);
        $result->setData('udf1',$burgan_info['UDF1']);
        $result->save();
        
        $order = Mage::getModel('sales/order');
        $order->loadByIncrementId($order_id);
        $order->cancel(); 
        $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true)->save();
        
        $this->_redirect('burgan/processing/failure');
    }

    //Display the failure page
    public function failureAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

 
    //Display the success page
    public function successAction()
    {
        $lastOrderId =  Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $this->loadLayout();
        Mage::dispatchEvent('burgan_processing_controller_success_action', array('order_ids' => array($lastOrderId)));
        $this->renderLayout();
    }

    protected function _getPendingPaymentStatus()
    {
        return Mage::helper('burgan')->getPendingPaymentStatus();
    }

    //Log debug data to file
    protected function _debug($debugData)
    {
        if (Mage::getStoreConfigFlag('payment/burgan_cc/debug')) {
            Mage::log($debugData, null, 'payment_burgan_cc.log', true);
        }
    }
}