<?php
 
  $installer = $this;

  $installer->startSetup();

  $installer->run("

    DROP TABLE IF EXISTS {$this->getTable('burgan_payment')};

    CREATE TABLE {$this->getTable('burgan_payment')} (
      `burgan_id` int(11) unsigned NOT NULL auto_increment,
      `order_id` varchar(255) NOT NULL default '',
      `amount` varchar(255) NOT NULL default '',
      `paymentid` varchar(255) NOT NULL default '',
      `result` varchar(255) NOT NULL default '',
      `postdate` varchar(255) NOT NULL default '',
      `tranid` varchar(255) NOT NULL default '',
      `auth` varchar(255) NOT NULL default '',
      `ref` varchar(255) NOT NULL default '',
      `trackid` varchar(255) NOT NULL default '',
      `udf1` varchar(255) NOT NULL default '',
      `udf2` varchar(255) NOT NULL default '',
	    `udf3` varchar(255) NOT NULL default '',
      `udf4` varchar(255) NOT NULL default '',
      `udf5` varchar(255) NOT NULL default '',
      PRIMARY KEY (`burgan_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  ");

  $installer->endSetup();
