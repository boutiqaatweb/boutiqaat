<?php
$installer = $this;
$installer->startSetup();


$installer->addAttribute("catalog_category", "boutique_name",  array(
    "group"             => "General Information",
    "type"              => "text",
    "backend"           => "",
    "frontend"          => "",
    "label"             => "Boutique Name",
    "input"             => "text",
    "class"             => "",
    "source"            => "",
    "global"            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "visible"           => true,
    "required"          => false,
    "user_defined"      => true,
    "default"           => "",
    "searchable"        => false,
    "filterable"        => false,
    "comparable"        => false,	
    "visible_on_front"  => false,
    "unique"            => false,
    "note"              => "",
    'sort_order'        => 90,
));

$installer->addAttribute("catalog_category", "instagram_name",  array(
    "group"             => "General Information",
    "type"              => "text",
    "backend"           => "",
    "frontend"          => "",
    "label"             => "Instagram Name",
    "input"             => "text",
    "class"             => "",
    "source"            => "",
    "global"            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    "visible"           => true,
    "required"          => false,
    "user_defined"      => true,
    "default"           => "",
    "searchable"        => false,
    "filterable"        => false,
    "comparable"        => false,	
    "visible_on_front"  => false,
    "unique"            => false,
    "note"              => "",
    'sort_order'        => 91,
));
$installer->endSetup();
	 