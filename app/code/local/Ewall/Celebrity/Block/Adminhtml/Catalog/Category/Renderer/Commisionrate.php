<?php
class Ewall_Celebrity_Block_Adminhtml_Catalog_Category_Renderer_Commisionrate extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    protected $_values;

    public function render(Varien_Object $row) {
		$serData = $row->getData($this->getColumn()->getIndex());
		$final = unserialize($serData);
		$cat_id = $this->getRequest()->getParam('id', 0);
		return (int)$final[$cat_id];
    } 
}
