<?php 
class Ewall_Celebrity_Block_Adminhtml_Catalog_Category_Tab_Product extends Mage_Adminhtml_Block_Catalog_Category_Tab_Product
{
    protected function _prepareCollection()
    {
        if ($this->getCategory()->getId()) {
            $this->setDefaultFilter(array('in_category'=>1));
        }
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('price')
            ->addAttributeToSelect('celebrity_commision')
            ->addStoreFilter($this->getRequest()->getParam('store'))
            ->joinField('position',
                'catalog/category_product',
                'position',
                'product_id=entity_id',
                'category_id='.(int) $this->getRequest()->getParam('id', 0),
                'left');
        $this->setCollection($collection);

        if ($this->getCategory()->getProductsReadonly()) {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            $this->getCollection()->addFieldToFilter('entity_id', array('in'=>$productIds));
        }

        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }
    protected function _prepareColumns()
    {
        $cat_id = $this->getRequest()->getParam('id', 0);
        $customer_celebrity = $this->getCelebrityCategories();
        if(in_array($cat_id, $customer_celebrity)){
            //add the commission rate collumn after the 'price' column
            $this->addColumnAfter('celebrity_commision', array(
                'header'    => Mage::helper('catalog')->__('Commission Ratio (%)'),
                'type'  => 'text',
                'width'     => '1',
                'filter'=>false,
                'sortable'=>false,
                'align'=>'center',
                'index'     => 'celebrity_commision',
                'renderer' => 'celebrity/adminhtml_catalog_category_renderer_commisionrate'
            ), 'price');
        }   
        return parent::_prepareColumns();
    }
    public function getCelebrityCategories()
    {
        $collection = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('celebrity')->addAttributeToFilter('is_celebrity', true);
        $_celebrity = array();
        foreach ($collection as $customer) {
            $_celebrity[] = $customer->getCelebrity();
        }
        return $_celebrity;
    }
}