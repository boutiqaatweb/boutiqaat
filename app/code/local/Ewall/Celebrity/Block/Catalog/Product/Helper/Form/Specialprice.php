<?php   
class Ewall_Celebrity_Block_Catalog_Product_Helper_Form_Specialprice extends Varien_Data_Form_Element_Textarea
{
    public function getAfterElementHtml()
    {
        $html = parent::getAfterElementHtml();
        $product = Mage::registry('product');
        $customer_celebrity = $this->getCelebrityCategories();
        $table = "";
	    if(Mage::app()->getRequest()->getControllerName() == 'catalog_product_action_attribute' && Mage::app()->getRequest()->getActionName() == 'edit'){
        	$html .= "  <script>";
        	$html .= "$('".$this->getHtmlId()."').up(1).hide();";
        	$html .= "</script>";
        }
        else{
        	$html .= "  <script>";
      		if(Mage::registry('product')->getId()){
      			if(count($product->getCategoryIds())){
      				if(count($customer_celebrity)){
      					if(count(array_intersect($product->getCategoryIds(),  $customer_celebrity))){
			      			$html .= "$('".$this->getHtmlId()."').hide();";
			      			$html .= "$('".$this->getHtmlId()."').up().addClassName('grid tier');";
			      			$html .= "$('".$this->getHtmlId()."').up().removeClassName('value');";
			      			$table .= '<table cellspacing="0" id="'.$this->getHtmlId().'_table" class="data border">';
			      				$table .= '<colgroup><col width="135"><col width="120"></colgroup>';
			      				$table .= '<thead><tr class="headings"><th>Celebrity</th><th>Special Price</th></tr></thead>';
			      				$table .= '<tbody id="'.$this->getHtmlId().'_container">';
			      				$data = unserialize($product->getCelebritySpecialPrice());
			      				foreach ($product->getCategoryIds() as $id) {
			      					if(in_array($id, $customer_celebrity)):
				      					$table .= '<tr>';
				      						$table .= '<td>'.Mage::getModel('catalog/category')->load($id)->getName().'</td>';
				      						$table .= '<td>';
				      							$table .='<input type="text" value="'.$data[$id].'" name="product['.$this->getHtmlId().']['.$id.']" class=" input-text required-entry validate-greater-than-zero ">';
				      						$table .='</td>';
				      					$table .= '</tr>';	
				      				endif;
			      				}
			      				$table .= '</tbody>';
			      			$table .= '</table>';	
			      			$html .= "$('".$this->getHtmlId()."').insert({ after: '".$table."'});";
			      			$html .= "document.observe('dom:loaded',function(){";
								$html .= "if($('".$this->getHtmlId()."_default') != null){";
									$html .= "toggleValueElements($('".$this->getHtmlId()."_default'), $('".$this->getHtmlId()."_default').parentNode.parentNode);";
									$html .= "toggleValueElements($('".$this->getHtmlId()."_default'), $('".$this->getHtmlId()."_default').parentNode.parentNode);";
								$html .= "}";
							$html .= "});";
						}
						else{
		      			
			      			$html .= "$('".$this->getHtmlId()."').up(1).hide();";
			      		} 
					}
					else{
	      			
		      			$html .= "$('".$this->getHtmlId()."').up(1).hide();";
		      		} 
				}
				else{
      			
	      			$html .= "$('".$this->getHtmlId()."').up(1).hide();";
	      		} 
      		}
      		else{
      			
      			$html .= "$('".$this->getHtmlId()."').up(1).hide();";
      		} 

	        				
	        $html .= "</script>"; 
	    }
        
        return $html;
    }

    public function getCelebrityCategories()
    {
    	$collection = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('celebrity')->addAttributeToFilter('is_celebrity', true);
    	$_celebrity = array();
    	foreach ($collection as $customer) {
    		$_celebrity[] = $customer->getCelebrity();
    	}
    	return $_celebrity;
    }
    
 
}