<?php   
class Ewall_Celebrity_Block_Catalog_Product_Helper_Form_Date extends Varien_Data_Form_Element_Textarea
{
    public function getAfterElementHtml()
    {
        $html = parent::getAfterElementHtml();
        $product = Mage::registry('product');
        $customer_celebrity = $this->getCelebrityCategories();
        $table = "";
        if(Mage::registry('product')->getId()){
  			if(count($product->getCategoryIds())){
  				if(count($customer_celebrity)){
  					if(count(array_intersect($product->getCategoryIds(),  $customer_celebrity))){
  						$table .= '<table cellspacing="0" id="'.$this->getHtmlId().'_table" class="data border">';
			      				$table .= '<colgroup><col width="135"><col width="120"></colgroup>';
			      				$table .= '<thead><tr class="headings"><th>Celebrity</th><th>Ad Date</th></tr></thead>';
			      				$table .= '<tbody id="'.$this->getHtmlId().'_container">';
			      				$data = unserialize($product->getCelebrityAdDate());
			      				foreach ($product->getCategoryIds() as $id) {
			      					if(in_array($id, $customer_celebrity)):
				      					$table .= '<tr>';
				      						$table .= '<td>'.Mage::getModel('catalog/category')->load($id)->getName().'</td>';
				      						$table .= '<td>';
				      							$table .='<input type="text" value="'.$data[$id].'" id="'.$this->getHtmlId().$id.'" name="product['.$this->getHtmlId().']['.$id.']" class=" input-text required-entry" >';
				      							$date_image = Mage::getDesign()->getSkinUrl('images/grid-cal.gif');
				      							$table .='<img style="margin-left:5px;" title="Select Date" id="'.$this->getHtmlId().$id.'_trig" class="v-middle" alt="" src="'.$date_image.'">';


				      							$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
				      							$table .='<script type="text/javascript">
												            //<![CDATA[
												                Calendar.setup({
												                    inputField: "'.$this->getHtmlId().$id.'",
												                    ifFormat: "%m/%e/%Y %I:%M %p",
												                    showsTime: true,
												                    button: "'.$this->getHtmlId().$id.'_trig",
												                    align: "Bl",
												                    singleClick : true
												                });
												            //]]>
														</script>';
				      						$table .='</td>';
				      					$table .= '</tr>';	
				      				endif;
			      				}
			      				$table .= '</tbody>';
			      			$table .= '</table>';
			      		$html .= $table;	
  					}
  				}
  			}
  		}
        $html .= "  <script>";
      		if(Mage::registry('product')->getId()){
      			if(count($product->getCategoryIds())){
      				if(count($customer_celebrity)){
      					if(count(array_intersect($product->getCategoryIds(),  $customer_celebrity))){
			      			$html .= "$('".$this->getHtmlId()."').hide();";
			      			$html .= "$('".$this->getHtmlId()."').up().addClassName('grid tier');";
			      			$html .= "$('".$this->getHtmlId()."').up().removeClassName('value');";
			      						
							//$html .= "$('".$this->getHtmlId()."').insert({ after: '".$table."'});";
							$html .= "document.observe('dom:loaded',function(){";
								$html .= "if($('".$this->getHtmlId()."_default') != null){";
									$html .= "toggleValueElements($('".$this->getHtmlId()."_default'), $('".$this->getHtmlId()."_default').parentNode.parentNode);";
									$html .= "toggleValueElements($('".$this->getHtmlId()."_default'), $('".$this->getHtmlId()."_default').parentNode.parentNode);";
								$html .= "}";
							$html .= "});";
						}
						else{
		      			
			      			$html .= "$('".$this->getHtmlId()."').up(1).hide();";
			      		} 
					}
					else{
	      			
		      			$html .= "$('".$this->getHtmlId()."').up(1).hide();";
		      		} 
				}
				else{
      			
	      			$html .= "$('".$this->getHtmlId()."').up(1).hide();";
	      		} 
      		}
      		else{
      			
      			$html .= "$('".$this->getHtmlId()."').up(1).hide();";
      		}        				
        				
        $html .= "</script>";
        return $html;
    }


    public function getCelebrityCategories()
    {
    	$collection = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('celebrity')->addAttributeToFilter('is_celebrity', true);
    	$_celebrity = array();
    	foreach ($collection as $customer) {
    		$_celebrity[] = $customer->getCelebrity();
    	}
    	return $_celebrity;
    }
 
}