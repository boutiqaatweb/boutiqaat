<?php   
class Ewall_Celebrity_Block_Catalog_Product_Helper_Form_Datetime extends Varien_Data_Form_Element_Text
{
    public function getAfterElementHtml()
    {
        $html = parent::getAfterElementHtml();        
        $html .= "  <script>";
      		$html .= "$('".$this->getHtmlId()."').up(1).hide();";
        $html .= "</script>";
        
        return $html;
    }
}