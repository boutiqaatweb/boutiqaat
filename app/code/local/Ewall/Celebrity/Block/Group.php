<?php   
class Ewall_Celebrity_Block_Group extends Mage_Core_Block_Template {	 

	protected $_locale;

	const END_OF_DAY_IN_SECONDS = 86399;

	public function __construct()
    {
        parent::__construct();
        $collection = Mage::getModel('sales/order_invoice_item')
        			->getCollection()
        			->addFieldToFilter('category_id', Mage::getSingleton('customer/session')->getCustomer()->getCelebrity());
        		
        $collection->getSelect()->columns('SUM(qty) as total')->columns('SUM(commision_amount) as amount')->group('sku');
        
        if(count($this->getRequest()->getPost())){
        	$collection->addFieldToFilter('date_invoiced', array(
				            'from' => $this->_convertDate($this->getRequest()->getPost('from'), 'en_US')->getTimestamp(), //Mage::app()->getLocale()->getLocaleCode()
				            'to' => $this->_convertDate($this->getRequest()->getPost('to'), 'en_US')->getTimestamp()+ self::END_OF_DAY_IN_SECONDS, //Mage::app()->getLocale()->getLocaleCode()
				            'datetime' => true,				            
					    ));
            Mage::getSingleton('core/session')->setCelebrityGroup($this->getRequest()->getPost());
		}
        elseif($params = Mage::getSingleton('core/session')->getCelebrityGroup()){
            
            $collection->addFieldToFilter('date_invoiced', array(
                            'from' => $this->_convertDate($params['from'], 'en_US')->getTimestamp(), //Mage::app()->getLocale()->getLocaleCode()
                            'to' => $this->_convertDate($params['to'], 'en_US')->getTimestamp()+ self::END_OF_DAY_IN_SECONDS, //Mage::app()->getLocale()->getLocaleCode()
                            'datetime' => true,                         
                        ));
        }
        else{
            $collection->addFieldToFilter('entity_id', 0);
        }
        
        $this->setCollection($collection);
    }
 
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
 
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,'all'=>'all'));
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    }
 
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Convert given date to default (UTC) timezone
     *
     * @param string $date
     * @param string $locale
     * @return Zend_Date
     */
    protected function _convertDate($date, $locale)
    {
        try {
            $dateObj = $this->getLocale()->date(null, null, $locale, false);

            //set default timezone for store (admin)
            $dateObj->setTimezone(
                Mage::app()->getStore()->getConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_TIMEZONE)
            );

            //set begining of day
            $dateObj->setHour(00);
            $dateObj->setMinute(00);
            $dateObj->setSecond(00);

            //set date with applying timezone of store
            $dateObj->set($date, Zend_Date::DATE_SHORT, $locale);

            //convert store date to default date in UTC timezone without DST
            $dateObj->setTimezone(Mage_Core_Model_Locale::DEFAULT_TIMEZONE);

            return $dateObj;
        }
        catch (Exception $e) {
            return null;
        }
    }

    /**
     * Retrieve locale
     *
     * @return Mage_Core_Model_Locale
     */
    public function getLocale()
    {
        if (!$this->_locale) {
            $this->_locale = Mage::app()->getLocale();
        }
        return $this->_locale;
    }
    
    public function getInvoiceCollection(){
        $collection = Mage::getModel('sales/order_invoice_item')
                    ->getCollection()
                    ->addFieldToSelect('category_id')
                    ->addFieldToSelect('qty')
                    ->addFieldToSelect('commision_amount')
                    ->addFieldToFilter('category_id', Mage::getSingleton('customer/session')->getCustomer()->getCelebrity());
        $collection->getSelect()
                ->columns('SUM(commision_amount) as commision_total, SUM(qty) AS total_ordered_qty')
                ->group('category_id');
        return $collection->getFirstItem();        
    }
}
?>