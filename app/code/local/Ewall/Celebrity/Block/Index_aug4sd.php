<?php   
class Ewall_Celebrity_Block_Index extends Mage_Core_Block_Template{   

	public function __construct()
    {
        parent::__construct();

        if(count($this->getRequest()->getPost())){
			$this->setFormData($this->getRequest()->getPost());
		}
		else if($params = Mage::getSingleton('core/session')->getCelebrity()){
			$this->setFormData($params);
		}
	}


}