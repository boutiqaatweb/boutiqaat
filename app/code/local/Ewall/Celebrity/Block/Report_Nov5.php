<?php   
class Ewall_Celebrity_Block_Report extends Mage_Core_Block_Template{   

	protected $_locale;

	const END_OF_DAY_IN_SECONDS = 86399;

	public function __construct()
    {
        parent::__construct();
        $collection = Mage::getModel('sales/order_invoice_item')
        			->getCollection()
        			->addFieldToFilter('category_id', Mage::getSingleton('customer/session')->getCustomer()->getCelebrity());
        
        if(count($this->getRequest()->getPost())){
        	$collection->addFieldToFilter('date_invoiced', array(
				            'from' => $this->_convertDate($this->getRequest()->getPost('from'), 'en_US')->getTimestamp(), //Mage::app()->getLocale()->getLocaleCode()
				            'to' => $this->_convertDate($this->getRequest()->getPost('to'), 'en_US')->getTimestamp() + 86399, //Mage::app()->getLocale()->getLocaleCode()
				            'datetime' => true,				            
					    ));
            Mage::getSingleton('core/session')->setCelebrity($this->getRequest()->getPost());
		}
        elseif($params = Mage::getSingleton('core/session')->getCelebrity()){
            
            $collection->addFieldToFilter('date_invoiced', array(
                            'from' => $this->_convertDate($params['from'], 'en_US')->getTimestamp(), //Mage::app()->getLocale()->getLocaleCode()
                            'to' => $this->_convertDate($params['to'], 'en_US')->getTimestamp() +  86399, //Mage::app()->getLocale()->getLocaleCode()
                            'datetime' => true,                         
                        ));
        }
        $collection->getSelect()->joinLeft(array('invoice' =>'sales_flat_invoice'), 'invoice.entity_id = main_table.parent_id', array('invoice.order_id'));
        $collection->getSelect()->joinLeft(array('ord_id' =>'sales_flat_order'), 'ord_id.entity_id = invoice.order_id', array('ord_id.status'))->where('ord_id.status = ?', 'paid');  
     
        $this->setCollection($collection);
    }
 
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
 
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,'all'=>'all'));
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    }
 
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Convert given date to default (UTC) timezone
     *
     * @param string $date
     * @param string $locale
     * @return Zend_Date
     */
    protected function _convertDate($date, $locale)
    {
        try {
            $dateObj = $this->getLocale()->date(null, null, $locale, false);

            //set default timezone for store (admin)
            $dateObj->setTimezone(
                Mage::app()->getStore()->getConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_TIMEZONE)
            );

            //set begining of day
            $dateObj->setHour(00);
            $dateObj->setMinute(00);
            $dateObj->setSecond(00);

            //set date with applying timezone of store
            $dateObj->set($date, Zend_Date::DATE_SHORT, $locale);

            //convert store date to default date in UTC timezone without DST
            $dateObj->setTimezone(Mage_Core_Model_Locale::DEFAULT_TIMEZONE);

            return $dateObj;
        }
        catch (Exception $e) {
            return null;
        }
    }

    /**
     * Retrieve locale
     *
     * @return Mage_Core_Model_Locale
     */
    public function getLocale()
    {
        if (!$this->_locale) {
            $this->_locale = Mage::app()->getLocale();
        }
        return $this->_locale;
    }

}