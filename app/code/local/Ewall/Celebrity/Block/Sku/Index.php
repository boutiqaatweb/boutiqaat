<?php   
class Ewall_Celebrity_Block_Sku_Index extends Mage_Core_Block_Template{   

	public function __construct()
    {
        parent::__construct();
        if(count($this->getRequest()->getPost())){
			$this->setFormData($this->getRequest()->getPost());
		}

		else if($params = Mage::getSingleton('core/session')->getCelebrity()){
			$this->setFormData($params);
		}
	}
	public function getSkuCollection(){
		
         $collection = Mage::getModel('sales/order_item')
                    ->getCollection()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('category_id', Mage::getSingleton('customer/session')->getCustomer()->getCelebrity());
      
        $data = 'paid';
        $collection->getSelect()->joinLeft(array('ord_id' =>'sales_flat_order'), 'ord_id.entity_id = main_table.order_id', array('ord_id.status'))->where('ord_id.status IN(?)', $data); 
        $collection->getSelect()
                   ->columns('SUM(commision_amount) as commision_total, SUM(qty_ordered) AS total_ordered_qty')
                   ->group('category_id');        
        return $collection->getFirstItem();
	}

}
?>