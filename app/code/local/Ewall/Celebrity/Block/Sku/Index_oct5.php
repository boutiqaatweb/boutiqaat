<?php   
class Ewall_Celebrity_Block_Sku_Index extends Mage_Core_Block_Template{   

	public function __construct()
    {
        parent::__construct();
        if(count($this->getRequest()->getPost())){
			$this->setFormData($this->getRequest()->getPost());
		}

		else if($params = Mage::getSingleton('core/session')->getCelebrity()){
			$this->setFormData($params);
		}
	}
	public function getSkuCollection(){
		$collection = Mage::getModel('sales/order_invoice_item')
        			->getCollection()    
          			->addFieldToSelect('category_id')
                	->addFieldToSelect('qty')
                	->addFieldToSelect('sku')
                	->addFieldToSelect('name')
        			->addFieldToFilter('category_id', Mage::getSingleton('customer/session')->getCustomer()->getCelebrity());
        			
        $collection->getSelect()
                ->columns('SUM(commision_amount) as commision_total, SUM(qty) AS total_ordered_qty,SUM(commision_percent) as commision_total_percent')
                ->group('category_id');               
        return $collection->getFirstItem();        
	}

}
?>