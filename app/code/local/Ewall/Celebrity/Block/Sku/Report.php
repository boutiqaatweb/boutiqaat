<?php
class Ewall_Celebrity_Block_Sku_Report extends Mage_Core_Block_Template{


	protected $_locale;

    protected $_direction = 'ASC';

	const END_OF_DAY_IN_SECONDS = 86399;

    public function __construct(){
        parent::__construct();
        $collection = Mage::getModel('sales/order_item')
                    ->getCollection()
                    ->addFieldToFilter('category_id', Mage::getSingleton('customer/session')->getCustomer()->getCelebrity());

       $collection->getSelect()->joinLeft(array('ordertbl' =>'sales_flat_order'), 'ordertbl.entity_id = main_table.order_id', array('ordertbl.status as status'));
        $collection->getSelect()
                   ->columns('SUM(commision_amount) as commision_total, SUM(qty_ordered) AS total_ordered_qty')
                   ->group('sku')
                   ->group('status');        
        if(count($this->getRequest()->getPost())){

            if($this->getRequest()->getPost('from') && $this->getRequest()->getPost('to')) {

                   $collection->addFieldToFilter('main_table.created_at', array(
                            'from' => $this->_convertDate($this->getRequest()->getPost('from'), 'en_US')->getTimestamp(), //Mage::app()->getLocale()->getLocaleCode()
                            'to' => $this->_convertDate($this->getRequest()->getPost('to'), 'en_US')->getTimestamp() + 86399, //Mage::app()->getLocale()->getLocaleCode()
                            'datetime' => true,                 
                        ));                  
                    if($this->getRequest()->getPost('status') == ''){
                        $data = array('canceled', 'complete', 'delivered', 'paid', 'pending', 'received', 'processing', 'rescheduled', 'shipped');
                        $collection->addFieldToFilter('status', $data);
                        
                    }
                    elseif($this->getRequest()->getPost('status') == 'received') {
                        $data = array('pending', 'received', 'processing');
                        $collection->addFieldToFilter('status', $data);
                    }
                    else {
                        $data = $this->getRequest()->getPost('status');
                        $collection->addFieldToFilter('status', $data);
                    }                       
                }
                if($this->getRequest()->getPost('sku') && $this->getRequest()->getPost('from') && $this->getRequest()->getPost('to')) {
                        $collection->addFieldToFilter('main_table.created_at', array(
                            'from' => $this->_convertDate($this->getRequest()->getPost('from'), 'en_US')->getTimestamp(), //Mage::app()->getLocale()->getLocaleCode()
                            'to' => $this->_convertDate($this->getRequest()->getPost('to'), 'en_US')->getTimestamp() + 86399, //Mage::app()->getLocale()->getLocaleCode()
                            'datetime' => true,                 
                        ))->addFieldToFilter('sku',array('like'=>$this->getRequest()->getPost('sku').'%'));
                        if($this->getRequest()->getPost('status') == ''){
                            $data = array('canceled', 'complete', 'delivered', 'paid', 'pending', 'received', 'processing', 'rescheduled', 'shipped');
                            $collection->addFieldToFilter('status', $data);
                            
                        }
                        elseif($this->getRequest()->getPost('status') == 'received') {
                            $data = array('pending', 'received', 'processing');
                            $collection->addFieldToFilter('status', $data);
                        }
                        else {
                            $data = $this->getRequest()->getPost('status');
                            $collection->addFieldToFilter('status', $data);
                        } 
                }
                if($this->getRequest()->getPost('sku') && $this->getRequest()->getPost('status')) {                    
                    $collection->addFieldToFilter('sku',array('like'=>$this->getRequest()->getPost('sku').'%'));
                    if($this->getRequest()->getPost('status') == ''){
                        $data = array('canceled', 'complete', 'delivered', 'paid', 'pending', 'received', 'processing', 'rescheduled', 'shipped');
                        $collection->addFieldToFilter('status', $data);
                        
                    }
                    elseif($this->getRequest()->getPost('status') == 'received') {
                        $data = array('pending', 'received', 'processing');
                        $collection->addFieldToFilter('status', $data);
                    }
                    else {
                        $data = $this->getRequest()->getPost('status');
                        $collection->addFieldToFilter('status', $data);
                    }     
                 
                }

            Mage::getSingleton('core/session')->setCelebritySearch($this->getRequest()->getPost());
        }

        if($params = Mage::getSingleton('core/session')->getCelebritySearch()){
            if($params['from'] && $params['to'] ){
                $collection->addFieldToFilter('main_table.created_at', array(
                            'from' => $this->_convertDate($params['from'], 'en_US')->getTimestamp(), //Mage::app()->getLocale()->getLocaleCode()
                            'to' => $this->_convertDate($params['to'], 'en_US')->getTimestamp()+ self::END_OF_DAY_IN_SECONDS, //Mage::app()->getLocale()->getLocaleCode()
                            'datetime' => true,                         
                       ));
                if($params['status'] == '') {
                    $data = array('canceled', 'complete', 'delivered', 'paid', 'pending', 'received', 'processing', 'rescheduled', 'shipped');
                    $collection->addFieldToFilter('status', $data);
                }
                elseif($params['status'] == 'received') {
                    $data = array('pending', 'received', 'processing');
                    $collection->addFieldToFilter('status', $data);
                }
                else {
                    $collection->addFieldToFilter('status', $params['status']);
                }            
                
            }
            if($params['sku'] && $params['from'] && $params['to']){
                $collection->addFieldToFilter('main_table.created_at', array(
                            'from' => $this->_convertDate($params['from'], 'en_US')->getTimestamp(), //Mage::app()->getLocale()->getLocaleCode()
                            'to' => $this->_convertDate($params['to'], 'en_US')->getTimestamp()+ self::END_OF_DAY_IN_SECONDS, //Mage::app()->getLocale()->getLocaleCode()
                            'datetime' => true,                         
                       ))->addFieldToFilter('sku',array('like' => $params['sku'].'%'));
                if($params['status'] == '') {
                    $data = array('canceled', 'complete', 'delivered', 'paid', 'pending', 'received', 'processing', 'rescheduled', 'shipped');
                    $collection->addFieldToFilter('status', $data);
                }
                elseif($params['status'] == 'received') {
                    $data = array('pending', 'received', 'processing');
                    $collection->addFieldToFilter('status', $data);
                }
                else {
                    $collection->addFieldToFilter('status', $params['status']);
                } 
                
            }
            if($params['sku'] && $params['status']) {   
                $collection->addFieldToFilter('sku',array('like' => $params['sku'].'%'));
                if($params['status'] == '') {
                    $data = array('canceled', 'complete', 'delivered', 'paid', 'pending', 'received', 'processing', 'rescheduled', 'shipped');
                    $collection->addFieldToFilter('status', $data);
                }
                elseif($params['status'] == 'received') {
                    $data = array('pending', 'received', 'processing');
                    $collection->addFieldToFilter('status', $data);
                }
                else {
                    $collection->addFieldToFilter('status', $params['status']);
                } 
            }
        }

        if($this->_direction = $this->getRequest()->getParam('sort')){
            $collection->setOrder('sku', $this->_direction);
        }
        else{   
            $collection->setOrder('sku', $this->_direction);
        }

        $this->setCollection($collection);       
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,'all'=>'all'));
        //echo "<pre>";print_r($this->getCollection());echo "</pre>";exit();
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    }
	
	public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Convert given date to default (UTC) timezone
     *
     * @param string $date
     * @param string $locale
     * @return Zend_Date
     */
    protected function _convertDate($date, $locale)
    {
        try {
            $dateObj = $this->getLocale()->date(null, null, $locale, false);

            //set default timezone for store (admin)
            $dateObj->setTimezone(
                Mage::app()->getStore()->getConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_TIMEZONE)
            );

            //set begining of day
            $dateObj->setHour(00);
            $dateObj->setMinute(00);
            $dateObj->setSecond(00);

            //set date with applying timezone of store
            $dateObj->set($date, Zend_Date::DATE_SHORT, $locale);

            //convert store date to default date in UTC timezone without DST
            $dateObj->setTimezone(Mage_Core_Model_Locale::DEFAULT_TIMEZONE);

            return $dateObj;
        }
        catch (Exception $e) {
            return null;
        }
    }

    /**
     * Retrieve locale
     *
     * @return Mage_Core_Model_Locale
     */
    public function getLocale()
    {
        if (!$this->_locale) {
            $this->_locale = Mage::app()->getLocale();
        }
        return $this->_locale;
    }

    public function getNewDirection(){
        
        if($this->_direction == 'ASC')
        {
            return 'DESC';
        }
        else
        {
            return 'ASC';
        }

    }
}
?>