<?php   
class Ewall_Celebrity_Block_Skugroup_Index extends Mage_Core_Block_Template{   

	public function __construct()
    {
        parent::__construct();

        if(count($this->getRequest()->getPost())){
			$this->setFormData($this->getRequest()->getPost());
		}
		else if($params = Mage::getSingleton('core/session')->getCelebrity()){
			$this->setFormData($params);
		}
	}
	public function getInvoiceCollection(){
        $collection = Mage::getModel('sales/order_invoice_item')
                    ->getCollection()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('category_id', Mage::getSingleton('customer/session')->getCustomer()->getCelebrity());
        $collection->getSelect()
                ->columns('SUM(commision_amount) as commision_total, SUM(qty) AS total_ordered_qty')
                ->group('category_id');
        $collection->getSelect()->joinLeft(array('invoice' =>'sales_flat_invoice'), 'invoice.entity_id = main_table.parent_id', array('invoice.order_id'));
        $collection->getSelect()->joinLeft(array('ord_id' =>'sales_flat_order'), 'ord_id.entity_id = invoice.order_id', array('ord_id.status'))->where('ord_id.status = ?', 'paid'); 
        return $collection->getFirstItem();        
    }

}