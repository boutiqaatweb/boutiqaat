<?php   
class Ewall_Celebrity_Block_Skugroup_Report extends Mage_Core_Block_Template {    

	protected $_locale;

	const END_OF_DAY_IN_SECONDS = 86399;
	
    public function __construct()
    {
        parent::__construct();
        $collection = Mage::getModel('sales/order_item')
                    ->getCollection()
                    ->addFieldToFilter('category_id', Mage::getSingleton('customer/session')->getCustomer()->getCelebrity());
                
        if(count($this->getRequest()->getPost())) {
            $collection->addFieldToFilter('main_table.created_at', array(
                            'from' => $this->_convertDate($this->getRequest()->getPost('from'), 'en_US')->getTimestamp(), //Mage::app()->getLocale()->getLocaleCode()
                            'to' => $this->_convertDate($this->getRequest()->getPost('to'), 'en_US')->getTimestamp() + 86399, //Mage::app()->getLocale()->getLocaleCode()
                            'datetime' => true,                         
                        ));
            if($this->getRequest()->getPost('status') == ''){
                $data = array('canceled', 'complete', 'delivered', 'paid', 'pending', 'received', 'processing', 'rescheduled', 'shipped');
                $collection->getSelect()->joinLeft(array('ordertbl' =>'sales_flat_order'), 'ordertbl.entity_id = main_table.order_id', array('ordertbl.status as status'))->where('status IN(?)', $data);
                $collection->getSelect()->columns('SUM(qty_ordered) as total')->columns('SUM(commision_amount) as amount')->group('sku')->group('status');
                
            }
            elseif($this->getRequest()->getPost('status') == 'received') {
                $data = array('pending', 'received', 'processing');
                $collection->getSelect()->joinLeft(array('ordertbl' =>'sales_flat_order'), 'ordertbl.entity_id = main_table.order_id', array('ordertbl.status as status'))->where('status IN(?)', $data);
                $collection->getSelect()->columns('SUM(qty_ordered) as total')->columns('SUM(commision_amount) as amount')->group('sku')->group('status');
                
            }
            else {
                $collection->getSelect()->joinLeft(array('ordertbl' =>'sales_flat_order'), 'ordertbl.entity_id = main_table.order_id', array('ordertbl.status as status'))->where('status = ?', $this->getRequest()->getPost('status'));
                $collection->getSelect()->columns('SUM(qty_ordered) as total')->columns('SUM(commision_amount) as amount')->group('sku');
               
            }
           Mage::getSingleton('core/session')->setCelebrityGroup($this->getRequest()->getPost());
        }
        elseif($params = Mage::getSingleton('core/session')->getCelebrityGroup()) {
            
            $collection->addFieldToFilter('main_table.created_at', array(
                            'from' => $this->_convertDate($params['from'], 'en_US')->getTimestamp(), //Mage::app()->getLocale()->getLocaleCode()
                            'to' => $this->_convertDate($params['to'], 'en_US')->getTimestamp() + 86399, //Mage::app()->getLocale()->getLocaleCode()
                            'datetime' => true,                         
                        ));          
            if($params['status'] == '') {
                $data = array('canceled', 'complete', 'delivered', 'paid', 'pending', 'received', 'processing', 'rescheduled', 'shipped');
                $collection->getSelect()->joinLeft(array('ordertbl' =>'sales_flat_order'), 'ordertbl.entity_id = main_table.order_id', array('ordertbl.status as status'))->where('status IN(?)', $data);
                $collection->getSelect()->columns('SUM(qty_ordered) as total')->columns('SUM(commision_amount) as amount')->group('sku')->group('status');
            }
            elseif($params['status'] == 'received') {
                $data = array('pending', 'received', 'processing');
                $collection->getSelect()->joinLeft(array('ordertbl' =>'sales_flat_order'), 'ordertbl.entity_id = main_table.order_id', array('ordertbl.status as status'))->where('status IN(?)', $data);
                $collection->getSelect()->columns('SUM(qty_ordered) as total')->columns('SUM(commision_amount) as amount')->group('sku')->group('status');
            }
            else {
                $collection->getSelect()->joinLeft(array('ordertbl' =>'sales_flat_order'), 'ordertbl.entity_id = main_table.order_id', array('ordertbl.status as status'))->where('status = ?', $params['status']);
                $collection->getSelect()->columns('SUM(qty_ordered) as total')->columns('SUM(commision_amount) as amount')->group('sku');
            }
            
        }
        else{
            $collection->addFieldToFilter('item_id', 0);
        }
        $this->setCollection($collection);
    }
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
 
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,'all'=>'all'));
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    }
 
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Convert given date to default (UTC) timezone
     *
     * @param string $date
     * @param string $locale
     * @return Zend_Date
     */
    protected function _convertDate($date, $locale)
    {
        try {
            $dateObj = $this->getLocale()->date(null, null, $locale, false);

            //set default timezone for store (admin)
            $dateObj->setTimezone(
                Mage::app()->getStore()->getConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_TIMEZONE)
            );

            //set begining of day
            $dateObj->setHour(00);
            $dateObj->setMinute(00);
            $dateObj->setSecond(00);

            //set date with applying timezone of store
            $dateObj->set($date, Zend_Date::DATE_SHORT, $locale);

            //convert store date to default date in UTC timezone without DST
            $dateObj->setTimezone(Mage_Core_Model_Locale::DEFAULT_TIMEZONE);

            return $dateObj;
        }
        catch (Exception $e) {
            return null;
        }
    }

    /**
     * Retrieve locale
     *
     * @return Mage_Core_Model_Locale
     */
    public function getLocale()
    {
        if (!$this->_locale) {
            $this->_locale = Mage::app()->getLocale();
        }
        return $this->_locale;
    }

}