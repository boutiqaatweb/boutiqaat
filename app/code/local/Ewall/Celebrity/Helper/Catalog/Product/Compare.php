<?php
class Ewall_Celebrity_Helper_Catalog_Product_Compare extends Mage_Catalog_Helper_Product_Compare
{

	/**
     * Get parameters used for build add product to compare list urls
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  array
     */
    protected function _getUrlParams($product)
    {
        $params = array();

        $params['product'] = $product->getId();
        $params[Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED] = $this->getEncodedUrl();
        $params[Mage_Core_Model_Url::FORM_KEY] = $this->_getSingletonModel('core/session')->getFormKey();
        if(Mage::registry('current_category')){
            $params['category'] = Mage::registry('current_category')->getId();
        }
        else if(Mage::app()->getRequest()->getControllerName() == 'index' && Mage::app()->getRequest()->getActionName() == 'configure'){
            if($category_id = Mage::getModel('wishlist/item')->load(Mage::app()->getRequest()->getParam('id'))->getCategoryId()){
                $params['category'] = $category_id;
            }
        }
        else if(Mage::app()->getRequest()->getControllerName() == 'cart' && Mage::app()->getRequest()->getActionName() == 'configure'){
            if($category_id = Mage::getModel('sales/quote_item')->load(Mage::app()->getRequest()->getParam('id'))->getCategoryId()){
                $params['category'] = $category_id;
            }
        }

        return $params;
        /*return array(
            'product' => $product->getId(),
            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->getEncodedUrl(),
            Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey()
        );*/
    }


    /**
     * Retrieve url for adding product to conpare list
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  string
     */
    public function getAddUrl($product)
    {
        return $this->_getUrl('catalog/product_compare/add', $this->_getUrlParams($product));
    }

    /**
     * Retrive add to wishlist url
     *
     * @param Mage_Catalog_Model_Product $product
     * @return string
     */
    public function getAddToWishlistUrl($product)
    {
        $beforeCompareUrl = Mage::getSingleton('catalog/session')->getBeforeCompareUrl();

        $params = array(
            'product' => $product->getId(),
            Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey(),
            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->getEncodedUrl($beforeCompareUrl)
        );

        if(Mage::registry('current_category')){
            $params['category'] = Mage::registry('current_category')->getId();
        }
        else if(Mage::app()->getRequest()->getControllerName() == 'index' && Mage::app()->getRequest()->getActionName() == 'configure'){
            if($category_id = Mage::getModel('wishlist/item')->load(Mage::app()->getRequest()->getParam('id'))->getCategoryId()){
                $params['category'] = $category_id;
            }
        }
        else if(Mage::app()->getRequest()->getControllerName() == 'cart' && Mage::app()->getRequest()->getActionName() == 'configure'){
            if($category_id = Mage::getModel('sales/quote_item')->load(Mage::app()->getRequest()->getParam('id'))->getCategoryId()){
                $params['category'] = $category_id;
            }
        }

        return $this->_getUrl('wishlist/index/add', $params);
    }

    /**
     * Retrive add to cart url
     *
     * @param Mage_Catalog_Model_Product $product
     * @return string
     */
    public function getAddToCartUrl($product)
    {
        $beforeCompareUrl = $this->_getSingletonModel('catalog/session')->getBeforeCompareUrl();
        $params = array(
            'product' => $product->getId(),
            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->getEncodedUrl($beforeCompareUrl),
            Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey()
        );

        if(Mage::registry('current_category')){
            $params['category'] = Mage::registry('current_category')->getId();
        }
        else if(Mage::app()->getRequest()->getControllerName() == 'index' && Mage::app()->getRequest()->getActionName() == 'configure'){
            if($category_id = Mage::getModel('wishlist/item')->load(Mage::app()->getRequest()->getParam('id'))->getCategoryId()){
                $params['category'] = $category_id;
            }
        }
        else if(Mage::app()->getRequest()->getControllerName() == 'cart' && Mage::app()->getRequest()->getActionName() == 'configure'){
            if($category_id = Mage::getModel('sales/quote_item')->load(Mage::app()->getRequest()->getParam('id'))->getCategoryId()){
                $params['category'] = $category_id;
            }
        }

        return $this->_getUrl('checkout/cart/add', $params);
    }

    /**
     * Retrieve remove item from compare list url
     *
     * @param   $item
     * @return  string
     */
    public function getRemoveUrl($item)
    {
        $params = array(
            'product' => $item->getId(),
            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->getEncodedUrl()
        );
        return $this->_getUrl('catalog/product_compare/remove', $params);
    }
}
		