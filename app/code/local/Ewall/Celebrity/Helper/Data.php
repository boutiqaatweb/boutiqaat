<?php
class Ewall_Celebrity_Helper_Data extends Mage_Core_Helper_Abstract
{

	public function getCelebrityCategoryIds()
    {
    	$collection = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('celebrity')->addAttributeToFilter('is_celebrity', true);
    	$_celebrity = array();
    	foreach ($collection as $customer) {
    		$_celebrity[] = $customer->getCelebrity();
    	}
    	return $_celebrity;
    }

    public function getCelebrityCategories()
    {
    	$categories = Mage::getModel('catalog/category')
    				->getCollection()
    				->addAttributeToSelect('*')
    				->addFieldToFilter('entity_id', array("in" => $this->getCelebrityCategoryIds()));
    	return $categories;
    }


    public function getMobileDetect(){
            require_once(Mage::getBaseDir('lib').'/Mobile_detect/Mobile_Detect.php'); 
            return $detect = new Mobile_Detect;
            
    }


}
	 