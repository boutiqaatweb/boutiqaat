<?php
class Ewall_Celebrity_Helper_Wishlist_Data extends Mage_Wishlist_Helper_Data
{

	/**
     * Retrieve url for adding product to wishlist with params
     *
     * @param Mage_Catalog_Model_Product|Mage_Wishlist_Model_Item $item
     * @param array $params
     *
     * @return  string|bool
     */
    public function getAddUrlWithParams($item, array $params = array())
    {
        $productId = null;
        if ($item instanceof Mage_Catalog_Model_Product) {
            $productId = $item->getEntityId();
        }
        if ($item instanceof Mage_Wishlist_Model_Item) {
            $productId = $item->getProductId();
        }

        if ($productId) {
            $params['product'] = $productId;
            $params[Mage_Core_Model_Url::FORM_KEY] = $this->_getSingletonModel('core/session')->getFormKey();

	        if(Mage::registry('current_category')){
	            $params['category'] = Mage::registry('current_category')->getId();
	        }
	        else if(Mage::app()->getRequest()->getControllerName() == 'index' && Mage::app()->getRequest()->getActionName() == 'configure'){
	            if($category_id = Mage::getModel('wishlist/item')->load(Mage::app()->getRequest()->getParam('id'))->getCategoryId()){
	                $params['category'] = $category_id;
	            }
	        }
	        else if(Mage::app()->getRequest()->getControllerName() == 'cart' && Mage::app()->getRequest()->getActionName() == 'configure'){
	            if($category_id = Mage::getModel('sales/quote_item')->load(Mage::app()->getRequest()->getParam('id'))->getCategoryId()){
	                $params['category'] = $category_id;
	            }
	        }
            return $this->_getUrlStore($item)->getUrl('wishlist/index/add', $params);
        }

        return false;
    }
}
		