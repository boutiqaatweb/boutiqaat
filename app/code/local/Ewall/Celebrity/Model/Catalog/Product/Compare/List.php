<?php
class Ewall_Celebrity_Model_Catalog_Product_Compare_List extends Mage_Catalog_Model_Product_Compare_List
{

	/**
     * Add product to Compare List
     *
     * @param int|Mage_Catalog_Model_Product $product
     * @return Mage_Catalog_Model_Product_Compare_List
     */
    public function addProduct($product)
    {
        /* @var $item Mage_Catalog_Model_Product_Compare_Item */
        $item = Mage::getModel('catalog/product_compare_item');

        $this->_addVisitorToItem($item);
        $this->_addCategoryIdToItem($item);
        $item->loadByProduct($product);

        if (!$item->getId()) {

            $item->addProductData($product);
            $item->save();
        }

        return $this;
    }


    protected function _addCategoryIdToItem($item)
    {
        
        if($category = Mage::app()->getRequest()->getParam('category')) {
            $item->addCategoryData($category);
        }

        return $this;
    }
}
		