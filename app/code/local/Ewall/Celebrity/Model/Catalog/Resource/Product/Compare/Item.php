<?php
class Ewall_Celebrity_Model_Catalog_Resource_Product_Compare_Item extends Mage_Catalog_Model_Resource_Product_Compare_Item
{

	/**
     * Load object by product
     *
     * @param Mage_Core_Model_Abstract $object
     * @param mixed $product
     * @return bool
     */
    public function loadByProduct(Mage_Catalog_Model_Product_Compare_Item $object, $product)
    {
        $read = $this->_getReadAdapter();
        if ($product instanceof Mage_Catalog_Model_Product) {
            $productId = $product->getId();
        } else {
            $productId = $product;
        }
        $select = $read->select()->from($this->getMainTable())
            ->where('product_id = ?', (int)$productId);

        if ($object->getCustomerId()) {
            $select->where('customer_id = ?', (int)$object->getCustomerId());
        } else {
            $select->where('visitor_id = ?', (int)$object->getVisitorId());
        }


        $select->where('category_id = ?', (int)$object->getCategoryId());

        $data = $read->fetchRow($select);

        if (!$data) {
            return false;
        }

        $object->setData($data);

        $this->_afterLoad($object);
        return true;
    }
}
		