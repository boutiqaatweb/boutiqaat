<?php
class Ewall_Celebrity_Model_Eav_Entity_Attribute_Source_Celebrityoptions extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = array();
            $collection = Mage::getResourceModel('catalog/category_collection');
            $collection->addAttributeToSelect('name')->addPathFilter('^1/[0-9/]+')->load();
            $cats = array();
            $options = array();
            foreach ($collection as $category) {
                $c = new stdClass();
                $c->label = $category->getName();
                $c->value = $category->getId();
                $c->level = $category->getLevel();
                $c->parentid = $category->getParentId();
                $cats[$c->value] = $c;
            }

            foreach ($cats as $id => $c) {
                if (isset($cats[$c->parentid])) {
                    if (!isset($cats[$c->parentid]->child)) {
                        $cats[$c->parentid]->child = array();
                    }
                    $cats[$c->parentid]->child[] =& $cats[$id];
                }
            }
            foreach ($cats as $id => $c) {
                if (!isset($cats[$c->parentid])) {
                    $stack = array($cats[$id]);
                    while (count($stack) > 0) {
                        $opt = array_pop($stack);
                        $this->_options[] = array(
                            'label' => ($opt->level > 1 ? str_repeat('- - ', $opt->level - 1) : '') . $opt->label,
                            'value' => $opt->value
                        );
                        array_push($options, $option);
                        if (isset($opt->child) && count($opt->child)) {
                            foreach (array_reverse($opt->child) as $child) {
                                array_push($stack, $child);
                            }
                        }
                    }
                }
            }
            unset($cats);
        }
        return $this->_options;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $_options = array();
        foreach ($this->getAllOptions() as $option) {
            $_options[$option["value"]] = $option["label"];
        }
        return $_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string
     */
    public function getOptionText($value)
    {
        $options = $this->getAllOptions();
        foreach ($options as $option) {
            if ($option["value"] == $value) {
                return $option["label"];
            }
        }
        return false;
    }

    /**
     * Retrieve Column(s) for Flat
     *
     * @return array
     */
    public function getFlatColums()
    {
        $columns = array();
        $columns[$this->getAttribute()->getAttributeCode()] = array(
            "type"      => "tinyint(1)",
            "unsigned"  => false,
            "is_null"   => true,
            "default"   => null,
            "extra"     => null
        );

        return $columns;
    }

    /**
     * Retrieve Indexes(s) for Flat
     *
     * @return array
     */
    public function getFlatIndexes()
    {
        $indexes = array();

        $index = "IDX_" . strtoupper($this->getAttribute()->getAttributeCode());
        $indexes[$index] = array(
            "type"      => "index",
            "fields"    => array($this->getAttribute()->getAttributeCode())
        );

        return $indexes;
    }

    /**
     * Retrieve Select For Flat Attribute update
     *
     * @param int $store
     * @return Varien_Db_Select|null
     */
    public function getFlatUpdateSelect($store)
    {
        return Mage::getResourceModel("eav/entity_attribute")
            ->getFlatUpdateSelect($this->getAttribute(), $store);
    }
}

			 