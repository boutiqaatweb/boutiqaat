<?php
class Ewall_Celebrity_Model_Sales_Quote_Item extends Mage_Sales_Model_Quote_Item
{	

	public function getProduct()
    {
        $product = $this->_getData('product');
        if ($product === null && $this->getProductId()) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId($this->getQuote()->getStoreId())
                ->load($this->getProductId());
            $this->setProduct($product);
        }

        /**
         * Reset product final price because it related to custom options
         */
        
        $product->setFinalPrice(null);
        if (is_array($this->_optionsByCode)) {
            $product->setCustomOptions($this->_optionsByCode);
        }
        
        if($category_id = $this->getCategoryId() ? $this->getCategoryId() : Mage::app()->getRequest()->getParam('category')){            
        	$product_data = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getStoreId())
                ->load($this->getProductId());       	
        	$data = unserialize($product_data->getCelebrityCommision());
            $special_data = unserialize($product_data->getCelebritySpecialPrice());
   	        if(isset($special_data[$category_id])){	
   		        $special_name = $product_data->getSpecialName();
                $ad_date = unserialize($product_data->getCelebrityAdDate());
            	$this->setCategoryId($category_id);	
            	$this->setCommisionPercent($data[$category_id]);
            	$this->setCommisionAmount($this->getQty() * (($special_data[$category_id] * $data[$category_id] ) / 100 ));	
            	$this->setBaseCommisionAmount($this->getQty() * (($special_data[$category_id] * $data[$category_id]) / 100));
            	$this->setSpecialName($special_name);
                $this->setCelebritySpecialPrice($special_data[$category_id]);
            	$this->setCelebrityAdDate($ad_date[$category_id]);
            	//$product->setPrice($special_data[$category_id]);
                
                $price = $special_data[$category_id];
                foreach ($this->_optionsByCode as $option) {
                    if($option->getCode() == 'option_ids'){
                        foreach(explode(',', $option->getValue()) as $optionId) {
                            $option = $this->getOptionByCode('option_'.$optionId); 
                            $options = $option->getProduct()->getOptionById($optionId)->getValues();
                            $price += $options[$option->getValue()]->getPrice(); 
                        }
                    }                          
                } 

            	$product->setFinalPrice($price);
                
                //if((int)$category_id != -1){
                    //$product->setName($category_id); //$product_data->getSpecialName()
                    //$this->setName($category_id); //$product_data->getSpecialName()
                //}
                
            }
            else{
                $this->setCategoryId($category_id);        
            }    
        }
        else{
            $this->setCategoryId($category_id);        
        } 
        
        return $product;
    }


    /**
     * Check product representation in item
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  bool
     */
    public function representProduct($product)
    {
        $itemProduct = $this->getProduct();        
        if (!$product || $itemProduct->getId() != $product->getId()) {
            return false;
        }

        /**
         * Check maybe product is planned to be a child of some quote item - in this case we limit search
         * only within same parent item
         */
        $stickWithinParent = $product->getStickWithinParent();
        if ($stickWithinParent) {
            if ($this->getParentItem() !== $stickWithinParent) {
                return false;
            }
        }

        // Check options
        $itemOptions = $this->getOptionsByCode();
        $productOptions = $product->getCustomOptions();

        if (!$this->compareOptions($itemOptions, $productOptions)) {
            return false;
        }
        if (!$this->compareOptions($productOptions, $itemOptions)) {
            return false;
        }


        
        $current_request = $product->getCustomOptions();  
        $info_buyRequest = unserialize($current_request['info_buyRequest']->getData('value')); 
        /*echo $this->getCategoryId().' ';    
        echo $info_buyRequest['category'].' ';  
        echo $this->getBuyRequest()->getCategory().' ';
        exit;  */
        if($this->getBuyRequest()->getCategory() == -1) {
             if($this->getCategoryId() != $this->getBuyRequest()->getCategory()) {
                return false;
            }
        }

        if($this->getBuyRequest()->getCategory()){
            if($this->getCategoryId() != $info_buyRequest['category']){
                return false;
            }
        }

        return true;
    }


    /**
     * Compare item
     *
     * @param   Mage_Sales_Model_Quote_Item $item
     * @return  bool
     */
    public function compare($item)
    {	
        if ($this->getProductId() != $item->getProductId()) {
            return false;
        }

        if($this->getCategoryId() != $item->getCategoryId()){
        	return false;
        }

        foreach ($this->getOptions() as $option) {
            if (in_array($option->getCode(), $this->_notRepresentOptions)) {
                continue;
            }
            if ($itemOption = $item->getOptionByCode($option->getCode())) {
                $itemOptionValue = $itemOption->getValue();
                $optionValue = $option->getValue();

                // dispose of some options params, that can cramp comparing of arrays
                if (is_string($itemOptionValue) && is_string($optionValue)) {
                    $_itemOptionValue = @unserialize($itemOptionValue);
                    $_optionValue = @unserialize($optionValue);
                    if (is_array($_itemOptionValue) && is_array($_optionValue)) {
                        $itemOptionValue = $_itemOptionValue;
                        $optionValue = $_optionValue;
                        // looks like it does not break bundle selection qty
                        unset($itemOptionValue['qty'], $itemOptionValue['uenc']);
                        unset($optionValue['qty'], $optionValue['uenc']);
                    }
                }

                if ($itemOptionValue != $optionValue) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }


    /**
     * Get item price used for quote calculation process.
     * This method get custom price (if it is defined) or original product final price
     *
     * @return float
     */
    public function getCalculationPrice()
    {
        $price = $this->getProduct()->getFinalPrice();
        if (is_null($price)) {
            if ($this->hasCustomPrice()) {
                $price = $this->getCustomPrice();
            } else {
                $price = $this->getConvertedPrice();
            }
            $this->setData('calculation_price', $price);
        }
        return $price;
    }
}
		