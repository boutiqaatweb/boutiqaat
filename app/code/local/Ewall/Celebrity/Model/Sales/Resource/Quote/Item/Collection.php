<?php
class Ewall_Celebrity_Model_Sales_Resource_Quote_Item_Collection extends Mage_Sales_Model_Resource_Quote_Item_Collection
{

	/**
     * After load processing
     *
     * @return Mage_Sales_Model_Resource_Quote_Item_Collection
     */
    protected function _afterLoad()
    {
        //parent::_afterLoad();
    	
        /**
         * Assign parent items
         */
        foreach ($this as $item) {
            if ($item->getParentItemId()) {
                $item->setParentItem($this->getItemById($item->getParentItemId()));
            }

            if ($this->_quote) {
                $item->setQuote($this->_quote);
            }

            if($category_id = $item->getCategoryId() ? $item->getCategoryId() : Mage::app()->getRequest()->getParam('category')){

                $product = $item->getProduct();          
                $data = unserialize($product->getCelebrityCommision());
                $special_data = unserialize($product->getCelebritySpecialPrice());
                if(isset($special_data[$category_id])){ 

                    $item->setCategoryId($category_id); 
                    $item->setCommisionPercent($data[$category_id]);
                    $item->setCommisionAmount($item->getQty() * (($special_data[$category_id] * $data[$category_id] ) / 100 )); 
                    $item->setBaseCommisionAmount($item->getQty() * (($special_data[$category_id] * $data[$category_id]) / 100));
                    $item->setPrice($special_data[$category_id]);
                    $item->setBasePrice($special_data[$category_id]);
                }
            }
        }

        /**
         * Assign options and products
         */
        $this->_assignOptions();
        $this->_assignProducts();
        $this->resetItemsDataChanged();

        return $this;
    }
}
		