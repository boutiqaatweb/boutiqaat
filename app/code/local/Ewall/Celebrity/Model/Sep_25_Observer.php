<?php
class Ewall_Celebrity_Model_Observer
{

	public function changeSpecialPrice(Varien_Event_Observer $observer)
	{
		if(Mage::registry('current_category')){
			$product = $observer->getEvent()->getProduct();
			$category_id = Mage::registry('current_category')->getId();
			$data = unserialize($product->getCelebritySpecialPrice());
			if(is_array($data)){
				if(isset($data[$category_id])){
					//$product->setPrice(10.0000);
					$product->setFinalPrice($data[$category_id]);
				}
			}				
		}	
		if(Mage::app()->getRequest()->getControllerName() == 'cart' && Mage::app()->getRequest()->getActionName() == 'configure'){
			if($category_id = Mage::getModel('sales/quote_item')->load(Mage::app()->getRequest()->getParam('id'))->getCategoryId()){
				$product = $observer->getEvent()->getProduct();
				$data = unserialize($product->getCelebritySpecialPrice());
				if(isset($data[$category_id])){
					//$product->setPrice(10.0000);
	            	$product->setFinalPrice($data[$category_id]);
	            }
	        }	
		}

		if(Mage::app()->getRequest()->getControllerName() == 'index' && Mage::app()->getRequest()->getActionName() == 'configure'){
			if($category_id = Mage::getModel('wishlist/item')->load(Mage::app()->getRequest()->getParam('id'))->getCategoryId()){
				$product = $observer->getEvent()->getProduct();
				$data = unserialize($product->getCelebritySpecialPrice());
				if(isset($data[$category_id])){
					//$product->setPrice(10.0000);
	            	$product->setFinalPrice($data[$category_id]);
	            }
	        }	
		}

		if(Mage::app()->getRequest()->getControllerName() == 'quickview' && Mage::app()->getRequest()->getActionName() == 'index'){
			if($category_id = Mage::app()->getRequest()->getParam('category')){
				$product = $observer->getEvent()->getProduct();
				$data = unserialize($product->getCelebritySpecialPrice());
				if(isset($data[$category_id])){
					//$product->setPrice(10.0000);
	            	$product->setFinalPrice($data[$category_id]);
	            }
	        }	
		}
		/*if(Mage::app()->getRequest()->getParam('uenc')){
			$url = (string) base64_decode(Mage::app()->getRequest()->getParam('uenc'));
			$pathinfo = str_replace((string)Mage::getBaseUrl(), '', $url); 
			$path = explode('/', $pathinfo);
			if(count($path) > 1){
				$path = array_reverse($path);
				$category = Mage::getModel ('catalog/category')
						->getCollection()
            			->addAttributeToFilter('url_key', $path[1])
           				->getFirstItem();
           		if($category_id = $category->getId()){
           			$product = $observer->getEvent()->getProduct();
           			$data = unserialize($product->getCelebritySpecialPrice());
					if(isset($data[$category_id])){
						//$product->setPrice(10.0000);
		            	$product->setFinalPrice($data[$category_id]);		            	
		            }	
           		}				
			}			
		}*/
		
	}

	public function changeListSpecialPrice(Varien_Event_Observer $observer)
	{	

		if(Mage::registry('current_category')){
			
			$category_id = Mage::registry('current_category')->getId();
			$collection = $observer->getEvent()->getCollection();
			foreach ($collection as $product) {
				$data = unserialize($product->getCelebritySpecialPrice());
				if(is_array($data)){
					if(isset($data[$category_id])){
						//$product->setPrice(10.0000);
	                	$product->setFinalPrice($data[$category_id]);
	                }
				}				                    
			}
		}
	}
		
	public function saveCelebrityData(Varien_Event_Observer $observer)
	{	
		$item = $observer->getQuoteItem();
		/*if($category_id = Mage::app()->getRequest()->getParam('category')){           			
   			$product = $item->getProduct();
   			$data = unserialize($product->getCelebrityCommision());
   			$special_data = unserialize($product->getCelebritySpecialPrice());
   			if(isset($data[$category_id])){	
            	$item->setCategoryId($category_id);
            	$item->setCommisionPercent($data[$category_id]);
            	$item->setCommisionAmount(($data[$category_id]*100) / $special_data[$category_id]);	
            	$item->setBaseCommisionAmount(($data[$category_id]*100) / $special_data[$category_id]);	            	
            }
            else{
	   			$item->setCategoryId(0);		
	   		}
            		
   		}
   		else{
   			$item->setCategoryId(0);		
   		}*/
	}


	public function SaveWishlistCelebrity(Varien_Event_Observer $observer)
	{

		$items = $observer->getItems();		
		if($category_id = Mage::app()->getRequest()->getParam('category')){  
			foreach ($items as $item) {
				$item->setCategoryId($category_id);
			}	
   		}
   		
	}

	public function SaveDateInvoiced(Varien_Event_Observer $observer)
	{
		$invoice = $observer->getInvoice();
		//$date = new Zend_Date(Mage::getModel('core/date')->timestamp());

		foreach($invoice->getAllItems() as $item){
			$item->setDateInvoiced($invoice->getCreatedAt())->save();	 //$date->toString('Y-M-d H:m:s')		
		}
	}

	public function saveCelebrityMaxData(Varien_Event_Observer $observer)
	{
		$order = $observer->getOrder();
		$max_data = array();
		foreach ($order->getAllItems() as $item) {
			$product = Mage::getModel('catalog/product')->load($item->getProductId());
			$max_data = unserialize($product->getMaxorder());
			if(count($max_data)){
				if(isset($max_data[$item->getCategoryId()])){
					$max_data[$item->getCategoryId()] = $max_data[$item->getCategoryId()]+$item->getQtyOrdered();
				}
				else{
					$max_data[$item->getCategoryId()] = $item->getQtyOrdered();	
				}
			}
			else{
				$max_data[$item->getCategoryId()] = $item->getQtyOrdered();
			}
			$product->setMaxorder(serialize($max_data))->save();
		}	

	}

	public function changeSalableByMax(Varien_Event_Observer $observer)
	{
		$salable = $observer->getSalable();
		$product = $observer->getProduct();

		if(Mage::registry('current_category')){			
			$category_id = Mage::registry('current_category')->getId();
			$max = unserialize($product->getMax());
			$maxorder = unserialize($product->getMaxorder());
			if(is_array($max) && is_array($maxorder)){
				if (isset($max[$category_id]) && isset($maxorder[$category_id])) {
					if($maxorder[$category_id] >= $max[$category_id]){
						$salable->setIsSalable(false);
					}
				}				
			}
		}
	}

	public function changeMaxorderOnCancel(Varien_Event_Observer $observer){
		$item = $observer->getItem();
		$product = Mage::getModel('catalog/product')->load($item->getProductId());
		if($product->getId()){
			$max_data = unserialize($product->getMaxorder());
			if(is_array($max_data) && count($max_data)){
				if(isset($max_data[$item->getCategoryId()])){
					$max_data[$item->getCategoryId()] = $max_data[$item->getCategoryId()] - $item->getQtyToCancel();
				}			
			}			
			$product->setMaxorder(serialize($max_data))->save();
		}
	}
}
