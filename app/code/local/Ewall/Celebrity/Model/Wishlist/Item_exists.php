<?php
class Ewall_Celebrity_Model_Wishlist_Item extends Mage_Wishlist_Model_Item
{

	/**
     * Retrieve item product instance
     *
     * @throws Mage_Core_Exception
     * @return Mage_Catalog_Model_Product
     */
    /*public function getProduct()
    {
        $product = $this->_getData('product');
        if (is_null($product)) {
            if (!$this->getProductId()) {
                Mage::throwException(Mage::helper('wishlist')->__('Cannot specify product.'));
            }

            $product = Mage::getModel('catalog/product')
                ->setStoreId($this->getStoreId())
                ->load($this->getProductId());

            $this->setData('product', $product);
        }

        $product->setFinalPrice(null);
        $product->setCustomOptions($this->_optionsByCode);
        
        if($category_id = $this->getCategoryId() ? $this->getCategoryId() : Mage::app()->getRequest()->getParam('category')){
        	$product_data = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getStoreId())
                ->load($this->getProductId());       	
        	$data = unserialize($product_data->getCelebrityCommision());
   			$special_data = unserialize($product_data->getCelebritySpecialPrice());
   			if(isset($special_data[$category_id])){	
            	$this->setCategoryId($category_id);	
            	$this->setCommisionPercent($data[$category_id]);
            	$qty = 1;
            	if($qty = $this->getQty()){}
            	$this->setCommisionAmount($qty * (($special_data[$category_id] * $data[$category_id] ) / 100 ));	
            	$this->setBaseCommisionAmount($qty * (($special_data[$category_id] * $data[$category_id]) / 100));
            	//$product->setPrice($special_data[$category_id]);
            	$product->setFinalPrice($special_data[$category_id]);
            }
        }



        return $product;
    }*/


    /**
     * Check product representation in item
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  bool
     */
    public function representProduct($product)
    {
        $itemProduct = $this->getProduct();

        /*if($this->getCategoryId()){ //Mage::app()->getRequest()->getActionName() != 'fromcart' && 
            if($this->getCategoryId() !=  Mage::app()->getRequest()->getParam('category')){
            	return false;
            }
        }

        $itemOptions    = $this->getOptionsByCode();
        $productOptions = $product->getCustomOptions();

        if(!$this->compareOptions($itemOptions, $productOptions)){
            return false;
        }
        if(!$this->compareOptions($productOptions, $itemOptions)){
            return false;
        }

        $current_request = $itemProduct->getCustomOptions();  
        $info_buyRequest = unserialize($current_request['info_buyRequest']->getData('value'));     
        
        if($this->getBuyRequest()->getCategory()){
            if($this->getCategoryId() != $info_buyRequest['category']){
                return false;
            }
        }*/
        return true;
    }
}
		