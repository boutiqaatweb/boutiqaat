<?php
class Ewall_Celebrity_Model_Wishlist_Wishlist extends Mage_Wishlist_Model_Wishlist
{
	/**
     * Adds new product to wishlist.
     * Returns new item or string on error.
     *
     * @param int|Mage_Catalog_Model_Product $product
     * @param mixed $buyRequest
     * @param bool $forciblySetQty
     * @return Mage_Wishlist_Model_Item|string
     */
    public function addNewItem($product, $buyRequest = null, $forciblySetQty = false)
    {
        /*
         * Always load product, to ensure:
         * a) we have new instance and do not interfere with other products in wishlist
         * b) product has full set of attributes
         */
        if ($product instanceof Mage_Catalog_Model_Product) {
            $productId = $product->getId();
            // Maybe force some store by wishlist internal properties
            $storeId = $product->hasWishlistStoreId() ? $product->getWishlistStoreId() : $product->getStoreId();
        } else {
            $productId = (int) $product;
            if ($buyRequest->getStoreId()) {
                $storeId = $buyRequest->getStoreId();
            } else {
                $storeId = Mage::app()->getStore()->getId();
            }
        }

        /* @var $product Mage_Catalog_Model_Product */
        $product = Mage::getModel('catalog/product')
            ->setStoreId($storeId)
            ->load($productId);

        if ($buyRequest instanceof Varien_Object) {
            $_buyRequest = $buyRequest;
        } elseif (is_string($buyRequest)) {
            $_buyRequest = new Varien_Object(unserialize($buyRequest));
        } elseif (is_array($buyRequest)) {
            $_buyRequest = new Varien_Object($buyRequest);
        } else {
            $_buyRequest = new Varien_Object();
        }
        
        $cartCandidates = $product->getTypeInstance(true)
            ->processConfiguration($_buyRequest, $product);

        /**
         * Error message
         */
        if (is_string($cartCandidates)) {
            return $cartCandidates;
        }

        /**
         * If prepare process return one object
         */
        if (!is_array($cartCandidates)) {
            $cartCandidates = array($cartCandidates);
        }

        $errors = array();
        $items = array();

        foreach ($cartCandidates as $candidate) {
            if ($candidate->getParentProductId()) {
                continue;
            }
            $candidate->setWishlistStoreId($storeId);

            if($_buyRequest->getCategory() || ($_buyRequest->getCategory()==0)){

                if(Mage::app()->getRequest()->getParam('type')){
                    if(Mage::app()->getRequest()->getParam('type') == 'rest'){
                        Mage::register('category_id', $_buyRequest->getCategory());
                    }
                }
            }

            $qty = $candidate->getQty() ? $candidate->getQty() : 1; // No null values as qty. Convert zero to 1.
            //$item = $this->_addCatalogProduct($candidate, $qty, $forciblySetQty);

            if($_buyRequest->getCategory() || ($_buyRequest->getCategory()==0)){

                $item = $this->_addCatalogProduct($candidate, $qty, $forciblySetQty, $_buyRequest);
            	//$item->setCategoryId($_buyRequest->getCategory());
                if(Mage::app()->getRequest()->getParam('type')){
                    if(Mage::app()->getRequest()->getParam('type') == 'rest'){
                        Mage::unregister('category_id');
                    }
                }
            }
            else{
                $item = $this->_addCatalogProduct($candidate, $qty, $forciblySetQty);
            }            
            $items[] = $item;

            // Collect errors instead of throwing first one
            if ($item->getHasError()) {
                $errors[] = $item->getMessage();
            }
        }

        Mage::dispatchEvent('wishlist_product_add_after', array('items' => $items));

        return $item;
    }
    protected function _addCatalogProduct(Mage_Catalog_Model_Product $product, $qty = 1, $forciblySetQty = false, $_buyRequest = null)
    {
        $item = null;
        foreach ($this->getItemCollection() as $_item) {
            if ($_item->representProduct($product)) {
                $item = $_item;
                break;
            }
        }

        if ($item === null) {
            $storeId = $product->hasWishlistStoreId() ? $product->getWishlistStoreId() : $this->getStore()->getId();
            $item = Mage::getModel('wishlist/item');
            $item->setProductId($product->getId())
                ->setWishlistId($this->getId())
                ->setAddedAt(now())
                ->setStoreId($storeId)
                ->setOptions($product->getCustomOptions())
                ->setProduct($product)
                ->setQty($qty)
                ->setCategoryId($_buyRequest->getCategory())
                ->save();

            Mage::dispatchEvent('wishlist_item_add_after', array('wishlist' => $this));

            if ($item->getId()) {
                $this->getItemCollection()->addItem($item);
            }
        } else {

            $qty = $forciblySetQty ? $qty : $item->getQty() + $qty;
            if(Mage::app()->getRequest()->getParam('type')){
                if(Mage::app()->getRequest()->getParam('type') == 'rest'){
                    $resource = Mage::getSingleton('core/resource');
                    $writeConnection = $resource->getConnection('core_write');
                    $table = $resource->getTableName('wishlist/item');
                    $query = "UPDATE {$table} SET qty = '{$qty}' WHERE wishlist_item_id = ". (int) $item->getWishlistItemId();
                    $writeConnection->query($query);
                }
            }
            else{                
                $item->setQty($qty)->save();
            }
        }

        $this->addItem($item);

        return $item;
    }

}
		