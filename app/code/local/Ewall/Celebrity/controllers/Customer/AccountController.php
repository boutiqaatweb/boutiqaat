<?php
require_once "Mage/Customer/controllers/AccountController.php";  
class Ewall_Celebrity_Customer_AccountController extends Mage_Customer_AccountController{


	/**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        // a brute-force protection here would be nice

        parent::preDispatch();

        if (!$this->getRequest()->isDispatched()) {
            return;
        }

        $action = $this->getRequest()->getActionName();
        $openActions = array(
            'create',
            'login',
            'logoutsuccess',
            'forgotpassword',
            'forgotpasswordpost',
            'resetpassword',
            'resetpasswordpost',
            'confirm',
            'confirmation'
        );
        $pattern = '/^(' . implode('|', $openActions) . ')/i';

        if (!preg_match($pattern, $action)) {
            if (!$this->_getSession()->authenticate($this)) {
                $this->setFlag('', 'no-dispatch', true);
            }
        } else {
            $this->_getSession()->setNoReferer(true);
        }

     //    if($this->_getSession()->getCustomer()->getIsCelebrity()){
    	// 	$this->_redirect('celebrity');
    	// 	return;
    	// }
    }

	 /**
     * Define target URL and redirect customer after logging in
     */
    protected function _loginPostRedirect()
    {
        $session = $this->_getSession();

    	if($session->getCustomer()->getIsCelebrity()){
    		$this->_redirect('celebrity');
    		return;
    	}
    	else{
	        if (!$session->getBeforeAuthUrl() || $session->getBeforeAuthUrl() == Mage::getBaseUrl()) {
	            // Set default URL to redirect customer to
	            $session->setBeforeAuthUrl($this->_getHelper('customer')->getAccountUrl());
	            // Redirect customer to the last page visited after logging in
	            if ($session->isLoggedIn()) {
	                if (!Mage::getStoreConfigFlag(
	                    Mage_Customer_Helper_Data::XML_PATH_CUSTOMER_STARTUP_REDIRECT_TO_DASHBOARD
	                )) {
	                    $referer = $this->getRequest()->getParam(Mage_Customer_Helper_Data::REFERER_QUERY_PARAM_NAME);
	                    if ($referer) {
	                        // Rebuild referer URL to handle the case when SID was changed
	                        $referer = $this->_getModel('core/url')
	                            ->getRebuiltUrl( $this->_getHelper('core')->urlDecode($referer));
	                        if ($this->_isUrlInternal($referer)) {
	                            $session->setBeforeAuthUrl($referer);
	                        }
	                    }
	                } else if ($session->getAfterAuthUrl()) {
	                    $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
	                }
	            } else {
	                $session->setBeforeAuthUrl( $this->_getHelper('customer')->getLoginUrl());
	            }
	        } else if ($session->getBeforeAuthUrl() ==  $this->_getHelper('customer')->getLogoutUrl()) {
	            $session->setBeforeAuthUrl( $this->_getHelper('customer')->getDashboardUrl());
	        } else {
	            if (!$session->getAfterAuthUrl()) {
	                $session->setAfterAuthUrl($session->getBeforeAuthUrl());
	            }
	            if ($session->isLoggedIn()) {
	                $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
	            }
	        }
	        $this->_redirectUrl($session->getBeforeAuthUrl(true));
	    }
    }
     protected function _welcomeCustomer(Mage_Customer_Model_Customer $customer, $isJustConfirmed = false)
    {
        $this->_getSession()->addSuccess(
            $this->__('Thank you for registering with Boutiqaat Store.')
        );
        if ($this->_isVatValidationEnabled()) {
            // Show corresponding VAT message to customer
            $configAddressType =  $this->_getHelper('customer/address')->getTaxCalculationAddressType();
            $userPrompt = '';
            switch ($configAddressType) {
                case Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING:
                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you shipping address for proper VAT calculation',
                        $this->_getUrl('customer/address/edit'));
                    break;
                default:
                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you billing address for proper VAT calculation',
                        $this->_getUrl('customer/address/edit'));
            }
            $this->_getSession()->addSuccess($userPrompt);
        }

        $customer->sendNewAccountEmail(
            $isJustConfirmed ? 'confirmed' : 'registered',
            '',
            Mage::app()->getStore()->getId()
        );

        $successUrl = $this->_getUrl('*/*/index', array('_secure' => true));
        if ($this->_getSession()->getBeforeAuthUrl()) {
            $successUrl = $this->_getSession()->getBeforeAuthUrl(true);
        }
        return $successUrl;
    }

}
				