<?php
	class Ewall_Celebrity_GroupController extends Mage_Core_Controller_Front_Action {

	 public function preDispatch()
    {
        parent::preDispatch();
        if(!Mage::getSingleton('customer/session')->authenticate($this)){
            $this->setFlag('', 'no-dispatch', true);
        } 
        if(!Mage::getSingleton('customer/session')->getCustomer()->getIsCelebrity()){
            $this->norouteAction();
            return;
        } 
                
    }

		public function SkugroupAction() {
			$this->loadLayout(); 
			
			$this->renderLayout();
		}
	}
?>