<?php
class Ewall_Celebrity_IndexController extends Mage_Core_Controller_Front_Action{

    public function preDispatch()
    {
        parent::preDispatch();

        if(!Mage::getSingleton('customer/session')->authenticate($this)){
            $this->setFlag('', 'no-dispatch', true);
        } 

        if(!Mage::getSingleton('customer/session')->getCustomer()->getIsCelebrity()){
            $this->norouteAction();
            return;
        } 
                
    }


    public function IndexAction() {
        
	      $this->loadLayout();   
	      $this->getLayout()->getBlock("head")->setTitle($this->__("Celebrity Report"));

        /*$breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		    ));

        $breadcrumbs->addCrumb("titlename", array(
                "label" => $this->__("Titlename"),
                "title" => $this->__("Titlename")
		    ));*/

        $this->renderLayout();
    }

    public function SkuAction(){
        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("SKU Report"));
        $this->renderLayout();
    }
}