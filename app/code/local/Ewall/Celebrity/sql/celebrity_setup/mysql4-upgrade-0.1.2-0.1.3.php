<?php
$installer = $this;
$installer->startSetup();


$installer->addAttribute("customer", "mobile",  array(
    "type"     => "varchar",
    "backend"  => "",
    "label"    => "Mobile",
    "input"    => "text",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
    "note"       => ""

    ));

$attribute   = Mage::getSingleton("eav/config")->getAttribute("customer", "mobile");

$used_in_forms = array(
            'customer_account_create',
            'customer_account_edit',
            'checkout_register',
            'adminhtml_customer',
        );
        $attribute->setData("used_in_forms", $used_in_forms)
        ->setData("is_used_for_customer_segment", true)
        ->setData("is_system", 0)
        ->setData("is_user_defined", 1)
        ->setData("is_visible", 0)
        ->setData("sort_order", 100);
        $attribute->save();
$installer->endSetup();