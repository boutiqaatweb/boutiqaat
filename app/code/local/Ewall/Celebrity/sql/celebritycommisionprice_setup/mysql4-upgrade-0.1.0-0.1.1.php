<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'celebrity_ad_date', array(
    'group'             => 'Prices',
    'input'             => 'textarea',
    'type'              => 'text',
    'backend'           => '',
    'input_renderer'    => 'celebrity/catalog_product_helper_form_date',
    'label'             => 'Ad Date',
    'class'             => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,bundle,grouped',
    'is_configurable'   => false,
    'sort_order'        => 52,
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'celebrity_ad_number', array(
    'group'             => 'Prices',
    'input'             => 'textarea',
    'type'              => 'text',
    'backend'           => '',
    'input_renderer'    => 'celebrity/catalog_product_helper_form_number',
    'label'             => 'Ad Number',
    'class'             => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,bundle,grouped',
    'is_configurable'   => false,
    'sort_order'        => 53,
));

$installer->endSetup();
	 