<?php
$installer = $this;
$installer->startSetup();
//$sales_order = $installer->getTable('sales/order');

$installer->run("
	ALTER TABLE `{$installer->getTable('sales/order_item')}` ADD COLUMN `celebrity_ad_number` VARCHAR(100);
	ALTER TABLE `{$installer->getTable('sales/quote_item')}` ADD COLUMN `celebrity_ad_number` VARCHAR(100);
	ALTER TABLE `{$installer->getTable('sales/invoice_item')}` ADD COLUMN `celebrity_ad_number` VARCHAR(100);
 ");
$installer->endSetup();