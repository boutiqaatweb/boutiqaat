<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'max', array(
    'group'             => 'Prices',
    'input'             => 'textarea',
    'type'              => 'text',
    'backend'           => '',
    'input_renderer'    => 'celebrity/catalog_product_helper_form_max',
    'label'             => 'MAX',
    'class'             => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,bundle,grouped',
    'is_configurable'   => false,
    'sort_order'        => 55,
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'maxorder', array(
    'group'             => 'Prices',
    'input'             => 'textarea',
    'type'              => 'text',
    'backend'           => '',
    'input_renderer'    => 'celebrity/catalog_product_helper_form_maxorder',
    'label'             => 'Max Order',
    'class'             => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,bundle,grouped',
    'is_configurable'   => false,
    'sort_order'        => 56,
));

$installer->endSetup();
	 