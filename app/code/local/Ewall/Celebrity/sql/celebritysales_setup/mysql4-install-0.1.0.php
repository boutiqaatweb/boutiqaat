<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("quote_item", "category_id", array("type"=>"int"));
$installer->addAttribute("order_item", "category_id", array("type"=>"int"));
$installer->addAttribute("invoice_item", "category_id", array("type"=>"int"));
$installer->addAttribute("quote_item", "commision_percent", array("type"=>"decimal", "default"=>"0.0000"));
$installer->addAttribute("order_item", "commision_percent", array("type"=>"decimal", "default"=>"0.0000"));
$installer->addAttribute("invoice_item", "commision_percent", array("type"=>"decimal", "default"=>"0.0000"));
$installer->addAttribute("quote_item", "commision_amount", array("type"=>"decimal", "default"=>"0.0000"));
$installer->addAttribute("order_item", "commision_amount", array("type"=>"decimal", "default"=>"0.0000"));
$installer->addAttribute("invoice_item", "commision_amount", array("type"=>"decimal", "default"=>"0.0000"));
$installer->addAttribute("quote_item", "base_commision_amount", array("type"=>"decimal", "default"=>"0.0000"));
$installer->addAttribute("order_item", "base_commision_amount", array("type"=>"decimal", "default"=>"0.0000"));
$installer->addAttribute("invoice_item", "base_commision_amount", array("type"=>"decimal", "default"=>"0.0000"));
$installer->addAttribute("invoice_item", "date_invoiced", array("type"=>"timestamp"));
$installer->endSetup();
	 