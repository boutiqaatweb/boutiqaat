<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("quote_item", "special_name", array("type"=>"varchar"));
$installer->addAttribute("order_item", "special_name", array("type"=>"varchar"));
$installer->addAttribute("invoice_item", "special_name", array("type"=>"varchar"));
$installer->addAttribute("quote_item", "celebrity_special_price", array("type"=>"decimal", "default"=>"0.0000"));
$installer->addAttribute("order_item", "celebrity_special_price", array("type"=>"decimal", "default"=>"0.0000"));
$installer->addAttribute("invoice_item", "celebrity_special_price", array("type"=>"decimal", "default"=>"0.0000"));
$installer->addAttribute("quote_item", "celebrity_ad_date", array("type"=>"timestamp"));
$installer->addAttribute("order_item", "celebrity_ad_date", array("type"=>"timestamp"));
$installer->addAttribute("invoice_item", "celebrity_ad_date", array("type"=>"timestamp"));
$installer->endSetup();
	 