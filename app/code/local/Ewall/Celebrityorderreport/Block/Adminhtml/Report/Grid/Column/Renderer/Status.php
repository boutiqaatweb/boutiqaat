<?php
	
	class Ewall_Celebrityorderreport_Block_Adminhtml_Report_Grid_Column_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
	{	  
		
		public function render(Varien_Object $row) {
				if ($row['status'] == 'pending' || $row['status'] == 'processing') {
					return 'received';
				} 
				else{
			  	return $row['status'];
			  }
   		}
	}

?>