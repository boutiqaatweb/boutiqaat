<?php
class Ewall_Celebrityorderreport_Block_Adminhtml_Report_Product_Sold extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Initialize container block settings
     *
     */
    public function __construct()
    {
       
        $this->_controller = 'adminhtml_report_product_sold';
        $this->_blockGroup = 'celebrityorderreport';
        $this->_headerText = Mage::helper('reports')->__('Celebrity Order Report');
        parent::__construct();
        $this->_removeButton('add');
    }
}
