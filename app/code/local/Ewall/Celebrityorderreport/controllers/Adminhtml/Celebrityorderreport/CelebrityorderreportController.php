<?php
class Ewall_Celebrityorderreport_Adminhtml_Celebrityorderreport_CelebrityorderreportController extends Mage_Adminhtml_Controller_Action {

	protected function _initAction() {
		$this->loadLayout()->_setActiveMenu('celebrityorderreport')->_addBreadcrumb(Mage::helper('celebrityorderreport')->__('Celebrity Order Report'), Mage::helper('celebrityorderreport')->__('Celebrity Order Report'));
			return $this;
	}
    
	public function indexAction(){
       
		$this->_title($this->__('Reports'))            
             ->_title($this->__('Celebrity Order Report'));
        
        $requestData = Mage::helper('adminhtml')->prepareFilterString($this->getRequest()->getParam('filter'));
        
        if(isset($requestData['celebrity_id']) && !empty($requestData['celebrity_id'])){
            Mage::getSingleton('admin/session')->setVendorFilter($requestData['celebrity_id']);
        }
        else if(Mage::getSingleton('admin/session')->getVendorFilter()){
            Mage::getSingleton('admin/session')->unsVendorFilter();
        }

		$this->_initAction()
            ->_setActiveMenu('report/celebrityorderreport')
            ->_addBreadcrumb(Mage::helper('reports')->__('Celebrity Order Report'), Mage::helper('reports')->__('Celebrity Order Report'))
            ->_addContent($this->getLayout()->createBlock('celebrityorderreport/adminhtml_report_product_sold'))
            ->renderLayout();            
	}

	/**
     * Export Sold Products report to CSV format action
     *
     */
    public function exportSoldCsvAction()
    {
        $fileName   = 'order.csv';
        $content    = $this->getLayout()
            ->createBlock('celebrityorderreport/adminhtml_report_product_sold_grid')
            ->getCsv();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export Sold Products report to XML format action
     *
     */
    public function exportSoldExcelAction()
    {
        $fileName   = 'order.xml';
        $content    = $this->getLayout()
            ->createBlock('celebrityorderreport/adminhtml_report_product_sold_grid')
            ->getExcel($fileName);

        $this->_prepareDownloadResponse($fileName, $content);
    }
}
?>