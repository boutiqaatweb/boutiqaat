<?php
class Ewall_Celebrityreport_Block_Adminhtml_Report_Product_Sold_Grid extends Mage_Adminhtml_Block_Report_Grid
{

    /**
     * Sub report size
     *
     * @var int
     */
    protected $_subReportSize = 0;

    /**
     * Initialize Grid settings
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('gridCelebrityProductsSold');          
        $this->setTemplate('celebrityreport/report/grid.phtml');
    }

    /**
     * Prepare collection object for grid
     *
     * @return Mage_Adminhtml_Block_Report_Product_Sold_Grid
     */
    protected function _prepareCollection()
    {
        parent::_prepareCollection();        
        $this->getCollection()
             ->initReport('celebrityreport/reports_product_sold_collection');
          
        return $this;    
    }

    /**
     * Prepare Grid columns
     *
     * @return Mage_Adminhtml_Block_Report_Product_Sold_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('name', array(
            'header'    =>Mage::helper('reports')->__('Product Name'),
            'index'     =>'order_items_name'
        ));


        $this->addColumn('ordered_qty', array(
            'header'    =>Mage::helper('reports')->__('Sold Quantity'),
            'width'     =>'120px',
            'align'     =>'right',
            'index'     =>'ordered_qty',
            'total'     =>'sum',
            'type'      =>'number'
        ));

        $this->addColumn('commision_percent', array(
            'header'    =>Mage::helper('reports')->__('Commision'),
            'index'     =>'commision_percent',
            'type'      =>'number'
        ));

        $this->addColumn('base_price_total', array(
            'header'    =>Mage::helper('reports')->__('Sale Value'),
            'width'     =>'120px',
            'index'     =>'base_price_total',
            'align'     =>'right',
            'total'     =>'sum',
            'type'      =>'number'
         
        ));

        $this->addColumn('commision_value', array(
            'header'    =>Mage::helper('reports')->__('Commision Value'),
            'width'     =>'120px',
            'index'     =>'commision_value',
            'align'     =>'right',
            'total'     =>'sum',
            'type'      =>'number'
        ));

        $this->addExportType('*/*/exportSoldCsv', Mage::helper('reports')->__('CSV'));
        $this->addExportType('*/*/exportSoldExcel', Mage::helper('reports')->__('Excel XML'));
        
        return parent::_prepareColumns();
    }

    public function getCelebrity() {

        $collection = Mage::getModel('customer/customer')
                        ->getCollection()
                        ->addAttributeToSelect('*')
                        ->addAttributeToFilter('is_celebrity', 1);
        return $collection;
    }
}
