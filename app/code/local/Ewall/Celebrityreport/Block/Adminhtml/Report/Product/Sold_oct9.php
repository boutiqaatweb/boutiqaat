<?php
class Ewall_Celebrityreport_Block_Adminhtml_Report_Product_Sold extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Initialize container block settings
     *
     */
    public function __construct()
    {
       
        $this->_controller = 'adminhtml_report_product_sold';
        $this->_blockGroup = 'celebrityreport';
        $this->_headerText = Mage::helper('reports')->__('Celebrity Products Ordered');
        parent::__construct();
        $this->_removeButton('add');
    }
}
