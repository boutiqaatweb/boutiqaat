<?php
class Ewall_Celebrityreport_Model_Resource_Reports_Product_Sold_Collection extends Mage_Reports_Model_Resource_Product_Sold_Collection    
{

    /**
     * Set Date range to collection
     *
     * @param int $from
     * @param int $to
     * @return Mage_Reports_Model_Resource_Product_Sold_Collection
     */
    public function setDateRange($from, $to)
    {    
       
         $this->_reset()
            ->addAttributeToSelect('*')
            ->addOrderedQty($from, $to)
            ->setOrder('ordered_qty', self::SORT_ORDER_DESC);           
        return $this;
    }

   
     public function addOrderedQty($from = '', $to = '')
    {
        $adapter              = $this->getConnection();  
        $dateJoinCondition = array('ordertbl.entity_id = order_items.order_id');
        if ($from != '' && $to != '') {
            $fieldName            = 'order_items.created_at';
            $dateJoinCondition[]     = $this->_prepareBetweenSql($fieldName, $from, $to);
        }
               
        $this->getSelect()->reset()
            ->from(
                array('order_items' => $this->getTable('sales/order_item')),                
                array(
                    'order_sku' => 'order_items.sku',
                    'ordered_qty' => 'order_items.qty_ordered',
                    'order_items_name' => 'order_items.name',
                    'base_price_total' => 'order_items.price',
                    'category_id' => 'order_items.category_id',
                    'commision_percent' => 'order_items.commision_percent',
                    'commision_value' => 'order_items.commision_amount',
                ));
                     
        $this->getSelect()->joinLeft(
                array('ordertbl' => $this->getTable('sales/order')),
                implode(' AND ', $dateJoinCondition),
                //'ordertbl.entity_id = order_items.order_id',
                array('status' => 'ordertbl.status'));

       if($vendor_id = Mage::getSingleton('admin/session')->getVendorFilter()) {
            $this->getSelect()->where('order_items.category_id = '.$vendor_id); //$dateJoinCondition
        } else{
            //$this->getSelect()->where('order_items.parent_item_id IS NULL');
        }
        if($status_id = Mage::getSingleton('admin/session')->getStatusFilter()) {
            if ($status_id == 'received') {                
               $data = array('pending', 'processing', 'received');
               $this->getSelect()->where('status IN(?)', $data);
            }
            else{
                $this->getSelect()->where('status = ' ."'$status_id'");
            }
        }        
        
        return $this;
    } 
 
}