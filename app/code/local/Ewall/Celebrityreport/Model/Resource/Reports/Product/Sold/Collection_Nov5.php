<?php
class Ewall_Celebrityreport_Model_Resource_Reports_Product_Sold_Collection extends Mage_Reports_Model_Resource_Product_Sold_Collection    
{

    /**
     * Set Date range to collection
     *
     * @param int $from
     * @param int $to
     * @return Mage_Reports_Model_Resource_Product_Sold_Collection
     */
    public function setDateRange($from, $to)
    {    
       
         $this->_reset()
            ->addAttributeToSelect('*')
            ->addOrderedQty($from, $to)
            ->setOrder('ordered_qty', self::SORT_ORDER_DESC);           
        return $this;
    }


  
    public function addOrderedQty($from = '', $to = '')
    {
        $adapter              = $this->getConnection();
        
        $invoiceJoinCondition = array('invoice_items.order_item_id = order_items.item_id');
        if ($from != '' && $to != '') {
            $fieldName            = 'invoice_items.date_invoiced';
            $invoiceJoinCondition[] = $this->_prepareBetweenSql($fieldName, $from, $to);
        }

        $this->getSelect()->reset()
            ->from(
                array('order_items' => $this->getTable('sales/order_item')),
                array(
                    'ordered_qty' => 'SUM(order_items.qty_invoiced)',
                    'order_items_name' => 'order_items.name',
                    'base_price_total' => 'order_items.price',
                    'category_id' => 'order_items.category_id',
                    'commision_percent' => 'order_items.commision_percent',
                    'commision_value' => 'SUM(order_items.commision_amount)',
                ))
            
            ->joinInner(
                array('invoice_items' => $this->getTable('sales/invoice_item')),
                implode(' AND ', $invoiceJoinCondition),
                array());
            
        if($vendor_id = Mage::getSingleton('admin/session')->getVendorFilter()){
            $this->getSelect()->where('order_items.parent_item_id IS NULL AND order_items.category_id = '.$vendor_id);
        } else{
            $this->getSelect()->where('order_items.parent_item_id IS NULL');
        }

        $this->getSelect()->group('order_items.product_id')
            ->having('SUM(order_items.qty_invoiced) > ?', 0);

        $this->getSelect()->joinLeft(
                array('invo_id' => $this->getTable('sales/invoice')),
                ('invo_id.entity_id = invoice_items.parent_id'),
                array('order_id' => 'invo_id.order_id')            
            )

            ->joinLeft(
                array('ord_id' => $this->getTable('sales/order')),
                ('invo_id.order_id = ord_id.entity_id'),
                array('status' => 'ord_id.status')               
            );
        $this->getSelect()->where('ord_id.status = ?', 'paid');
       
        return $this;
    }
}
