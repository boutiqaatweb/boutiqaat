<?php
class Ewall_Celebrityreport_Adminhtml_Celebrityreport_CelebrityreportController extends Mage_Adminhtml_Controller_Action {

	protected function _initAction() {
        $this->loadLayout()->_setActiveMenu('celebrityreport')->_addBreadcrumb(Mage::helper('celebrityreport')->__('Celebrity Report'), Mage::helper('celebrityreport')->__('Celebrity Report'));
            return $this;
    }
    
    public function indexAction(){
       
        $this->_title($this->__('Reports'))           
             ->_title($this->__('Celebrity Report'));
        
        $requestData = Mage::helper('adminhtml')->prepareFilterString($this->getRequest()->getParam('filter'));
        
        if(isset($requestData['celebrity_id']) && !empty($requestData['celebrity_id'])){
            Mage::getSingleton('admin/session')->setVendorFilter($requestData['celebrity_id']);
        }
        else if(Mage::getSingleton('admin/session')->getVendorFilter()){
            Mage::getSingleton('admin/session')->unsVendorFilter();
        }
      else
        {

        }

        if(isset($requestData['status']) && !empty($requestData['status'])){
            Mage::getSingleton('admin/session')->setStatusFilter($requestData['status']);
        }
        else if(Mage::getSingleton('admin/session')->getStatusFilter()){
            Mage::getSingleton('admin/session')->unsStatusFilter();
        }

        $this->_initAction()
            ->_setActiveMenu('report/celebrityreport')
            ->_addBreadcrumb(Mage::helper('reports')->__('Celebrity Report'), Mage::helper('reports')->__('Celebrity Report'))
            ->_addContent($this->getLayout()->createBlock('celebrityreport/adminhtml_report_product_sold'))
            ->renderLayout();
    }

    /**
     * Export Sold Products report to CSV format action
     *
     */
    public function exportSoldCsvAction()
    {
        $fileName   = 'products_ordered.csv';
        $content    = $this->getLayout()
            ->createBlock('celebrityreport/adminhtml_report_product_sold_grid')
            ->getCsv();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export Sold Products report to XML format action
     *
     */
    public function exportSoldExcelAction()
    {
        $fileName   = 'products_ordered.xml';
        $content    = $this->getLayout()
            ->createBlock('celebrityreport/adminhtml_report_product_sold_grid')
            ->getExcel($fileName);
        $this->_prepareDownloadResponse($fileName, $content);
    }
}
?>