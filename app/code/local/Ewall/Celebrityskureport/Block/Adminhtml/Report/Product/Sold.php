<?php
class Ewall_Celebrityskureport_Block_Adminhtml_Report_Product_Sold extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Initialize container block settings
     *
     */
    public function __construct()
    {
       
        $this->_controller = 'adminhtml_report_product_sold';
        $this->_blockGroup = 'celebrityskureport';
        $this->_headerText = Mage::helper('reports')->__('Celebrity SKU Report');
        parent::__construct();
        $this->_removeButton('add');
    }
}
