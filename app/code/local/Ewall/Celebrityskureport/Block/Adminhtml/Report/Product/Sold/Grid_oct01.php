<?php
class Ewall_Celebrityskureport_Block_Adminhtml_Report_Product_Sold_Grid extends Mage_Adminhtml_Block_Widget_Grid

{
    protected $_storeSwitcherVisibility = true;

    protected $_dateFilterVisibility = true;

    protected $_exportVisibility = false;

    protected $_subtotalVisibility = false;

    protected $_filters = array();

    protected $_defaultFilters = array(
            'report_from' => '',
            'report_to' => ''           
        );

    /**
     * Sub report size
     *
     * @var int
     */
    protected $_subReportSize = 0;

    /**
     * Initialize Grid settings
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('gridCelebritySkuSold');          
        $this->setTemplate('celebrityskureport/report/grid.phtml');
    }

    /**
     * Prepare collection object for grid
     *
     * @return Mage_Adminhtml_Block_Report_Product_Sold_Grid
     */
    protected function _prepareCollection()
    {
        $filter = $this->getParam($this->getVarNameFilter(), null);
        if (is_null($filter)) {
            $filter = $this->_defaultFilter;
        }
        if (is_string($filter)) {
            $data = array();
            $filter = base64_decode($filter);
            parse_str(urldecode($filter), $data);


            if (isset($data['report_from'])) {
                $from = date('Y-m-d', Mage::getModel('core/date')->timestamp(strtotime($data['report_from'])))." "."00:00:00"; //$this->_convertDate($data['report_from'], Mage::app()->getLocale()->getLocaleCode())->getTimestamp(); //date('Y-m-d',strtotime($data['report_from'])." "."00:00:00");    
            }

            if (isset($data['report_to'])) {
                $to = date('Y-m-d', Mage::getModel('core/date')->timestamp(strtotime($data['report_from'])))." "."23:59:59"; 
            }
        }

        $collection = Mage::getModel('sales/order_invoice_item')->getCollection()->addAttributeToSelect('*');
        $collection->addAttributeToFilter('category_id', $data['celebrity_id']); //$data['celebrity_id']


        $collection->addAttributeToFilter('date_invoiced', array(
                        'from' => $from,
                        'to' => $to,
                        'datetime' => true,
                        ));
        $collection->getSelect()->columns('SUM(qty) as ordered_qty')
                                ->columns('SUM(commision_amount) as commision_value')
                                ->group('sku');
           
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;    
    }

    /**
     * Prepare Grid columns
     *
     * @return Mage_Adminhtml_Block_Report_Product_Sold_Grid
     */
    protected function _prepareColumns()
    {
       

        $this->addColumn('sku', array(
            'header'    =>Mage::helper('reports')->__('SKU'),
            'index'     =>'sku'

           
        ));


        $this->addColumn('name', array(
            'header'    =>Mage::helper('reports')->__('Product Name'),
            'index'     =>'name',
            'sortable'  => false
        ));


        $this->addColumn('ordered_qty', array(
            'header'    =>Mage::helper('reports')->__('Sold Quantity'),
            'width'     =>'120px',
            'align'     =>'right',
            'index'     =>'ordered_qty',
            'total'     =>'sum',
            'type'      =>'number',
            'sortable'  => false
        ));

        $this->addColumn('commision_percent', array(
            'header'    =>Mage::helper('reports')->__('Commision'),
            'index'     =>'commision_percent',
            'type'      =>'number',
            'sortable'  => false
        ));

        $this->addColumn('price', array(
            'header'    =>Mage::helper('reports')->__('Sale Value'),
            'width'     =>'120px',
            'index'     =>'price',
            'align'     =>'right',
            'total'     =>'sum',
            'type'      =>'number',
            'sortable'  => false
         
        ));

        $this->addColumn('commision_value', array(
            'header'    =>Mage::helper('reports')->__('Commision Value'),
            'width'     =>'120px',
            'index'     =>'commision_value',
            'align'     =>'right',
            'total'     =>'sum',
            'type'      =>'number',
            'sortable'  => false
        ));

        $this->addExportType('*/*/exportSoldCsv', Mage::helper('reports')->__('CSV'));
        $this->addExportType('*/*/exportSoldExcel', Mage::helper('reports')->__('Excel XML'));
        
        return parent::_prepareColumns();
    }

    public function getCelebrity() {

        $collection = Mage::getModel('customer/customer')
                        ->getCollection()
                        ->addAttributeToSelect('*')
                        ->addAttributeToFilter('is_celebrity', 1);
        return $collection;
    }



    public function getStoreSwitcherVisibility()
    {
        return $this->_storeSwitcherVisibility;
    }

    /**
     * Return store switcher html
     *
     * @return string
     */
    public function getStoreSwitcherHtml()
    {
        return $this->getChildHtml('store_switcher');
    }

    /**
     * Set visibility of date filter
     *
     * @param boolean $visible
     */
    public function setDateFilterVisibility($visible=true)
    {
        $this->_dateFilterVisibility = $visible;
    }

    /**
     * Return visibility of date filter
     *
     * @return boolean
     */
    public function getDateFilterVisibility()
    {
        return $this->_dateFilterVisibility;
    }

    /**
     * Set visibility of export action
     *
     * @param boolean $visible
     */
    public function setExportVisibility($visible=true)
    {
        $this->_exportVisibility = $visible;
    }

    /**
     * Return visibility of export action
     *
     * @return boolean
     */
    public function getExportVisibility()
    {
        return $this->_exportVisibility;
    }


    public function getRefreshButtonHtml()
    {
        return $this->getChildHtml('refresh_button');
    }

    public function setFilter($name, $value)
    {
        if ($name) {
            $this->_filters[$name] = $value;
        }
    }

    public function getFilter($name)
    {
        if (isset($this->_filters[$name])) {
            return $this->_filters[$name];
        } else {
            return ($this->getRequest()->getParam($name))
                    ?htmlspecialchars($this->getRequest()->getParam($name)):'';
        }
    }

    /**
     * Retrieve locale
     *
     * @return Mage_Core_Model_Locale
     */
    public function getLocale()
    {
        if (!$this->_locale) {
            $this->_locale = Mage::app()->getLocale();
        }
        return $this->_locale;
    }

     public function getDateFormat()
    {
        return $this->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
    }

    /**
     * onlick event for refresh button to show alert if fields are empty
     *
     * @return string
     */
    public function getRefreshButtonCallback()
    {
        return "{$this->getJsObjectName()}.doFilter();";
    }

    protected function _setFilterValues($data)
    {
        foreach ($data as $name => $value) {
            //if (isset($data[$name])) {
                $this->setFilter($name, $data[$name]);
            //}
        }
        return $this;
    }

    /**
     * Convert given date to default (UTC) timezone
     *
     * @param string $date
     * @param string $locale
     * @return Zend_Date
     */
    protected function _convertDate($date, $locale)
    {
        try {
            $dateObj = $this->getLocale()->date(null, null, $locale, false);

            //set default timezone for store (admin)
            $dateObj->setTimezone(
                Mage::app()->getStore()->getConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_TIMEZONE)
            );

            //set begining of day
            $dateObj->setHour(00);
            $dateObj->setMinute(00);
            $dateObj->setSecond(00);

            //set date with applying timezone of store
            $dateObj->set($date, Zend_Date::DATE_SHORT, $locale);

            //convert store date to default date in UTC timezone without DST
            $dateObj->setTimezone(Mage_Core_Model_Locale::DEFAULT_TIMEZONE);

            return $dateObj;
        }
        catch (Exception $e) {
            return null;
        }
    }
}
