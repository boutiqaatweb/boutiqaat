<?php
class Ewall_Celebrityskureport_Adminhtml_Celebrityskureport_CelebrityskureportController extends Mage_Adminhtml_Controller_Action {

	protected function _initAction() {
		$this->loadLayout()->_setActiveMenu('celebrityskureport')->_addBreadcrumb(Mage::helper('celebrityskureport')->__('Celebrity SKU Report'), Mage::helper('celebrityskureport')->__('Celebrity SKU Report'));
			return $this;
	}
    
	public function indexAction(){
       
		$this->_title($this->__('Reports'))            
             ->_title($this->__('Celebrity SKU Report'));
        
        $requestData = Mage::helper('adminhtml')->prepareFilterString($this->getRequest()->getParam('filter'));
        
        if(isset($requestData['celebrity_id']) && !empty($requestData['celebrity_id'])){
            Mage::getSingleton('admin/session')->setVendorFilter($requestData['celebrity_id']);
        }
        else if(Mage::getSingleton('admin/session')->getVendorFilter()){
            Mage::getSingleton('admin/session')->unsVendorFilter();
        }

		$this->_initAction()
            ->_setActiveMenu('report/celebrityskureport')
            ->_addBreadcrumb(Mage::helper('reports')->__('Celebrity SKU Report'), Mage::helper('reports')->__('Celebrity SKU Report'))
            ->_addContent($this->getLayout()->createBlock('celebrityskureport/adminhtml_report_product_sold'))
            ->renderLayout();            
	}

	/**
     * Export Sold Products report to CSV format action
     *
     */
    public function exportSoldCsvAction()
    {
        $fileName   = 'sku_ordered.csv';
        $content    = $this->getLayout()
            ->createBlock('celebrityskureport/adminhtml_report_product_sold_grid')
            ->getCsv();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export Sold Products report to XML format action
     *
     */
    public function exportSoldExcelAction()
    {
        $fileName   = 'sku_ordered.xml';
        $content    = $this->getLayout()
            ->createBlock('celebrityskureport/adminhtml_report_product_sold_grid')
            ->getExcel($fileName);

        $this->_prepareDownloadResponse($fileName, $content);
    }
}
?>