<?php
class Ewall_Celebritysorting_Model_Observer
{
	public function SaveCelebritySortingData(Varien_Event_Observer $observer)
	{
		$product = $observer->getProduct();
		$ad_date_data = unserialize($product->getCelebrityAdDate());
		$ad_number_data = is_array(unserialize($product->getCelebrityAdNumber())) ? unserialize($product->getCelebrityAdNumber()) : array();

		if(is_array($ad_date_data) && count($ad_date_data)){			
			foreach ($ad_date_data as $celebrity_id => $date) {
				$model = Mage::getModel('celebritysorting/sorting');
				$sorting = $model->getCollection()->addFieldToFilter('product_id', $product->getId())->addFieldToFilter('celebrity_id', $celebrity_id);
				if($sorting->count()){
					$model = $sorting->getFirstItem();					
				}
				else{
					$model->setProductId($product->getId());
					$model->setCelebrityId($celebrity_id); 	
				}
				$number	= isset($ad_number_data[$celebrity_id]) ? $ad_number_data[$celebrity_id] : '';
				$date = Mage::getModel('core/date')->timestamp(strtotime($date));
				$model->setAdDate($date)->setAdNumber($number);
				
				$model->save();
			}
		}		
	}
	
}
