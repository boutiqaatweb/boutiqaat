<?php
$installer = $this;
$installer->startSetup();
$installer->run("-- DROP TABLE IF EXISTS {$this->getTable('celebrity_sorting')};
	CREATE TABLE {$this->getTable('celebrity_sorting')} (
	  `id` int(11) unsigned NOT NULL auto_increment,
	  `product_id` int(11) NOT NULL default '0',
	  `celebrity_id` int(11) NOT NULL default '0',
	  `ad_date` datetime NULL,
	  `ad_number` int(11) NOT NULL default '0',
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();
	 