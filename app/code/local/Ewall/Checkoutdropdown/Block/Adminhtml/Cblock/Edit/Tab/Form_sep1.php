<?php
class Ewall_Checkoutdropdown_Block_Adminhtml_Cblock_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("cblock_form", array("legend"=>Mage::helper("checkoutdropdown")->__("Item information")));
						
						$fieldset->addField('city_code', 'select', array(
                        'label'     => Mage::helper('checkoutdropdown')->__('Area'),
                        'values'   => Mage::helper('checkoutdropdown/region')->getArea(),
                        'name' => 'city_code',
                        "required"=>true,
                        ));

						// $fieldset->addField('store_id', 'multiselect', array(
      //                   'label'     => Mage::helper('checkoutdropdown')->__('Store View'),
      //                   'values'   => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
      //                   'name' => 'store_id',
      //                   "required"=>true,
      //                   ));

						$fieldset->addField("block_code", "text", array(
						"label" => Mage::helper("checkoutdropdown")->__("Block Code"),
						"required"=> true,
						"name" => "block_code",
						));
					
						$fieldset->addField("default_name", "text", array(
						"label" => Mage::helper("checkoutdropdown")->__("Block Name"),
						"required"=> true,
						"name" => "default_name",
						));
				if (Mage::getSingleton("adminhtml/session")->getCblockData())
                {
                    $form->setValues(Mage::getSingleton("adminhtml/session")->getCblockData());
                    Mage::getSingleton("adminhtml/session")->setCblockData(null);
                } 
                elseif(Mage::registry("cblock_data")) {
                    $form->setValues(Mage::registry("cblock_data")->getData());
                }
				return parent::_prepareForm();
		}
}
	
