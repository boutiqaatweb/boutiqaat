<?php
class Ewall_Checkoutdropdown_Block_Adminhtml_Cblock_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("cblock_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("checkoutdropdown")->__("Block Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("checkoutdropdown")->__("Block Information"),
				"title" => Mage::helper("checkoutdropdown")->__("Block Information"),
				"content" => $this->getLayout()->createBlock("checkoutdropdown/adminhtml_cblock_edit_tab_form")->toHtml(),
				
				));
				
				 $param = Mage::app()->getRequest()->get('activeTab');
						if (array_key_exists($param, $this->_tabs)) {
						$this->_tabs[$param]->setActive();
				}
				return parent::_beforeToHtml();
		}

}
