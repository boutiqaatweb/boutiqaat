<?php
class Ewall_Checkoutdropdown_Block_Adminhtml_Cblock_Renderer_cityname extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    protected $_values;

    public function render(Varien_Object $row) {
		$id = $row->getId();
		$collection = Mage::getModel('checkoutdropdown/blockdropdown')->load($id);
		$city = $collection->getCityCode();
		$reg_coll = Mage::getModel('checkoutdropdown/checkoutdropdown')->getCollection()->addFieldToFilter('city_code',$city)->getFirstItem();
		return $reg_coll->getDefaultName();
    } 
}
