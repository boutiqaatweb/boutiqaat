<?php
class Ewall_Checkoutdropdown_Block_Adminhtml_City_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("city_form", array("legend"=>Mage::helper("checkoutdropdown")->__("Item information")));
						
						$fieldset->addField('region_id', 'select', array(
                        'label'     => Mage::helper('checkoutdropdown')->__('Governorate'),
                        'values'   => Mage::helper('checkoutdropdown/region')->getRegion(),
                        'name' => 'region_id',
                        "required"=>true,
                        ));

						// $fieldset->addField('store_id', 'multiselect', array(
      //                   'label'     => Mage::helper('checkoutdropdown')->__('Store View'),
      //                   'values'   => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
      //                   'name' => 'store_id',
      //                   "required"=>true,
      //                   ));

						$fieldset->addField("city_code", "text", array(
						"label" => Mage::helper("checkoutdropdown")->__("Area Code"),
						"required"=>true,
						"name" => "city_code",
						));
					
						$fieldset->addField("default_name", "text", array(
						"label" => Mage::helper("checkoutdropdown")->__("Area Name"),
						"required"=>true,
						"name" => "default_name",
						));
				if (Mage::getSingleton("adminhtml/session")->getCityData())
                {
                    $form->setValues(Mage::getSingleton("adminhtml/session")->getCityData());
                    Mage::getSingleton("adminhtml/session")->setCityData(null);
                } 
                elseif(Mage::registry("city_data")) {
                    $form->setValues(Mage::registry("city_data")->getData());
                }
				return parent::_prepareForm();
		}
}
	
