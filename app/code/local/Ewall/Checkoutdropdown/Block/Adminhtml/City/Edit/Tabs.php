<?php
class Ewall_Checkoutdropdown_Block_Adminhtml_City_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("city_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("checkoutdropdown")->__("Area Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("checkoutdropdown")->__("Area Information"),
				"title" => Mage::helper("checkoutdropdown")->__("Area Information"),
				"content" => $this->getLayout()->createBlock("checkoutdropdown/adminhtml_city_edit_tab_form")->toHtml(),
				
				));
				
				 $param = Mage::app()->getRequest()->get('activeTab');
						if (array_key_exists($param, $this->_tabs)) {
						$this->_tabs[$param]->setActive();
				}
				return parent::_beforeToHtml();
		}

}
