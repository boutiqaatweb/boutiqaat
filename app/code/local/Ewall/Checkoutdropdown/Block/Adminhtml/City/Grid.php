<?php

class Ewall_Checkoutdropdown_Block_Adminhtml_City_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("cityGrid");
				$this->setDefaultSort("city_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
				$this->setUseAjax(true);
		}
		protected function _prepareCollection()
		{
				$collection = Mage::getModel("checkoutdropdown/checkoutdropdown")->getCollection();
				foreach($collection as $link){
			        if($link->getStoreId() && $link->getStoreId() != 0 ){
			            $link->setStoreId(explode(',',$link->getStoreId()));
			        }
			        else{
			            $link->setStoreId(array('0'));
			        }
			    }
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("city_id", array(
				"header" => Mage::helper("checkoutdropdown")->__("City ID"),
				"align" =>"right",
				"width" => "50px",
				"index" => "city_id",
				"type" => "number",
				));
                $this->addColumn('store_id',array(
			        "header"   => "Store",
			        "index"    => "store_id",
			        "type"     => "store",
			        'store_view'    => true,
			    ));
			 
			 	$this->addColumn("city_code", array(
				"header" => Mage::helper("checkoutdropdown")->__("City Code"),
				"index" => "city_code",
				));
				$this->addColumn("default_name", array(
				"header" => Mage::helper("checkoutdropdown")->__("City"),
				"index" => "default_name",
				));
				$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
				$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}

		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			return $this;
		}
		
}
