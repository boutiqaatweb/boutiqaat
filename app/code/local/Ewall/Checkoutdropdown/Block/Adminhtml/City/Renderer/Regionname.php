<?php
class Ewall_Checkoutdropdown_Block_Adminhtml_City_Renderer_Regionname extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    protected $_values;

    public function render(Varien_Object $row) {
		$id = $row->getId();
		$collection = Mage::getModel('checkoutdropdown/checkoutdropdown')->load($id);
		$region = $collection->getRegionId();
		$reg_coll = Mage::getModel('directory/region')->getCollection()->addFieldToFilter('code',$region)->getFirstItem();
		return $reg_coll->getDefaultName();
    } 
}
