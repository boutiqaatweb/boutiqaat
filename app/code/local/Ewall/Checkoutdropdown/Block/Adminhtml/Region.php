<?php


class Ewall_Checkoutdropdown_Block_Adminhtml_Region extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_region";
	$this->_blockGroup = "checkoutdropdown";
	$this->_headerText = Mage::helper("checkoutdropdown")->__("Region Manager");
	$this->_addButtonLabel = Mage::helper("checkoutdropdown")->__("Add New Item");
	parent::__construct();
	}

}
