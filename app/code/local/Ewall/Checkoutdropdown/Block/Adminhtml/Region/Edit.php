<?php
	
class Ewall_Checkoutdropdown_Block_Adminhtml_Region_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "checkoutdropdown";
				$this->_controller = "adminhtml_region";
				$this->_updateButton("save", "label", Mage::helper("checkoutdropdown")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("checkoutdropdown")->__("Delete Item"));
				
				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("checkoutdropdown")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);

				//$this->_removeButton('save');
				//$this->_removeButton('delete');
				//$this->_removeButton('reset');
				//$this->_removeButton('saveandcontinue');

				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}
		public function getHeaderText()
		{				

			if( Mage::registry("region_data") && Mage::registry("region_data")->getId() ){
				return Mage::helper("checkoutdropdown")->__("Edit Governorate %s", $this->htmlEscape(Mage::registry("region_data")->getDefaultName()));
			} 
			else{
			    return Mage::helper("checkoutdropdown")->__("Add Kuwait Governorate");
			}

		}
}
