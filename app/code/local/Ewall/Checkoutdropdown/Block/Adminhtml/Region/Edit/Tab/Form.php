<?php
class Ewall_Checkoutdropdown_Block_Adminhtml_Region_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("region_form", array("legend"=>Mage::helper("checkoutdropdown")->__("Item information")));
					
						$fieldset->addField("code", "text", array(
						"label" => Mage::helper("checkoutdropdown")->__("Code"),
						"required"=>true,
						"name" => "code",
						));
					
						$fieldset->addField("default_name", "text", array(
						"label" => Mage::helper("checkoutdropdown")->__("Governorate"),
						"required"=>true,
						"name" => "default_name",
						));
				if (Mage::getSingleton("adminhtml/session")->getRegionData())
                {
                    $form->setValues(Mage::getSingleton("adminhtml/session")->getRegionData());
                    Mage::getSingleton("adminhtml/session")->setRegionData(null);
                } 
                elseif(Mage::registry("region_data")) {
                    $form->setValues(Mage::registry("region_data")->getData());
                }
				return parent::_prepareForm();
		}
}
	
