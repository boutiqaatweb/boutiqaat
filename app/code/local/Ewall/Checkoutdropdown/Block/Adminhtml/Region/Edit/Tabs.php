<?php
class Ewall_Checkoutdropdown_Block_Adminhtml_Region_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("region_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("checkoutdropdown")->__("Governorate Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("checkoutdropdown")->__("Governorate Information"),
				"title" => Mage::helper("checkoutdropdown")->__("Governorate Information"),
				"content" => $this->getLayout()->createBlock("checkoutdropdown/adminhtml_region_edit_tab_form")->toHtml(),
				
				));
				
				 $param = Mage::app()->getRequest()->get('activeTab');
						if (array_key_exists($param, $this->_tabs)) {
						$this->_tabs[$param]->setActive();
				}
				return parent::_beforeToHtml();
		}

}
