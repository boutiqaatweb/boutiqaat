<?php

class Ewall_Checkoutdropdown_Block_Adminhtml_Region_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("regionGrid");
				$this->setDefaultSort("region_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
				$this->setUseAjax(true);
		}
		protected function _prepareCollection()
		{
				$collection = Mage::getModel("directory/region")->getCollection()
				->addFieldToFilter('country_id','KW');
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("region_id", array(
				"header" => Mage::helper("checkoutdropdown")->__("Region ID"),
				"align" =>"right",
				"width" => "50px",
				"index" => "region_id",
				"filter" => false,
				));
                
				$this->addColumn("code", array(
				"header" => Mage::helper("checkoutdropdown")->__("Region Code"),
				"index" => "code",
				));
				$this->addColumn("default_name", array(
				"header" => Mage::helper("checkoutdropdown")->__("Governorate"),
				"index" => "default_name",
				));
				//$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
				//$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			return $this;
		}
			

}
