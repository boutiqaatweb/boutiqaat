<?php
class Ewall_Checkoutdropdown_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getUaeCities($selectedstate)
    {
        $helper = Mage::helper('directory');
        $region = Mage::getModel('directory/region')->load($selectedstate);        
        $cities = Mage::getModel('checkoutdropdown/checkoutdropdown')->getCollection()
        		->addFieldToFilter('country_id','KW')
        		->addFieldToFilter('region_id',$region->getCode());
        // $cities = array(
        //     $helper->__('Abu Dhabi'),
        //     $helper->__('Ajman'),
        //     $helper->__('Al Ain'),
        //     $helper->__('Dubai'),
        //     $helper->__('Fujairah'),
        //     $helper->__('Ras al Khaimah'),
        //     $helper->__('Sharjah'),
        //     $helper->__('Umm al Quwain'),
        // );
        
        return $cities->getData();
    }
 
    public function getUaeCitiesAsDropdown($selectedstate = '')
    {
        $cities = $this->getUaeCities($selectedstate);
        $options = '';
        foreach($cities as $city){
            $isSelected = $selectedCity == $city ? ' selected="selected"' : null;
            $options .= '<option value="' . $city . '"' . $isSelected . '>' . $city['default_name'] . '</option>';
        }
        return $options;
    }
}

	 