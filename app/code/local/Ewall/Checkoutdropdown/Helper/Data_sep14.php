<?php
class Ewall_Checkoutdropdown_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getUaeCities($selectedstate)
    {
        $helper = Mage::helper('directory');
        $city = array();
        //$region = Mage::getModel('directory/region')->load($selectedstate);  
        $storeId = Mage::app()->getStore()->getStoreId();    
        $cities = Mage::getModel('checkoutdropdown/checkoutdropdown')->getCollection()
        		->addFieldToFilter('store_id',array('in'=>array(0,$storeId)));
        		//->addFieldToFilter('region_id',$region->getCode());
        $city['cities'] = $cities->getData();
        ///$city['region'] = $region->getCode();
        return $city;
    }
    public function getUaeBlocks($selectedcity)
    {
        $helper = Mage::helper('directory');        
        $storeId = Mage::app()->getStore()->getStoreId(); 
        $blocks = Mage::getModel('checkoutdropdown/blockdropdown')->getCollection()
                ->addFieldToFilter('city_code',$selectedcity)->addFieldToFilter('store_id',array('in'=>array(0,$storeId)));
        
        return $blocks->getData();
    }
    public function getUaeCitiesAsDropdown($selectedstate = '',$address_id)
    {
        $cities = $this->getUaeCities($selectedstate);
        $stateCode = $cities['region'];
        $options = '';
        $load = Mage::getModel('customer/address')->load($address_id);
        $city_value = $load->getCity();
        foreach($cities['cities'] as $city){
            $isSelected = ($city_value == $city['default_name']) ? ' selected="selected"' : null;
            //$isSelected = $stateCode == $city['region_id'] ? ' selected="selected"' : null;
            //$options .= '<option dataval="' . $city['city_code'] . '" value="' . $city['default_name'] . '"' . $isSelected . '>' . $city['default_name'] . '</option>';
            $options .= '<option dataval="' . $city['city_code'] . '" value="' . $city['default_name'] . '"'.$isSelected.'>' . $city['default_name'] . '</option>';
        }
        return $options;
    }
    //for getting block values based on cities
    public function getUaeBlocksAsDropdown($selectedcity = '',$address_id)
    {
        $blocks = $this->getUaeBlocks($selectedcity);
        $options = '';
        $load = Mage::getModel('customer/address')->load($address_id);
        $block_value = $load->getAddrBlock();
        foreach($blocks as $block){
            $isSelected = ($block_value == $block['default_name']) ? ' selected="selected"' : null;
            //$isSelected = $selectedcity == $block['city_code'] ? ' selected="selected"' : null;
            //$options .= '<option data-val="' . $block['block_code'] . '" value="' . $block['default_name'] . '"' . $isSelected . '>' . $block['default_name'] . '</option>';
            $options .= '<option data-val="' . $block['block_code'] . '" value="' . $block['default_name'] . '"'.$isSelected.'>' . $block['default_name'] . '</option>';
        }
        return $options;
    }
    public function getUaeCitiesAsDropdownAdmin($selectedstate = '',$address_id)
    {
        $cities = $this->getUaeCities($selectedstate);
        $stateCode = $cities['region'];
        $options = '';
        $load = Mage::getModel('customer/address')->load($address_id);
        $city_value = $load->getCity();
        foreach($cities['cities'] as $city){
             $isSelected = ($city_value == $city['default_name']) ? ' selected="selected"' : null;
            //$options .= '<option dataval="' . $city['city_code'] . '" value="' . $city['default_name'] . '"' . $isSelected . '>' . $city['default_name'] . '</option>';
            
            $options .= '<option dataval="' . $city['city_code'] . '" value="' . $city['default_name'] . '"'.$isSelected.'>' . $city['default_name'] . '</option>';
        }
        return $options;
    }
    //for getting block values based on cities
    public function getUaeBlocksAsDropdownAdmin($selectedcity = '',$address_id)
    {
        $blocks = $this->getUaeBlocks($selectedcity);
        $options = '';
        $load = Mage::getModel('customer/address')->load($address_id);
        $block_value = $load->getAddrBlock();
        foreach($blocks as $block){
            $isSelected = ($block_value == $block['default_name']) ? ' selected="selected"' : null;
            //$options .= '<option data-val="' . $block['block_code'] . '" value="' . $block['default_name'] . '"' . $isSelected . '>' . $block['default_name'] . '</option>';
            $options .= '<option data-val="' . $block['block_code'] . '" value="' . $block['default_name'] . '"'.$isSelected.'>' . $block['default_name'] . '</option>';
        }
        return $options;
    }
}

	 