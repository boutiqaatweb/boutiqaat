<?php
class Ewall_Checkoutdropdown_Helper_Region extends Mage_Core_Helper_Abstract
{
	public function getRegion()
    {
        $region = Mage::getModel('directory/region')->getCollection()->addFieldToFilter('country_id',"KW");
        $reg = array();
        $reg[""] = '-- Select Governorate --';
        foreach ($region as $rg) {
            $reg[$rg->getCode()] =  $rg['default_name'];
        }
        return $reg;
    }
    public function getArea()
    {
        $region = Mage::getModel('checkoutdropdown/checkoutdropdown')->getCollection();
        $reg = array();
        $reg[""] = '-- Select Area --';
        foreach ($region as $rg) {
            $reg[$rg->getCityCode()] =  $rg['default_name'];
        }
        return $reg;
    }
    
}

	 