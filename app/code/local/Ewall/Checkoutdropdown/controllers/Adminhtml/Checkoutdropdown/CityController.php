<?php

class Ewall_Checkoutdropdown_Adminhtml_Checkoutdropdown_CityController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("checkoutdropdown/city")->_addBreadcrumb(Mage::helper("adminhtml")->__("Area Manager"),Mage::helper("adminhtml")->__("Area Manager"));
				return $this;
		}
		public function indexAction() 
		{		//for ajax
			if($this->getRequest()->getParam("ajax") && $this->getRequest()->getParam("isAjax")){
					echo $this->getLayout()->createBlock("checkoutdropdown/adminhtml_city_grid")->toHtml();exit;
			}else{
				$this->_title($this->__("Area"));
				$this->_title($this->__("Manager Area"));

				$this->_initAction();
				$this->renderLayout();
			}
		}
		public function editAction()
		{
			$this->_title($this->__("Governorate"));
			$this->_title($this->__("Area"));
		    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("checkoutdropdown/checkoutdropdown")->load($id);
				if ($model->getId()) {
					Mage::register("city_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("checkoutdropdown/city");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Area Manager"), Mage::helper("adminhtml")->__("Area Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Area Description"), Mage::helper("adminhtml")->__("Area Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("checkoutdropdown/adminhtml_city_edit"))->_addLeft($this->getLayout()->createBlock("checkoutdropdown/adminhtml_city_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("checkoutdropdown")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{
			$this->_title($this->__("Governorate"));
			$this->_title($this->__("Area"));
		    $this->_title($this->__("Edit Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("checkoutdropdown/checkoutdropdown")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("city_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("checkoutdropdown/city");
		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Area Manager"), Mage::helper("adminhtml")->__("Area Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Area Description"), Mage::helper("adminhtml")->__("Area Description"));

		$this->_addContent($this->getLayout()->createBlock("checkoutdropdown/adminhtml_city_edit"))->_addLeft($this->getLayout()->createBlock("checkoutdropdown/adminhtml_city_edit_tabs"));

		$this->renderLayout();

		}
	public function saveAction()
		{
			$post_data = $this->getRequest()->getPost();
		    $post_data['store_id'] = implode(",", $post_data['store_id']);
			if ($post_data) {
				try {
					$model = Mage::getModel("checkoutdropdown/checkoutdropdown")
					->addData($post_data)
					->setId($this->getRequest()->getParam("id"))
					->save();
					Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Area has been saved successfully"));
					Mage::getSingleton("adminhtml/session")->setRegionData(false);

					if ($this->getRequest()->getParam("back")) {
						$this->_redirect("*/*/edit", array("id" => $model->getId()));
						return;
					}
					$this->_redirect("*/*/");
					return;
				} 
				catch (Exception $e) {
					Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
					Mage::getSingleton("adminhtml/session")->setRegionData($this->getRequest()->getPost());
					$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
				return;
				}
			}
			$this->_redirect("*/*/");
		}
		public function deleteAction()
	    {
	        if ($id = $this->getRequest()->getParam('id')) {
	            $area = Mage::getModel('checkoutdropdown/checkoutdropdown')
	                ->load($id);
				$code = $area->getCityCode();
				$store_id = $area->getStoreId();	               
	            try {
	                $area->delete();
	                $this->_getSession()->addSuccess($this->__('The Area has been deleted.'));
	               	$block_collection = Mage::getModel("checkoutdropdown/blockdropdown")->getCollection()->addFieldToFilter('city_code',$code)->addFieldToFilter('store_id',$store_id);
	               	foreach($block_collection as $collection){
	               		$_id = $collection->getBlockId();
	               		$collection = Mage::getModel("checkoutdropdown/blockdropdown")->load($_id);
	               		$collection->delete();
	               	}
	            } catch (Exception $e) {
	                $this->_getSession()->addError($e->getMessage());
	            }
	        }
	        $this->getResponse()
	            ->setRedirect($this->getUrl('*/*/', array('store'=>$this->getRequest()->getParam('store'))));
	    }

	    public function exportCsvAction()
	    {
	        $fileName   = 'city.csv';
	        $grid       = $this->getLayout()->createBlock('checkoutdropdown/adminhtml_city_grid');
	        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
	    }

	    public function exportExcelAction()
	    {
	        $fileName   = 'city.xls';
	        $grid       = $this->getLayout()->createBlock('checkoutdropdown/adminhtml_city_grid');
	        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
	    }
}
