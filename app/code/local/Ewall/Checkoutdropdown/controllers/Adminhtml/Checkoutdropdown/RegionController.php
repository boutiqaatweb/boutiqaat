<?php

class Ewall_Checkoutdropdown_Adminhtml_Checkoutdropdown_RegionController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("checkoutdropdown/region")->_addBreadcrumb(Mage::helper("adminhtml")->__("Region Manager"),Mage::helper("adminhtml")->__("Region Manager"));
				return $this;
		}
		public function indexAction() 
		{		//for ajax
			if($this->getRequest()->getParam("ajax") && $this->getRequest()->getParam("isAjax")){
					echo $this->getLayout()->createBlock("checkoutdropdown/adminhtml_region_grid")->toHtml();exit;
			}else{
				$this->_title($this->__("Region"));
				$this->_title($this->__("Manager Region"));

				$this->_initAction();
				$this->renderLayout();
			}
		}
		public function editAction()
		{
			$this->_title($this->__("Country"));
			$this->_title($this->__("Governorate"));
		    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("directory/region")->load($id);
				if ($model->getId()) {
					Mage::register("region_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("checkoutdropdown/region");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Governorate Manager"), Mage::helper("adminhtml")->__("Governorate Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Governorate Description"), Mage::helper("adminhtml")->__("Governorate Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("checkoutdropdown/adminhtml_region_edit"))->_addLeft($this->getLayout()->createBlock("checkoutdropdown/adminhtml_region_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("checkoutdropdown")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{
			$this->_title($this->__("Country"));
			$this->_title($this->__("Governorate"));
		    $this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("directory/region")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("region_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("checkoutdropdown/region");
		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Region Manager"), Mage::helper("adminhtml")->__("Region Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Region Description"), Mage::helper("adminhtml")->__("Region Description"));


		$this->_addContent($this->getLayout()->createBlock("checkoutdropdown/adminhtml_region_edit"))->_addLeft($this->getLayout()->createBlock("checkoutdropdown/adminhtml_region_edit_tabs"));

		$this->renderLayout();

		}
	public function saveAction()
		{
			$post_data = $this->getRequest()->getPost();
				if ($post_data) {
					try {
						$post_data['country_id'] = 'KW';
						$model = Mage::getModel("directory/region")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Governorate was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setRegionData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setRegionData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}
				}
				$this->_redirect("*/*/");
		}
}
