<?php
require_once 'Mage/Checkout/controllers/CartController.php';
class Ewall_Checkoutdropdown_CartController extends Mage_Checkout_CartController
{
  public function indexAction()
    {
        $cart = $this->_getCart();
        if ($cart->getQuote()->getItemsCount()) {
            $cart->init();
            $cart->save();

            if (!$this->_getQuote()->validateMinimumAmount()) {
                $minimumAmount = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())
                    ->toCurrency(Mage::getStoreConfig('sales/minimum_order/amount'));

                $warning = Mage::getStoreConfig('sales/minimum_order/description')
                    ? Mage::getStoreConfig('sales/minimum_order/description')
                    : Mage::helper('checkout')->__('Minimum order amount is %s', $minimumAmount);

                $cart->getCheckoutSession()->addNotice($warning);
            }
        }

        // Compose array of messages to add
        $messages = array();
        foreach ($cart->getQuote()->getMessages() as $message) {
            if ($message) {
                // Escape HTML entities in quote message to prevent XSS
                $message->setCode(Mage::helper('core')->escapeHtml($message->getCode()));
                $messages[] = $message;
            }
        }
        $cart->getCheckoutSession()->addUniqueMessages($messages);

        /**
         * if customer enteres shopping cart we should mark quote
         * as modified bc he can has checkout page in another window.
         */
        $this->_getSession()->setCartWasUpdated(true);

        Varien_Profiler::start(__METHOD__ . 'cart_display');
        $this
            ->loadLayout()
            ->_initLayoutMessages('checkout/session')
            ->_initLayoutMessages('catalog/session')
            ->getLayout()->getBlock('head')->setTitle($this->__('Shopping Bag'));
        $this->renderLayout();
        Varien_Profiler::stop(__METHOD__ . 'cart_display');
    }
    
    public function addAction()
    {
        if (!$this->_validateFormKey()) {
            $this->_goBack();
            return;
        }
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }
            $product_price = $product->getFinalPrice();
            $storeId = Mage::app()->getStore()->getStoreId();
            if($product_price <= 0){
              if($storeId == 1) {
                Mage::getSingleton('core/session')->addError('Cannot add the product with zero price');
                Mage::app()->getResponse()->setRedirect($product->getProductUrl());
                return;
              }
              else{
                Mage::getSingleton('core/session')->addError('لا يمكن إضافة هذا المنتج مع سعر الصفر');
                Mage::app()->getResponse()->setRedirect($product->getProductUrl());
                return;
              }
            }
            $productId = $product->getId();
            //$categoryIds
            //$product = Mage::getModel('catalog/product')->load($_item['product_id']);
            $categoryId = $params['category'];
            
            $cartitemscount = Mage::helper('checkout/cart')->getCart()->getItemsCount();
            $quote = Mage::getSingleton('checkout/session')->getQuote();

            //Qty restriction by celebrities
            $item_qty = $quote->getItemQtyForRestrict($productId , $categoryId);
            $getAllowedQty = $quote->getAllowedQty($productId , $categoryId);
            $paramsQty = isset($params['qty']) ? $params['qty'] : 1;
            $fututureQty = (int)$paramsQty+$item_qty;
            if($categoryId && $fututureQty>6 && $getAllowedQty>6){
                if($storeId == 1){
                    Mage::getSingleton('core/session')->addError('Maximum number of qty per item is 6. Already '.$item_qty.' qty(s) in your bag.');
                }
                else {
                    Mage::getSingleton('core/session')->addError('الحد الأقصى لعدد الكمية لكل بند هو 6. إذا كنت '.$item_qty.' الكمية (ق) في حقيبتك.');
                }
                Mage::app()->getResponse()->setRedirect($product->getProductUrl());
                return;
            }elseif($categoryId && $fututureQty>$getAllowedQty){
                if($storeId == 1){
                    Mage::getSingleton('core/session')->addError('Maximum number of qty per item is '.$getAllowedQty.'. Already '.$item_qty.' qty(s) in your bag.');
                }
                else {
                    Mage::getSingleton('core/session')->addError('الحد الأقصى لعدد الكمية لكل بند هو '.$getAllowedQty.'. إذا كنت '.$item_qty.' الكمية (ق) في حقيبتك.');
                }
                Mage::app()->getResponse()->setRedirect($product->getProductUrl());
                return;
            }
            //////////////////////////////////
            
            if ((!$quote->hasCategoryId($productId , $categoryId)) && $cartitemscount >= 15) {
                if($storeId == 1){
                    Mage::getSingleton('core/session')->addError('Maximum number of items per order is 15');
                    Mage::app()->getResponse()->setRedirect($product->getProductUrl());
                    return;
                }
                else {
                    Mage::getSingleton('core/session')->addError('الحد الأقصى لعدد العناصر في النظام هو 15');
                    Mage::app()->getResponse()->setRedirect($product->getProductUrl());
                    return;
                }
            }
            
            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }
            $cart_url = "'".Mage::getUrl('checkout/cart')."'";
            // $storeId = Mage::app()->getStore()->getStoreId();
            $quote = Mage::getSingleton('checkout/session')->getQuote();
            $product_image = '<p><img src="'.$product_image_src.'" class="product-image" alt=""/></p>';
            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );
            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()) {
                    if($categoryId != -1){
                        $message = $this->__('%s was added to your shopping bag.', Mage::helper('core')->escapeHtml($product->getSpecialName()));
                    }
                    else{
                        $message = $this->__('%s was added to your shopping bag.', Mage::helper('core')->escapeHtml($product->getName()));
                    }
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping bag.'));
            Mage::logException($e);
            $this->_goBack();
        }
    }

    public function ajaxUpdateAction()
    {
        if(empty($this->getRequest()->getParams())){
            $this->_redirectUrl(Mage::getUrl('checkout/cart'));
        }
        if ($this->getRequest()->isPost()) {
            $item_id = (int)$this->getRequest()->getPost('item_id');
            $qty = $this->getRequest()->getPost('qty');
            $storeId = Mage::app()->getStore()->getStoreId();
            $quoteItem = null;
            $cart = $this->_getCart();
            if ($item_id) {
                $quoteItem = $cart->getQuote()->getItemById($item_id);
                $product = Mage::getModel('catalog/product')->load($quoteItem->getProductId());
            }

            if (!$quoteItem) {
                $result['error'] = -1;
                $result['message'] = $this->__('Quote item is not found.');
            }else{
                try{
                    $val[$item_id] = array('qty'=>$qty);
                    $cartData = $val;
                    if (is_array($cartData)) {
                        $filter = new Zend_Filter_LocalizedToNormalized(
                            array('locale' => Mage::app()->getLocale()->getLocaleCode())
                        );
                        foreach ($cartData as $index => $data) {
                            if (isset($data['qty'])) {
                                $cartData[$index]['qty'] = $filter->filter(trim($data['qty']));
                            }
                        }
                        $cart = $this->_getCart();
                        if (! $cart->getCustomerSession()->getCustomer()->getId() && $cart->getQuote()->getCustomerId()) {
                            $cart->getQuote()->setCustomerId(null);
                        }

                        $cartData = $cart->suggestItemsQty($cartData);
                        $cart->updateItems($cartData)
                            ->save();
                    }
                    $this->_getSession()->setCartWasUpdated(true);
                    $total = '';
                    $result['status'] = 1;
                    $result['success'] = 1;
                    if($storeId==1){
                        $result['message'] = $this->__("%s was updated successfully",$product->getName());
                    }else{
                        $result['message'] = $this->__("%s تم تحديث بنجاح",$product->getName());
                    }
                    $result['item_id'] = $item_id;
                    $cart_page_total = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml();
                    $result['update_total'] = 1;
                    $result['update_total_content'] = $cart_page_total;
                } catch (Mage_Core_Exception $e) {
                    $result['error'] = -1;
                    $result['message'] = $this->__(Mage::helper('core')->escapeHtml($e->getMessage()));
                    $result['item_id'] = $item_id;
                } catch (Mage_Core_Exception $e) {
                    $result['error'] = -1;
                    $result['message'] = $this->__("Cannot update %s.",$product->getName());
                    $result['item_id'] = $item_id;
                }
            }
            
        }
        $this->loadLayout();
        $checkout_cart = "";
        if($this->getLayout()->getBlock('checkout.cart.ajaxupdate')):
            $checkout_cart = $this->getLayout()->getBlock('checkout.cart.ajaxupdate')->toHtml();
            $result['cart_content_status'] = 1;
            $result['cart_content'] = $checkout_cart;
        endif;
        $toplink = "";
        if($this->getLayout()->getBlock('top.links'))
                    $toplink = $this->getLayout()->getBlock('top.links')->toHtml();
        $minicart = "";
        if($this->getLayout()->getBlock('minicart'))
            $minicart = $this->getLayout()->getBlock('minicart')->toHtml();
        $cart_sidebar = "";
        if($this->getLayout()->getBlock('cart_sidebar'))
            $cart_sidebar = $this->getLayout()->getBlock('cart_sidebar')->toHtml();
        
        $result['formKey'] = Mage::helper('core/url')->getEncodedUrl();
        $result['minicart'] = $minicart;
        $result['toplink'] = $toplink;
        $result['cart_sidebar'] = $cart_sidebar;
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

}