<?php
class Ewall_Checkoutdropdown_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Titlename"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("titlename", array(
                "label" => $this->__("Titlename"),
                "title" => $this->__("Titlename")
		   ));

      $this->renderLayout(); 
	  
    }
    public function AjaxcityAction() {
      $post = $this->getRequest()->getParams();
      $cities = Mage::helper('checkoutdropdown')->getUaeCitiesAsDropdown($post['state'],$post['address_id']);
      return $this->getResponse()->setBody($cities);
    }
    public function AjaxblockAction() {
      $post = $this->getRequest()->getParams();
      $blocks = Mage::helper('checkoutdropdown')->getUaeBlocksAsDropdown($post['city'],$post['address_id']);
      return $this->getResponse()->setBody($blocks);
    }
    public function AjaxcityadminAction() {
      $post = $this->getRequest()->getParams();
      $cities = Mage::helper('checkoutdropdown')->getUaeCitiesAsDropdownAdmin($post['state'],$post['address_id']);
      return $this->getResponse()->setBody($cities);
    }
    public function AjaxblockadminAction() {
      $post = $this->getRequest()->getParams();
      $blocks = Mage::helper('checkoutdropdown')->getUaeBlocksAsDropdownAdmin($post['city'],$post['address_id']);
      return $this->getResponse()->setBody($blocks);
    }
}