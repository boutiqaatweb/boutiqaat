<?php
$installer = $this;
$installer->startSetup();
$installer->run("
CREATE TABLE `{$installer->getTable('directory_country_region_city')}` (
  `city_id` mediumint(8) unsigned NOT NULL auto_increment,
  `region_id` varchar(4) NOT NULL default '0',
  `city_code` varchar(32) NOT NULL default '',
  `default_name` varchar(255) default NULL,
  PRIMARY KEY  (`city_id`),
  KEY `FK_REGION_COUNTRY_REGION` (`region_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Country region cities';
");

$installer->endSetup();