<?php
$installer = $this;
$installer->startSetup();
$installer->run("
CREATE TABLE `{$installer->getTable('directory_region_city_block')}` (
  `block_id` mediumint(8) unsigned NOT NULL auto_increment,
  `city_code` varchar(32) NOT NULL default '',
  `block_code` varchar(32) NOT NULL default '',
  `default_name` varchar(255) default NULL,
  PRIMARY KEY  (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Region city blocks';
");

$installer->endSetup();