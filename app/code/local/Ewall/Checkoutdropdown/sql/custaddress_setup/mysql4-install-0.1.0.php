<?php
$installer = $this;

$installer->startSetup();
//$installer->removeAttribute('customer','mobile');
$installer->removeAttribute('customer','mobile');
$installer->addAttribute('customer_address', 'addr_block', array(
    "type"     => "varchar",
    "backend"  => "",
    "label"    => "Block",
    "input"    => "text",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
    "note"       => ""
));
Mage::getSingleton('eav/config')
    ->getAttribute('customer_address', 'addr_block')
    ->setData('used_in_forms', array('customer_register_address','customer_address_edit','adminhtml_customer_address', 'checkout_register'))
    ->setData("is_used_for_customer_segment", true)
        ->setData("is_system", 0)
        ->setData("is_user_defined", 1)
        ->setData("is_visible", 1)
        ->setData("sort_order", 100)
        ->save();

$installer->addAttribute('customer_address', 'addr_landline', array(
    "type"     => "varchar",
    "backend"  => "",
    "label"    => "Land Line",
    "input"    => "text",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
    "note"       => ""
));
Mage::getSingleton('eav/config')
    ->getAttribute('customer_address', 'addr_landline')
    ->setData('used_in_forms', array('customer_register_address','customer_address_edit','adminhtml_customer_address', 'checkout_register'))
    ->setData("is_used_for_customer_segment", true)
        ->setData("is_system", 0)
        ->setData("is_user_defined", 1)
        ->setData("is_visible", 1)
        ->setData("sort_order", 120)
        ->save();

$installer->addAttribute('customer_address', 'addr_villa', array(
    "type"     => "varchar",
    "backend"  => "",
    "label"    => "Villa/Building",
    "input"    => "text",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
    "note"       => ""
));
Mage::getSingleton('eav/config')
    ->getAttribute('customer_address', 'addr_villa')
    ->setData('used_in_forms', array('customer_register_address','customer_address_edit','adminhtml_customer_address', 'checkout_register'))
    ->setData("is_used_for_customer_segment", true)
        ->setData("is_system", 0)
        ->setData("is_user_defined", 1)
        ->setData("is_visible", 1)
        ->setData("sort_order", 130)
        ->save();

$installer->addAttribute('customer_address', 'addr_street', array(
    "type"     => "varchar",
    "backend"  => "",
    "label"    => "Street",
    "input"    => "text",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
    "note"       => ""
));
Mage::getSingleton('eav/config')
    ->getAttribute('customer_address', 'addr_street')
    ->setData('used_in_forms', array('customer_register_address','customer_address_edit','adminhtml_customer_address', 'checkout_register'))
    ->setData("is_used_for_customer_segment", true)
        ->setData("is_system", 0)
        ->setData("is_user_defined", 1)
        ->setData("is_visible", 1)
        ->setData("sort_order", 140)
        ->save();

$installer->addAttribute('customer_address', 'addr_avenue', array(
    "type"     => "varchar",
    "backend"  => "",
    "label"    => "Avenue",
    "input"    => "text",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
    "note"       => ""
));
Mage::getSingleton('eav/config')
    ->getAttribute('customer_address', 'addr_avenue')
    ->setData('used_in_forms', array('customer_register_address','customer_address_edit','adminhtml_customer_address', 'checkout_register'))
    ->setData("is_used_for_customer_segment", true)
        ->setData("is_system", 0)
        ->setData("is_user_defined", 1)
        ->setData("is_visible", 1)
        ->setData("sort_order", 150)
        ->save();

$installer->endSetup();
