<?php
$installer = $this;
 
$installer->startSetup();
$installer->run("

        ALTER TABLE  `".$installer->getTable('sales_flat_quote_address')."` ADD  `addr_villa` VARCHAR(100) NOT NULL;
        ALTER TABLE  `".$installer->getTable('sales_flat_quote_address')."` ADD  `addr_block` VARCHAR(100) NOT NULL;
        ALTER TABLE  `".$installer->getTable('sales_flat_quote_address')."` ADD  `addr_street` VARCHAR(100) NOT NULL;
        ALTER TABLE  `".$installer->getTable('sales_flat_quote_address')."` ADD  `addr_avenue` VARCHAR(100) NOT NULL;
        ALTER TABLE  `".$installer->getTable('sales_flat_quote_address')."` ADD  `addr_landline` VARCHAR(100) NOT NULL;

        ALTER TABLE  `".$installer->getTable('sales_flat_order_address')."` ADD  `addr_villa` VARCHAR(100) NOT NULL;
        ALTER TABLE  `".$installer->getTable('sales_flat_order_address')."` ADD  `addr_block` VARCHAR(100) NOT NULL;
        ALTER TABLE  `".$installer->getTable('sales_flat_order_address')."` ADD  `addr_street` VARCHAR(100) NOT NULL;
        ALTER TABLE  `".$installer->getTable('sales_flat_order_address')."` ADD  `addr_avenue` VARCHAR(100) NOT NULL;
        ALTER TABLE  `".$installer->getTable('sales_flat_order_address')."` ADD  `addr_landline` VARCHAR(100) NOT NULL;

        ");
$installer->endSetup();
