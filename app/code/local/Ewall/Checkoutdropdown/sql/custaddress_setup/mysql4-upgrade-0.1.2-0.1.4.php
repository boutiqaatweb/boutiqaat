<?php
$installer = $this;

$installer->startSetup();
//$installer->removeAttribute('customer','mobile');
$installer->addAttribute('customer_address', 'addr_flatenumber', array(
    "type"     => "varchar",
    "backend"  => "",
    "label"    => "Flatenumber",
    "input"    => "text",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
    "note"       => ""
));
Mage::getSingleton('eav/config')
    ->getAttribute('customer_address', 'addr_flatenumber')
    ->setData('used_in_forms', array('customer_register_address','customer_address_edit','adminhtml_customer_address', 'checkout_register'))
    ->setData("is_used_for_customer_segment", true)
        ->setData("is_system", 0)
        ->setData("is_user_defined", 1)
        ->setData("is_visible", 1)
        ->setData("sort_order", 140)
        ->save();

$installer->addAttribute('customer_address', 'addr_floornumber', array(
    "type"     => "varchar",
    "backend"  => "",
    "label"    => "Floornumber",
    "input"    => "text",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
    "note"       => ""
));
Mage::getSingleton('eav/config')
    ->getAttribute('customer_address', 'addr_floornumber')
    ->setData('used_in_forms', array('customer_register_address','customer_address_edit','adminhtml_customer_address', 'checkout_register'))
    ->setData("is_used_for_customer_segment", true)
        ->setData("is_system", 0)
        ->setData("is_user_defined", 1)
        ->setData("is_visible", 1)
        ->setData("sort_order", 140)
        ->save();

$installer->run("

        ALTER TABLE  `".$installer->getTable('sales_flat_quote_address')."` ADD  `addr_flatenumber` VARCHAR(100) NOT NULL;
        ALTER TABLE  `".$installer->getTable('sales_flat_quote_address')."` ADD  `addr_floornumber` VARCHAR(100) NOT NULL;

        ALTER TABLE  `".$installer->getTable('sales_flat_order_address')."` ADD  `addr_flatenumber` VARCHAR(100) NOT NULL;
        ALTER TABLE  `".$installer->getTable('sales_flat_order_address')."` ADD  `addr_floornumber` VARCHAR(100) NOT NULL;
        ");
$installer->endSetup();