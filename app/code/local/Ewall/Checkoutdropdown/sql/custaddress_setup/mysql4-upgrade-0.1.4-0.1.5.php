<?php
$installer = $this;
 
$installer->startSetup();
$city = 'Area';
$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer_address', 'city');
$oAttribute->setData('frontend_label', $city);
$oAttribute->save();

$installer->startSetup();
$state = 'Governorate';
$sAttribute = Mage::getSingleton('eav/config')->getAttribute('customer_address', 'region_id');
$sAttribute->setData('frontend_label', $state);
$sAttribute->save();

$installer->startSetup();
$region = 'Governorate';
$rAttribute = Mage::getSingleton('eav/config')->getAttribute('customer_address', 'region');
$rAttribute->setData('frontend_label', $region);
$rAttribute->save();

$installer->startSetup();
$mobile = 'Mobile';
$mAttribute = Mage::getSingleton('eav/config')->getAttribute('customer_address', 'telephone');
$mAttribute->setData('frontend_label', $mobile);
$mAttribute->save();

$installer->startSetup();
$address = 'Details';
$aAttribute = Mage::getSingleton('eav/config')->getAttribute('customer_address', 'street');
$aAttribute->setData('frontend_label', $address);
$aAttribute->save();

$installer->endSetup();
