<?php
$installer = $this;
$installer->startSetup();
$city = 'Area/City';
$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer_address', 'city');
$oAttribute->setData('frontend_label', $city);
$oAttribute->save();

$installer->startSetup();
$state = 'Governorate/State';
$sAttribute = Mage::getSingleton('eav/config')->getAttribute('customer_address', 'region_id');
$sAttribute->setData('frontend_label', $state);
$sAttribute->save();

$installer->startSetup();
$region = 'Governorate/State';
$rAttribute = Mage::getSingleton('eav/config')->getAttribute('customer_address', 'region');
$rAttribute->setData('frontend_label', $region);
$rAttribute->save();

$installer->endSetup();
