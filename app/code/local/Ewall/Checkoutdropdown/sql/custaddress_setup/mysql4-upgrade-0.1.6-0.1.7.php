<?php
 
$installer = $this;
$connection = $installer->getConnection();
 
$installer->startSetup();

    $installer->run("

        ALTER TABLE  `".$installer->getTable('directory_country_region_city')."` ADD  `store_id` VARCHAR(100) NOT NULL AFTER `city_id`;
        ALTER TABLE  `".$installer->getTable('directory_region_city_block')."` ADD  `store_id` VARCHAR(100) NOT NULL AFTER `block_id`;

        ");
$installer->endSetup();