<?php
/**
 * News Data helper
 *
 * @author Magento
 */
class Ewall_Erp_Helper_Data extends Mage_Core_Helper_Data
{
	public function getErpSession($order_data)
	{
    	$api_url_v2 = Mage::getStoreConfig('erp/erp_config/url');
		$username = Mage::getStoreConfig('erp/erp_config/username');
		$password = Mage::getStoreConfig('erp/erp_config/password'); 
		// $cli = new SoapClient($api_url_v2, array('cache_wsdl' => WSDL_CACHE_NONE));
		$cli = new SoapClient($api_url_v2, array('cache_wsdl' => WSDL_CACHE_NONE,'trace' => 1,
        'exceptions'=> 1,'connection_timeout' => 30));
		$login = array('as_userid'=>$username,'as_pwd'=>$password);
	    $login_object = (object)$login;
	    $session_id_function = $cli->login($login_object);
	    $session_id_user = array('a_session_id'=>$session_id_function);
	    $session = (object)$session_id_user;
		//$session_id = $cli->login($username, $password);
		$result = $cli->createorder($order_data,$session);
		return $result->createorderResult;
	}
  
}