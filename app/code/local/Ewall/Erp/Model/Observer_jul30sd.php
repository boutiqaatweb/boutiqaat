<?php

class Ewall_Erp_Model_Observer

{        

    public function createOrderToErp($observer)
    {
        
        $orderIds = $observer->getOrderIds();
        if(empty($orderIds[0])){
            return;
        }
        $this->_allDataUpdate($orderIds[0]);

    }

    public function _pushOrderToErp($order_data)
    {
    	return $result = Mage::helper('erp')->getErpSession($order_data);
    }

    public function setErpStatus()
    {
        $collection = Mage::getModel('sales/order')->getCollection()->addFieldToSelect('entity_id')->addFieldToSelect('erp_update')->addFieldToFilter('erp_update',array('in'=>array(0,'null')));
        if(count($collection)>0){
            foreach ($collection as $key => $value) {
                $this->_allDataUpdate($value->getId());
            }
        }
    }

    public function _allDataUpdate($id)
    {
        if(empty($id)){
            return;
        }
        $order = Mage::getModel('sales/order')->load($id);
        $shipping_address = $order->getShippingAddress();
        $ddate = Mage::getModel('ddate/ddate_store')->load($order->getId(),'sales_order_id')->getDdateId();
        $dtime = Mage::getModel('ddate/ddate')->load($ddate);
        $order_data = array();
        $order_lines = array();
        foreach ($order->getAllItems() as $order_item) {
            $order_items['sku'] = $order_item['sku'];
            $order_items['product_name'] = $order_item['name'];
            $order_items['price'] = number_format($order_item['price'],4);
            $order_items['qty'] = number_format($order_item['qty_ordered'],4);
            $order_items['total'] = number_format($order_item['row_total'],4);
            $order_items['discount'] = number_format($order_item['discount_amount'],4);
            $order_lines[] = $order_items;
        }
        $order_data['order_date'] = $order->getCreatedAt();
        $order_data['order_lines'] = $order_lines;
        $order_data['order_number'] = $order->getIncrementId();
        if($order->getCustomerIsGuest()){
            $order_data['customer_idspecified'] = '0';
            $order_data['customer_id'] = '';
        }else{
            $order_data['customer_idspecified'] = '1';
            $order_data['customer_id'] = $order->getCustomerId();
        }
        $order_data['store_id'] = Mage::getModel('core/store')->load($order->getStoreId())->getCode();//$order->getStoreName();
        $order_data['shipping_firstname'] = $shipping_address->getFirstname();
        $order_data['shipping_lastname'] = $shipping_address->getLastname();
        $order_data['customer_email'] = $order->getCustomerEmail();
        $order_data['discount'] = number_format($order->getDiscountAmount(),4);
        $order_data['payment_method_id'] = $order->getPayment()->getMethodInstance()->getCode();
        $order_data['shipping_method'] = $order->getShippingMethod();
        if($ddate){
            $order_data['delivery_date'] = $dtime['ddate'];
        }
        $order_data['sales_center'] = '1';
        $order_data['city'] = $shipping_address->getCity();
        $order_data['street_address'] = $shipping_address->getStreetFull();
        $order_data['phone_number'] = $shipping_address->getTelephone();
        $order_data['time_slot'] = $dtime->getDtimetext();
        $order_data_inner = array('order_data'=>$order_data);
        $data = (object)$order_data_inner;
        // echo '<pre>'; print_r($data);die();
        $result = $this->_pushOrderToErp($data);
        $order->setErpUpdate(1)->save();
    }

}