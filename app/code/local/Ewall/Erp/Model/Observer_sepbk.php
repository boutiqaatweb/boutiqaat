<?php

class Ewall_Erp_Model_Observer
{        
    public function createOrderToErp($observer)
    {
        
        $orderIds = $observer->getOrderIds();
        if(empty($orderIds[0])){
            return;
        }
        $this->_allDataUpdate($orderIds[0]);

    }

    public function _pushOrderToErp($order_data)
    {
        return $result = Mage::helper('erp')->getErpSession($order_data);
    }

    public function setErpStatus()
    {
        $collection = Mage::getModel('sales/order')->getCollection()->addFieldToSelect('entity_id')->addFieldToSelect('erp_update')->addFieldToFilter('erp_update',array('in'=>array(0,'null','')));
        // echo count($collection);
        if($collection->count()>0){
            foreach ($collection as $key => $value) {
                $this->_allDataUpdate($value->getId());
            }
        }
    }

    public function _allDataUpdate($id)
    {
        if(empty($id)){
            return;
        }
        $order = Mage::getModel('sales/order')->load($id);

        $payment_method_code = $order->getPayment()->getMethodInstance()->getCode();
        $status = $order->getStatus();
        if($payment_method_code =='knet_cc' && $status == 'pending'){
            return;
        }

        $shipping_address = $order->getShippingAddress();
        //$ddate = Mage::getModel('ddate/ddate_store')->load($order->getId(),'sales_order_id')->getDdateId();
       // $dtime = Mage::getModel('ddate/ddate')->load($ddate);
        $order_data = array();
        $order_lines = array();
        foreach ($order->getAllItems() as $order_item) {
            $order_items['sku'] = (string)$order_item['sku'];
            $order_items['product_name'] = (string)$order_item['name'];
            $order_items['price'] = (string)number_format($order_item['price'],4);
            $order_items['qty'] = (string)number_format($order_item['qty_ordered'],4);
            $order_items['total'] = (string)number_format($order_item['row_total'],4);
            $order_items['discount'] = (string)number_format($order_item['discount_amount'],4);
            
            /* Celebrity Value*/
            $order_items['category_id'] = (string)$order_item['category_id'];
            $order_items['special_name'] = isset($order_item['special_name'])?(string)$order_item['special_name']:'';
            $order_items['celebrity_price'] = (string)$order_item['celebrity_special_price'];
            $order_items['celebrity_commision'] = (string)$order_item['commision_amount'];
            $order_items['special_name'] = (string)$order_item['special_name'];
            $order_items['ad_date'] = (string)$order_item['celebrity_ad_date'];
            $order_lines[] = $order_items;
        }
        // print_r($order_lines);die();

        $order_data['order_date'] = (string)$order->getCreatedAt();
        $order_data['order_lines'] = $order_lines;
        $order_data['order_number'] = (string)$order->getIncrementId();
        if($order->getCustomerIsGuest()){
            $order_data['customer_idspecified'] = (string)'0';
            $order_data['customer_id'] = '';
        }else{
            $order_data['customer_idspecified'] = (string)'1';
            $order_data['customer_id'] = (string)$order->getCustomerId();
        }
        $order_data['store_id'] = Mage::getModel('core/store')->load($order->getStoreId())->getCode();//$order->getStoreName();
        $order_data['shipping_firstname'] = (string)$shipping_address->getFirstname();
        $order_data['shipping_lastname'] = (string)$shipping_address->getLastname();
        $order_data['customer_email'] = (string)$order->getCustomerEmail();
        $order_data['discount'] = number_format($order->getDiscountAmount(),4);
        $order_data['payment_method_id'] = (string)$order->getPayment()->getMethodInstance()->getCode();
        $order_data['shipping_method'] = (string)$order->getShippingMethod();
        //if($ddate){
            $order_data['delivery_date'] = '0000-00-00';//$dtime['ddate'];
        //}
        $order_data['sales_center'] = '1';
        $order_data['city'] = (string)$shipping_address->getCity();
        $order_data['order_streetnumber'] = (string)$shipping_address->getAddrStreet();
        $order_data['phone_number'] = (string)$shipping_address->getTelephone();
        $order_data['time_slot'] = (string)'00:00';//$dtime->getDtimetext();
        if($shipping_address->getCountryId() == 'KW'){
            $order_data['order_area'] = (string)$shipping_address->getCity();
            $order_data['order_block'] = (string)$shipping_address->getAddrBlock();
            $order_data['order_mobile'] = (string)$shipping_address->getTelephone();
            $order_data['order_buildingnumber'] = (string)$shipping_address->getAddrVilla();
            $order_data['order_flatenumber'] = (string)$shipping_address->getAddrFlatenumber();
            $order_data['order_floornumber'] = (string)$shipping_address->getAddrFloornumber();
            $order_data['order_landline'] = (string)$shipping_address->getAddrLandline();
        }
        $order_data_inner = array('order_data'=>$order_data);
        $data = (object)$order_data_inner;
        // echo '<pre>'; print_r($data);die();
        $result = $this->_pushOrderToErp($data);
        $order->setErpUpdate(1)->save();
    }
}