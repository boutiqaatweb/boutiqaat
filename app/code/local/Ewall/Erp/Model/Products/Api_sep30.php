<?php

class Ewall_Erp_Model_Products_Api extends Mage_Api_Model_Resource_Abstract

{        

        

	public function catalogproductcreate($sku, $qty, $name, $price)

    {	

    	Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

		$product = Mage::getModel('catalog/product');

		try{

			$product

			    ->setWebsiteIds(array(1)) 

			    ->setAttributeSetId(4) 

			    ->setTypeId('simple') 

			    ->setCreatedAt(strtotime('now'))

				//->setUpdatedAt(strtotime('now'))

			    ->setSku(trim($sku)) 

			    ->setName($name)

			    ->setWeight(0.0000)

			    ->setStatus(2)

			    ->setTaxClassId(0) 

			    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)

			    ->setPrice($price)

			    ->setStockData(array(

			                       'use_config_manage_stock' => 0, 

			                       'manage_stock'=>1,

			                       'is_in_stock' => (($qty > 0) ? 1 : 0),

			                       'qty' => $qty 

			                   )

			    );

			$product->getResource()->save($product);

			$product->save();



			$products =  array();

			$productsEntity = new stdClass();

			$productsEntity->sku 	= $product->getSku().$product->getId();

			$productsEntity->qty 	= $qty;

			$productsEntity->product_name 	= $product->getName();

			$productsEntity->price 	= $product->getPrice();

			$productsEntity->creation_status = 'success';

			$productsEntity->message = Mage::helper('erp')->__('Product %s was successfuly created', $product->getName());

			$products[]     = $productsEntity;

			

		}catch(Exception $e){

			$products =  array();

			$productsEntity = new stdClass();

			$productsEntity->sku 	= trim($sku);

			$productsEntity->qty 	= $qty;

			$productsEntity->product_name 	= $name;

			$productsEntity->price 	= $price;

			$productsEntity->creation_status = 'error';

			$productsEntity->message = $e->getMessage();

			$products[]     = $productsEntity;

		}

		return $products;

    }



    public function importinventoryupdateqty($skus, $qtys)

    {	

    	$_all_sku = explode(',', $skus);

		$_all_qty = explode(',', $qtys);



		$products =  array();

		foreach ($_all_sku as $key => $sku) {

			$productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));

			$productsEntity = new stdClass();

			if($productid){

				$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productid);

				if($_all_qty[$key] > 0):

					$stockItem->setIsInStock(true);

				else:

					$stockItem->setIsInStock(false);

				endif;

				$stockItem->setQty($_all_qty[$key]);

				$stockItem->save();



				$productsEntity->sku 	= trim($sku);

				$productsEntity->qty 	= $stockItem->getQty();

				$productsEntity->update_status = 'success';

			}

			else{

				$productsEntity->sku 	= trim($sku);

				$productsEntity->qty 	= $_all_qty[$key];

				$productsEntity->update_status = 'error';

			}

			$products[]     = $productsEntity;

		}



		return $products;

    }

	// update product price
    public function importinventoryupdateprice($skus, $prices)

    {	

    	$_all_sku = explode(',', $skus);

		$_all_price = explode(',', $prices);



		$products =  array();

		foreach ($_all_sku as $key => $sku) {

			$productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));

			$productsEntity = new stdClass();

			if($productid){
				$product = Mage::getModel('catalog/product')->load($productid);
				$product->setPrice($_all_price[$key]);
				$product->save();


				$productsEntity->sku 	= trim($sku);

				$productsEntity->price 	= $product->getPrice();

				$productsEntity->update_status = 'success';

			}

			else{

				$productsEntity->sku 	= trim($sku);

				$productsEntity->price 	= $_all_price[$key];

				$productsEntity->update_status = 'error';

			}

			$products[]     = $productsEntity;

		}



		return $products;

    }

	// update product status
    public function importbulkupdatestatus($skus, $status)

    {	

    	$_all_sku = explode(',', $skus);

		$_all_status = explode(',', $status);



		$products =  array();

		foreach ($_all_sku as $key => $sku) {

			$productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));

			$productsEntity = new stdClass();

			if($productid){

				$product = Mage::getModel('catalog/product')->load($productid);

				$product->setStatus($_all_status[$key]);
				$product->save();

				//$state = ($product->getStatus() == 1) ? 'enabled' : 'disabled';
				$productsEntity->sku 	= trim($sku);

				$productsEntity->status 	= $product->getStatus();

				$productsEntity->update_status = 'success';

			}

			else{

				$productsEntity->sku 	= trim($sku);

				$productsEntity->status = $_all_status[$key];

				$productsEntity->update_status = 'error';

			}

			$products[]     = $productsEntity;

		}



		return $products;

    }

    // update product status
    public function updateproductnamedesc($sku, $store_id, $name, $splname, $desc)
    {
    	$productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));
		$productsEntity = new stdClass();
		if($productid){
			$product = Mage::getModel('catalog/product')->setStoreId($store_id)->load($productid);
			$curname = ($name == '' || !isset($name)) ? $product->getName() : $name;
			$cursplname = ($splname == '' || !isset($splname)) ? $product->getSpecialName() : $splname;
			$curdescription = ($desc == '' || !isset($desc)) ? $product->getDescription() : $desc;
			$product->setName($curname)
					->setSpecialName($cursplname)
					->setDescription($curdescription)
					->save();
			$productsEntity->sku 	= trim($sku);
			$productsEntity->store_id 	= trim($store_id);
			$productsEntity->name 	= $product->getName();
			$productsEntity->special_name 	= $product->getSpecialName();
			$productsEntity->description 	= $product->getDescription();
			$productsEntity->update_status = 'success';
		}
		else{
			$productsEntity->sku 	= trim($sku);
			$productsEntity->store_id 	= trim($store_id);
			$productsEntity->name 	= $name;
			$productsEntity->special_name 	= $splname;
			$productsEntity->description 	= $description();
			$productsEntity->update_status = 'error';
		}
		$products[]     = $productsEntity;
		return $products;
    }




	public function celebrityupdate($sku, $celebrity_id, $special_price, $comission, $ad_date, $ad_number, $max = 0)

    {	
    	Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    	
		$productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));

		$productsEntity = new stdClass();

		if($productid){

			$product = Mage::getModel('catalog/product')->load($productid);
			if(!in_array($celebrity_id, $product->getCategoryIds())){
				$categoryids = array();
				$categoryids = $product->getCategoryIds();
				$categoryids[] = $celebrity_id;
				$product->setCategoryIds($categoryids);
			}

			//if(!empty($product->getCelebritySpecialPrice())){
				if(count(unserialize($product->getCelebritySpecialPrice()))){
					$celebrity_special_price = unserialize($product->getCelebritySpecialPrice());
					$celebrity_special_price[$celebrity_id] =  $special_price;
					$product->setCelebritySpecialPrice(serialize($celebrity_special_price));
				}
				else{
					$product->setCelebritySpecialPrice(serialize(array($celebrity_id => $special_price)));
				}
			//}
			//else{
				//$product->setCelebritySpecialPrice(serialize(array($celebrity_id => $special_price)));
			//}

			//if(!empty($product->getCelebrityCommision())){
				if(count(unserialize($product->getCelebrityCommision()))){
					$celebrity_commision = unserialize($product->getCelebrityCommision());
					$celebrity_commision[$celebrity_id] =  $comission;
					$product->setCelebrityCommision(serialize($celebrity_commision));
				}
				else{
					$product->setCelebrityCommision(serialize(array($celebrity_id => $comission)));
				}
			//}
			//else{
				//$product->setCelebrityCommision(serialize(array($celebrity_id => $commision)));
			//}

			//if(!empty($product->getCelebrityAdDate())){
				if(count(unserialize($product->getCelebrityAdDate()))){
					$celebrity_ad_date = unserialize($product->getCelebrityAdDate());
					$celebrity_ad_date[$celebrity_id] =  $ad_date;
					$product->setCelebrityAdDate(serialize($celebrity_ad_date));
				}
				else{
					$product->setCelebrityAdDate(serialize(array($celebrity_id => $ad_date)));
				}
			//}
			//else{
				//$product->setCelebrityAdDate(serialize(array($celebrity_id => $ad_date)));
			//}

			//if(!empty($product->getCelebrityAdNumber())){
				if(count(unserialize($product->getCelebrityAdNumber()))){
					$celebrity_ad_number = unserialize($product->getCelebrityAdNumber());
					$celebrity_ad_number[$celebrity_id] =  $ad_number;
					$product->setCelebrityAdNumber(serialize($celebrity_ad_number));
				}
				else{
					$product->setCelebrityAdNumber(serialize(array($celebrity_id => $ad_number)));
				}
			//}
			//else{
				//$product->setCelebrityAdNumber(serialize(array($celebrity_id => $ad_number)));
			//}
			
			if(count(unserialize($product->getMax()))){
				$celebrity_max = unserialize($product->getMax());
				$celebrity_max[$celebrity_id] =  $max;
				$product->setMax(serialize($celebrity_max));
			}
			else{
				$product->setMax(serialize(array($celebrity_id => $max)));
			}

			//$product->setSpecialName($special_name);

			$product->save();
			
			$productsEntity->sku 	= trim($sku);				

			$productsEntity->update_status = 'success';

		}
		else{

			$productsEntity->sku 	= trim($sku);

			$productsEntity->update_status = 'error';

		}

		$products[]     = $productsEntity;

		return $products;

    }

    public function celebritystatusupdate($sku, $celebrity_id, $status)

    {
    	Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    	
		$productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));

		$productsEntity = new stdClass();

		if($productid){

			$product = Mage::getModel('catalog/product')->load($productid);
			
			$categoryids = array();
			
			$categoryids = $product->getCategoryIds();

			if(!in_array($celebrity_id, $product->getCategoryIds()) && $status){				
				
				$categoryids[] = $celebrity_id;
				
				$product->setCategoryIds($categoryids);
			}
			elseif(in_array($celebrity_id, $product->getCategoryIds()) && !$status) {

				if(($key = array_search($celebrity_id, $categoryids)) !== false) {
				    
				    unset($categoryids[$key]);

				}
				
				$product->setCategoryIds($categoryids);
			}			

			$product->save();
			
			$productsEntity->sku 	= trim($sku);				

			$productsEntity->update_status = 'success';

		}
		else{

			$productsEntity->sku 	= trim($sku);

			$productsEntity->update_status = 'error';

		}

		$products[]     = $productsEntity;

		return $products;
    }
	public function bulkcelebrityupdate($skus, $celebrity_id){
	    $skus_array = array_filter(explode(',',$skus));
	    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
	    $category = Mage::getModel('catalog/category')->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)->load($celebrity_id);
        //get the current products
        $products = $category->getProductsPosition();
        
        $skus_array = array_filter(explode(',',$skus));
        $product_data = Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToSelect(array('entity_id'))
                        ->addAttributeToFilter('sku',array('IN' => $skus_array));
        $productsEntity = new stdClass();
        $productsEntity->sku_status = '';
        //now attach the other products.
        $newProductIds = array();
        foreach($product_data as $_product){
            $newProductIds[] = $_product->getId();
        }
        foreach ($newProductIds as $id){
            $products[$id] = 1;//you can put any other position number instead of 1.
        }
        try{
            //attach all the products to the category
            $category->setPostedProducts($products);
            //save the category.
            $category->save();
            $productsEntity->sku_status = "success";
        }catch(Exception $e) {
            echo $e->getMessage();
            $productsEntity->sku_status = "error";
        }
        $_products[]     = $productsEntity;
		return $_products;
	}
	    
}