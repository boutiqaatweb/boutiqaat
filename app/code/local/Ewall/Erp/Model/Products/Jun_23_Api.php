<?php

class Ewall_Erp_Model_Products_Api extends Mage_Api_Model_Resource_Abstract

{        

        

	public function catalogproductcreate($sku, $qty, $name, $price)

    {	

    	Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

		$product = Mage::getModel('catalog/product');

		try{
			
			if(!$product->getIdBySku(trim($sku))):
				
				$product

				    ->setWebsiteIds(array(1)) 

				    ->setAttributeSetId(4) 

				    ->setTypeId('simple') 

				    ->setCreatedAt(strtotime('now'))

					//->setUpdatedAt(strtotime('now'))

				    ->setSku(trim($sku)) 

				    ->setName($name)

				    ->setWeight(0.0000)

				    ->setStatus(1)

				    ->setTaxClassId(0) 

				    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)

				    ->setPrice($price);

				$product->setStockData(array(

				                       'use_config_manage_stock' => 0, 

				                       'manage_stock'=>1,

				                       'is_in_stock' => (($qty > 0) ? 1 : 0),

				                       'qty' => $qty 

				                   )

				    );

				$product->getResource()->save($product);

				$product->save();

				
				$products =  array();

				$productsEntity = new stdClass();

				$productsEntity->sku 	= $product->getSku();

				$productsEntity->qty 	= $qty;

				$productsEntity->product_name 	= $product->getName();

				$productsEntity->price 	= $product->getPrice();

				$productsEntity->creation_status = 'success';

				$productsEntity->message = Mage::helper('erp')->__('Product %s was successfuly created', $product->getName());

				$products[]     = $productsEntity;
			else:
				$products =  array();

				$productsEntity = new stdClass();

				$productsEntity->sku 	= trim($sku);

				$productsEntity->qty 	= $qty;

				$productsEntity->product_name 	= $name;

				$productsEntity->price 	= $price;

				$productsEntity->creation_status = 'error';

				$productsEntity->message = Mage::helper('erp')->__('Product %s was already exist', $name);

				$products[]     = $productsEntity;
			endif;

		} catch(Exception $e){

			$products =  array();

			$productsEntity = new stdClass();

			$productsEntity->sku 	= trim($sku);

			$productsEntity->qty 	= $qty;

			$productsEntity->product_name 	= $name;

			$productsEntity->price 	= $price;

			$productsEntity->creation_status = 'error';

			$productsEntity->message = $e->getMessage();

			$products[]     = $productsEntity;

		}



		return $products;

    }





	public function importinventoryupdateqty($skus, $qtys)

    {	

    	$_all_sku = explode(',', $skus);

		$_all_qty = explode(',', $qtys);



		$products =  array();

		foreach ($_all_sku as $key => $sku) {

			$productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));

			$productsEntity = new stdClass();

			if($productid){

				$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productid);

				if($_all_qty[$key] > 0):

					$stockItem->setIsInStock(true);

				else:

					$stockItem->setIsInStock(false);

				endif;

				$stockItem->setQty($_all_qty[$key]);

				$stockItem->save();



				$productsEntity->sku 	= trim($sku);

				$productsEntity->qty 	= $stockItem->getQty();

				$productsEntity->update_status = 'success';

			}

			else{

				$productsEntity->sku 	= trim($sku);

				$productsEntity->qty 	= $_all_qty[$key];

				$productsEntity->update_status = 'error';

			}

			$products[]     = $productsEntity;

		}



		return $products;

    }

}