<?php

class Ewall_Erp_Model_Products_Api extends Mage_Api_Model_Resource_Abstract

{        

        

	public function catalogproductcreate($sku, $qty, $name, $price)

    {	

    	Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

		$product = Mage::getModel('catalog/product');

		try{

			$product

			    ->setWebsiteIds(array(1)) 

			    ->setAttributeSetId(4) 

			    ->setTypeId('simple') 

			    ->setCreatedAt(strtotime('now'))

				//->setUpdatedAt(strtotime('now'))

			    ->setSku(trim($sku)) 

			    ->setName($name)

			    ->setWeight(0.0000)

			    ->setStatus(1)

			    ->setTaxClassId(0) 

			    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)

			    ->setPrice($price)

			    ->setStockData(array(

			                       'use_config_manage_stock' => 0, 

			                       'manage_stock'=>1,

			                       'is_in_stock' => (($qty > 0) ? 1 : 0),

			                       'qty' => $qty 

			                   )

			    );

			$product->getResource()->save($product);

			$product->save();



			$products =  array();

			$productsEntity = new stdClass();

			$productsEntity->sku 	= $product->getSku().$product->getId();

			$productsEntity->qty 	= $qty;

			$productsEntity->product_name 	= $product->getName();

			$productsEntity->price 	= $product->getPrice();

			$productsEntity->creation_status = 'success';

			$productsEntity->message = Mage::helper('erp')->__('Product %s was successfuly created', $product->getName());

			$products[]     = $productsEntity;

			

		}catch(Exception $e){

			$products =  array();

			$productsEntity = new stdClass();

			$productsEntity->sku 	= trim($sku);

			$productsEntity->qty 	= $qty;

			$productsEntity->product_name 	= $name;

			$productsEntity->price 	= $price;

			$productsEntity->creation_status = 'error';

			$productsEntity->message = $e->getMessage();

			$products[]     = $productsEntity;

		}

		return $products;

    }



    public function importinventoryupdateqty($skus, $qtys)

    {	

    	$_all_sku = explode(',', $skus);

		$_all_qty = explode(',', $qtys);



		$products =  array();

		foreach ($_all_sku as $key => $sku) {

			$productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));

			$productsEntity = new stdClass();

			if($productid){

				$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productid);

				if($_all_qty[$key] > 0):

					$stockItem->setIsInStock(true);

				else:

					$stockItem->setIsInStock(false);

				endif;

				$stockItem->setQty($_all_qty[$key]);

				$stockItem->save();



				$productsEntity->sku 	= trim($sku);

				$productsEntity->qty 	= $stockItem->getQty();

				$productsEntity->update_status = 'success';

			}

			else{

				$productsEntity->sku 	= trim($sku);

				$productsEntity->qty 	= $_all_qty[$key];

				$productsEntity->update_status = 'error';

			}

			$products[]     = $productsEntity;

		}



		return $products;

    }






	public function celebrityupdate($sku, $celebrity_id, $special_price, $comission, $ad_date, $ad_number)

    {	
    	Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    	
		$productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));

		$productsEntity = new stdClass();

		if($productid){

			$product = Mage::getModel('catalog/product')->load($productid);
			if(!in_array($celebrity_id, $product->getCategoryIds())){
				$categoryids = array();
				$categoryids = $product->getCategoryIds();
				$categoryids[] = $celebrity_id;
				$product->setCategoryIds($categoryids);
			}

			//if(!empty($product->getCelebritySpecialPrice())){
				if(count(unserialize($product->getCelebritySpecialPrice()))){
					$celebrity_special_price = unserialize($product->getCelebritySpecialPrice());
					$celebrity_special_price[$celebrity_id] =  $special_price;
					$product->setCelebritySpecialPrice(serialize($celebrity_special_price));
				}
				else{
					$product->setCelebritySpecialPrice(serialize(array($celebrity_id => $special_price)));
				}
			//}
			//else{
				//$product->setCelebritySpecialPrice(serialize(array($celebrity_id => $special_price)));
			//}

			//if(!empty($product->getCelebrityCommision())){
				if(count(unserialize($product->getCelebrityCommision()))){
					$celebrity_commision = unserialize($product->getCelebrityCommision());
					$celebrity_commision[$celebrity_id] =  $comission;
					$product->setCelebrityCommision(serialize($celebrity_commision));
				}
				else{
					$product->setCelebrityCommision(serialize(array($celebrity_id => $comission)));
				}
			//}
			//else{
				//$product->setCelebrityCommision(serialize(array($celebrity_id => $commision)));
			//}

			//if(!empty($product->getCelebrityAdDate())){
				if(count(unserialize($product->getCelebrityAdDate()))){
					$celebrity_ad_date = unserialize($product->getCelebrityAdDate());
					$celebrity_ad_date[$celebrity_id] =  $ad_date;
					$product->setCelebrityAdDate(serialize($celebrity_ad_date));
				}
				else{
					$product->setCelebrityAdDate(serialize(array($celebrity_id => $ad_date)));
				}
			//}
			//else{
				//$product->setCelebrityAdDate(serialize(array($celebrity_id => $ad_date)));
			//}

			//if(!empty($product->getCelebrityAdNumber())){
				if(count(unserialize($product->getCelebrityAdNumber()))){
					$celebrity_ad_number = unserialize($product->getCelebrityAdNumber());
					$celebrity_ad_number[$celebrity_id] =  $ad_number;
					$product->setCelebrityAdNumber(serialize($celebrity_ad_number));
				}
				else{
					$product->setCelebrityAdNumber(serialize(array($celebrity_id => $ad_number)));
				}
			//}
			//else{
				//$product->setCelebrityAdNumber(serialize(array($celebrity_id => $ad_number)));
			//}

			$product->save();
			
			$productsEntity->sku 	= trim($sku);				

			$productsEntity->update_status = 'success';

		}
		else{

			$productsEntity->sku 	= trim($sku);

			$productsEntity->update_status = 'error';

		}

		$products[]     = $productsEntity;

		



		return $products;

    }

}