<?php
$installer = $this;
$installer->startSetup();
$installer->run("
    ALTER TABLE `{$this->getTable('sales/order')}` ADD COLUMN `erp_update` VARCHAR(45) NOT NULL;
    ");
$installer->endSetup();