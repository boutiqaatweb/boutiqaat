<?php
class Ewall_Knet_Block_Info extends Mage_Payment_Block_Info
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('knet/info.phtml');
    }

    public function getMethodCode()
    {
        return $this->getInfo()->getMethodInstance()->getCode();
    }

    public function toPdf()
    {
        $this->setTemplate('knet/pdf/info.phtml');
        return $this->toHtml();
    }

    public function getKnetData()
    {
        return $this->getInfo()->getMethodInstance()->getKnetInfo();
    }
	
}