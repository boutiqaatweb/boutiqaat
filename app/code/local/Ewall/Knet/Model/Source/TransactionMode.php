<?php
class Ewall_Knet_Model_Source_TransactionMode
{
    public function toOptionArray()
    {
        $options =  array();       ;
        foreach (Mage::getSingleton('knet/config')->getTransactionModes() as $code => $name) {
            $options[] = array(
            	   'value' => $code,
            	   'label' => $name
            );
        }

        return $options;
    }
}