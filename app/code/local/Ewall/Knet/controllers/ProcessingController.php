<?php
class Ewall_Knet_ProcessingController extends Mage_Core_Controller_Front_Action
{

    protected $_order = NULL;
    protected $_paymentInst = NULL;

    /**
     * Get singleton of Checkout Session Model
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * when customer selects KNET payment method
     */
    public function redirectAction()
    {
        try {
            $session = $this->_getCheckout();

            $order = Mage::getModel('sales/order');
            $order->loadByIncrementId($session->getLastRealOrderId());
            if (!$order->getId()) {
                Mage::throwException('No order for processing found');
            }
            if ($order->getState() == Mage_Sales_Model_Order::STATE_NEW) {
                 $order->setState(Mage_Sales_Model_Order::STATE_NEW, true, Mage::helper('knet')->__('Customer was redirected to KNET.'));
                 $order->save();
            }

            if ($session->getQuoteId() && $session->getLastSuccessQuoteId()) {
                $session->setKnetQuoteId($session->getQuoteId());
                $session->setKnetSuccessQuoteId($session->getLastSuccessQuoteId());
                $session->setKnetRealOrderId($session->getLastRealOrderId());
                $session->getQuote()->setIsActive(false)->save();
                $session->clear();
            }

            $this->loadLayout();
            $this->renderLayout();
            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            $this->_debug('KNET error: ' . $e->getMessage());
            Mage::logException($e);
        }
        $this->_redirect('checkout/cart');
    }

  
    /**
     * KNET return action for success
     */
    public function responseAction()
    {
        try {
            // Retreive values from URL
            $knet_info = $this->getRequest()->getParams();
            $order_id = $knet_info['UDF1'];
            
            $session = $this->_getCheckout();
            $session->unsKnetRealOrderId();
            $session->setQuoteId($session->getKnetQuoteId(true));
            $session->setLastSuccessQuoteId($session->getKnetSuccessQuoteId(true));

             // Show message if security is breached
            if ($order_id  == "") {
                $session->addError(Mage::helper('knet')->__('The payment has been declined. Kindly contact the site administrator.'));
                $this->_redirect('checkout/cart');
                return;
            }
            
            //Write the response to the table
            $result = Mage::getModel('knet/knet')->load($order_id, 'order_id');
            $result->setData('paymentid',$knet_info['PaymentID']);
            $result->setData('result',$knet_info['Result']);
            $result->setData('postdate',$knet_info['PostDate']);
            $result->setData('tranid',$knet_info['TranID']);
            $result->setData('auth',$knet_info['Auth']);
            $result->setData('ref',$knet_info['Ref']);
            $result->setData('trackid',$knet_info['TrackID']);
            $result->setData('udf1',$knet_info['UDF1']);
            $result->save();

			$order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
            if($order->canInvoice()) {
                //START Handle Invoice
                $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                $invoice->register();
 
                $invoice->getOrder()->setCustomerNoteNotify(true);          
                $invoice->getOrder()->setIsInProcess(true);
                $order->addStatusHistoryComment('The payment has been registered successfully.', false);
 
                $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());
 
                $transactionSave->save();
                $order->sendNewOrderEmail();
                //END Handle Invoice
             }

             $this->_redirect('knet/processing/success');
             return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            $this->_debug('KNET error: ' . $e->getMessage());
            Mage::logException($e);
        }
        $this->_redirect('checkout/cart');
    }

    //KNET return action failure
    public function cancelAction()
    {
		// Retreive values from URL
        $knet_info = $this->getRequest()->getParams();
        $order_id = $knet_info['UDF1'];

        // set quote to active
        $session = $this->_getCheckout();
        if ($quoteId = $session->getKnetQuoteId()) {
            $quote = Mage::getModel('sales/quote')->load($quoteId);
            if ($quote->getId()) {
                $quote->setIsActive(true)->save();
                $session->setQuoteId($quoteId);
            }
        }

        // Show message if security is breached
        if ($order_id  == "") {
            $session->addError(Mage::helper('knet')->__('The payment has been declined. Kindly contact the site administrator.'));
            $this->_redirect('checkout/cart');
            return;
        }
		   
        //Write the response to the table
        $result = Mage::getModel('knet/knet')->load($order_id, 'order_id');
        $result->setData('paymentid',$knet_info['PaymentID']);
        $result->setData('result',$knet_info['Result']);
        $result->setData('postdate',$knet_info['PostDate']);
        $result->setData('tranid',$knet_info['TranID']);
        $result->setData('auth',$knet_info['Auth']);
        $result->setData('ref',$knet_info['Ref']);
        $result->setData('trackid',$knet_info['TrackID']);
        $result->setData('udf1',$knet_info['UDF1']);
        $result->save();
        
        $order = Mage::getModel('sales/order');
        $order->loadByIncrementId($order_id);
        $order->cancel(); 
        $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true)->save();
        
        $this->_redirect('knet/processing/failure');
    }

    //Display the failure page
    public function failureAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

 
    //Display the success page
    public function successAction()
    {
        $lastOrderId =  Mage::getSingleton('checkout/session')->getLastRealOrderId();
		$_obs = new Ewall_Erp_Model_Observer();
		$_obs->createOrderToErpForKnet($lastOrderId);
        $this->loadLayout();
        Mage::dispatchEvent('knet_processing_controller_success_action', array('order_ids' => array($lastOrderId)));
        $this->renderLayout();
    }

    protected function _getPendingPaymentStatus()
    {
        return Mage::helper('knet')->getPendingPaymentStatus();
    }

    //Log debug data to file
    protected function _debug($debugData)
    {
        if (Mage::getStoreConfigFlag('payment/knet_cc/debug')) {
            Mage::log($debugData, null, 'payment_knet_cc.log', true);
        }
    }
}