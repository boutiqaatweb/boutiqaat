<?php
class Ewall_Knetpay_Block_Info extends Mage_Payment_Block_Info
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('knetpay/info.phtml');
    }

    public function getMethodCode()
    {
        return $this->getInfo()->getMethodInstance()->getCode();
    }

    public function toPdf()
    {
        $this->setTemplate('knetpay/pdf/info.phtml');
        return $this->toHtml();
    }

    public function getKnetpayData()
    {
        return $this->getInfo()->getMethodInstance()->getKnetpayInfo();
    }
	
}