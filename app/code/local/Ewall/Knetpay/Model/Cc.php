<?php
class Ewall_Knetpay_Model_Cc extends Mage_Payment_Model_Method_Abstract
{

	protected $_code = 'knet_cc';

    protected $_paymentMethod			= 'cc';
    protected $_defaultLocale			= 'en';

    protected $_testUrl	= "https://secure.knet.dk/payment/transaction.ew";
    protected $_liveUrl	= "https://secure.knet.dk/payment/transaction.ew";

    protected $_formBlockType = 'knetpay/form';
    protected $_infoBlockType = 'knetpay/info';

    protected $_order;

    /**
     * Get order model
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
		if (!$this->_order) {
			$this->_order = $this->getInfoInstance()->getOrder();
		}
		return $this->_order;
    }

    public function getOrderPlaceRedirectUrl()
    {
          return Mage::getUrl('knetpay/processing/redirect');
    }

    public function getPaymentMethodType()
    {
        return $this->_paymentMethod;
    }

    public function getUrl()
    {
    	if ($this->getConfigData('transaction_mode') == 'live')
    		return $this->_liveUrl;
    	return $this->_testUrl;
    }
   
	//Get the payment information.
	public function getKnetpayInfo()
    {

		if($this->getOrder()) {
		$order_id = $this->getOrder()->getIncrementId(); 
		}
		
		$result = Mage::getModel('knetpay/knetpay')->load($order_id, 'order_id');
        return  $result;
    }

    protected function _debug($debugData)
    {
        if (method_exists($this, 'getDebugFlag')) {
            return parent::_debug($debugData);
        }

        if ($this->getConfigData('debug')) {
            Mage::log($debugData, null, 'payment_' . $this->getCode() . '.log', true);
        }
    }
}