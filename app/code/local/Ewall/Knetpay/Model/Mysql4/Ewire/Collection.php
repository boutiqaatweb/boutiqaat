<?php
class Ewall_Knetpay_Model_Mysql4_Knetpay_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
 {
     public function _construct()
     {
         parent::_construct();
         $this->_init('knetpay/knetpay');
     }
}