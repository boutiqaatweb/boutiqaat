<?php
class Ewall_Knetpay_Model_Source_TransactionMode
{
    public function toOptionArray()
    {
        $options =  array();       ;
        foreach (Mage::getSingleton('knetpay/config')->getTransactionModes() as $code => $name) {
            $options[] = array(
            	   'value' => $code,
            	   'label' => $name
            );
        }

        return $options;
    }
}