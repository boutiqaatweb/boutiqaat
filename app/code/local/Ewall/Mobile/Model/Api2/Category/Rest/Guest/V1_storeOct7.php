<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Category_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Category
{
	/**
	 * Retrieve list of category list.
	 *
	 * @return array
	 */
	protected function _retrieveCollection()
	{
	    $ruleId = $this->getRequest()->getParam('cat_id');

	    return $this->getTreeCategories($ruleId);

	}

	protected function getTreeCategories($parentId, $isChild){

		$_all_categories = Mage::getModel('catalog/category')->getCollection()
	                ->addAttributeToSelect('*')
	                ->addAttributeToFilter('is_active', true)
	                ->addAttributeToFilter('include_in_menu', true)
	                ->addAttributeToFilter('parent_id', array('eq' => $parentId));

	    $cur_category = array();
	    $node = array();
	    foreach ($_all_categories as $category)
	    {
	    	$subcats = $category->getChildren();
	    	$node['category_id'] = $category->getId();
		    $node['name'] = $category->getName();
		    $node['parent_id'] = $category->getParentId();
		    $node['child_id'] = $subcats;
		    $node['active'] = $category->getIsActive();
		    $node['level'] = $category->getLevel();
		    $node['position'] = $category->getPosition();

		    if($subcats == ''){
	            $node['children'] = $subcats;
	        }
	        else{
	        	$node['children'] = $this->getTreeCategories($category->getId(), true);
	        }
	        $cur_category[] = $node;
	    }

	    return $cur_category;
	}
}
