<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Celebrity_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Celebrity
{
	/**
	 * Retrieve list of celebrity list
	 *
	 * @return array
	 */
	protected function _retrieveCollection()
	{
        $post = $this->getRequest()->getParams();
		$categoryslider = array();
		$parentId = 95;
        $storeId = ($post['lang'] == 'ar') ? 3 : 1;
		$categories = Mage::getModel('catalog/category')
    			->getCollection()
                ->setStoreId($storeId)
    			->addAttributeToSelect('name')
    			->addAttributeToSelect('url')
    			->addAttributeToSelect('image')
    			->addAttributeToSelect('thumbnail')
    			->addAttributeToFilter('is_active', 1)
                ->addAttributeToFilter('parent_id', array('eq' => $parentId));
    			//->addFieldToFilter('entity_id', array("in" => $this->getCelebrityCategoryIds()));
    	foreach($categories as $category){
    		$slider = array();
            $slider['celebrity_id'] = $category->getId();
    		$slider['name'] = $category->getName();
    		$slider['url'] = $category->getUrl();
    		if($category->getThumbnail()){
	    		$ThumbnailUrl = Mage::getBaseUrl('media').'catalog/category/'.$category->getThumbnail();
	    		$slider['thumbnail'] = $ThumbnailUrl;
    		}
    		else{
    			$slider['thumbnail'] = '';	
    		}
    		if($category->getImage()){
	    		$ImageUrl = Mage::getBaseUrl('media').'catalog/category/'.$category->getImage();
	    		$slider['image'] = $ImageUrl;
    		}
    		else{
    			$slider['image'] = '';	
    		}
    		$categoryslider[] = $slider;
    	}
	    return $categoryslider;
	}
    public function getCelebrityCategoryIds()
    {
        $collection = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('celebrity')->addAttributeToFilter('is_celebrity', true);
        $_celebrity = array();
        foreach ($collection as $customer) {
            $_celebrity[] = $customer->getCelebrity();
        }
        return $_celebrity;
    }	
}
