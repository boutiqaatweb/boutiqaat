<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Countrycity_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Countrycity
{   
	protected function _retrieveCollection()
    {
        $response = array();
        $post = $this->getRequest()->getParams();
        $storeId = ($post['lang'] == 'ar') ? 3 : 1;
            try {
                if($storeId == 3){
                    $countries = array('KW' => 'الكويت');
                }
                else{
                    $countries = array('KW' => 'Kuwait');  
                }
                $storeIds = array(0,$storeId);
                $cities = Mage::getModel('checkoutdropdown/checkoutdropdown')->getCollection()
                        ->addFieldToFilter('store_id',array('in'=>$storeIds));
                foreach ($countries as $key=>$value) {
                	$combination[$key]['country_code'] = $key;
                	$combination[$key]['country_name'] = $value;
	                foreach ($cities as $city) {
	                	$combination[$key]['city'][$city['city_code']]['city_code'] = $city['city_code'];
	                	$combination[$key]['city'][$city['city_code']]['city_name'] = $city['default_name'];
	                	$blocks = Mage::getModel('checkoutdropdown/blockdropdown')->getCollection()
	                	->addFieldToFilter('city_code',$city['city_code'])->addFieldToFilter('store_id',array('in'=>$storeIds));
	                    foreach ($blocks as $block) {
	                        $combination[$key]['city'][$city['city_code']]['blocks'][$block['block_code']] = $block['default_name'];
	                    }
	                }
	            }
                $response['combination'] = array('country'=>$combination);
            }catch (Exception $e) {
            } 
        return $response;
    }
}
