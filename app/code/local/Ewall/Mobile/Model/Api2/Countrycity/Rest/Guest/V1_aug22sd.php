<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Countrycity_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Countrycity
{   
	protected function _retrieveCollection()
    {
        $response = array();
        //$post = $this->getRequest()->getParams();
        //if ( $post ) {
            try {
                // $postObject = new Varien_Object();
                // $postObject->setData($post);

                // $error = false;

                // if ($error) {
                //     throw new Exception();
                // }
                /*$response['country']['kw']['city']= 'city1';['city1']['block']['block1 for city1'];
                $response['country']['kw']['city']['city1']['block']['block2 for city1'];
                $response['country']['kw']['city']['city2']['block']['block1 for city2'];
                $response['country']['kw']['city']['city2']['block']['block2 for city2'];*/
                $country = array('KW' => 'Kuwait');
                $cities = array('city1', 'city2');
                $blocks = array('block1', 'block2');

                $combination = array();

                foreach ($country as $key => $value) {
                    foreach ($cities as $city) {
                        foreach ($blocks as $block) {
                            $combination[$key][$city][] = $block;
                        }
                    }                    
                }

                //$response['country'][] = $country;
                //$response['city'][] = $cities;
                //$response['block'][] = $block;
                $response['combination'][] = $combination;


                //$response['country'] = '10';
                return $response;

                $msgsuccess = Mage::helper('contacts')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.');
                $json = array('status' => 'Success', 'message' => $msgsuccess);
                echo json_encode($json);
                exit();
            } catch (Exception $e) {
                $msgerror = Mage::helper('contacts')->__('Unable to submit your request. Please, try again later');
                $json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
            }
        //} 
        return $response;
    }
}
