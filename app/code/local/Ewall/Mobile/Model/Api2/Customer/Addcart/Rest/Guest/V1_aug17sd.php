<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Addcart_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Addcart
{
   protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
            $customer_id = $post['customer_id'];
            $decrypt = Mage::getSingleton('core/encryption');
            $customer_id = base64_decode($decrypt->decrypt($customer_id));
            $customer = Mage::getSingleton('customer/customer')->load($customer_id);

            if($customer->getId()){
                $product_id = $post['product_id'];
                $qty = $post['quantity'];
                $cat = $post['category'];
                $productIds = explode(',', $product_id);
                $quantity = explode(',', $qty);
                $category = explode(',', $cat);
                try{
                    $quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('customer_id', $customer->getId())
                                ->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
                        if(!$quote->getId()){
                            // $cart = Mage::getModel('checkout/cart');
                            // $cart->init();
                            $quote = Mage::getModel('sales/quote');
                            $quote->setStoreId($storeId)
                                ->setCustomerId($customer->getId())
                                ->setIsActive(true)
                                ->setIsMultiShipping(false)
                                ->save();
                            //$quote = Mage::getModel("sales/quote")->loadByCustomer($customer);
                            //$quote->setCustomerId($customer->getId());
                        }
                        $quote->setCustomer($customer);
                        $quote->setStoreId(1);
                    $newqty = array();
                    for($i = 0; $i < count($productIds); $i++){
                       $nqty = isset($quantity[$i]) ? $quantity[$i] : 1;
                       $ncat = isset($category[$i]) ? $category[$i] : -1;
                       $newqty[$i] = $nqty.'_'.$ncat;
                    }
                    $items = array_combine($productIds,$newqty);
                    $dismessage = '';
                    foreach($items as $key=>$value){
                        $qtycat = explode('_', $value);
                        //$quote = Mage::getModel("sales/quote")->loadByCustomer($customer);
                        //$quote->setCustomerId($customer->getId());
                        
                        $product = Mage::getModel('catalog/product')->setStoreId(1)->load($key);
                        if($product->getStatus()==2){
                            if(count($items)==1){
                                $err = Mage::helper('customer')->__('The product %s is disabled', $product->getName());
                                throw new Exception($err);
                            }
                            else{
                                $dismessage .=',';
                                $dismessage .= Mage::helper('customer')->__('The product %s is disabled', $product->getName());
                                $dismessage .='';
                                continue;
                            }
                        }
                        //$product = Mage::getModel('catalog/product')->setStoreId(1)->load(2012);
                        $params = array(
                            'product' => $key,
                            'qty' => $qtycat[0],
                            'category'=> $qtycat[1],
                            'request_type'=>'restapi'
                        );
                        $request = new Varien_Object();
                        $request->setData($params);
                        $result = $quote->addProduct($product, $request);
                        //$quote->collectTotals()->save();
                        $quote->save();
                    }
                    $msgsuccess = Mage::helper('customer')->__('The product has been added to cart');
                    $msgsuccess .= $dismessage;
                    $itemDet = array();

                    $quoteFinal = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('customer_id', $customer->getId())
                    ->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
                    if($quoteFinal->getId()){
                       $quoteCollection = $quoteFinal->getAllVisibleItems();
                    
                    foreach ($quoteCollection as $_item)
                    {
                        $_product = Mage::getModel('catalog/product')->load($_item->getProductId());
                        $subtotal = $_item->getPrice() * $_item->getQty();
                        $itemDet[] = array('entity_id'=>$_item->getProductId(),
                                            'name' => $_item->getName(),
                                            'qty'=> $_item->getQty(),
                                            'image_url' => $_product->getImageUrl(),
                                            'price' => $_item->getPrice(),
                                            'subtotal' => number_format($subtotal, 4),
                                            );    
                    }
                    }
                    $info = array('price'=>$product->getPrice(), 'status' => 'Success', 'message' => $msgsuccess);
                    echo json_encode(array_merge($info, array('items'=>$itemDet)));
                    exit();
                }catch(Exception $e){
                    $msgerror = $e->getMessage().''.$dismessage;
                    $json = array('status' => 'error', 'message' => $msgerror);
                    echo json_encode($json);
                    exit();
                }
            }
            else{
                $msgerror = Mage::helper('customer')->__('Invalid customer');
                $json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
            }
        }
    }
}
