<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Addcart_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Addcart
{
   protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
            $customer_id = $post['customer_id'];
            $storeId = ($post['lang'] == 'ar') ? 3 : 1;
            Mage::app()->setCurrentStore($storeId);
            $decrypt = Mage::getSingleton('core/encryption');
            $customer_id = pack("H*", $customer_id);
            $customer_id = $decrypt->decrypt($customer_id);
            $customer = Mage::getSingleton('customer/customer')->load($customer_id);
            if($customer->getId()){
                $product_id = $post['product_id'];
                $qty = $post['quantity'];
                $cat = $post['category'];
                $quantity = array();
                $category = array();
                $productIds = array();
                $productIds = explode(',', $product_id);
                $quantity = explode(',', $qty);
                $category = explode(',', $cat);
                try{
                    $newqty = array();
                    for($i = 0; $i < count($productIds); $i++){
                       $nqty = isset($quantity[$i]) ? $quantity[$i] : 1;
                       $ncat = isset($category[$i]) ? $category[$i] : -1;
                       $npro = isset($productIds[$i]) ? $productIds[$i] : 0;
                       $newqty[$i] = $nqty.'_'.$ncat.'_'.$npro;
                    }
                    //$items = array_combine($productIds,$newqty);
                    $dismessage = '';

                    $quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('customer_id', $customer->getId())->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
                    // $quote = Mage::getModel('sales/quote')->setStore($store)->load($quote_by_customer->getId());
                    if(!$quote->getId()){
                        $quote = Mage::getModel('sales/quote');
                        $quote->setCustomerId($customer->getId());
                    }

                    $quote->assignCustomer($customer);
                    $quote->setStoreId($storeId);
                    $celebrity = Mage::helper('celebrity')->getCelebrityCategoryIds();
                    foreach($newqty as $key=>$value){
                        $qtycat = explode('_', $value);
                        if($qtycat[1] == '' || $qtycat[1]<=0 || (!in_array($qtycat[1], $celebrity))){
                            $qtycat[1] = -1;
                        }
                        if($qtycat[0] == ''){
                            $qtycat[0] = 1;
                        }
                        if($qtycat[2] == ''){
                            $qtycat[2] = 0;
                        }
                        $product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($qtycat[2]);
                        //get final price from celebrity
                        $data = unserialize($product->getCelebritySpecialPrice());
                        $cel_price = (isset($data[$qtycat[1]])) ? $data[$qtycat[1]] : $product->getFinalPrice();
                        if((!$product->getId()) || ($cel_price<=0)){
                            if(count($newqty)==1){
                                if($storeId == 3){
                                    $err = (!$product->getId()) ? Mage::helper('customer')->__('معرف المنتج غير صالح') : Mage::helper('customer')->__('لا يمكنك إضافة المنتج مع سعر الصفر');
                                }
                                else{
                                    $err = (!$product->getId()) ? Mage::helper('customer')->__('Invalid Product ID') : Mage::helper('customer')->__('Cannot add product with zero price');
                                }
                                throw new Exception($err);
                            }
                            continue;
                        }
                        if($product->getStatus()==2){
                            if(count($newqty)==1){
                                $err = ($storeId == 3) ? Mage::helper('customer')->__('تم تعطيل %s المنتج', $product->getName()) : Mage::helper('customer')->__('The product %s is disabled', $product->getName());
                                
                                throw new Exception($err);
                            }
                            else{
                                $dismessage .=',';
                                $dismessage .= ($storeId == 3) ? Mage::helper('customer')->__('تم تعطيل %s المنتج', $product->getName()) : Mage::helper('customer')->__('The product %s is disabled', $product->getName());
                                $dismessage .='';
                                continue;
                            }
                        }
                        //$product = Mage::getModel('catalog/product')->setStoreId(1)->load(2012);
                        $params = array(
                            'product' => (int)$qtycat[2],
                            'qty' => (int)$qtycat[0],
                            'category'=> (int)$qtycat[1],
                            'request_type'=>'restapi'
                        );
                        Mage::register('cat_id',(int)$qtycat[1]);
                        $request = new Varien_Object();
                        $request->setData($params);
                        $result = $quote->addProduct($product, $request);
                        Mage::unregister('cat_id');
                        
                    }
                    $quote->collectTotals()->save();
                    
                    $msgsuccess = ($storeId == 3) ? Mage::helper('customer')->__('تمت إضافة المنتج إلى سلة التسوق') : Mage::helper('customer')->__('The product has been added to cart');
                    $msgsuccess .= $dismessage;
                    $itemDet = array();

                    
                    if($quote->getId()){
                       $quoteCollection = $quote->getAllVisibleItems();
                       //get celebrities children
                        $category = Mage::getModel('catalog/category')->load(95);
                        $celebrityCat = explode(',', $category->getChildren());
                        $categoryIds = Mage::helper('celebrity')->getCelebrityCategoryIds();

                        foreach ($quoteCollection as $_item)
                        {
                            $_product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($_item->getProductId());
                            $splname = ((in_array($_item['category_id'], $celebrity) || in_array($_item['category_id'], $celebrityCat)) && $_product->getSpecialName()) ? $_product->getSpecialName() : $_product->getName();
                            $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
                            $subtotal = $_item->getPrice() * $_item->getQty();
                            $itemDet[] = array('item_id'=>$_item->getId(),
                                                'entity_id'=>$_item->getProductId(),
                                                'name' => $splname,
                                                'short_description'=>$_product->getShortDescription(),
                                                'quantity'=> $_item->getQty(),
                                                'image_url' => $_product->getImageUrl(),
                                                'regular_price' => $_product->getPrice(),
                                                'final_price' => $_item->getPrice(),
                                                'is_saleable'=> $stock->getIsInStock(),
                                                'subtotal' => number_format($subtotal, 4),
                                                );    
                        }
                    }
                    $info = array('status' => 'Success', 'message' => $msgsuccess);
                    echo json_encode(array_merge($info, array('items'=>$itemDet)));
                    exit();
                }catch(Exception $e){
                    $msgerror = $e->getMessage().''.$dismessage;
                    $json = array('status' => 'error', 'message' => $msgerror);
                    echo json_encode($json);
                    exit();
                }
            }
            else{
                $msgerror = ($storeId == 3) ? Mage::helper('customer')->__('العميل غير صالح') : Mage::helper('customer')->__('Invalid customer');
                $json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
            }
        }
    }
}
