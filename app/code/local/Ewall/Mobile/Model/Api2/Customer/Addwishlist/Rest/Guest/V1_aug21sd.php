<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Addwishlist_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Addwishlist
{
   protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
        	$customer_id = $post['customer_id'];
            $decrypt = Mage::getSingleton('core/encryption');
            $customer_id = base64_decode($decrypt->decrypt($customer_id));
		    $customer = Mage::getSingleton('customer/customer')->load($customer_id);

		    if($customer->getId()){
                $product_id = $post['product_id'];
                $qty = $post['qty'];
                $cat = $post['category'];
                $quantity = array();
                $productIds = explode(',', $product_id);
                $quantity = explode(',', $qty);
                $category = explode(',', $cat);
		    	try{
                    $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($customer->getId(), true);
                    //$wishlist = Mage::getModel('wishlist/wishlist')->getCollection()->addFieldToFilter('customer_id', $customer->getId())->getFirstItem();
                    //$wishlist = Mage::getModel('wishlist/wishlist')->load(22);
                    $newqty = array();
                    for($i = 0; $i < count($productIds); $i++){
                       //$newqty[$i] = isset($quantity[$i]) ? $quantity[$i] : 1;
                       $nqty = isset($quantity[$i]) ? $quantity[$i] : 1;
                       $ncat = isset($category[$i]) ? $category[$i] : -1;
                       $newqty[$i] = $nqty.'_'.$ncat;
                    }
                    $items = array_combine($productIds,$newqty);
                    foreach($items as $key=>$value){
                        $qtycat = explode('_', $value);
                        $product = Mage::getModel('catalog/product')->setStoreId(1)->load($key);
                        $buyRequest = new Varien_Object(array('qty'=>$qtycat[0],'category'=>$qtycat[1]));
                        $item = $wishlist->addNewItem($product, $buyRequest);
                        //$wishlist->_addCatalogProduct($product,2,1);
                        //$item->setQty(5);
                        //$item->save();
                        //$wishlist->save();

                    }
                    $wishlist->save();
                    //Mage::helper('wishlist')->calculate();
                    //$count = count($wishlist);
                    $msgsuccess = Mage::helper('customer')->__('The product has been added to wishlist');
                    $wish = Mage::getModel('wishlist/wishlist')->loadByCustomer($customer->getId(), true);
                    $wishlistCollection = $wish->getItemCollection();
                    $itemDet = array();
                    foreach($wishlistCollection as $_item){
                        $_product = Mage::getModel('catalog/product')->load($_item->getProductId());
                        $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
                        $itemDet[] = array('entity_id'=>$_product->getId(), 
                                            'short_description' => $_product->getShortDescription(),
                                            'name' => $_product->getName(),
                                            'qty'=> $_item->getQty(),
                                            'image_url' => $_product->getImageUrl(),
                                            'regular_price' => $_product->getPrice(),
                                            'final_price' => number_format($_product->getFinalPrice(),4),
                                            'is_saleable' => $stock->getIsInStock(),
                                            );    
                    }
                    $info = array('status' => 'Success', 'message' => $msgsuccess);
                    echo json_encode(array_merge($info, array('items'=>$itemDet)));
                    exit();
			    }catch(Exception $e){
			    	$msgerror = $e->getMessage();
			    	$json = array('status' => 'error', 'message' => $msgerror);
                    echo json_encode($json);
                    exit();
			    }
	        }
	        else{
	        	$msgerror = Mage::helper('customer')->__('Invalid customer');
		    	$json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
	        }
    	}
    }
}