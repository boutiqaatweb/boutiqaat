<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Cart_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Cart
{
    
	protected function _retrieveCollection()
    {
        $post = $this->getRequest()->getParams();
        if ($post) {
        	$customer_id = $post['customer_id'];
        	$cart = array();
        	$decrypt = Mage::getSingleton('core/encryption');
            $customer_id = base64_decode($decrypt->decrypt($customer_id));
		    $customer = Mage::getModel('customer/customer')->load($customer_id);
		    if($customer->getId()){
				//$quote = Mage::getModel('sales/quote')->loadByCustomer($customer, true);
				$quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('customer_id', $customer_id)
				->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
				$address = $quote->getShippingAddress();
				if($quote){
					//$quoteCollection = $quote->getItemsCollection(false);
					$quoteCollection = $quote->getAllVisibleItems();
				}
				if(!$quoteCollection){
					$error = array();
		        	$message = Mage::helper('customer')->__("You have no items in your shopping cart");
		        	$error['status'] = "error";
		        	$error['message'] = $message;
		        	$cart[] = $error;
	             	return $cart;
				}
			    foreach ($quoteCollection as $_item)
			    {
			    	$_product = Mage::getModel('catalog/product')->load($_item->getProductId());
			    	$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
			        $products = array();
			        $subtotal = $_item->getPrice() * $_item->getQty();
			        $products['item_id'] = $_item->getId();
			        $products['entity_id'] = $_item->getProductId();
			        $products['short_description'] = $_product->getShortDescription();
			        $products['name'] = $_item->getName();
			        $options = $_item->getProduct()->getTypeInstance(true)->getOrderOptions($_item->getProduct());
				    $customOptions = $options['options'];
				    if(!empty($customOptions))
				    {
				    	$customOption = array();
						foreach ($customOptions as $option)
						{
						    $_custom_options[] = array('label' => $option['label'], 'value' => $option['value']);
						}
						$products['custom_options'] = array('options' => $_custom_options);
				    }
			        $products['image_url'] = $_product->getImageUrl();
			        $products['regular_price'] = $_product->getPrice();
                    $products['final_price'] = $_item->getPrice();
			        $products['quantity'] = $_item->getQty();
			        $products['subtotal'] = number_format($subtotal, 4);
			        $products['is_saleable'] = $stock->getIsInStock();
			        $cart[] = $products;
			    }
			    $subTotal = $quote->getSubtotal();
				$grandTotal = $quote->getGrandTotal();
				$discount = $address->getDiscountAmount();
				$tax = $address->getTaxAmount();
				$shipp = $address->getShippingAmount();
				$totals['subtotal'] = $subTotal;
				if($tax != 0){
					$totals['tax'] = $tax;
				}
				if($shipp != 0){
					$ship = array();
					$ship['amount'] = $address->getShippingAmount();
					$ship['method'] = $address->getShippingMethod();
					$totals['shipping'][] = $ship;
				}
				if($discount != 0){
					$coupon = array();
					$coupon['discount_amount'] = $address->getDiscountAmount();
					$coupon['coupon_code'] = $quote->getCouponCode();
					$totals['discount'][] = $coupon;
				}
				$totals['grandtotal'] = $grandTotal;
				$total = array();
				$total[] = $totals;
				if(count($quoteCollection)){
					$cart[]['totals'] = $total;
				}
			    return $cart;
	        }
	        else{
	        	$error = array();
	        	$message = Mage::helper('customer')->__("Enter valid customer");
	        	$error['status'] = "error";
	        	$error['message'] = $message;
	        	$cart[] = $error;
             	return $cart;   
	        }
    	}
    }
}
