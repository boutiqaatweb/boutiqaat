<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Cartstock_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Cartstock
{
   protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
            $customer_id = $post['customer_id'];
            $storeId = ($post['lang'] == 'ar') ? 3 : 1;
            Mage::app()->setCurrentStore($storeId);
            $item_id = $post['item_id'];
            $decrypt = Mage::getSingleton('core/encryption');
            $customer_id = pack("H*", $customer_id);
            $customer_id = $decrypt->decrypt($customer_id);
            $customer = Mage::getSingleton('customer/customer')->load($customer_id);
            if($customer->getId()){
                $product_id = $post['product_id'];
                $qty = $post['quantity'];
                $quantity = array();
                $productIds = explode(',', $product_id);
                $ordered_qty = explode(',', $qty);
                try{
                        $quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('customer_id', $customer->getId())
                                    ->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
                        $cartarr = array();                                
                        for($i = 0; $i < count($productIds); $i++){
                            $cartarr[$productIds[$i]] = (isset($cartarr[$productIds[$i]]) ? $cartarr[$productIds[$i]] : 0) + 
                            isset($ordered_qty[$i]) ? $ordered_qty[$i] : 0;
                        }
                        $newQty = array();
                        foreach ($cartarr as $key => $value) {
                            $_product = Mage::getModel('catalog/product')->load($key);
                            $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
                            if($stock->getQty() >= $value){
                                continue;
                            }
                            else{
                                $newQty[$key] = $stock->getQty();
                            }
                        }
                        $quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('customer_id', $customer->getId())
                                ->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
                        $quoteCollection = $quote->getAllVisibleItems();
                        $priceArr = array();
                        foreach ($quoteCollection as $_item) {
                            $priceArr[$_item->getId()]['item_id'] = $_item->getId();
                            $priceArr[$_item->getId()]['price'] = $_item->getPrice();
                            $priceArr[$_item->getId()]['category_id'] = $_item->getCategoryId();
                            $price[] = $_item->getPrice();
                        }
                        array_multisort($price, SORT_ASC, $priceArr);
                        //get celebrities children
                        $category = Mage::getModel('catalog/category')->load(95);
                        $celebrityCat = explode(',', $category->getChildren());
                        $categoryIds = Mage::helper('celebrity')->getCelebrityCategoryIds();
                        foreach ($priceArr as $_item) {
                            $item = $quote->getItemById($_item['item_id']);
                            $productId = $item->getProductId();
                            $_product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($productId);
                            //get current price for the product
                            $splname = ((in_array($_item['category_id'], $celebrity) || in_array($_item['category_id'], $celebrityCat)) && $_product->getSpecialName()) ? $_product->getSpecialName() : $_product->getName();
                            $data = unserialize($_product->getCelebritySpecialPrice());
                            $cel_price = (isset($data[$_item['category_id']])) ? $data[$_item['category_id']] : $_product->getFinalPrice();
                            if($cel_price <= 0){
                                $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
                                $zeroprice[] = array('item_id'=>$item->getId(),
                                                    'entity_id'=>$productId,
                                                    'short_description'=>$_product->getShortDescription(),
                                                    'name' => $splname,
                                                    'image_url' => $_product->getImageUrl(),
                                                    'quantity'=>$item->getQty(),
                                                    'regular_price'=>$_product->getPrice(),
                                                    'final_price'=>$cel_price,
                                                    'subtotal' => number_format($subtotal,4),
                                                    'is_saleable'=> $stock->getIsInStock()
                                                    );
                                $newQty[$productId] = $newQty[$productId] - $item->getQty();
                                $quote->removeItem($item->getId())->save();
                                continue;
                            }
                            if(isset($newQty[$productId])){
                                if($newQty[$productId]>=$item->getQty()){
                                    $newQty[$productId] = $newQty[$productId] - $item->getQty();
                                }
                                else{
                                    if($newQty[$productId]>=1){
                                        $out_qty = $item->getQty() - $newQty[$productId];
                                        $subtotal = $out_qty * $item->getPrice();
                                        $outofstock[] = array('item_id'=>$item->getId(),
                                                        'entity_id'=>$productId,
                                                        'short_description'=>$_product->getShortDescription(),
                                                        'name' => $splname,
                                                        'image_url' => $_product->getImageUrl(),
                                                        'quantity'=>$out_qty,
                                                        'regular_price'=>$_product->getPrice(),
                                                        'final_price'=>$item->getPrice(),
                                                        'subtotal' => number_format($subtotal,4),
                                                        'is_saleable'=>0
                                                        );
                                        $item->setQty($newQty[$productId]);
                                        $item->save();
                                    }
                                    else{
                                        $out_qty = $item->getQty() - $newQty[$productId];
                                        $subtotal = $out_qty * $item->getPrice();
                                        $outofstock[] = array('item_id'=>$item->getId(),
                                                        'entity_id'=>$productId,
                                                        'short_description'=>$_product->getShortDescription(),
                                                        'name' => $splname,
                                                        'image_url' => $_product->getImageUrl(),
                                                        'quantity'=>$out_qty,
                                                        'regular_price'=>$_product->getPrice(),
                                                        'final_price'=>$item->getPrice(),
                                                        'subtotal' => number_format($subtotal,4),
                                                        'is_saleable'=>0
                                                        );
                                        $quote->removeItem($item->getItemId())->save();
                                    }
                                    $newQty[$productId] = 0;   
                                }
                            }
                        }
                        $quote->collectTotals()->save();
                        $msgsuccess = ($storeId == 3) ? Mage::helper('customer')->__("تم تحديث هذا البند الأسهم") : Mage::helper('customer')->__("The stock item has been updated");
                        $itemDet = array();
                        $quoteFinal = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('customer_id', $customer->getId())
                        ->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
                        if($quoteFinal->getId()){
                           $quoteCollection = $quoteFinal->getAllVisibleItems();
                            if(!count($quoteCollection)){
                                $msgerror = ($storeId == 3) ? Mage::helper('customer')->__("لا يوجد لديك عناصر في سلة التسوق الخاصة بك") : Mage::helper('customer')->__("You have no items in your shopping cart");
                                $json = array('status' => 'error', 'message' => $msgerror);
                                if(!empty($zeroprice)){
                                   $json = array_merge($json, array('zeroprice_items'=>$zeroprice));
                                }
                                echo json_encode($json);
                                exit();
                            }
                            foreach ($quoteCollection as $_item)
                            {
                                $_product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($_item->getProductId());
                                $splname = ((in_array($_item['category_id'], $celebrity) || in_array($_item['category_id'], $celebrityCat)) && $_product->getSpecialName()) ? $_product->getSpecialName() : $_product->getName();
                                $subtotal = $_item->getPrice() * $_item->getQty();
                                $itemDet[] = array('item_id'=>$_item->getId(), 'entity_id'=>$_item->getProductId(),
                                                    'short_description'=>$_product->getShortDescription(),
                                                    'name' => $splname,
                                                    'image_url' => $_product->getImageUrl(),
                                                    'regular_price' => $_product->getPrice(),
                                                    'final_price' => $_item->getPrice(),
                                                    'quantity'=> $_item->getQty(),
                                                    'subtotal' => $_item->getRowTotal(),
                                                    'is_saleable'=>1
                                                    );
                            }
                        }
                        $shipmethods = Mage::getSingleton('shipping/config')->getActiveCarriers();
                        $options = array();
                        foreach($shipmethods as $carrierCode => $carrierModel){                            
                           if( $carrierMethods = $carrierModel->getAllowedMethods() )
                           {
                               foreach ($carrierMethods as $methodCode => $method)
                               {    
                                    $code = $carrierCode.'_'.$methodCode;
                               }
                               $carrierTitle = Mage::getStoreConfig('carriers/'.$carrierCode.'/title', $storeId);
                               $price = Mage::getStoreConfig('carriers/'.$carrierCode.'/price');
                               $options[] = array('code'=>$code,"title"=>$carrierTitle,"shipping_price"=>$price);
                           }
                        }
                        $primaryShipping = $customer->getDefaultShippingAddress();
                        $addressId = $customer->getDefaultShipping();
                        $address_detail = array();
                            if(!empty($primaryShipping)){
                                //$region = ($primaryShipping->getRegionId()!="") ? $primaryShipping->getRegionId() : $primaryShipping->getRegion();
                                $address_detail = array('customer_address_id'=>$addressId, 'firstname' => $primaryShipping->getFirstname(),
                                'lastname' => $primaryShipping->getLastname(),
                                'company' => $primaryShipping->getCompany(),
                                'country_id' => $primaryShipping->getCountryId(),
                                'city' => $primaryShipping->getCity(),
                                'addr_block' => $primaryShipping->getAddrBlock(),
                                'addr_villa' => $primaryShipping->getAddrVilla(),
                                'addr_avenue' => $primaryShipping->getAddrAvenue(),
                                'addr_street' => $primaryShipping->getAddrStreet(),
                                'addr_flatenumber' => $primaryShipping->getAddrFlatenumber(),
                                'addr_floornumber' => $primaryShipping->getAddrFloornumber(),
                                //'state' => $region,
                                'address_line_1' => $primaryShipping->getStreet1(),
                                'postcode' => $primaryShipping->getPostcode(),
                                'telephone'=> $primaryShipping->getTelephone(),
                                'addr_landline' => $primaryShipping->getAddrLandline(),
                                'fax' => $primaryShipping->getFax());
                            }
                        $info = array('quote_id'=>$quoteFinal->getId(),'status' => 'Success', 'message' => $msgsuccess);
                        echo json_encode(array_merge($info, array('items'=>$itemDet),array('outofstock_items'=>$outofstock),array('zeroprice_items'=>$zeroprice),array('shipping_address'=>$address_detail),array('shipping_method'=>$options)));
                        exit();
                }catch(Exception $e){
                    $msgerror = $e->getMessage();
                    $json = array('status' => 'error', 'message' => $msgerror);
                    echo json_encode($json);
                    exit();
                }
            }
            else{
                $msgerror = ($storeId == 3) ? Mage::helper('customer')->__('العميل غير صالح') : Mage::helper('customer')->__('Invalid customer');
                $json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
		}	
	   }
       }
}
