<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
require_once 'Ewall/Knet/Block/e24PaymentPipe.inc.php';
class Ewall_Mobile_Model_Api2_Customer_Createorder_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Createorder
{
   protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
        	$customer_id = $post['customer_id'];
            $decrypt = Mage::getSingleton('core/encryption');
            $customer_id = pack("H*", $customer_id);
         	$customer_id = $decrypt->decrypt($customer_id);
		    $customer = Mage::getSingleton('customer/customer')->load($customer_id);
		    if($customer->getId()){
                $quote_id = $post['quote_id'];
                $shipping_address_id = $post['shipping_address_id'];
                $shipping_method = $post['shipping_method'];
                //$shipping_amount = $post['shipping_amount'];
                $payment = $post['payment'];
                
		    	try{
		    		$store = Mage::getModel('core/store')->load(1);
                    $quote = Mage::getModel('sales/quote')->setStore($store)->load($quote_id);
                      
                    if($quote->getId() && $quote->getIsActive()){//&& $quote->getIsActive()
                    	$address = Mage::getModel('customer/address')->load($shipping_address_id);
                    	$billingAddress = $quote->getBillingAddress()->addData($address->getData());
                    	$shippingAddress = $quote->getShippingAddress()->addData($address->getData());
                    	$shippingAddress->setCollectShippingRates(true)
						                ->collectShippingRates()
						                ->setShippingMethod($shipping_method)
						                //->setShippingAmount($shipping_amount)
                						//->setBaseShippingAmount($shipping_amount)
						                ->setPaymentMethod($payment);
						$quote->getPayment()->importData(array('method' => $payment));
						$quote->collectTotals()->save();
						$service = Mage::getModel('sales/service_quote', $quote);
						$service->submitAll();
						$increment_id = $service->getOrder()->getRealOrderId();
						$order_id = $service->getOrder()->getId();
						$quote->setIsActive(0)->save();
						$quote = $customer = $service = null;
						$order = Mage::getModel("sales/order")->load($order_id);
						//$order = Mage::getModel("sales/order")->load(324);
						$order->sendNewOrderEmail();
						//for knet payment only
						$pay = array();
						if($post['payment']=='knet_cc'){
				        	$base_dir = Mage::getBaseDir();
				        	$trackid_rand = rand(11111111,99999999);
				        	$price  = $order->getGrandTotal();
				        	$Pipe = new e24PaymentPipe;
					        $Pipe->setAction(1);
					        $Pipe->setCurrency(414);
					        $Pipe->setLanguage('ENG'); //change it to "ARA" for arabic language
					        // $Pipe->setResponseURL("http://www.boutiqaat.com/testknet/response.php"); 
					        // $Pipe->setErrorURL("http://www.boutiqaat.com/testknet/error.php");
					        $Pipe->setResponseURL("http://www.mobile.com/success.php"); 
					        $Pipe->setErrorURL("http://www.mobile.com/error.php");
					        $Pipe->setAmt($price);
					        //$Pipe->setResourcePath("/Applications/MAMP/htdocs/php-toolkit/resource/");
					        //$Pipe->setResourcePath($base_dir."/knet/");
					        $Pipe->setResourcePath("/var/www/vhosts/boutiqaat.com/httpdocs/testknet/resource/");
					        $Pipe->setAlias('bout');

					        $Pipe->setTrackId($trackid_rand);
					        $Pipe->setUdf1($increment_id);
					        $Pipe->setUdf2("");
					        $Pipe->setUdf3("");
					        $Pipe->setUdf4("");
					        $Pipe->setUdf5("");
					        
					        //get results
					        if($Pipe->performPaymentInitialization()!=$Pipe->SUCCESS){

					            $data = array('order_id'=>$increment_id,'amount'=>$price, 'paymentid'=>'', 'result'=>'','postdate'=>'','tranid'=>'','auth'=>'','ref'=>'', 'trackid' =>$trackid_rand, 'udf1' =>$increment_id, 'udf2' =>'', 'udf3' =>'', 'udf4' =>'', 'udf5' =>'');
					            $model = Mage::getModel('knet/knet')->setData($data);
					            $model->save();
					            //$this->_debug($Pipe->getErrorMsg());
					            $errpaymsg = Mage::helper('checkout')->__("There is an issue with your payment process. Kindly contact the site administrator.");
					            $pay = array('payment_status'=>'error','payment_message'=>$errpaymsg);
					        } else {
					            $payID = $Pipe->getPaymentId();
					            $payURL = $Pipe->getPaymentPage();
					            //$payURL = 'www.google.com';

					            $data = array('order_id'=>$increment_id,'amount'=>$price, 'paymentid'=>$payID, 'result'=>'','postdate'=>'','tranid'=>'','auth'=>'','ref'=>'', 'trackid' =>$trackid_rand, 'udf1' =>$increment_id, 'udf2' =>'', 'udf3' =>'', 'udf4' =>'', 'udf5' =>'');
					            $model = Mage::getModel('knet/knet')->setData($data);
					            $model->save();
					            $location = $payURL."?PaymentID=".$payID;
					            $pay = array('payment_status'=>'success','payment_url'=>$location);
					        }
				        }
				        // burgan payment functionality
				        if($post['payment']=='burgan_cc'){
				        	$base_dir = Mage::getBaseDir();
				        	$trackid_rand = rand(11111111,99999999);
				        	$price  = $order->getGrandTotal();
				        	$Pipe = new e24PaymentPipe;
					        $Pipe->setAction(1);
					        $Pipe->setCurrency(414);
					        $Pipe->setLanguage('ENG'); //change it to "ARA" for arabic language
					        //$Pipe->setResponseURL("http://www.boutiqaat.com/testburgan/response.php"); 
					        //$Pipe->setErrorURL("http://www.boutiqaat.com/testburgan/error.php");
					        $Pipe->setResponseURL("http://www.mobile.com/success.php"); 
					        $Pipe->setErrorURL("http://www.mobile.com/error.php");
					        $Pipe->setAmt($price);
					        //$Pipe->setResourcePath("/Applications/MAMP/htdocs/php-toolkit/resource/");
					        //$Pipe->setResourcePath($base_dir."/knet/");
					        $Pipe->setResourcePath("/var/www/vhosts/boutiqaat.com/httpdocs/testburgan/resource/");
					        $Pipe->setAlias('t2802');

					        $Pipe->setTrackId($trackid_rand);
					        $Pipe->setUdf1($increment_id);
					        $Pipe->setUdf2("");
					        $Pipe->setUdf3("");
					        $Pipe->setUdf4("");
					        $Pipe->setUdf5("");
					        
					        //get results
					        if($Pipe->performPaymentInitialization()!=$Pipe->SUCCESS){

					            $data = array('order_id'=>$increment_id,'amount'=>$price, 'paymentid'=>'', 'result'=>'','postdate'=>'','tranid'=>'','auth'=>'','ref'=>'', 'trackid' =>$trackid_rand, 'udf1' =>$increment_id, 'udf2' =>'', 'udf3' =>'', 'udf4' =>'', 'udf5' =>'');
					            $model = Mage::getModel('burgan/burgan')->setData($data);
					            $model->save();
					            //$this->_debug($Pipe->getErrorMsg());
					            $errpaymsg = Mage::helper('checkout')->__("There is an issue with your payment process. Kindly contact the site administrator.");
					            $pay = array('payment_status'=>'error','payment_message'=>$errpaymsg);
					        } else {
					            $payID = $Pipe->getPaymentId();
					            $payURL = $Pipe->getPaymentPage();
					            //$payURL = 'www.google.com';

					            $data = array('order_id'=>$increment_id,'amount'=>$price, 'paymentid'=>$payID, 'result'=>'','postdate'=>'','tranid'=>'','auth'=>'','ref'=>'', 'trackid' =>$trackid_rand, 'udf1' =>$increment_id, 'udf2' =>'', 'udf3' =>'', 'udf4' =>'', 'udf5' =>'');
					            $model = Mage::getModel('burgan/burgan')->setData($data);
					            $model->save();
					            $location = $payURL."?PaymentID=".$payID;
					            $pay = array('payment_status'=>'success','payment_url'=>$location);
					        }
				        }
						$increment_id = $order->getRealOrderId();
						$subTotal = $order->getSubtotal();
						$grandTotal = $order->getGrandTotal();
						$shipping_amount = $order->getShippingAmount();
						$discount = $order->getDiscountAmount();
						$tax = $order->getTaxAmount();
						$orderItems = $order->getItemsCollection();
						$billingAddr = $order->getBillingAddress();
						$shippingAddr = $order->getShippingAddress();
						$shipping_method = $order->getShippingDescription();
						$payment = $order->getPayment()->getMethodInstance()->getTitle();
						$billing = array('billing_address'=> array('customer_address_id'=>$billingAddr->getCustomerAddressId(),
                    			'firstname' => $billingAddr->getFirstname(),
			                    'lastname' => $billingAddr->getLastname(),
			                    'company' => $billingAddr->getCompany(),
			                    'country_id' => $billingAddr->getCountryId(),
			                    'city' => $billingAddr->getCity(),
			                    'addr_block' => $billingAddr->getAddrBlock(),
			                    'addr_villa' => $billingAddr->getAddrVilla(),
			                    'addr_avenue' => $billingAddr->getAddrAvenue(),
			                    'addr_street' => $billingAddr->getAddrStreet(),
			                    'addr_flatenumber' => $billingAddr->getAddrFlatenumber(),
			                    'addr_floornumber' => $billingAddr->getAddrFloornumber(),
			                    //'state' => $region,
			                    'address_line_1' => $billingAddr->getStreet1(),
			                    'postcode' => $billingAddr->getPostcode(),
			                    'telephone'=> $billingAddr->getTelephone(),
			                    'addr_landline' => $billingAddr->getAddrLandline(),
			                    'fax' => $billingAddr->getFax())
						);
						$shipping = array('shipping_address'=> array('customer_address_id'=>$shippingAddr->getCustomerAddressId(),
                    			'firstname' => $shippingAddr->getFirstname(),
			                    'lastname' => $shippingAddr->getLastname(),
			                    'country_id' => $shippingAddr->getCountryId(),
			                    'company' => $shippingAddr->getCompany(),
			                    'city' => $shippingAddr->getCity(),
			                    'addr_block' => $shippingAddr->getAddrBlock(),
			                    'addr_villa' => $shippingAddr->getAddrVilla(),
			                    'addr_avenue' => $shippingAddr->getAddrAvenue(),
			                    'addr_street' => $shippingAddr->getAddrStreet(),
			                    'addr_flatenumber' => $shippingAddr->getAddrFlatenumber(),
			                    'addr_floornumber' => $shippingAddr->getAddrFloornumber(),
			                    //'state' => $region,
			                    'address_line_1' => $shippingAddr->getStreet1(),
			                    'postcode' => $shippingAddr->getPostcode(),
			                    'telephone'=> $shippingAddr->getTelephone(),
			                    'addr_landline' => $shippingAddr->getAddrLandline(),
			                    'fax' => $shippingAddr->getFax())
						);
						$itemDet = array();
						foreach ($orderItems as $_item)
                    	{
                        $itemDet[] = array('item_id'=>$_item->getId(),
                                            'entity_id'=>$_item->getProductId(),
                                            'name' => $_item->getName(),
                                            'quantity'=> $_item->getQtyOrdered(),
                                            'price' => $_item->getPrice(),
                                            'subtotal' => $_item->getRowTotal(),
                                            );    
                    	}
						$total = array();
	                    $msgsuccess = Mage::helper('customer')->__('The order has been saved successfully');
	                    $info = array('status' => 'Success', 'message' => $msgsuccess, 
	                    			'order_id'=>$increment_id, 
	                    			'order_date'=>$order->getCreatedAt(),
	                    			'order_status'=>$order->getStatus(),
	                    			'shipping_method'=>$shipping_method,
	                    			'payment_method'=>$payment);
	                    $total = array('subtotal'=>$subTotal,'shipping'=>$shipping_amount,'grandtotal' => $grandTotal);
	                    if($discount!=0){
	                    	$total[] = $discount;
	                    }
	                    if($tax != 0){
	                    	$total[] = $tax;
	                    }
	                    $json = array_merge($info, $billing, $shipping, array('ordered_items'=>$itemDet), array('total'=>$total));
	                    if(!empty($pay)){
	                    	$json = array_merge($json, $pay);
	                    }
	                    echo json_encode($json);
	                    exit();
	                }
	                else{
	                	$msgerror = Mage::helper('checkout')->__('Invalid Quote Id');
				    	$json = array('status' => 'error', 'message' => $msgerror);
	                    echo json_encode($json);
	                    exit();	
	                }
			    }catch(Exception $e){
			    	$msgerror = $e->getMessage();
			    	$json = array('status' => 'error', 'message' => $msgerror);
                    echo json_encode($json);
                    exit();
			    }
	        }
	        else{
	        	$msgerror = Mage::helper('customer')->__('Invalid customer');
		    	$json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
	        }
    	}
    }
}