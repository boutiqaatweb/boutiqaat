<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Defaultshipping_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Defaultshipping
{
    
	protected function _create()
    {
    	
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
        	$customer_id = $post['customer_id'];
        	$decrypt = Mage::getSingleton('core/encryption');
			$customer_id = pack("H*", $customer_id);
         	$customer_id = $decrypt->decrypt($customer_id);
        	$address_id = $post['address_id'];
        	$shipping = array();
		    $customer = Mage::getSingleton('customer/customer')->load($customer_id);
		    if($customer->getId()){
				$address = Mage::getModel('customer/address')->load($address_id);
				if(($address->getEntityId()>0) && ($address->getParentId()==$customer->getId())){
					$address->setIsDefaultShipping(1);
					$address->save();
					$message = Mage::helper('customer')->__('The Address has been updated.');
	                $json = array('status' => 'success', 'message' => $message);
	                echo json_encode($json);
	                exit();
				}
				else{
					$message = Mage::helper('customer')->__('Invalid Address ID');
	                $json = array('status' => 'error', 'message' => $message);
	                echo json_encode($json);
	                exit();
				}
	        }
	        else{
	        	$message = Mage::helper('customer')->__('Invalid Customer');
                $json = array('status' => 'error', 'message' => $message);
                echo json_encode($json);
                exit();   
	        }
    	}
    }
}
