<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Editprofile_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Editprofile
{
    public function _create()
    {
        $requestData = $this->getRequest()->getBodyParams();
        if ($requestData) {
            $customer_id = $requestData['customer_id'];
            $customer = Mage::getSingleton('customer/customer')->load($customer_id);
            if($customer->getId()){
                $errors = array();
                //If password change was requested then add it to common validation scheme
                if ($requestData['change_password']) {
                    $currPass   = $requestData['current_password'];
                    $newPass    = $requestData['password'];
                    $confPass   = $requestData['cpassword'];

                    $oldPass = $customer->getPasswordHash();
                    if ( Mage::helper('core/string')->strpos($oldPass, ':')) {
                        list($_salt, $salt) = explode(':', $oldPass);
                    } else {
                        $salt = false;
                    }

                    if ($customer->hashPassword($currPass, $salt) == $oldPass) {
                        if (strlen($newPass)) {
                            $customer->setPassword($newPass);
                            $customer->setPasswordConfirmation($confPass);
                        } else {
                            $errors[] = Mage::helper('customer')->__('New password field cannot be empty.');
                        }
                    } else {
                        $errors[] = Mage::helper('customer')->__('Invalid current password');
                    }
                }
                $customer->setFirstname($requestData['first_name'])
                        ->setLastname($requestData['last_name'])
                        ->setEmail($requestData['email'])
                        ->setMobile($requestData['mobile'])
                        ->setIsSubscribed($requestData['newsletter_subscription']);
                $customerErrors = $customer->validate();
                if (is_array($customerErrors)) {
                   $errors = array_merge($errors, $customerErrors);
                }
                if (!empty($errors)) {
                    $errmsg = implode(",", $errors);
                    $json = array('status' => 'error', 'message' => $errmsg);
                    echo json_encode($json);
                    exit();
                }
                try {
                    $street  = array();
                    $street[0] = $requestData['street_1'];
                    $street[1] = $requestData['street_2'];
                    $address = $customer->getPrimaryBillingAddress();
                    $addressData =  array (
                            'firstname' => $requestData['first_name'],
                            'lastname' => $requestData['last_name'],
                            'street' => $street,
                            'city' => $requestData['city'],
                            'region' => $requestData['area'],
                            'country_id' => $requestData['country_id'],
                            'telephone' => $requestData['landline'],
                            'is_default_billing' => 1,
                            'is_default_shipping' => 1,
                        );
                    $address->addData($addressData);
                    $address->setCustomerId($customer->getId());
                    
                    // $addressErrors = $address->validate();
                    // if ($addressErrors !== true) {
                    //     $errors = array_merge($errors, $addressErrors);
                    // }
                    // if (!empty($errors)) {
                    //     $errmsg = implode(" ", $errors);
                    //     $json = array('status' => 'error', 'message' => $errmsg);
                    //     echo json_encode($json);
                    //     exit();  
                    // }
                    // else{
                    //     $customer->save();
                    //     $address->save();    
                    // }
                    if (empty($errors)) {
                        $customer->save();
                        $address->save();
                    }
                    $json = array('status' => 'Success',
                            'message'=>Mage::helper('customer')->__('The account information has been saved.')
                            );
                    echo json_encode($json);
                    exit();
                }catch (Mage_Core_Exception $e) {
                    $json = array('status' => 'error', 'message' => Mage::helper('customer')->__($e->getMessage()));
                    echo json_encode($json);
                    exit();
                }catch (Exception $e) {
                    $json = array('status' => 'error', 'message' => Mage::helper('customer')->__('Cannot save the customer.'));
                    echo json_encode($json);
                    exit();
                }
            }
            else{
                $json = array('status' => 'error', 'message' => 'Customer not valid');
                echo json_encode($json);
                exit();
            }
        }
    }
}
