<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Login_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Login
{   
    
	protected function _retrieve()
    {
        $response = array();
        $post = $this->getRequest()->getParams();
        if ( $post ) {
            try {
                $email = $post['email'];
                $password = $post['password'];

                $error = false;

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                    $message = Mage::Helper('customer')->__('Invalid Email Address');
                }

                if ($error) {
                    throw new Exception();
                }
                $websiteId = 1;
                $customer = Mage::getModel('customer/customer')
                            ->setWebsiteId($websiteId);
                try {
                    //$customer->loadByEmail($email);
                    if ($customer->authenticate($email, $password)) {
                        //$session->setCustomerAsLoggedIn($customer);
                        //encrypt customer id
                        $encrypt = Mage::getSingleton('core/encryption');
                        $customer_id = $encrypt->encrypt($customer->getId());
                        //decrypt customer id
                        //$org_id = $encrypt->decrypt($customer_id);
                        $response['customer_id'] = $customer_id;
                        $response['login_status'] = 'Success';
                        $response['message'] = Mage::helper('contacts')->__('Customer login details has been verified successfully.');    
                    }
                    else{
                        $error = true;    
                    }
                    
                }catch(Exception $e){
                    $error = true;
                    $message = $e->getMessage();

                }
            if ($error) {
                throw new Exception();
            }
                
            } catch (Exception $e) {

                $response['customer_id'] = '';
                $response['login_status'] = 'error';
                $response['message'] = Mage::helper('customer')->__($message);
            }
        } 
        return $response;
    }
}
