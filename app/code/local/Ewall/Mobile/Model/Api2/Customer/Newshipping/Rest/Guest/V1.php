<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Newshipping_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Newshipping
{
    
	protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
        	$customer_id = $post['customer_id'];
            $decrypt = Mage::getSingleton('core/encryption');
            //$customer_id = base64_decode($decrypt->decrypt($customer_id));
            $customer_id = pack("H*", $customer_id);
            $customer_id = $decrypt->decrypt($customer_id);
        	$shipping = array();
		    $customer = Mage::getSingleton('customer/customer')->load($customer_id);
		    if($customer->getId()){
		    	$customerAddress = Mage::getModel('customer/address');
                $defshipping = $customer->getDefaultShipping();
		    	try{
		    	    $details  = array();
                    $details[0] = $post['address_line_1'];
                    $details[1] = $post['address_line_2'];
                    $post['street'] = $details;
                    if(!$defshipping){
                        $post['is_default_shipping'] = 1;
                    }
                    $address = Mage::getModel("customer/address");
                    /*
                    $addressData =  array (
                            'firstname' => $post['first_name'],
                            'lastname' => $post['last_name'],
                            'street' => $details,
                            'city' => $post['city'],
                            'region' => $post['region'],
                            'region_id' => $post['region_id'],
                            'addr_block' => $post['block'],
                            'addr_villa' => $post['villa'],
                            'addr_avenue' => $post['avenue'],
                            'addr_flatenumber' => $post['flatnumber'],
                            'addr_floornumber' => $post['floornumber'],
                            'country_id' => $post['country_id'],
                            'addr_landline' => $post['landline'],
                            'addr_street' => $post['street'],
                            'telephone' => $post['mobile'],
                            'company' => $post['company'],
                            'postcode' => $post['postcode'],
                            'is_default_shipping' => 1,
                        );
                    */
                    $address->addData($post);
                    $customer->addAddress($address);
                    $address->setCustomerId($customer->getId());
                    $customer->save();
                    $address->save();
                    $msgsuccess = Mage::helper('customer')->__('The Address has been saved');
                    /* send back all existing shipping address begin */
                    //get primary shipping address
                    $primaryShipping = $customer->getDefaultShippingAddress();
                    $custAddress = array();
                    if(isset($primaryShipping['entity_id'])) {
                        $custAddress[] = array (
                            'customer_address_id'=> $primaryShipping->getId(),
                            'firstname' => $primaryShipping->getFirstname(),
                            'lastname' => $primaryShipping->getLastname(),
                            'address_line_1' => $primaryShipping->getStreet1() ? $primaryShipping->getStreet1() : '',
                            'city' => $primaryShipping->getCity(),
                            'addr_block' => $primaryShipping->getAddrBlock() ? $primaryShipping->getAddrBlock() : '',
                            'addr_villa' => $primaryShipping->getAddrVilla() ? $primaryShipping->getAddrVilla() : '',
                            'addr_avenue' => $primaryShipping->getAddrAvenue() ? $primaryShipping->getAddrAvenue() : '',
                            'addr_flatenumber' => $primaryShipping->getAddrFlatenumber() ? $primaryShipping->getAddrFlatenumber() : '',
                            'addr_floornumber' => $primaryShipping->getAddrFloornumber() ? $primaryShipping->getAddrFloornumber() : '',
                            'country_id' => $primaryShipping->getCountryId(),
                            'addr_landline' => $primaryShipping->getAddrLandline() ? $primaryShipping->getAddrLandline() : '',
                            'addr_street' => $primaryShipping->getAddrStreet() ? $primaryShipping->getAddrStreet() : '',
                            'telephone' => $primaryShipping->getTelephone() ? $primaryShipping->getTelephone() : '',
                            'company' => $primaryShipping->getCompany(),
                            'postcode' => $primaryShipping->getPostcode(),
                            'is_default_shipping' => $primaryShipping->getIsDefaultShipping(),
                            'fax' => $primaryShipping->getFax(),
                        );
                    }
                    //get additional shipping address
                    if(count($customer->getAdditionalAddresses())){
                        foreach($customer->getAdditionalAddresses() as $address1){
                            $custAddress[] = array (
                            'customer_address_id'=> $address1->getId(),
                            'firstname' => $address1->getFirstname(),
                            'lastname' => $address1->getLastname(),
                            'address_line_1' => $address1->getStreet1() ? $address1->getStreet1() : '',
                            'city' => $address1->getCity(),
                            'addr_block' => $address1->getAddrBlock() ? $address1->getAddrBlock() : '',
                            'addr_villa' => $address1->getAddrVilla() ? $address1->getAddrVilla() : '',
                            'addr_avenue' => $address1->getAddrAvenue() ? $address1->getAddrAvenue() : '',
                            'addr_flatenumber' => $address1->getAddrFlatenumber() ? $address1->getAddrFlatenumber() : '',
                            'addr_floornumber' => $address1->getAddrFloornumber() ? $address1->getAddrFloornumber() : '',
                            'country_id' => $address1->getCountryId(),
                            'addr_landline' => $address1->getAddrLandline() ? $address1->getAddrLandline() : '',
                            'addr_street' => $address1->getAddrStreet() ? $address1->getAddrStreet() : '',
                            'telephone' => $address1->getTelephone() ? $address1->getTelephone() : '',
                            'company' => $address1->getCompany(),
                            'postcode' => $address1->getPostcode(),
                            'is_default_shipping' => $address1->getIsDefaultShipping(),
                            'fax' => $address1->getFax(),
                        );
                        }
                    }
                    $json = array('status' => 'Success', 'message' => $msgsuccess, 'shipping_address'=> $custAddress);
                    echo json_encode($json);
                    exit();
			    }catch(Exception $e){
			    	$msgerror = $e->getMessage();
			    	$json = array('status' => 'error', 'message' => $msgerror);
                    echo json_encode($json);
                    exit();
			    }
	        }
	        else{
	        	$msgerror = Mage::helper('customer')->__('Invalid customer');
		    	$json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
	        }
    	}
    }
}
