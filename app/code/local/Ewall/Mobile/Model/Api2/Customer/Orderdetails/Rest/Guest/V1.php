<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer orderdetails
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Orderdetails_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Orderdetails
{
   protected function _retrieveCollection()
    {
        $post = $this->getRequest()->getParams();
        if ($post) {
        	$customer_id = $post['customer_id'];
        	Mage::app()->setCurrentStore(1);
            $decrypt = Mage::getSingleton('core/encryption');
            //$customer_id = base64_decode($decrypt->decrypt($customer_id));
            $customer_id = pack("H*", $customer_id);
            $customer_id = $decrypt->decrypt($customer_id);
		    $customer = Mage::getSingleton('customer/customer')->load($customer_id);
		    if($customer->getId()){
                $orderIncrementId = $post['order_id'];
                $response = array();
                $result = array();
		    	try{
		    		$order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
                    if($order->getId() && ($order->getCustomerId()==$customer->getId())){
						$increment_id = $order->getRealOrderId();
						$subTotal = $order->getSubtotal();
						$grandTotal = $order->getGrandTotal();
						$shipping_amount = $order->getShippingAmount();
						$discount = $order->getDiscountAmount();
						$tax = $order->getTaxAmount();
						$orderItems = $order->getItemsCollection();
						$billingAddr = $order->getBillingAddress();
						$shippingAddr = $order->getShippingAddress();
						$shipping_method = $order->getShippingDescription();
						$payment = $order->getPayment()->getMethodInstance()->getTitle();
						//get order info
						$response['status'] = 'Success';
	                    //$response['order_id'] = $order->getId();
	                    $response['order_id'] = $increment_id;
	                    $response['order_date'] = $order->getCreatedAt();//Formated('short');
	                    $response['order_status'] = $order->getStatus();
	                    $response['shipping_method'] = $shipping_method;
	                    $response['payment_method'] = $payment;
	                    //get billing and shipping address
						$response['billing_address'] = array('customer_address_id'=>$billingAddr->getCustomerAddressId(),
                    			'firstname' => $billingAddr->getFirstname(),
			                    'lastname' => $billingAddr->getLastname(),
			                    'company' => $billingAddr->getCompany(),
			                    'country_id' => $billingAddr->getCountryId(),
			                    'city' => $billingAddr->getCity(),
			                    'addr_block' => $billingAddr->getAddrBlock(),
			                    'addr_villa' => $billingAddr->getAddrVilla(),
			                    'addr_avenue' => $billingAddr->getAddrAvenue(),
			                    'addr_street' => $billingAddr->getAddrStreet(),
			                    'addr_flatenumber' => $billingAddr->getAddrFlatenumber(),
			                    'addr_floornumber' => $billingAddr->getAddrFloornumber(),
			                    //'state' => $region,
			                    'address_line_1' => $billingAddr->getStreet1(),
			                    'postcode' => $billingAddr->getPostcode(),
			                    'telephone'=> $billingAddr->getTelephone(),
			                    'addr_landline' => $billingAddr->getAddrLandline(),
			                    'fax' => $billingAddr->getFax()
						);
						$response['shipping_address'] = array('customer_address_id'=>$shippingAddr->getCustomerAddressId(),
                    			'firstname' => $shippingAddr->getFirstname(),
			                    'lastname' => $shippingAddr->getLastname(),
			                    'country_id' => $shippingAddr->getCountryId(),
			                    'company' => $shippingAddr->getCompany(),
			                    'city' => $shippingAddr->getCity(),
			                    'addr_block' => $shippingAddr->getAddrBlock(),
			                    'addr_villa' => $shippingAddr->getAddrVilla(),
			                    'addr_avenue' => $shippingAddr->getAddrAvenue(),
			                    'addr_street' => $shippingAddr->getAddrStreet(),
			                    'addr_flatenumber' => $shippingAddr->getAddrFlatenumber(),
			                    'addr_floornumber' => $shippingAddr->getAddrFloornumber(),
			                    //'state' => $region,
			                    'address_line_1' => $shippingAddr->getStreet1(),
			                    'postcode' => $shippingAddr->getPostcode(),
			                    'telephone'=> $shippingAddr->getTelephone(),
			                    'addr_landline' => $shippingAddr->getAddrLandline(),
			                    'fax' => $shippingAddr->getFax()
						);
	                    //ordered_items
						foreach ($orderItems as $_item)
                    	{
                        $response['ordered_items'][] = array('item_id'=>$_item->getId(),
                                            'entity_id'=>$_item->getProductId(),
                                            'name' => $_item->getName(),
                                            'quantity'=> $_item->getQtyOrdered(),
                                            'price' => $_item->getPrice(),
                                            'subtotal' => $_item->getRowTotal(),
                                            );    
                    	}
						$total = array();
	                    $total = array('subtotal'=>$subTotal,'shipping'=>$shipping_amount,'grandtotal' => $grandTotal);
	                    if($discount != 0){
	                    	$total[] = $discount;
	                    }
	                    if($tax != 0){
	                    	$total[] = $tax;
	                    }
	                    foreach ($total as $key => $value) {
							$response['total'][$key] = $value;	                    	
	                    }
	                    $result[] = $response;
	                    return $result;
	                }
	                else{
	                	$msgerror = Mage::helper('checkout')->__('Invalid Order or Customer Id');
				    	$response[] = array('status' => 'error', 'message' => $msgerror);
				    	return $response;
	                }
			    }catch(Exception $e){
			    	$msgerror = $e->getMessage();
			    	$response[] = array('status' => 'error', 'message' => $msgerror);
			    	return $response;
			    }
	        }
	        else{
	        	$msgerror = Mage::helper('customer')->__('Invalid customer');
		    	$response[] = array('status' => 'error', 'message' => $msgerror);
                return $response;
	        }
    	}
    }
}