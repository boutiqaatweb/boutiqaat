<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Orderdetails_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Orderdetails
{
   protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
        	$customer_id = $post['customer_id'];
            $decrypt = Mage::getSingleton('core/encryption');
            $customer_id = base64_decode($decrypt->decrypt($customer_id));
		    $customer = Mage::getSingleton('customer/customer')->load($customer_id);
		    if($customer->getId()){
                $order_id = $post['order_id'];
		    	try{
		    		$order = Mage::getModel("sales/order")->load($order_id);
                    if($order->getId() && ($order->getCustomerId()==$customer->getId())){
						$increment_id = $order->getRealOrderId();
						$subTotal = $order->getSubtotal();
						$grandTotal = $order->getGrandTotal();
						$shipping_amount = $order->getShippingAmount();
						$discount = $order->getDiscountAmount();
						$tax = $order->getTaxAmount();
						$orderItems = $order->getItemsCollection();
						$billingAddr = $order->getBillingAddress();
						$shippingAddr = $order->getShippingAddress();
						$shipping_method = $order->getShippingDescription();
						$payment = $order->getPayment()->getMethodInstance()->getTitle();
						$billing = array('billing_address'=> array('customer_address_id'=>$billingAddr->getCustomerAddressId(),
                    			'firstname' => $billingAddr->getFirstname(),
			                    'lastname' => $billingAddr->getLastname(),
			                    'country' => $billingAddr->getCountryId(),
			                    'city' => $billingAddr->getCity(),
			                    'addr_block' => $billingAddr->getAddrBlock(),
			                    'addr_villa' => $billingAddr->getAddrVilla(),
			                    'addr_avenue' => $billingAddr->getAddrAvenue(),
			                    'addr_street' => $billingAddr->getAddrStreet(),
			                    'addr_flatenumber' => $billingAddr->getAddrFlatenumber(),
			                    'addr_floornumber' => $billingAddr->getAddrFloornumber(),
			                    //'state' => $region,
			                    'address_line_1' => $billingAddr->getStreet1(),
			                    'mobile'=> $billingAddr->getTelephone(),
			                    'landline' => $billingAddr->getAddrLandline())
						);
						$shipping = array('shipping_address'=> array('customer_address_id'=>$shippingAddr->getCustomerAddressId(),
                    			'firstname' => $shippingAddr->getFirstname(),
			                    'lastname' => $shippingAddr->getLastname(),
			                    'country' => $shippingAddr->getCountryId(),
			                    'city' => $shippingAddr->getCity(),
			                    'addr_block' => $shippingAddr->getAddrBlock(),
			                    'addr_villa' => $shippingAddr->getAddrVilla(),
			                    'addr_avenue' => $shippingAddr->getAddrAvenue(),
			                    'addr_street' => $shippingAddr->getAddrStreet(),
			                    'addr_flatenumber' => $shippingAddr->getAddrFlatenumber(),
			                    'addr_floornumber' => $shippingAddr->getAddrFloornumber(),
			                    //'state' => $region,
			                    'address_line_1' => $shippingAddr->getStreet1(),
			                    'mobile'=> $shippingAddr->getTelephone(),
			                    'landline' => $shippingAddr->getAddrLandline())
						);
						$itemDet = array();
						foreach ($orderItems as $_item)
                    	{
                        $itemDet[] = array('item_id'=>$_item->getId(),
                                            'entity_id'=>$_item->getProductId(),
                                            'name' => $_item->getName(),
                                            'quantity'=> $_item->getQtyOrdered(),
                                            'price' => $_item->getPrice(),
                                            'subtotal' => $_item->getRowTotal(),
                                            );    
                    	}
						$total = array();
	                    //$msgsuccess = Mage::helper('customer')->__('The order has been saved successfully');
	                    $info = array('status' => 'Success', 'order_id'=>$order_id, 
	                    			'increment_id'=>$increment_id, 
	                    			'order_date'=>$order->getCreatedAtFormated('short'),
	                    			'order_status'=>$order->getStatus(),
	                    			'shipping_method'=>$shipping_method,
	                    			'payment_method'=>$payment);
	                    $total = array('subtotal'=>$subTotal,'shipping'=>$shipping_amount,'grandtotal' => $grandTotal);
	                    if($discount != 0){
	                    	$total[] = $discount;
	                    }
	                    if($tax != 0){
	                    	$total[] = $tax;
	                    }
	                    $json = array_merge($info, $billing, $shipping, array('ordered_items'=>$itemDet), array('total'=>$total));
	                    echo json_encode($json);
	                    exit();
	                }
	                else{
	                	$msgerror = Mage::helper('checkout')->__('Invalid Order or Customer Id');
				    	$json = array('status' => 'error', 'message' => $msgerror);
	                    echo json_encode($json);
	                    exit();	
	                }
			    }catch(Exception $e){
			    	$msgerror = $e->getMessage();
			    	$json = array('status' => 'error', 'message' => $msgerror);
                    echo json_encode($json);
                    exit();
			    }
	        }
	        else{
	        	$msgerror = Mage::helper('customer')->__('Invalid customer');
		    	$json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
	        }
    	}
    }
}