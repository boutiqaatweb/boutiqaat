<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Orderhistory_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Orderhistory
{
   protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
        	$customer_id = $post['customer_id'];
            $decrypt = Mage::getSingleton('core/encryption');
            $customer_id = base64_decode($decrypt->decrypt($customer_id));
		    $customer = Mage::getSingleton('customer/customer')->load($customer_id);
		    if($customer->getId()){
                $orders = Mage::getResourceModel('sales/order_collection')
				            ->addFieldToSelect('*')
				            ->addFieldToFilter('customer_id', $customer->getId())
				            ->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
				            ->setOrder('created_at', 'desc');
		    	try{
		    		$orderDet = array();
		    		foreach($orders as $_order){
			    		$order = Mage::getModel("sales/order")->load($_order->getId());
	                    if($order->getId() && ($order->getCustomerId()==$customer->getId())){
							$increment_id = $order->getRealOrderId();
							$grandTotal = $order->getGrandTotal();
							$shippingAddr = $order->getShippingAddress();
							$ordered_date = Mage::getModel('core/date')->date('m/d/Y', strtotime($order->getCreatedAt()));
							$shipping = array('shipping_address'=> array('customer_address_id'=>$shippingAddr->getCustomerAddressId(),
	                    			'firstname' => $shippingAddr->getFirstname(),
				                    'lastname' => $shippingAddr->getLastname(),
				                    'country' => $shippingAddr->getCountryId(),
				                    'city' => $shippingAddr->getCity(),
				                    'addr_block' => $shippingAddr->getAddrBlock(),
				                    'addr_villa' => $shippingAddr->getAddrVilla(),
				                    'addr_avenue' => $shippingAddr->getAddrAvenue(),
				                    'addr_street' => $shippingAddr->getAddrStreet(),
				                    'addr_flatenumber' => $shippingAddr->getAddrFlatenumber(),
				                    'addr_floornumber' => $shippingAddr->getAddrFloornumber(),
				                    //'state' => $region,
				                    'address_line_1' => $shippingAddr->getStreet1(),
				                    'mobile'=> $shippingAddr->getTelephone(),
				                    'landline' => $shippingAddr->getAddrLandline())
							);
		                    $info = array('order_id'=>$order->getId(), 
		                    			'increment_id'=>$increment_id, 
		                    			'order_date'=>$ordered_date,
		                    			'order_status'=>$order->getStatus(),
		                    			'grandtotal' => $grandTotal);
		                    $orderDet[] = array_merge($info, $shipping);
		                }
		                else{
		                	$msgerror = Mage::helper('checkout')->__('Invalid Order or Customer Id');
					    	$json = array('status' => 'error', 'message' => $msgerror);
		                    echo json_encode($json);
		                    exit();	
		                }
		            }
		            $msg = array('status'=>'Success'); 
		            $results = array_merge($msg, array('order_history'=>$orderDet));
                    echo json_encode($results);
                    exit();
			    }catch(Exception $e){
			    	$msgerror = $e->getMessage();
			    	$json = array('status' => 'error', 'message' => $msgerror);
                    echo json_encode($json);
                    exit();
			    }
	        }
	        else{
	        	$msgerror = Mage::helper('customer')->__('Invalid customer');
		    	$json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
	        }
    	}
    }
}