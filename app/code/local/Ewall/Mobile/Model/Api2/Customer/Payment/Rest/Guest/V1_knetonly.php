<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Payment_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Payment
{
   protected function _create()
    {
        $knet_info = $this->getRequest()->getBodyParams();
        //increment_id as order_id
        $order_id = $knet_info['order_id'];
        if ($order_id  == "") {
            $msgerror = Mage::helper('customer')->__('The payment has been declined. Kindly contact the site administrator.');
            $json = array('status' => 'error', 'message' => $msgerror);
            echo json_encode($json);
            exit();
        }
        if ($knet_info) {
	    	try{
                //Write the response to the table
                $result = Mage::getModel('knet/knet')->load($order_id, 'order_id');
                $result->setData('paymentid',$knet_info['paymentid']);
                $result->setData('result',$knet_info['result']);
                $result->setData('postdate',$knet_info['postdate']);
                $result->setData('tranid',$knet_info['tranid']);
                $result->setData('auth',$knet_info['auth']);
                $result->setData('ref',$knet_info['ref']);
                $result->setData('trackid',$knet_info['trackid']);
                $result->setData('udf1',$knet_info['udf1']);
                $result->setData('udf2',$knet_info['udf2']);
                $result->setData('udf3',$knet_info['udf3']);
                $result->setData('udf4',$knet_info['udf4']);
                $result->setData('udf5',$knet_info['udf5']);
                $result->save();
                $order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
                if($knet_info['result'] == 'CAPTURED'){
                    if($order->canInvoice()) {
                        //Start Handle Invoice
                        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                        $invoice->register();
         
                        $invoice->getOrder()->setCustomerNoteNotify(true);          
                        $invoice->getOrder()->setIsInProcess(true);
                        $order->addStatusHistoryComment('The payment has been registered successfully.', false);
         
                        $transactionSave = Mage::getModel('core/resource_transaction')
                            ->addObject($invoice)
                            ->addObject($invoice->getOrder());
         
                        $transactionSave->save();
                        $order->sendNewOrderEmail();
                        //End Handle Invoice
                    }
                    $increment_id = $order->getRealOrderId();
                    $subTotal = $order->getSubtotal();
                    $grandTotal = $order->getGrandTotal();
                    $shipping_amount = $order->getShippingAmount();
                    $discount = $order->getDiscountAmount();
                    $tax = $order->getTaxAmount();
                    $orderItems = $order->getItemsCollection();
                    $billingAddr = $order->getBillingAddress();
                    $shippingAddr = $order->getShippingAddress();
                    $shipping_method = $order->getShippingDescription();
                    $payment = $order->getPayment()->getMethodInstance()->getTitle();
                    $billing = array('billing_address'=> array('customer_address_id'=>$billingAddr->getCustomerAddressId(),
                            'firstname' => $billingAddr->getFirstname(),
                            'lastname' => $billingAddr->getLastname(),
                            'company' => $billingAddr->getCompany(),
                            'country_id' => $billingAddr->getCountryId(),
                            'city' => $billingAddr->getCity(),
                            'addr_block' => $billingAddr->getAddrBlock(),
                            'addr_villa' => $billingAddr->getAddrVilla(),
                            'addr_avenue' => $billingAddr->getAddrAvenue(),
                            'addr_street' => $billingAddr->getAddrStreet(),
                            'addr_flatenumber' => $billingAddr->getAddrFlatenumber(),
                            'addr_floornumber' => $billingAddr->getAddrFloornumber(),
                            //'state' => $region,
                            'address_line_1' => $billingAddr->getStreet1(),
                            'postcode' => $billingAddr->getPostcode(),
                            'telephone'=> $billingAddr->getTelephone(),
                            'addr_landline' => $billingAddr->getAddrLandline(),
                            'fax' => $billingAddr->getFax())
                    );
                    $shipping = array('shipping_address'=> array('customer_address_id'=>$shippingAddr->getCustomerAddressId(),
                            'firstname' => $shippingAddr->getFirstname(),
                            'lastname' => $shippingAddr->getLastname(),
                            'company' => $shippingAddr->getCompany(),
                            'country_id' => $shippingAddr->getCountryId(),
                            'city' => $shippingAddr->getCity(),
                            'addr_block' => $shippingAddr->getAddrBlock(),
                            'addr_villa' => $shippingAddr->getAddrVilla(),
                            'addr_avenue' => $shippingAddr->getAddrAvenue(),
                            'addr_street' => $shippingAddr->getAddrStreet(),
                            'addr_flatenumber' => $shippingAddr->getAddrFlatenumber(),
                            'addr_floornumber' => $shippingAddr->getAddrFloornumber(),
                            //'state' => $region,
                            'address_line_1' => $shippingAddr->getStreet1(),
                            'postcode' => $shippingAddr->getPostcode(),
                            'telephone'=> $shippingAddr->getTelephone(),
                            'addr_landline' => $shippingAddr->getAddrLandline(),
                            'fax' => $shippingAddr->getFax())
                    );
                    $itemDet = array();
                    foreach ($orderItems as $_item)
                    {
                    $itemDet[] = array('item_id'=>$_item->getId(),
                                        'entity_id'=>$_item->getProductId(),
                                        'name' => $_item->getName(),
                                        'quantity'=> $_item->getQtyOrdered(),
                                        'price' => $_item->getPrice(),
                                        'subtotal' => $_item->getRowTotal(),
                                        );    
                    }
                    $total = array();
                    $info = array('order_id'=>$increment_id, 
                            'order_date'=>$order->getCreatedAtFormated('short'),
                            'order_status'=>$order->getStatus(),
                            'shipping_method'=>$shipping_method,
                            'payment_method'=>$payment);
                    $total = array('subtotal'=>$subTotal,'shipping'=>$shipping_amount,'grandtotal' => $grandTotal);
                    if($discount!=0){
                        $total[] = $discount;
                    }
                    if($tax != 0){
                        $total[] = $tax;
                    }
                    $msgsuccess = Mage::helper('customer')->__('The payment has been updated successfully');
                    $def = array('status' => 'Success', 'message' => $msgsuccess);
                    $json = array_merge($def, $info, $billing, $shipping, array('ordered_items'=>$itemDet), array('total'=>$total));
                    echo json_encode($json);
                    exit();
                }else{
                    $order->cancel();
                    $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true)->save();
                    $msgerror = Mage::helper('customer')->__('The order has been cancelled. Kindly contact the site administrator.');
                    $json = array('status' => 'error', 'message' => $msgerror);
                    echo json_encode($json);
                    exit();
                }  
		    }catch(Exception $e){
		    	$msgerror = $e->getMessage();
		    	$json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
		    }
    	}
    }
}
