<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Register_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Register
{
    protected function getCustomerByEmail($email){
        $collection = Mage::getModel('customer/customer')->getCollection()
                    ->addFieldToFilter('email', $email);
        if(count($collection) == 0){
        	return true;
        }else{
        	return false;
        }
    }
    protected function createCustomer($data){
        $customer = Mage::getModel('customer/customer')
                    ->setEmail($data['email']);
                    
        $newPassword = $customer->generatePassword();
        $customer->setFirstname($data['first_name']);
        $customer->setLastname($data['last_name']);
        $customer->setPassword($newPassword);
        $customer->setWebsiteId(1);
        try{
            $customer->save();
        }catch(Exception $e){

        }
        
        $customer->sendPasswordReminderEmail();
        return $customer;
    }
	protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        $encrypt = Mage::getSingleton('core/encryption');
        if(!isset($post['password'])){
            $allow_registered = $this->getCustomerByEmail($post['email']);
            if($allow_registered){
                $data = $this->createCustomer($post);
                if($data->getId()) {
                    $json = array('customer_id' => $encrypt->encrypt($data->getId()), 'login_status' => 'sucess', 'message' => 'Customer successfully registered');
                    echo json_encode($json);
                    exit();
                }
            }else{
	            $json = array('customer_id' => '', 'login_status' => 'error', 'message' => 'There is already an account with this email address');
	            echo json_encode($json);
	            exit();	
	        }
        }
        if ($post) {

            try {  
                $error = false;
                $exists = 0;
                $message = array();
                if (!Zend_Validate::is(trim($post['first_name']) , 'NotEmpty')) {
                    $error = true;
                    $message[] = Mage::helper('customer')->__('Firstname is required.');
                }
                if (!Zend_Validate::is(trim($post['last_name']) , 'NotEmpty')) {
                    $error = true;
                    $message[] = Mage::helper('customer')->__('Lastname is required.');
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                    $message[] = Mage::helper('customer')->__('Invalid EmailAddress.');
                }
                if (!Zend_Validate::is(trim($post['password']) , 'NotEmpty')) {
                    $error = true;
                    $message[] = Mage::helper('customer')->__('Password is required.');
                }
                $website_id = 1;
                if ($error) {
                    throw new Exception();   
                }
                $customer = Mage::getModel('customer/customer');
                $customer->setWebsiteId($website_id);
                $customer->loadByEmail($post['email']);
                if(!$customer->getId()) {
                  // customer registration process
                    $customer->setEmail($post['email']); 
                    $customer->setFirstname($post['first_name']);
                    $customer->setLastname($post['last_name']);
                    $customer->setPassword($post['password']);
                    $customer->setWebsiteId($website_id);
                    $customer->setIsSubscribed($post['newsletter_subscription']);                                     
                }
                else{
                    $exists = 1;   
                }
                try{
                    $street  = array();
                    $street[0] = $post['street_1'];
                    $street[1] = $post['street_2'];
                    $address = Mage::getModel("customer/address");
                    $addressData =  array (
                            'firstname' => $post['first_name'],
                            'lastname' => $post['last_name'],
                            'street' => $street,
                            'city' => $post['city'],
                            'region' => $post['area'],
                            'country_id' => $post['country_id'],
                            'telephone' => $post['mobile'],
                            'addr_landline' => $post['landline'],
                            'is_default_billing' => 1,
                            'is_default_shipping' => 1,
                        );
                    $address->addData($addressData);
                    $customer->addAddress($address);
                    $customer->save();
                    $customer->setConfirmation(null);
                    $customer->save();
                    //encrypt customer id
                    if(!$exists){
                        $customer->sendNewAccountEmail();
                        $customer_id = $encrypt->encrypt($customer->getId());
                        $status = 'Success';
                        $msgsuccess = Mage::helper('customer')->__('Customer successfully registered');    
                        if($customer->getId()) {
                            $json = array('customer_id' => $customer_id , 'login_status' => $status, 'message' => $msgsuccess);
                            echo json_encode($json);
                            exit();
                        }
                    }
                    else{
                        throw new Exception();
                    }
                }catch(Exception $e){
                    if($exists){
                        $message[] = Mage::helper('customer')->__('There is already an account with this email address');
                    }
                    else{
                        $message[] = Mage::helper('customer')->__('Unable to create customer. Please, try again later');   
                    }
                    $error = true;
                }
                if ($error) {
                    throw new Exception();
                }
            } catch (Exception $e) {
                $msgerror = implode(" ", $message);
                $json = array('customer_id' => '', 'login_status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
            }
        }
    }
}