<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Shipping_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Shipping
{
    
	protected function _retrieveCollection()
    {
        $post = $this->getRequest()->getParams();
        if ($post) {
        	$customer_id = $post['customer_id'];
        	$decrypt = Mage::getSingleton('core/encryption');
         	//$customer_id = base64_decode($decrypt->decrypt($customer_id));
         	$customer_id = pack("H*", $customer_id);
            $customer_id = $decrypt->decrypt($customer_id);
        	$shipping = array();
		    $customer = Mage::getSingleton('customer/customer')->load($customer_id);
		    if($customer->getId()){
		    	//$customerAddress = Mage::getModel('customer/address');
				$shippingId = $customer->getDefaultShipping();
				$billingId = $customer->getDefaultBilling();
				$primaryShipping = $customer->getDefaultShippingAddress();
				$custAddress = array();
				$m = 0;
				//$custAddress[] = $primaryShipping;
				if(isset($primaryShipping['entity_id'])) {
					$custAddress['shipping_address']['shipping_address'][$m]['entity_id'] = $primaryShipping->getId();
					$custAddress['shipping_address']['shipping_address'][$m]['is_default_shipping'] = 1;
					$custAddress['shipping_address']['shipping_address'][$m]['firstname'] = $primaryShipping->getFirstname() ? $primaryShipping->getFirstname() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['lastname'] = $primaryShipping->getLastname() ? $primaryShipping->getLastname() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['company'] = $primaryShipping->getCompany() ? $primaryShipping->getCompany() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['country_id'] = $primaryShipping->getCountryId() ? $primaryShipping->getCountryId() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['city'] = $primaryShipping->getCity() ? $primaryShipping->getCity() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['addr_block'] = $primaryShipping->getAddrBlock() ? $primaryShipping->getAddrBlock() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['addr_villa'] = $primaryShipping->getAddrVilla() ? $primaryShipping->getAddrVilla() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['addr_avenue'] = $primaryShipping->getAddrAvenue() ? $primaryShipping->getAddrAvenue() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['addr_street'] = $primaryShipping->getAddrStreet() ? $primaryShipping->getAddrStreet() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['addr_flatenumber'] = $primaryShipping->getAddrFlatenumber() ? $primaryShipping->getAddrFlatenumber() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['addr_floornumber'] = $primaryShipping->getAddrFloornumber() ? $primaryShipping->getAddrFloornumber() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['address_line_1'] = $primaryShipping->getStreet1() ? $primaryShipping->getStreet1() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['telephone'] = $primaryShipping->getTelephone() ? $primaryShipping->getTelephone() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['addr_landline'] = $primaryShipping->getAddrLandline() ? $primaryShipping->getAddrLandline() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['postcode'] = $primaryShipping->getPostcode() ? $primaryShipping->getPostcode() : '';
			    	$custAddress['shipping_address']['shipping_address'][$m]['fax'] = $primaryShipping->getFax() ? $primaryShipping->getFax() : '';
			    	$m++;
				}
				if(count($customer->getAdditionalAddresses())){
					foreach($customer->getAdditionalAddresses() as $address1){
				    	//$custAddress[] = $address1->getData();
				    	$custAddress['shipping_address']['shipping_address'][$m]['entity_id'] = $address1->getId();
				    	$custAddress['shipping_address']['shipping_address'][$m]['is_default_shipping'] = 0;
				    	$custAddress['shipping_address']['shipping_address'][$m]['firstname'] = $address1->getFirstname() ? $address1->getFirstname() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['lastname'] = $address1->getLastname() ? $address1->getLastname() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['company'] = $address1->getCompany() ? $address1->getCompany() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['country_id'] = $address1->getCountryId() ? $address1->getCountryId() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['city'] = $address1->getCity() ? $address1->getCity() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['addr_block'] = $address1->getAddrBlock() ? $address1->getAddrBlock() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['addr_villa'] = $address1->getAddrVilla() ? $address1->getAddrVilla() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['addr_avenue'] = $address1->getAddrAvenue() ? $address1->getAddrAvenue() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['addr_street'] = $address1->getAddrStreet() ? $address1->getAddrStreet() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['addr_flatenumber'] = $address1->getAddrFlatenumber() ? $address1->getAddrFlatenumber() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['addr_floornumber'] = $address1->getAddrFloornumber() ? $address1->getAddrFloornumber() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['address_line_1'] = $address1->getStreet1() ? $address1->getStreet1() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['telephone'] = $address1->getTelephone() ? $address1->getTelephone() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['addr_landline'] = $address1->getAddrLandline() ? $address1->getAddrLandline() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['postcode'] = $address1->getPostcode() ? $address1->getPostcode() : '';
				    	$custAddress['shipping_address']['shipping_address'][$m]['fax'] = $address1->getFax() ? $address1->getFax() : '';
				    	$m++;
					}	
				}
				/*return $custAddress;
				$shipp = array();
				foreach($custAddress as $address){
					// $shipping['shipping_address'] = $address->getId();
					// return $shipping;
				    // if(($ShippingId != $BillingId) && ($address->getEntityId() == $BillingId)){
				    //     continue;
				    // }
				    $addressArr = array();
				    $addressArr['address_id'] = $address->getId();
				    if($address->getId() == $shippingId){
				    	$addressArr['is_default_shipping'] = 1;
					}
					else{
						$addressArr['is_default_shipping'] = 0;
					}
					//var_dump((int)$address->getCountryId());exit;
			        $addressArr['firstname'] = $address->getFirstname() ? $address->getFirstname() : '';
			        $addressArr['lastname'] = $address->getLastname() ? $address->getLastname() : '';
			        $addressArr['company'] = $address->getCompany() ? $address->getCompany() : '';
			        $addressArr['country_id'] = $address->getCountryId() ? $address->getCountryId() : '';
			        //$addressArr['state'] = $address->getRegion();
			        $addressArr['city'] = $address->getCity() ? $address->getCity() : '';
			        $addressArr['addr_block'] = $address->getAddrBlock() ? $address->getAddrBlock() : '';
			        $addressArr['addr_villa'] = $address->getAddrVilla() ? $address->getAddrVilla() : '';
			        $addressArr['addr_avenue'] = $address->getAddrAvenue() ? $address->getAddrAvenue() : '';
			        $addressArr['addr_street'] = $address->getAddrStreet() ? $address->getAddrStreet() : '';
			        $addressArr['addr_flatenumber'] = $address->getAddrFlatenumber() ? $address->getAddrFlatenumber() : '';
			        $addressArr['addr_floornumber'] = $address->getAddrFloornumber() ? $address->getAddrFloornumber() : '';
			        $addressArr['address_line_1'] = $address->getStreet1() ? $address->getStreet1() : '';
			        //, 'address_line_2'=> $address->getStreet2()
			        $addressArr['telephone'] = $address->getTelephone() ? $address->getTelephone() : '';
			        $addressArr['addr_landline'] = $address->getAddrLandline() ? $address->getAddrLandline() : '';
			        $addressArr['postcode'] = $address->getPostcode() ? $address->getPostcode() : '';
			        $addressArr['fax'] = $address->getFax() ? $address->getFax() : '';
			        $ship['shipping_address'][] = $addressArr;

			        
				}

				$shipping['shipping_address'] = $ship;*/
				//get shipping methods
				$shipmethods = Mage::getSingleton('shipping/config')->getActiveCarriers();
                $shipping_method = array();
                foreach($shipmethods as $carrierCode => $carrierModel){ 
                	$ship1 = array();                           
                   if( $carrierMethods = $carrierModel->getAllowedMethods() )
                   {
                       foreach ($carrierMethods as $methodCode => $method)
                       {    
                            $code = $carrierCode.'_'.$methodCode;
                       }
                       $carrierTitle = Mage::getStoreConfig('carriers/'.$carrierCode.'/title');
                       $price = Mage::getStoreConfig('carriers/'.$carrierCode.'/price');
                       $ship1['code'] = $code;
                       $ship1['title'] = $carrierTitle;
                       $ship1['shipping_price'] = $price;
                       $shipp['shipping_method'][] = $ship1;
                   }
                }
                if($m>0){
                	$custAddress['shipping_method'] = $shipp;	
                }
			    return $custAddress;
	        }
	        else{
	        	$error = array();
	        	$message = Mage::helper('customer')->__("Enter valid customer");
	        	$error['status'] = $customer_id;
	        	$error['message'] = $message;
	        	$shipping[] = $error;
             	return $shipping;   
	        }
    	}
    }
}