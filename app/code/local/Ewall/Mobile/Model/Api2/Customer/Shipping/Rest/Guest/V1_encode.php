<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Shipping_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Shipping
{
    
	protected function _retrieveCollection()
    {
        $post = $this->getRequest()->getParams();
        if ($post) {
        	$customer_id = $post['customer_id'];
        	$decrypt = Mage::getSingleton('core/encryption');
         	$customer_id = base64_decode($decrypt->decrypt($customer_id));
        	$shipping = array();
		    $customer = Mage::getSingleton('customer/customer')->load($customer_id);
		    if($customer->getId()){
		    	//$customerAddress = Mage::getModel('customer/address');
				$shippingId = $customer->getDefaultShipping();
				$billingId = $customer->getDefaultBilling();
				$primaryShipping = $customer->getDefaultShippingAddress();
				$custAddress = array();
				$custAddress[] = $primaryShipping;
				foreach($customer->getAdditionalAddresses() as $address){
				    $custAddress[] = $address;
				}
				$shipp = array();
				foreach($custAddress as $address){
				    // if(($ShippingId != $BillingId) && ($address->getEntityId() == $BillingId)){
				    //     continue;
				    // }
				    $addressArr = array();
				    $addressArr['address_id'] = $address->getEntityId();
				    if($address->getEntityId() == $shippingId){
				    	$addressArr['is_default_shipping'] = 1;
					}
					else{
						$addressArr['is_default_shipping'] = 0;
					}
					//var_dump((int)$address->getCountryId());exit;
			        $addressArr['first_name'] = $address->getFirstname();
			        $addressArr['last_name'] = $address->getLastname();
			        /*
			        if(strlen($address->getCountryId()) < = 0){
			        	$country = Mage::getModel('directory/country')->loadByCode($address->getCountryId());
			        	$addressArr['country'] = $country->getName();
			        }else{
			        	$addressArr['country'] = $address->getCountryId();
			        }
			        */
			        $addressArr['country'] = $address->getCountryId();
			        //$addressArr['state'] = $address->getRegion();
			        $addressArr['city'] = $address->getCity();
			        $addressArr['block'] = $address->getAddrBlock();
			        $addressArr['villa'] = $address->getAddrVilla();
			        $addressArr['avenue'] = $address->getAddrAvenue();
			        $addressArr['street'] = $address->getAddrStreet();
			        $addressArr['flatnumber'] = $address->getAddrFlatenumber();
			        $addressArr['floornumber'] = $address->getAddrFloornumber();
			        $addressArr['details'][] = array('address_line_1'=> $address->getStreet1()
			        //, 'address_line_2'=> $address->getStreet2()
			         );
			        $addressArr['mobile'] = $address->getTelephone();
			        $addressArr['landline'] = $address->getAddrLandline();
			        $ship['shipping_address'][] = $addressArr;
				}
				$shipping['shipping_address'] = $ship;
				//get shipping methods
				$shipmethods = Mage::getSingleton('shipping/config')->getActiveCarriers();
                $shipping_method = array();
                foreach($shipmethods as $carrierCode => $carrierModel){ 
                	$ship1 = array();                           
                   if( $carrierMethods = $carrierModel->getAllowedMethods() )
                   {
                       foreach ($carrierMethods as $methodCode => $method)
                       {    
                            $code = $carrierCode.'_'.$methodCode;
                       }
                       $carrierTitle = Mage::getStoreConfig('carriers/'.$carrierCode.'/title');
                       $price = Mage::getStoreConfig('carriers/'.$carrierCode.'/price');
                       $ship1['code'] = $code;
                       $ship1['title'] = $carrierTitle;
                       $ship1['shipping_price'] = $price;
                       $shipp['shipping_method'][] = $ship1;
                   }
                }
                $shipping['shipping_method'] = $shipp;
			    return $shipping;
	        }
	        else{
	        	$error = array();
	        	$message = Mage::helper('customer')->__("Enter valid customer");
	        	$error['status'] = $customer_id;
	        	$error['message'] = $message;
	        	$shipping[] = $error;
             	return $shipping;   
	        }
    	}
    }
}