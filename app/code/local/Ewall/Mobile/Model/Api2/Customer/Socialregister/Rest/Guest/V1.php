<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Socialregister_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Socialregister
{
    protected function getCustomerByEmail($customer, $email){
        $customer->loadByEmail($email);
        return $customer;
    }
    public function _create()
    {
        $requestData = $this->getRequest()->getBodyParams();
        if ($requestData) {
            try {
                $email = $requestData['email'];
                //$password = $requestData['password'];

                $error = false;

                if (!Zend_Validate::is(trim($email), 'EmailAddress')) {
                    $error = true;
                    $message = Mage::Helper('customer')->__('Invalid Email Address');
                }

                if ($error) {
                    throw new Exception();
                }
                $websiteId = 1;
                if(strtotime($requestData['dob']) === false){
                    $requestData['dob'] = '';
                }
                else{
                    $requestData['dob'] = date('Y-m-d', strtotime($requestData['dob']));
                }
                
                $customer = Mage::getModel('customer/customer')
                            ->setWebsiteId($websiteId);
                try {
                    $customer = $this->getCustomerByEmail($customer, $email);
                    //encrypt customer id
                    $encrypt = Mage::getSingleton('core/encryption');
                    if($customer->getId()){
                        $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($customer->getEmail());
                        $status = $subscriber->isSubscribed();
                    }
                    else{
                        $customer->setEmail($email);
                        $newPassword = $customer->generatePassword();
                        $customer->setFirstname($requestData['first_name']);
                        $customer->setLastname($requestData['last_name']);
                        $customer->setPassword($newPassword);
                        $customer->setGender($requestData['gender']);
                        $customer->setDob($requestData['dob']);
                        $customer->setWebsiteId(1);
                        $customer->save();
                        $customer->sendPasswordReminderEmail();
                        $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($customer->getEmail());
                        $status = $subscriber->isSubscribed();
                    }
                    //$customer = $this->getCustomerByEmail($customer, $email);
                    $customer_id = $encrypt->encrypt($customer->getId());
                    $customer_id = bin2hex($customer_id);
                    $message = Mage::helper('contacts')->__('Customer login details has been verified successfully.');    
                    $gender = '';
                    if($customer->getGender() == 1){
                        $gender = 'Male';
                    }
                    if($customer->getGender() == 2){
                        $gender = 'Female';
                    }
                    $info = array('login_status' => 'Success', 
                            'message' => $message, 'customer_id' => $customer_id , 'first_name' => $customer->getFirstname(),
                            'last_name' => $customer->getLastname(),
                            'email' => $customer->getEmail(),
                            'gender' => $gender,
                            //'password'=> $customer->getPasswordHash(),
                            'dob' => $customer->getDob(),
                            'newsletter_subscription' => $status);

                    /** Add Address details **/
                    // $address = $customer->getPrimaryBillingAddress();
                    // $address_detail = array();
                    // if(!empty($address)){
                    //     $address_detail = array(
                    //         'country' => $address->getCountryId(),
                    //         'city' => $address->getCity(),
                    //         'area' => $address->getRegion(),
                    //         'address_line_1' => $address->getStreet1(),
                    //         'address_line_2' => $address->getStreet2(),
                    //         'mobile'=> $address->getTelephone(),
                    //         'landline' => $address->getAddrLandline());
                    // }              
                    $json = $info;
                    echo json_encode($json);
                    exit();

                }catch(Exception $e){
                    $error = true;
                    $message = $e->getMessage();
                }
                if ($error) {
                    throw new Exception();
                }
                
            } catch (Exception $e) {
                $message = Mage::helper('customer')->__($message);
                $json = array('customer_id' => '', 'login_status' => 'error', 'message' => $message);
                echo json_encode($json);
                exit();            
            }
        }
    }
}