<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Updatecart_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Updatecart
{
   protected function _create()
    {
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
            $customer_id = $post['customer_id'];
            $storeId = ($post['lang'] == 'ar') ? 3 : 1;
            Mage::app()->setCurrentStore($storeId);
            $item_id = $post['item_id'];
            $qty = $post['quantity'];
            $decrypt = Mage::getSingleton('core/encryption');
            //$customer_id = base64_decode($decrypt->decrypt($customer_id));
            $customer_id = pack("H*", $customer_id);
            $customer_id = $decrypt->decrypt($customer_id);
            $customer = Mage::getSingleton('customer/customer')->load($customer_id);
            if($customer->getId()){
                try{
                    
                    $quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('customer_id', $customer->getId())
                    		->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
                   	$item = $quote->getItemById($item_id);
                   	if($qty>0){
                   		$item->setQty($qty);	
                   	}
                   	else{
                   		$quote->removeItem($item->getItemId());
                   	}
                    $quote->collectTotals()->save();
                    $msgsuccess = ($storeId == 3) ? Mage::helper('customer')->__('تم تحديث المنتج') : Mage::helper('customer')->__('The product has been updated');
                    $itemDet = array();
                    $quoteFinal = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('customer_id', $customer->getId())
                    ->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
                    if($quoteFinal->getId()){
                       $quoteCollection = $quoteFinal->getAllVisibleItems();
                        //get celebrities children
                        $category = Mage::getModel('catalog/category')->load(95);
                        $celebrityCat = explode(',', $category->getChildren());
                        $categoryIds = Mage::helper('celebrity')->getCelebrityCategoryIds();
                        foreach ($quoteCollection as $_item)
                        {
                            $_product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($_item->getProductId());
                            $splname = ((in_array($_item['category_id'], $celebrity) || in_array($_item['category_id'], $celebrityCat)) && $_product->getSpecialName()) ? $_product->getSpecialName() : $_product->getName();
                            $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
                            $subtotal = $_item->getPrice() * $_item->getQty();
                            $itemDet[] = array('item_id'=>$_item->getId(), 'entity_id'=>$_item->getProductId(),
                                                'name' => $splname,
                                                'short_description' => $_product->getShortDescription(),
                                                'quantity'=> $_item->getQty(),
                                                'image_url' => $_product->getImageUrl(),
                                                'regular_price' => $_product->getPrice(),
                                                'final_price' => $_item->getPrice(),
                                                'is_saleable' => $stock->getIsInStock(),
                                                'subtotal' => number_format($subtotal, 4),
                                                );
                        }
                    }
                    $info = array('status' => 'Success', 'message' => $msgsuccess);
                    echo json_encode(array_merge($info, array('items'=>$itemDet)));
                    exit();
                }catch(Exception $e){
                    $addmsg = ($storeId == 3) ? Mage::helper('customer')->__('لا يمكن تحديث المنتج') : Mage::helper('customer')->__('Product cannot be updated');
                    $msgerror = $e->getMessage().' '.$addmsg;
                    $json = array('status' => 'error', 'message' => $msgerror);
                    echo json_encode($json);
                    exit();
                }
            }
            else{
                $msgerror = ($storeId == 3) ? Mage::helper('customer')->__('العميل غير صالح') : Mage::helper('customer')->__('Invalid customer');
                $json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
		}
			
	     }
       }
}

