<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Customer_Wishlist_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Customer_Wishlist
{
    
	protected function _retrieveCollection()
    {
    	$wishlist = array();
        $post = $this->getRequest()->getParams();
        if ($post) {
        	$storeId = ($post['lang'] == 'ar') ? 3 : 1;
        	$customer_id = $post['customer_id'];
        	$decrypt = Mage::getSingleton('core/encryption');
        	$customer_id = pack("H*", $customer_id);
         	$customer_id = $decrypt->decrypt($customer_id);
        	$wishlist = array();
		    $customer = Mage::getSingleton('customer/customer')->load($customer_id);
		    if($customer->getId()){
				$wishColl = Mage::getModel('wishlist/wishlist')->loadByCustomer($customer, true);
				$wishListItemCollection = $wishColl->getItemCollection();
				$category = Mage::getModel('catalog/category')->load(95);
            	$celebrityCat = explode(',', $category->getChildren());
			    foreach ($wishListItemCollection as $_item)
			    {
			    	$_product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($_item->getProductId());
			    	$data = unserialize($_product->getCelebritySpecialPrice());
			    	$celebrity = Mage::helper('celebrity')->getCelebrityCategoryIds();
        			$spl_price = (isset($data[$_item['category_id']])) ? $data[$_item['category_id']] : $_product->getFinalPrice();
        			$spl_name = (in_array($_item['category_id'], $celebrity) || in_array($_item['category_id'], $celebrityCat)) ? $_product->getSpecialName() : $_product->getName();
			    	$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
			        $products = array();
			        $products['item_id'] = $_item->getId();
			        $products['entity_id'] = $_product->getEntityId();
			        $products['short_description'] = $_product->getShortDescription();
			        $products['name'] = $spl_name;
			        $products['image_url'] = $_product->getImageUrl();
			        $products['quantity'] = $_item->getQty();
			        $products['regular_price'] = $_product->getPrice();
			        $products['final_price'] = number_format($spl_price,4);
			        $products['is_saleable'] = $stock->getIsInStock();
			        $wishlist[] = $products;
			    }
			    return $wishlist;
	        }
	        else{
	        	$error = array();
	        	$message = ($storeId == 3) ? Mage::helper('customer')->__('العميل غير صالح') : Mage::helper('customer')->__('Invalid customer');
	        	$error['status'] = "error";
	        	$error['message'] = $message;
	        	$wishlist[] = $error;
             	return $wishlist;   
	        }
    	}
    }
}
