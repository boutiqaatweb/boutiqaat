<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Faq_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Faq
{
	/**
	 * Retrieve list of celebrity list
	 *
	 * @return array
	 */
	protected function _retrieve()
	{
		$content = array();
        //get order related faq
        $post = $this->getRequest()->getParams();
        $store_id = ($post['lang'] == 'ar') ? 3 : 1;
        Mage::app()->setCurrentStore($store_id);
        $faqs = array('order-related','delivery-related','website-and-account-related');
        foreach ($faqs as $key => $value) {
            $page = Mage::getModel('cms/page');
            $page->setStoreId($store_id);
            $page->load($value,'identifier');
            $helper = Mage::helper('cms');
            $processor = $helper->getPageTemplateProcessor();
            $faq_content = $processor->filter($page->getContent());
            $content['content'][$key]['title'] = $page->getTitle();
            $content['content'][$key]['content'] = $faq_content;
        }
        return $content;
	}	
}
