<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Forgotpass_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Forgotpass
{   
	protected function _retrieve()
    {
        $response = array();
        $post = $this->getRequest()->getParams();
        if ( $post ) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                //$postObject = new Varien_Object();
                //$postObject->setData($post);

                $error = false;
                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                    $message = Mage::Helper('customer')->__('Invalid Email Address');
                }

                if ($error) {
                    throw new Exception();
                }
                $customer = $this->_getModel('customer/customer')
                ->setWebsiteId(1)
                ->loadByEmail($post['email']);

                if ($customer->getId()) {
                    try {
                        $newResetPasswordLinkToken =  Mage::helper('customer')->generateResetPasswordLinkToken();
                        $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                        $customer->sendPasswordResetConfirmationEmail();
                        $reseturl = Mage::getUrl().'customer/account/resetpassword/?id='.$customer->getId().'&token='.$newResetPasswordLinkToken;
                    } catch (Exception $exception) {
                        $error = true;
                        $message = Mage::helper('customer')->__('Unable to process your request. Please, try again later');
                    }       
                }
                if ($error) {
                    throw new Exception();
                }
                $translate->setTranslateInline(true);
                $response['mail_status'] = 'success';
                $response['resetpassword_url'] = $reseturl;
                // $response['message'] = Mage::Helper('customer')->__('If there is an account associated with %s you will receive an email with a link to reset your password.',
                //     $post['email']);
            } catch (Exception $e) {
                $translate->setTranslateInline(true);
                $response['mail_status'] = 'error';
                $response['message'] = Mage::helper('customer')->__($message);
            }
        } 
        return $response;
    }
    public function _getModel($path, $arguments = array())
    {
        return Mage::getModel($path, $arguments);
    }
}
