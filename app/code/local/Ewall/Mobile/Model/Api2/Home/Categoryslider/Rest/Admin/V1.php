<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Home_Categoryslider_Rest_Admin_V1 extends Ewall_Mobile_Model_Api2_Home_Categoryslider
{
    /**
     * Retrieve list of category slider list.
     *
     * @return array
     */
    protected function _retrieveCollection()
    {
        $categoryslider = array();
        
        $categories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('url')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToFilter('is_active',1)
                ->addAttributeToFilter('include_home_slider',1)
                ->addAttributeToSort('sort_order', 'asc');
        foreach($categories as $category){
            $slider = array();
            $slider['celebrity_id'] = $category->getId();
            $slider['name'] = $category->getName();
            if($category->getThumbnail()){
                $ThumbnailUrl = Mage::getBaseUrl('media').'catalog/category/'.$category->getThumbnail();
                $slider['thumbnail'] = $ThumbnailUrl;
            }
            else{
                $slider['thumbnail'] = '';  
            }
            $categoryslider[] = $slider;
        }
        return $categoryslider;
    }   
}