<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Home_Homeproducts_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Home_Homeproducts
{
	/**
	 * Retrieve list of Homeproducts.
	 *
	 * @return array
	 */
	protected function _retrieveCollection()
	{
		$homeproducts = array();
		$post = $this->getRequest()->getParams();
        $storeId = ($post['lang'] == 'ar') ? 3 : 1;
		//$hotdeals = $this->hotdeals();
		$newproducts = $this->newproducts($storeId);
		$trends = $this->trending($storeId);
		$celebrity = $this->celebritySlider($storeId);

	    $homeproducts['celebrityslider'][] = $celebrity;
	    //$homeproducts['hotdeals'][] = $hotdeals;
	    $homeproducts['newproducts'][] = $newproducts;
	    $homeproducts['trending'][] = $trends;
	    return $homeproducts;
	}
	private function _addCategoryFilter(& $collection, $category_ids){
		if ( empty($category_ids) ){
			return ;
		}
		$category_collection = Mage::getModel('catalog/category')->getCollection();
		$category_collection->addAttributeToSelect('*');
		$category_collection->addIsActiveFilter();
		if (count($category_ids)>0){
			$category_collection->addIdFilter($category_ids);
		}
		if (!Mage::helper('catalog/category_flat')->isEnabled()) {
		    $category_collection->groupByAttribute('entity_id');
		}
		$category_products = array();
		foreach ($category_collection as $category){
			$cid = $category->getId();
			if ( !array_key_exists( $cid, $category_products) ){
				$category_products[$cid] = $category->getProductCollection()->getAllIds();
			}
		}
		$product_ids = array();
		if (count($category_products)){
			foreach ($category_products as $cp) {
				$product_ids = array_merge($product_ids, $cp);
			}
		}
		$collection->addIdFilter($product_ids);
	}
	//get hotdeals products
	public function hotdeals(){
		$hotdeal = array();
		$collection = Mage::getSingleton('catalog/product')->getCollection();
		$collection->addAttributeToSelect('*');
		$collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
		$visibility = array(
				Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
				Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG
			);
		$collection->addAttributeToFilter('visibility', $visibility);
		if(!$this->filter) {
				$this->filter = 'deals';
			}
			
			if($this->filter){
				$todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
				$tomorrow = mktime(0, 0, 0, date('m'), date('d')+1, date('y'));
				$dateTomorrow = date('m/d/y', $tomorrow);
				$collection->addAttributeToFilter('special_price', array('neq' => ""));
				if($filter == 'deals') {
					$collection->addAttributeToFilter('special_from_date', array('date' => true, 'to' => $todayDate))
						->addAttributeToFilter('special_to_date', array('or'=> array(0 => array('date' => true, 'from' => $dateTomorrow), 1 => array('isNot' => new Zend_Db_Expr('null')))), 'left');
				}
			}
			$productSource = Mage::getStoreConfig('sns_products_cfg/general/product_source');
			if ( $productSource == 'product'){
				// product
				$ids = Mage::getStoreConfig('sns_products_cfg/general/product_ids');
				if ($ids){
					$ids = preg_split('#[\s|,]+#', $ids, -1, PREG_SPLIT_NO_EMPTY);
					$ids = array_map('intval', $ids);
					$ids = array_unique($ids);
					$collection->addIdFilter($ids);
				}
			} else {
				// catalog
				$category_ids = Mage::getStoreConfig('sns_products_cfg/general/product_category');
				$category_ids = $category_ids ? $category_ids : '';
				$category_ids = preg_split('#[\s|,]+#', $category_ids, -1, PREG_SPLIT_NO_EMPTY);
				$category_ids = array_map('intval', $category_ids);
				$category_ids = array_unique($category_ids);
				$this->_addCategoryFilter($collection, $category_ids);
				// var_dump($category_ids);
			}
			$orderBy = Mage::getStoreConfig('sns_products_cfg/general/product_order_by');
			$collection->addStoreFilter();
			$collection->setOrder('created_at', $orderBy);
			//Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
			$collection->setPage(1, 10);
            
			foreach($collection as $_product){
			        $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
				$products = array();
				$products['entity_id'] = $_product->getEntityId();
				$products['short_description'] = $_product->getShortDescription();
				$products['name'] = $_product->getName();
				$products['image_url'] = $_product->getImageUrl();
				$products['regular_price'] = (string) $_product->getPrice();
				$products['final_price'] = (string) $_product->getFinalPrice();
				$products['is_saleable'] = $stock->getIsInStock();
				/** Fetch Custom Option **/
			            $options = Mage::getModel('catalog/product_option')->getProductOptionCollection($_product);
			            $_custom_options = array();
			            foreach ($options as $option) {
			                $_custom_options[$option->getOptionId()] = array('name' => $option->getDefaultTitle(),
			                                                                'type' => $option->getType());
			                $values = Mage::getSingleton('catalog/product_option_value')->getValuesCollection($option);
			                $i = 1;
			                foreach ($option->getValues() as  $value) {
			                    $_custom_options[$option->getOptionId()]['value'][] = array('title' => $value->getTitle(),
			                                                                                'price' => $value->getPrice(),'price_type' => $value->getPriceType(),);
			                    
			                    $i++;
			                }
			            }
			            /** End of Fetch Custom Option **/
			            $products['has_custom_options'] = count($options) > 0;
			            $products['custom_options'] = (!empty($_custom_options))?$_custom_options:'';
				$hotdeal[] = $products;
			}
		return $hotdeal;
	}
	//get newarrivals products
	public function newproducts($storeId){
		$newproducts = array();
		$filter = 'new';
		$collection = Mage::getSingleton('catalog/product')->getCollection()->setStoreId($storeId);
		$collection->addAttributeToSelect('*');
		$collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
		$visibility = array(
				Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
				Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG
			);
		$collection->addAttributeToFilter('visibility', $visibility);
		if($filter == 'new') {
			$collection->addAttributeToFilter('is_new', 1);
		}
		$productSource = Mage::getStoreConfig('sns_products_cfg/general/product_source');
		if ( $productSource == 'product'){
			// product
			$ids = Mage::getStoreConfig('sns_products_cfg/general/product_ids');
			if ($ids){
				$ids = preg_split('#[\s|,]+#', $ids, -1, PREG_SPLIT_NO_EMPTY);
				$ids = array_map('intval', $ids);
				$ids = array_unique($ids);
				$collection->addIdFilter($ids);
			}
		} else {
			// catalog
			$category_ids = Mage::getStoreConfig('sns_products_cfg/general/product_category');
			$category_ids = $category_ids ? $category_ids : '';
			$category_ids = preg_split('#[\s|,]+#', $category_ids, -1, PREG_SPLIT_NO_EMPTY);
			$category_ids = array_map('intval', $category_ids);
			$category_ids = array_unique($category_ids);
			$this->_addCategoryFilter($collection, $category_ids);
			// var_dump($category_ids);
		}
		$orderBy = Mage::getStoreConfig('sns_products_cfg/general/product_order_by');

		$collection->addStoreFilter();
		$collection->setOrder('created_at', $orderBy);
		//Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
		$collection->setPage(1, 10);
        
		foreach($collection as $_product){
			$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
			$products = array();
			$products['entity_id'] = $_product->getEntityId();
			$products['short_description'] = $_product->getShortDescription();
			$products['name'] = $_product->getName();
			$products['image_url'] = $_product->getImageUrl();
			$products['regular_price'] = (string) $_product->getPrice();
			$products['final_price'] = (string) $_product->getFinalPrice();
			$products['is_saleable'] = $stock->getIsInStock();
			/** Fetch Custom Option **/
		            $options = Mage::getModel('catalog/product_option')->getProductOptionCollection($_product);
		            $_custom_options = array();
		            foreach ($options as $option) {
		                $_custom_options[$option->getOptionId()] = array('name' => $option->getDefaultTitle(),
		                                                                'type' => $option->getType());
		                $values = Mage::getSingleton('catalog/product_option_value')->getValuesCollection($option);
		                $i = 1;
		                foreach ($option->getValues() as  $value) {
		                    $_custom_options[$option->getOptionId()]['value'][] = array('title' => $value->getTitle(),
		                                                                                'price' => $value->getPrice(),'price_type' => $value->getPriceType(),);
		                    
		                    $i++;
		                }
		            }
		            /** End of Fetch Custom Option **/
		            $products['has_custom_options'] = count($options) > 0;
		            $products['custom_options'] = (!empty($_custom_options))?$_custom_options:'';
			$newproducts[] = $products;
		}
	    return $newproducts;
	}
	//get trending products
	protected function trending($storeId){
		$trends = array();
		$collection = Mage::getSingleton('catalog/product')->getCollection()->setStoreId($storeId);
		$collection->addAttributeToSelect('*');
		$collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
		$visibility = array(
				Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
				Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG
			);
		$collection->addAttributeToFilter('visibility', $visibility);
		$filterBy = Mage::getStoreConfig('sns_products_cfg/general/product_filter_by');
		if($filterBy == 'new') {
			$collection->addAttributeToFilter('is_new', 1);
		}
		if($filterBy == 'featured') {
			$collection->addAttributeToFilter('is_featured', 1);
		}
		$productSource = Mage::getStoreConfig('sns_products_cfg/general/product_source');
		if ( $productSource == 'product'){
			// product
			$ids = Mage::getStoreConfig('sns_products_cfg/general/product_ids');
			if ($ids){
				$ids = preg_split('#[\s|,]+#', $ids, -1, PREG_SPLIT_NO_EMPTY);
				$ids = array_map('intval', $ids);
				$ids = array_unique($ids);
				$collection->addIdFilter($ids);
			}
		} else {
			// catalog
			$category_ids = Mage::getStoreConfig('sns_products_cfg/general/product_category');
			$category_ids = $category_ids ? $category_ids : '';
			$category_ids = preg_split('#[\s|,]+#', $category_ids, -1, PREG_SPLIT_NO_EMPTY);
			$category_ids = array_map('intval', $category_ids);
			$category_ids = array_unique($category_ids);
			$this->_addCategoryFilter($collection, $category_ids);
			// var_dump($category_ids);
		}
		$orderBy = Mage::getStoreConfig('sns_products_cfg/general/product_order_by');

		$collection->addStoreFilter();
		$collection->setOrder('created_at', $orderBy);
		//Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
		$collection->setPage(1, 10);
        
		foreach($collection as $_product){
			$products = array();
			$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
			$products['entity_id'] = $_product->getEntityId();
			$products['short_description'] = $_product->getShortDescription();
			$products['name'] = $_product->getName();
			$products['image_url'] = $_product->getImageUrl();
			$products['regular_price'] = (string) $_product->getPrice();
			$products['final_price'] = (string) $_product->getFinalPrice();
			$products['is_saleable'] = $stock->getIsInStock();
			/** Fetch Custom Option **/
		            $options = Mage::getModel('catalog/product_option')->getProductOptionCollection($_product);
		            $_custom_options = array();
		            foreach ($options as $option) {
		                $_custom_options[$option->getOptionId()] = array('name' => $option->getDefaultTitle(),
		                                                                'type' => $option->getType());
		                $values = Mage::getSingleton('catalog/product_option_value')->getValuesCollection($option);
		                $i = 1;
		                foreach ($option->getValues() as  $value) {
		                    $_custom_options[$option->getOptionId()]['value'][] = array('title' => $value->getTitle(),
		                                                                                'price' => $value->getPrice(),'price_type' => $value->getPriceType(),);
		                    
		                    $i++;
		                }
		            }
		            /** End of Fetch Custom Option **/
		            $products['has_custom_options'] = count($options) > 0;
		            $products['custom_options'] = (!empty($_custom_options))?$_custom_options:'';
			$trends[] = $products;
		}
	    return $trends;
	}
    public function celebritySlider($storeId){

    	$categoryslider = array();
    	$parentId = 95;
		$categories = Mage::getModel('catalog/category')
    			->getCollection()
    			->setStoreId($storeId)
    			->addAttributeToSelect('name')
                ->addAttributeToSelect('url')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToSelect('image')
                ->addAttributeToFilter('is_active',1)
    		->addAttributeToFilter('include_home_slider',1)
    		->addAttributeToFilter('parent_id', array('eq' => $parentId))
    		->addAttributeToSort('sort_order', 'asc');
    	$categories ->getSelect()->order('RAND()');
		//$categories ->getSelect()->limit(9);
    	foreach($categories as $category){
    		$slider = array();
    		$slider['celebrity_id'] = $category->getId();
    		$slider['name'] = $category->getName();
    		if($category->getThumbnail()){
	    		$ThumbnailUrl = Mage::getBaseUrl('media').'catalog/category/'.$category->getThumbnail();
	    		$slider['thumbnail'] = $ThumbnailUrl;
    		}
    		else{
    			$slider['thumbnail'] = '';	
    		}
    		if($category->getImage()){
	    		$ImageUrl = Mage::getBaseUrl('media').'catalog/category/'.$category->getImage();
	    		$slider['image'] = $ImageUrl;
		}
		else{
			$slider['image'] = '';	
		}
    		$categoryslider[] = $slider;
    	}
	    return $categoryslider;
    }
}