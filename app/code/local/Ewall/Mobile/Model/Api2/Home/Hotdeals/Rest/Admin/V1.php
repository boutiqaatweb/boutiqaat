<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Home_Hotdeals_Rest_Admin_V1 extends Ewall_Mobile_Model_Api2_Home_Hotdeals
{
   /**
     * Retrieve list of Hotdeals.
     *
     * @return array
     */
    protected function _retrieveCollection()
    {
        $hotdeal = array();
        
            $collection = Mage::getSingleton('catalog/product')->getCollection();
            $collection->addAttributeToSelect('*');
            $collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
            
            if(!$this->filter) {
                $this->filter = 'deals';
            }
            
            if($this->filter){
                $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
                $tomorrow = mktime(0, 0, 0, date('m'), date('d')+1, date('y'));
                $dateTomorrow = date('m/d/y', $tomorrow);
                $collection->addAttributeToFilter('special_price', array('neq' => ""));
                if($filter == 'deals') {
                    $collection->addAttributeToFilter('special_from_date', array('date' => true, 'to' => $todayDate))
                        ->addAttributeToFilter('special_to_date', array('or'=> array(0 => array('date' => true, 'from' => $dateTomorrow), 1 => array('isNot' => new Zend_Db_Expr('null')))), 'left');
                }
            }
            $visibility = array(
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG
            );
            $collection->addAttributeToFilter('visibility', $visibility);

            $productSource = Mage::getStoreConfig('sns_products_cfg/general/product_source');
            if ( $productSource == 'product'){
                // product
                $ids = Mage::getStoreConfig('sns_products_cfg/general/product_ids');
                if ($ids){
                    $ids = preg_split('#[\s|,]+#', $ids, -1, PREG_SPLIT_NO_EMPTY);
                    $ids = array_map('intval', $ids);
                    $ids = array_unique($ids);
                    $collection->addIdFilter($ids);
                }
            } else {
                // catalog
                $category_ids = Mage::getStoreConfig('sns_products_cfg/general/product_category');
                $category_ids = $category_ids ? $category_ids : '';
                $category_ids = preg_split('#[\s|,]+#', $category_ids, -1, PREG_SPLIT_NO_EMPTY);
                $category_ids = array_map('intval', $category_ids);
                $category_ids = array_unique($category_ids);
                $this->_addCategoryFilter($collection, $category_ids);
                // var_dump($category_ids);
            }
            $orderBy = Mage::getStoreConfig('sns_products_cfg/general/product_order_by');
            $collection->addStoreFilter();
            $collection->setOrder('created_at', $orderBy);
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
            $collection->setPage(1, 10);
            
            foreach($collection as $_product){
                $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
                $products = array();
                $products['entity_id'] = $_product->getEntityId();
                $products['short_description'] = $_product->getShortDescription();
                $products['name'] = $_product->getName();
                $products['image_url'] = $_product->getImageUrl();
                $products['regular_price'] = (string) $_product->getPrice();
                $products['final_price'] = (string) $_product->getFinalPrice();
                $products['is_saleable'] = $stock->getIsInStock();;
                $hotdeal[] = $products;
            }
        return $hotdeal;
    }
    private function _addCategoryFilter(& $collection, $category_ids){
        if ( empty($category_ids) ){
            return ;
        }
        $category_collection = Mage::getModel('catalog/category')->getCollection();
        $category_collection->addAttributeToSelect('*');
        $category_collection->addIsActiveFilter();
        if (count($category_ids)>0){
            $category_collection->addIdFilter($category_ids);
        }
        if (!Mage::helper('catalog/category_flat')->isEnabled()) {
            $category_collection->groupByAttribute('entity_id');
        }
        $category_products = array();
        foreach ($category_collection as $category){
            $cid = $category->getId();
            if ( !array_key_exists( $cid, $category_products) ){
                $category_products[$cid] = $category->getProductCollection()->getAllIds();
            }
        }
        $product_ids = array();
        if (count($category_products)){
            foreach ($category_products as $cp) {
                $product_ids = array_merge($product_ids, $cp);
            }
        }
        $collection->addIdFilter($product_ids);
    }
    protected function applyTaxToPrice($price, $withTax = true)
    {
        return $price;
    }
}