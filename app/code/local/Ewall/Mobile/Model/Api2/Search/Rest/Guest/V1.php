<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ewall_Mobile_Model_Api2_Search_Rest_Guest_V1 extends Ewall_Mobile_Model_Api2_Search
{
	/**
     * Retrieve list of Search results.
     *
     * @return array
     */
    protected function _retrieveCollection()
    {
        $post = $this->getRequest()->getParams();
        $storeId = ($post['lang'] == 'ar') ? 3 : 1;
        Mage::app()->setCurrentStore($storeId);
        $searchText = $this->getRequest()->getParam('query_text');
        // $query = Mage::helper("catalogsearch")->getQuery();
        // $query->setQueryText($searchText);
        // $collection = $query->getSearchCollection();
        // $collection->addSearchFilter($searchText);
        Mage::register('custom_store_id', $storeId);
        $query = Mage::getModel('catalogsearch/query');
        //echo $query->getStoreId();
        $query = $query->loadByQuery($searchText);
        
        if (!$query->getId()) {
            $query->setQueryText($searchText)->prepare();
        }
        $fulltextResource = Mage::getResourceModel('catalogsearch/fulltext')->prepareResult(
                Mage::getModel('catalogsearch/fulltext'), 
                $searchText, 
                $query
                );
        $collection = Mage::getResourceModel('catalog/product_collection')->setStoreId($storeId);
        $collection->getSelect()->joinInner(
            array('search_result' => $collection->getTable('catalogsearch/result')),
            $collection->getConnection()->quoteInto(
                'search_result.product_id=e.entity_id AND search_result.query_id=?',
                $query->getId()
            )
        );

        $collection->addAttributeToSelect("*");
        $collection->addAttributeToFilter("status", 1);
        //$collection->setPage(1,10);
        $visibility = array(
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG
            );
        $collection->addAttributeToFilter('visibility', $visibility);
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
        $result = array();
        foreach ($collection as $_product) {
            $products = array();
            $_product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($_product->getId());
            $products['entity_id'] = $_product->getEntityId();
            $products['short_description'] = $_product->getShortDescription();
            $products['name'] = $_product->getName();
            $products['image_url'] = $_product->getImageUrl();
            $products['regular_price'] = (string) $_product->getPrice();
            $products['final_price'] = (string) $_product->getFinalPrice();
            $products['is_saleable'] = $_product->getIsSalable();
            $result[] = $products;
        }
        Mage::unregister('custom_store_id');
        return $result;
    }
}