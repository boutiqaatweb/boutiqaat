<?php
class Ewall_Ordergridpayment_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
	protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
       /*
	   $collection->getSelect()->joinLeft('knet_payment', 'knet_payment.order_id = main_table.increment_id', array('knetpaymentid'=>'paymentid', 'knettranid'=>'tranid','knetauth'=>'auth'))->distinct(true);
        $collection->getSelect()->joinLeft('burgan_payment', 'burgan_payment.order_id = main_table.increment_id', array('burganpaymentid'=>'paymentid','burgantranid'=>'tranid','burganauth'=>'auth'))->distinct(true);
		$collection->getSelect()->joinLeft(array('sfoa'=>'sales_flat_order_address'),'main_table.entity_id = sfoa.parent_id AND sfoa.address_type="shipping"',array('sfoa.street','sfoa.city','sfoa.region','sfoa.postcode','sfoa.telephone')); 
		*/
		$collection->getSelect()->joinLeft(array('payment'=>'sales_flat_order_payment'), 'payment.parent_id=main_table.entity_id',array('payment_method'=>'method'));
		$collection->getSelect()->joinLeft(array('sfoa'=>'sales_flat_order_address'),'main_table.entity_id = sfoa.parent_id AND sfoa.address_type="shipping"',array('sfoa.telephone')); 
		$collection->getSelect()->joinLeft('knet_payment', 'knet_payment.order_id = main_table.increment_id', array('knetpaymentid'=>'paymentid', 'knettranid'=>'tranid','knetauth'=>'auth','result'=>'result'));
		$collection->getSelect()->joinLeft('burgan_payment', 'burgan_payment.order_id = main_table.increment_id', array('burganpaymentid'=>'paymentid','burgantranid'=>'tranid','burganauth'=>'auth','result'=>'result'));
		$collection->getSelect()->group('main_table.entity_id');
        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }
	protected function _prepareColumns()
    {
		$payments = Mage::getSingleton('payment/config')->getActiveMethods();
		$methods = array();
		foreach ($payments as $paymentCode=>$paymentModel)
		{
			$paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
			$methods[$paymentCode] = $paymentTitle;
		}

		$this->addColumnAfter('method', array(
			'header' => Mage::helper('sales')->__('Payment Method'),
			'index' => 'payment_method',
			'filter_index' => 'payment.method',
			'type'  => 'options',
			'width' => '70px',
			'options' => $methods,
		),'shipping_name');
		
		$this->addColumnAfter('telephone', array(
            'header' 	=> Mage::helper('sales')->__('Telephone'),
            'width'  	=> '80px',
            'type'  	=> 'text',
            'index' 	=> 'telephone',
			'filter_index' => 'sfoa.telephone',
        ), 'shipping_name');
		
        $this->addColumnAfter('knetpaymentid', array(
            'header' 	=> Mage::helper('sales')->__('Paymentid'),
            'width'  	=> '80px',
            'type'  	=> 'text',
            'index' 	=> 'knetpaymentid',
            'filter'	=> false,
            'sortable'  => false,
            'renderer'	=> 'ordergridpayment/adminhtml_sales_order_renderer_paymentid'
        ), 'shipping_name');

        $this->addColumnAfter('knettranid', array(
        	'header' 	=> Mage::helper('sales')->__('Transaction Id'),        	
        	'index' 	=> 'knettranid',
            'filter'	=> false,
            'sortable'  => false,
            'renderer'	=> 'ordergridpayment/adminhtml_sales_order_renderer_transactionid'
        ), 'knetpaymentid');

        /* $this->addColumnAfter('knetauth', array(
        	'header' 	=> Mage::helper('sales')->__('Authentication'),        	
        	'index' 	=> 'knetauth',
            'filter'	=> false,
            'sortable'  => false,
            'renderer'	=> 'ordergridpayment/adminhtml_sales_order_renderer_authentication'
        ), 'knettranid');
		*/
		$this->addColumnAfter('result', array(
        	'header' 	=> Mage::helper('sales')->__('Result'),        	
        	'index' 	=> 'result',
            'filter'	=> false,
            'sortable'  => false,
            'renderer'	=> 'ordergridpayment/adminhtml_sales_order_renderer_result'
        ), 'knettranid');


        $this->addRssList('rss/order/new', Mage::helper('sales')->__('New Order RSS'));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

        parent::_prepareColumns();
		unset($this->_columns['billing_name']);   
		return $this;
    }
}
			