<?php
class Ewall_Ordergridpayment_Block_Adminhtml_Sales_Order_Renderer_Result extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    protected $_values;

    public function render(Varien_Object $row) {
    	
    	
		$collect = $row->getData($this->getColumn()->getIndex());
		if($row->getData('payment_method') == 'knet_cc'){
			return $row->getResult();
		}
		elseif($row->getData('payment_method') == 'burgan_cc'){
			return $row->getResult();
		}
		else{
			return '';
		}
    } 
}
