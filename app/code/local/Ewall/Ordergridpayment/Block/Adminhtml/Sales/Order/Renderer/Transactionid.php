<?php
class Ewall_Ordergridpayment_Block_Adminhtml_Sales_Order_Renderer_Transactionid extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {   
    protected $_values;

    public function render(Varien_Object $row) {
    	
		$collect = $row->getData($this->getColumn()->getIndex());
		if($row->getData('payment_method') == 'knet_cc'){
			return $row->getKnettranid();
		}
		elseif($row->getData('payment_method') == 'burgan_cc'){
			return $row->getBurgantranid();
		}
		else{
			return '';
		}
    } 
}
