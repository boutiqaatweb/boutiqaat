<?php
$installer = $this;
 
$statusStateTable = $installer->getTable('sales/order_status_state');
 
// Insert states and mapping of statuses to states
$installer->getConnection()->insertArray(
    $statusStateTable,
    array(
        'status',
        'state',
        'is_default'
    ),
    array(
        array(
            'status' => 'shipped',
            'state' => 'shipped',
            'is_default' => 1
        ),
        array(
            'status' => 'received',
            'state' => 'received',
            'is_default' => 1
        ),
        array(
            'status' => 'delivered',
            'state' => 'delivered',
            'is_default' => 1
        ),
        array(
            'status' => 'rescheduled',
            'state' => 'rescheduled',
            'is_default' => 1
        )
    )
);