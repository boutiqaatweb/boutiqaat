<?php


class Ewall_Zeropricereport_Block_Adminhtml_Zeropricereport extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_zeropricereport";
	$this->_blockGroup = "zeropricereport";
	$this->_headerText = Mage::helper("zeropricereport")->__("Zeroprice Report Manager");
	$this->_addButtonLabel = Mage::helper("zeropricereport")->__("Add New Item");
	parent::__construct();
	$this->_removeButton('add');
	}

}