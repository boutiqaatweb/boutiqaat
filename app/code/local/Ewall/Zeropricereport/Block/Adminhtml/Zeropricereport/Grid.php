<?php

class Ewall_Zeropricereport_Block_Adminhtml_Zeropricereport_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("zeropricereportGrid");
				$this->setDefaultSort("entity_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}
		protected function _getStore()
	    {
	        $storeId = (int) $this->getRequest()->getParam('store', 0);
	        return Mage::app()->getStore($storeId);
	    }
		protected function _prepareCollection()
		{
			$store = $this->_getStore();
			$collection = Mage::getModel("catalog/product")->getCollection()
						->addAttributeToSelect('sku')
						->addAttributeToSelect('price')
						->addAttributeToSelect('special_price')
						->addAttributeToFilter(array(
						    array(
						        'attribute' => 'price',
						        'eq'        => 0,
						        )
						    ));
			// if ($store->getId()) {
	  //           $adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
	  //           $collection->addStoreFilter($store);
	  //           $collection->joinAttribute(
	  //               'price',
	  //               'catalog_product/price',
	  //               'entity_id',
	  //               null,
	  //               'left',
	  //               $store->getId()
	  //           );
   //      	}
        	//echo count($collection);
			$this->setCollection($collection);
			return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("entity_id", array(
				"header" => Mage::helper("zeropricereport")->__("Product ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "entity_id",
				));
                
				$this->addColumn("sku", array(
				"header" => Mage::helper("zeropricereport")->__("SKU"),
				"index" => "sku",
				));
				$store = $this->_getStore();
				$this->addColumn("price", array(
				"header" => Mage::helper("zeropricereport")->__("Price"),
				'currency_code' => $store->getBaseCurrency()->getCode(),
				'type'=>'currency',
				"index" => "price",
				));
				$this->addColumn("action", array(
				"header" => Mage::helper("zeropricereport")->__("Action"),
				"index" => "stores",
				'type'      => 'action',
                'getter'     => 'getId',
                'align' => 'center',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('catalog')->__('Edit'),
                        'url'     => array('base' => 'adminhtml/catalog_product/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
				));
				$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
				$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));
				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/catalog_product/edit',array('id' => $row->getEntityId()));
		}
		// protected function _prepareMassaction()
		// {
		// 	$this->setMassactionIdField('id');
		// 	$this->getMassactionBlock()->setFormFieldName('ids');
		// 	$this->getMassactionBlock()->setUseSelectAll(true);
		// 	// $this->getMassactionBlock()->addItem('remove_partnerunsubscribe', array(
		// 	// 		 'label'=> Mage::helper('partnerunsubscribe')->__('Remove Partnerunsubscribe'),
		// 	// 		 'url'  => $this->getUrl('*/adminhtml_partnerunsubscribe/massRemove'),
		// 	// 		 'confirm' => Mage::helper('partnerunsubscribe')->__('Are you sure?')
		// 	// 	));
		// 	$this->getMassactionBlock()->addItem('approved', array(
  //            'label'    => Mage::helper('customer')->__('approved'),
  //            'url'      => $this->getUrl('*/adminhtml_partnerunsubscribe/massapproved')
	 //        ));
		// 	$this->getMassactionBlock()->addItem('declined', array(
	 //             'label'    => Mage::helper('customer')->__('Declined'),
	 //             'url'      => $this->getUrl('*/adminhtml_partnerunsubscribe/massdeclined')
	 //        ));
		// 	return $this;
		// }
}