<?php


class Ewall_Zeropricereport_Block_Adminhtml_Zerosplpricereport extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_zerosplpricereport";
	$this->_blockGroup = "zeropricereport";
	$this->_headerText = Mage::helper("zeropricereport")->__("Zero Specialprice Report Manager");
	$this->_addButtonLabel = Mage::helper("zeropricereport")->__("Add New Item");
	parent::__construct();
	$this->_removeButton('add');
	}

}