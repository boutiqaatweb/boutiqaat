<?php

class Ewall_Zeropricereport_Adminhtml_Zeropricereport_ZeropricereportController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("catalog/zeropricereport")->_addBreadcrumb(Mage::helper("adminhtml")->__("Zeroprice Report  Manager"),Mage::helper("adminhtml")->__("Zeroprice Report Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Zeroprice Report"));
			    $this->_title($this->__("Zeroprice Report Manager"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function exportCsvAction()
	    {
	        $fileName   = 'zeropriceproducts.csv';
	        $grid       = $this->getLayout()->createBlock('zeropricereport/adminhtml_zeropricereport_grid');
	        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
	    }

	    public function exportExcelAction()
	    {
	        $fileName   = 'zeropriceproducts.xls';
	        $grid       = $this->getLayout()->createBlock('zeropricereport/adminhtml_zeropricereport_grid');
	        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
	    }
}
