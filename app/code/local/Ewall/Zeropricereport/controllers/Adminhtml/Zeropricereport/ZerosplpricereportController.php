<?php

class Ewall_Zeropricereport_Adminhtml_Zeropricereport_ZerosplpricereportController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("catalog/zeropricereport")->_addBreadcrumb(Mage::helper("adminhtml")->__("Zero Specialprice Report  Manager"),Mage::helper("adminhtml")->__("Zero Specialprice Report Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Zero Specialprice Report"));
			    $this->_title($this->__("Zero Specialprice Report Manager"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function exportCsvAction()
	    {
	        $fileName   = 'zerosplpriceproducts.csv';
	        $grid       = $this->getLayout()->createBlock('zeropricereport/adminhtml_zerosplpricereport_grid');
	        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
	    }

	    public function exportExcelAction()
	    {
	        $fileName   = 'zerosplpriceproducts.xls';
	        $grid       = $this->getLayout()->createBlock('zeropricereport/adminhtml_zerosplpricereport_grid');
	        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
	    }
}
