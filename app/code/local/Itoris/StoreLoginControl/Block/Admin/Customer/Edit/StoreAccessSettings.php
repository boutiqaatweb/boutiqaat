<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Block_Admin_Customer_Edit_StoreAccessSettings extends Mage_Core_Block_Template
		implements Mage_Adminhtml_Block_Widget_Tab_Interface{

	/**
	 * Set template for block
	 */
	public function __construct(){
		parent::__construct();
		$this->setTemplate('itoris/storelogincontrol/customer/edit/storeAccessSettings.phtml');
	}

	/**
	 * Return customer website
	 *
	 * @return string
	 */
	public function getCustomerWebsite(){
		$wid = $this->getCustomer()->getWebsiteId();
		/** @var $website Mage_Core_Model_Website */
		$website = Mage::getModel('core/website')->load($wid);
		return $website->getName();
	}

	/**
	 * @return Mage_Core_Model_Mysql4_Store_Collection
	 */
	public function getStoresCollection(){
		/** @var $collection Mage_Core_Model_Mysql4_Store_Collection */
		$collection = Mage::getModel('core/store')->getCollection();
		//$collection->addWebsiteFilter($this->getCustomer()->getWebsiteId());
		return $collection;
	}

	/**
	 * Check is customer has access to the store
	 *
	 * @param Mage_Core_Model_Store $store
	 * @return mixed
	 */
	public function hasAccess(Mage_Core_Model_Store $store){

		if($this->accessData === null){
			/** @var $model Itoris_StoreLoginControl_Model_Customer */
			$model = Mage::getModel('itoris_stlc/customer');
			$this->accessData = $model->loadCustomer($this->getCustomer(), true);
		}

		return isset($this->accessData[$store->getId()]) && $this->accessData[$store->getId()];
	}

	public function isDisabled() {
		return count($this->accessData) == 1;
	}

	/**
	 * @param $store Mage_Core_Model_Store
	 * @return bool
	 */
	public function cannotShare($store) {
		return !$this->getDataHelper()->isGlobalShare() && $store->getWebsiteId() != $this->getCustomer()->getWebsiteId();
	}

	/**
	 * @return Mage_Customer_Model_Customer 
	 */
	public function getCustomer(){
		return Mage::registry('current_customer');
	}

	/**
	 * Return Tab label
	 *
	 * @return string
	 */
	public function getTabLabel()
	{
		return $this->__("Store Login Access Settings");
	}

	/**
	 * Return Tab title
	 *
	 * @return string
	 */
	public function getTabTitle()
	{
		return $this->getTabLabel();
	}

	/**
	 * Can show tab in tabs
	 *
	 * @return boolean
	 */
	public function canShowTab()
	{
		$isAllowed = Mage::getSingleton('admin/session')->isAllowed('customer/manage/itoris_storelogincontrol_customer_store_access_entity');
		$isNew = $this->getRequest()->getParam('id', null) == null;
		return $this->getDataHelper()->isActiveAdmin() && !$isNew && $isAllowed;
	}

	/**
	 * Tab is hidden
	 *
	 * @return boolean
	 */
	public function isHidden()
	{
		return false;
	}

	public function getAfter(){
		return 'tags';
	}

	/**
	 * @return Itoris_StoreLoginControl_Helper_Data
	 */
	public function getDataHelper(){
		return Mage::helper('itoris_stlc');
	}

	public function escapeHtml($data, $allowedTags = null)
	{
		if(method_exists('Mage_Core_Block_Abstract', 'escapeHtml')){
			return parent::escapeHtml($data, $allowedTags);
		}else{
			return parent::htmlEscape($data, $allowedTags);
		}

	}

	protected $accessData = null;
}

?>