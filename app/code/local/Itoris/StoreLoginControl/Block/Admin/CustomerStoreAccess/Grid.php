<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 
 
class Itoris_StoreLoginControl_Block_Admin_CustomerStoreAccess_Grid extends Mage_Adminhtml_Block_Widget_Grid{

	/**
	 * Prepare grid
	 */
	public function __construct(){
		parent::__construct();
		$this->setId('itorisCustomerGrid');
		$this->setUseAjax(true);
		$this->setDefaultSort('entity_id');
		$this->setSaveParametersInSession(true);
	}

	/**
	 * Prepare customers collection
	 *
	 * @return this
	 */
	protected function _prepareCollection()
	{
		/** @var $collection Mage_Customer_Model_Entity_Customer_Collection */
		$collection = Mage::getResourceModel('itoris_stlc/customer_collection')
			->addNameToSelect()
			->addAttributeToSelect('email')
			->addAttributeToSelect('group_id');


		$collection->addAttributeToFilter('website_id', array('neq' => 0));
		$this->getDataHelper()->addStoresAccessColumnsToSelect($collection->getSelect());
		$this->setCollection($collection);

		return parent::_prepareCollection();
	}

	/**
	 * Prepare grid columns
	 *
	 * @return Mage_Adminhtml_Block_Widget_Grid
	 */
	protected function _prepareColumns()
	{
		$this->addColumn('name', array(
			'header'    => Mage::helper('customer')->__('Name'),
			'index'     => 'name',
			'renderer'	=> 'itoris_stlc/admin_customerStoreAccess_NameRenderer'
		));
		$this->addColumn('email', array(
			'header'    => Mage::helper('customer')->__('Email'),
//            'width'     => '150',
			'index'     => 'email'
		));

		$groups = Mage::getResourceModel('customer/group_collection')
			->addFieldToFilter('customer_group_id', array('gt'=> 0))
			->load()
			->toOptionHash();

		$this->addColumn('group', array(
			'header'    =>  Mage::helper('customer')->__('Group'),
			'width'     =>  '100',
			'index'     =>  'group_id',
			'type'      =>  'options',
			'options'   =>  $groups,
		));

		$storesCollection = Mage::getModel('core/store')->getCollection();
		foreach($storesCollection as $store){
			$this->addColumn('store_'.$store->getId(), array(
				'header'    =>  $store->getWebsite()->getName() . ': ' . $this->__($store->getName()),
				'width'     =>  '100',
				'index'     =>  'store_'.$store->getId(),
				'type'      =>  'checkbox',
				'align'     => 'center',
				'sortable'  => false,
				'store_id'	=> $store->getId(),
				'filter'	=> 'itoris_stlc/admin_customerStoreAccess_StoreColumnFilter',
				'renderer'	=> 'itoris_stlc/admin_customerStoreAccess_StoreColumnRenderer',
				'inline_css'=> 'store_'.$store->getId(),
				'values'	=> array(1)
			));
		}

		if (!Mage::app()->isSingleStoreMode()) {
			$this->addColumn('website_id', array(
				'header'    => Mage::helper('customer')->__('Website'),
				'align'     => 'center',
				'width'     => '120',
				'type'      => 'options',
				'options'   => Mage::getSingleton('adminhtml/system_store')->getWebsiteOptionHash(),
				'index'     => 'website_id',
			));
		}

		$this->addColumn('action',
			array(
				'header'    =>  Mage::helper('customer')->__('Action'),
				'width'     => '100',
				'type'      => 'action',
				'getter'    => 'getId',
				'actions'   => array(
					array(
						'caption'   => Mage::helper('customer')->__('Edit'),
						'url'       => array('base'=> 'adminhtml/customer/edit/tab/customer_info_tabs_store_access_settings/'),
						'field'     => 'id'
					)
				),
				'filter'    => false,
				'sortable'  => false,
				'index'     => 'stores',
				'is_system' => true,
		));

		return parent::_prepareColumns();
	}

	public function getGridUrl()
	{
		return $this->getUrl('*/*/grid', array('_current'=> true));
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('adminhtml/customer/edit/tab/customer_info_tabs_store_access_settings/', array('id'=>$row->getId()));
	}

	/**
	 * @return Itoris_StoreLoginControl_Helper_Data
	 */
	public function getDataHelper(){
		return Mage::helper('itoris_stlc');
	}
}
?>