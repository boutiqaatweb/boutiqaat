<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Block_Admin_CustomerStoreAccess_StoreColumnFilter
		extends Mage_Adminhtml_Block_Widget_Grid_Column_Filter_Select{

	/**
	 * Prepare options for the filter
	 *
	 * @return array
	 */
	protected function _getOptions(){
		return array(
			array(
				'label' => '',
				'value' => ''
			),
			array(
				'label' => $this->__('Allowed'),
				'value' => 1
			),
			array(
				'label' => $this->__('Disallowed'),
				'value' => 0
			),
		);
	}

	/**
	 * Render store column filter
	 *
	 * @return string
	 */
	public function getHtml(){
		$parent = parent::getHtml();
		$parent = '<div>'.$parent.'</div>';
		$cb = '<input onchange="CustomerStoreAccess.toggleStore('.$this->getColumn()->getStoreId().', this)" type="checkbox" class="toggle_store_'.$this->getColumn()->getStoreId().'" />';
		//$current = $this->getColumn()->getStoreId();
		$cb = '<div style="text-align:center;">'.$cb.'</div>';
		return $parent.$cb;
	}

	public function getCondition(){
		$value = (int)$this->getValue();
		$storeId = $this->getColumn()->getStoreId();

		return array('eq' => (int)$value);
	}
	
}

?>