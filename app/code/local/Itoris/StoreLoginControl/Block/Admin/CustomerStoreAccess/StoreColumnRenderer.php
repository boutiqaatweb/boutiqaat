<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Block_Admin_CustomerStoreAccess_StoreColumnRenderer
			extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Checkbox{

	/**
	 * Render checkbox
	 *
	 * @param $value
	 * @param $checked
	 * @param $name
	 * @param $class
	 * @return string
	 */
	protected function _getCheckboxHtmlMy($value, $checked, $name, $class)
	{
		$hidden = $this->getDisabled() ? '' : '<input type="hidden" name="'.$name.'[ex]" value="1" />';
		$disabled = $this->getDisabled() ? ' disabled="disabled"' : '';

		$additional = '';
		if($checked != '' && $this->getDisabled()){
			$additional = '<input type="hidden" name="'.$name.'[conn]" value="1" />';
		}

		return $hidden.'<input type="checkbox" name="'.$name.'[conn]" value="1" class="'.$class.' '.
				($this->getColumn()->getInlineCss() ? $this->getColumn()->getInlineCss() : 'checkbox' ).'"'.
				$checked.$disabled.'/>'.$additional;
	}

	/** Custom renderer for store column
	 *
	 * @param Varien_Object $row
	 * @return string
	 */
	public function render(Varien_Object $row){
		$values = $this->getColumn()->getValues();
		$value  = $row->getData($this->getColumn()->getIndex());
		if (is_array($values)) {
			$checked = in_array($value, $values) ? ' checked="checked"' : '';
		}
		else {
			$checked = ($value === $this->getColumn()->getValue()) ? ' checked="checked"' : '';
		}

		$this->setDisabled($this->isDisabled($row->getWebsiteId(), $this->getColumn()->getStoreId()));

		if ($this->getNoObjectId() || $this->getColumn()->getUseIndex()){
			$v = $value;
		} else {
			$v = ($row->getId() != "") ? $row->getId():$value;
		}

		$name = "access[{$row->getEntityId()}][{$this->getColumn()->getStoreId()}]";
		$class = 'ch-customer-'.$row->getEntityId();

		return $this->_getCheckboxHtmlMy($v, $checked, $name, $class);
	}

	public function isDisabled($websiteId, $storeId) {
		/** @var $store Mage_Core_Model_Store */
		$store = Mage::getModel('core/store')->load($storeId);
		return $websiteId != $store->getWebsiteId() && !$this->getDataHelper()->isGlobalShare();
	}

	/**
	 * @return Itoris_StoreLoginControl_Helper_Data
	 */
	public function getDataHelper(){
		return Mage::helper('itoris_stlc');
	}
}
?>