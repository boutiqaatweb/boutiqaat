<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Block_Admin_Index extends Mage_Adminhtml_Block_Widget_Form_Container{

	/**
	 * Prepare settings form container
	 */
	public function __construct() {
		parent::__construct();

		$this->_updateButton('save', 'label', $this->__('Save Settings'));
		$this->_removeButton('delete');
		$this->_removeButton('back');
		$this->_removeButton('reset');
		
		$this->_blockGroup = false;
		$this->_controller = false;
		$this->_mode = false;
	}

	public function getHeaderText() {
		return $this->__('Store Login Access Control');
	}

	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this->setChild('form', $this->getLayout()->createBlock(self::$FORM_BLOCK_NAME));
	}
	
	public static $FORM_BLOCK_NAME = 'itoris_stlc/admin_index_settingsForm';
}

?>