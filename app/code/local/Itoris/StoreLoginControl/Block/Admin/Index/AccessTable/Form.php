<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Block_Admin_Index_AccessTable_Form extends Mage_Core_Block_Template{

	public function getHierarchy(){
		return $this->getModel()->getHierarchy();
	}

	public function getWebsiteSelectHtml(){
		/** @var $select Mage_Core_Block_Html_Select */
		$select = $this->getLayout()->createBlock('core/html_select');
		$select->setOptions($this->getModel()->getWebsiteNames());
		$select->setId('website_select');
		$select->setValue($this->getFirstShownTabWebsite());

		return $select->toHtml();
	}

	public function getFirstShownTabWebsite(){
		$this->getModel()->getHierarchy();
		$result = false;
		foreach($this->getModel()->getHierarchy() as $wid => $w){
			if($result === false || $w['one_store'] == false){
				$result = $wid;
				break;
			}
		}
		
		return $result;
	}

	public function getStoreName($id, $withWebsiteName = false){
		$names = $this->getModel()->getStoreNames($withWebsiteName);
		return $names[$id];
	}

	public function getAllStores($withWebsiteName = false) {
		$names = $this->getModel()->getStoreNames($withWebsiteName);
		return $names;
	}

	public function isWebsiteRegistered($websiteId){
		$website = Mage::getModel('core/website')->load($websiteId);
		return $this->getDataHelper()->isWebsiteRegistered($website);
	}

	public function isDisabledShare($websiteId, $storeId) {
		/** @var $store Mage_Core_Model_Store */
		$store = Mage::getModel('core/store')->load($storeId);
		return $websiteId != $store->getWebsiteId() && !$this->isGlobalShare();
	}

	public function isGlobalShare() {
		return $this->getDataHelper()->isGlobalShare();
	}

	/**
	 * @return Itoris_StoreLoginControl_Helper_Data
	 */
	public function getDataHelper(){
		return Mage::helper('itoris_stlc');
	}
}

?>