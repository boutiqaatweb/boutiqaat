<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Block_Admin_Index_Renderer_Fieldset_Access
		extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset_Element {

    protected $_element;

    protected function _construct() {
        $this->setTemplate('itoris/storelogincontrol/index/index/renderer/fieldset/access.phtml');
    }

    public function getElement() {
        return $this->_element;
    }

    public function render(Varien_Data_Form_Element_Abstract $element) {
        $this->_element = $element;
        return $this->toHtml();
    }

	/**
	 * @return Itoris_StoreLoginControl_Helper_Data
	 */
	public function getDataHelper() {
		return Mage::helper('itoris_stlc');
	}
}
?>