<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Block_Admin_Index_SettingsForm extends Mage_Adminhtml_Block_Widget_Form {

	/**
	 * Prepare settings form
	 *
	 * @return Mage_Adminhtml_Block_Widget_Form
	 */
	public function _prepareForm(){
		$form = new Varien_Data_Form();
		$fieldset = $form->addFieldset('settings_fieldset', array('legend' => $this->__("Settings")));

		$fieldset->addField('enabled', 'select', array(
				'name' => 'settings[enabled]',
				'label' => $this->__('Enabled'),
				'title' => $this->__('Enabled'),
				'required' => true,
				'values' => array(1  =>  $this->__('Yes'), 0 => $this->__('No'))
			)
		);

		$fieldset->addField('allowed_urls', 'textarea', array(
				'name' => 'settings[allowed_urls]',
				'label' => $this->__('Exclude URLs starting with'),
				'title' => $this->__('Exclude URLs starting with'),
				'style' => 'width: 700px',
				'after_element_html' => "<script type=\"text/javascript\">document.observe('dom:loaded', function(){\$('allowed_urls').wrap = 'off';})</script>",
			)
		);

		$accessFieldset = $form->addFieldset('access_fieldset', array('legend' => $this->__('Store Access Configuration')));

		$websitesStores = $this->getDataHelper()->getWebsitesStores();
		$accessFieldset->addField('access', 'checkboxes', array(
				'options' => $websitesStores,
				'values' => $this->getModel()->getStoreAccess(),
			)
		)->setRenderer(new Itoris_StoreLoginControl_Block_Admin_Index_Renderer_Fieldset_Access());

		$redirectFieldset = $form->addFieldset('redirect_fieldset', array('legend' => $this->__('Store Login Redirects')));

		$redirectFieldset->addField('redirects', 'checkboxes', array(
				'options' => $websitesStores,
				'values' => $this->getModel()->getStoreAccess(),
			)
		)->setRenderer(new Itoris_StoreLoginControl_Block_Admin_Settings_Renderer_Fieldset_Redirect());

		$element = new Itoris_StoreLoginControl_Block_Admin_Index_AccessTable();
		$element->setId('accesstable_fieldset');
		$form->addElement($element, false);
		
		$form->setValues($this->getModel()->getData());
		$form->setUseContainer(true);
		$form->setAction($this->getUrl('*/*/saveSettings'));
		$form->setMethod('post');
		$form->setId('edit_form');
		
		$this->setForm($form);
		return parent::_prepareForm();
	}

	/**
	 * @return Itoris_StoreLoginControl_Helper_Data
	 */
	public function getDataHelper(){
		return Mage::helper('itoris_stlc');
	}
}

?>