<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_StoreLoginControl_Block_Login extends Mage_Core_Block_Template {

	private $regFieldsSettings = null;
	private $regFieldsSettingsChecked = false;

	protected function _construct() {
		$this->setTemplate('itoris/storelogincontrol/login.phtml');
		$storesSwitcher = Mage::registry('storesSwitcher');
		$this->setData('storesSwitcher', $this->prepareStoreSwitcher($storesSwitcher));
	}

	private function prepareStoreSwitcher($data) {
		if (empty($data)) {
			return 0;
		}
		$values = array();
		foreach ($data as $store) {
			$values[] = "['" . $store['name'] . "', '" . $store['url'] . "', " . $store['active'] . "]";
		}
		return implode(',', $values);
	}

	protected function _prepareLayout() {
		$headBlock = $this->getLayout()->getBlock('head');
		$headBlock->addItem('js_css', 'itoris/storelogincontrol/css/login.css');
		$headBlock->addItem('js_css', 'calendar/calendar-win2k-1.css');
		if ($this->getDataHelper()->isCustomRegFormActive()) {
			$headBlock->addItem('js_css', 'itoris/regfields/css/main.css');
		}
		$headBlock->addJs('calendar/calendar.js');
		$headBlock->addJs('calendar/calendar-setup.js');

		if (@class_exists('Mage_Captcha_Block_Captcha')) {
			if ($headBlock) {
				$headBlock->addJs('mage/captcha.js');
			}
			$captchaBlock = $this->getLayout()->createBlock('itoris_stlc/form_captcha');
			$captchaBlock->setFormId('user_create')
				->setImgWidth(224)
				->setImgHeight(50);
			$this->setChild('captcha', $captchaBlock);
		}

		return parent::_prepareLayout();
	}

	public function getCustomRegFormHtml() {
		$settingsModel = $this->isActiveRegFieldsManager();
		if ($settingsModel) {
			$registerForm = Mage::app()->getLayout()->createBlock('customer/form_register');
			/** @var $formModel Itoris_RegFields_Model_Form */
			$formModel = Mage::getModel('itoris_regfields/form');
			$websiteId = Mage::app()->getWebsite()->getId();
			$storeId = Mage::app()->getStore()->getId();
			$registerForm->setFormConfig($formModel->getFormConfig($settingsModel->getActiveViewId($websiteId, $storeId)));
			$registerForm->setTemplate('itoris/storelogincontrol/custom_register.phtml');
			return $registerForm->toHtml();
		}

		return '';
	}

	/**
	 * If Registration Fields Manager enabled returned Itoris_RegFields_Model_Settings
	 * or null if not enabled
	 *
	 * @return null|Itoris_RegFields_Model_Settings
	 */
	public function isActiveRegFieldsManager() {
		if (!$this->regFieldsSettingsChecked && Mage::helper('itoris_stlc')->isAllowCustomRegForm() && Mage::helper('itoris_stlc')->isCustomRegFormActive()) {
			$regFieldsHelper = Mage::helper('itoris_regfields');
			if ($regFieldsHelper->isRegisteredAutonomous()) {
				$this->regFieldsSettings = $regFieldsHelper->isEnabled();
			}
		}
		$this->regFieldsSettingsChecked = true;

		return $this->regFieldsSettings;
	}

	public function getCreateAccountUrl() {
		if ($this->isActiveRegFieldsManager()) {
			return Mage::getUrl('itoris_storelogincontrol/customRegistration/createPost');
		}

		return Mage::getUrl('itoris_storelogincontrol/login/createAccount');
	}

	public function getPendingMessage() {
		/** @var $customerSession Mage_Customer_Model_Session */
		$customerSession = Mage::getSingleton('customer/session');
		$customerSession->setUseConfirmAction(false);
		return $this->__('Your account requires moderation');
	}

	public function canShowPendingMessage() {
		/** @var $customerSession Mage_Customer_Model_Session */
		$customerSession = Mage::getSingleton('customer/session');
		return $customerSession->getUseConfirmAction() && !$customerSession->getCustomer()->getConfirmation();
	}

	/**
	 * @return Itoris_StoreLoginControl_Helper_Data
	 */
	public function getDataHelper() {
		return Mage::helper('itoris_stlc');
	}
}
?>