<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Helper_Data extends Mage_Adminhtml_Helper_Data{

	/**
	 * Add store access columns to select
	 *
	 * @param Varien_Db_Select $select
	 */
	public function addStoresAccessColumnsToSelect(Varien_Db_Select $select){
		/** @var $customers Itoris_StoreLoginControl_Model_Customer */
		$customers = Mage::getModel('itoris_stlc/customer');
		
		$stores = Mage::app()->getStores();

		$storeAccess = "select st.store_id from {$customers->getTableName()} as st where st.customer_id=e.entity_id";
		$singleWebsite = count($stores);
		$count = "select count(*) from `{$customers->getTableName()}` as ustcount where ustcount.customer_id = e.entity_id";
		/** @var $store Mage_Core_Model_Store */
		foreach ($stores as $store) {
			$userWebsiteCurrentStore = $store->getWebsiteId();
			$s = new Zend_Db_Expr("if(({$store->getId()} in ({$storeAccess})) or ($singleWebsite)=1 or (($count)=0 and ($userWebsiteCurrentStore)=e.website_id), 1, 0) as store_{$store->getId()}");
			$select->columns($s);
		}
		$select->group('e.entity_id');
	}

	/**
	 * Check database schema
	 *
	 * since 1.2.2 use sql script to create tables in the database
	 */
	public function checkConfiguration() {
		try{
			/** @var $model Itoris_StoreLoginControl_Model_Defaults */
			$model = Mage::getModel('itoris_stlc/defaults');
			if (!$model->hasEntries()) {
				$stores = Mage::getModel('core/store')->getCollection();
				$data = array(
					0 => array(0 => 1), /** admin store. Model should has entries */
				);
				/** @var $storeOrig Mage_Core_Model_Store */
				foreach($stores as $storeOrig) {
					/** @var $storeEnd Mage_Core_Model_Store */
					foreach($stores as $storeEnd) {
						if($storeOrig->getId() != $storeEnd->getId()) {
							if(empty($data[$storeOrig->getId()])) {
								$data[$storeOrig->getId()] = array();
							}

							$data[$storeOrig->getId()][$storeEnd->getId()] = 1;
						}
					}
				}

				$model->save($data);
			}
		} catch (Exception $e) {/* ignore */}
	}

	/**
	 * Is component enabled
	 *
	 * @return bool
	 */
	public function isActive(){
		$this->checkConfiguration();
		$this->initSettings();
		return ((bool)$this->settings->getEnabled()) && $this->isRegisteredAutonomous();
	}

	public function getSettings() {
		$this->initSettings();
		return $this->settings;
	}

	public function isMustLogin(){
		$this->initStoreAccess();
		if(empty($this->storeAccess)){
			return false;
		}
		return ((bool)$this->storeAccess['must_login']);
	}

	public function isAllowRegistration(){
		$this->initStoreAccess();
		if(empty($this->storeAccess)){
			//it means no record in db, so registration allowed by default
			return true;
		}
		return ((bool)$this->storeAccess['allow_registration']);
	}

	public function isAllowCustomRegForm() {
		$this->initStoreAccess();
		if(empty($this->storeAccess)){
			return false;
		}
		return ((bool)$this->storeAccess['use_custom_reg_form']);
	}

	public function initStoreAccess(){
		$this->checkConfiguration();
		if(empty($this->storeAccess)){
			$storeId = Mage::app()->getStore()->getId();
			$this->storeAccess = $this->settings->loadStoreAccess($storeId);
		}
	}

	public function initSettings(){
		if($this->settings === null){
			$this->settings = Mage::getModel('itoris_stlc/settings');
			$this->settings->load();
		}
	}

	public function isActiveAdmin(){
		$this->initSettings();
		$active = (bool)$this->settings->getEnabled();
		return $active && $this->isAdminRegistered();
	}

	public function getAlias(){
		return $this->alias;
	}

	public function tryRegister()
	{
		$this->request = Mage::app()->getRequest();
		if($this->request->isPost() && $this->request->getPost('registration', null) == 'true'){
			$sn = $this->request->getPost('sn', null);
			if($sn == null){
				Mage::getSingleton('adminhtml/session')->addError($this->__('Invalid serial number.'));
				return false;
			}

			$sn = trim($sn);
			try{
				$response = Itoris_Installer_Client::registerCurrentStoreHost($this->getAlias(), $sn);
				if($response == 0){
					Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The component has been registered!'));
					Mage::app()->cleanCache();
				}else{
					Mage::getSingleton('adminhtml/session')->addError($this->__('Invalid serial number!'));
				}
			}catch(Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
	}

	public function isAdminRegistered(){
		try{
			return Itoris_Installer_Client::isAdminRegistered($this->getAlias());
		}catch(Exception $e){
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			return false;
		}
	}

	public function isRegisteredAutonomous(){
		return Itoris_Installer_Client::isRegisteredAutonomous($this->getAlias());
	}

	public function isWebsiteRegistered(Mage_Core_Model_Website $website){
		try{
			return Itoris_Installer_Client::isRegistered($this->getAlias(), $website);
		}catch(Exception $e){
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			return false;
		}
	}

	public function getWebsitesStores(){
		$websites = Mage::app()->getWebsites();
		$data = array();
		foreach($websites as $website){
			$data[$website->getId()]['name'] = $website->getName();
			$stores = $website->getStores();
			foreach($stores as $store){
				$data[$website->getId()]['stores'][] = array('id' => $store->getId(),
															'name' => $store->getName(),
														);
			}
		}

		return $data;
	}

	public function isGlobalShare() {
		/** @var $shareConfig Mage_Customer_Model_Config_Share */
		$shareConfig = Mage::getModel('customer/config_share');
		return $shareConfig->isGlobalScope();
	}

	/**
	 * Check if Registration Fields Manager is installed and active
	 *
	 * @return bool
	 */
	public function isCustomRegFormActive() {
		/** @var $config Mage_Core_Model_Config_Element */
		$config = Mage::getConfig()->getModuleConfig('Itoris_RegFields');
		if ($config->active == 'false') {
			return false;
		}
		return (bool)$config->active;
	}

	public function getRedirectsOptions() {
		return array(
			array(
				'label' => $this->__('Magento Default'),
				'value'	=> Itoris_StoreLoginControl_Model_Redirect::REDIRECT_DEFAULT,
			),
			array(
				'label' => $this->__('Customer Dashboard of Current store'),
				'value' => Itoris_StoreLoginControl_Model_Redirect::REDIRECT_CURRENT_STORE,
			),
			array(
				'label' => 'Customer Dashboard of Native store',
				'value' => Itoris_StoreLoginControl_Model_Redirect::REDIRECT_NATIVE_STORE,
			),
			array(
				'label' => 'Homepage of Current store',
				'value' => Itoris_StoreLoginControl_Model_Redirect::REDIRECT_HOMECURRENT_STORE,
			),
			array(
				'label' => 'Homepage of Native store',
				'value' => Itoris_StoreLoginControl_Model_Redirect::REDIRECT_HOMENATIVE_STORE,
			),
			array(
				'label' => 'Custom URL',
				'value' => Itoris_StoreLoginControl_Model_Redirect::REDIRECT_CUSTOM_URL,
			)
		);
	}

	/** @var Itoris_StoreLoginControl_Model_Settings */
	protected $settings = null;
	protected $storeAccess = array();
	protected $alias = 'store_login_control';
	/** @var Mage_Core_Controller_Request_Http */
	protected $request;
}

?>