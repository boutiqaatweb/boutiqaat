<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Model_Customer extends Varien_Object{

	public function __construct(){
		$this->tableName = Mage::getSingleton('core/resource')->getTableName($this->tableName);
	}

	public function getTableName(){
		return $this->tableName;
	}

	public function loadCustomer(Mage_Customer_Model_Customer $customer, $allWebsites = false){
		$websiteId = (int) $customer->getWebsiteId();
		$customerId = (int) $customer->getId();

		$websiteSql = $allWebsites ? '' : "and st.website_id = $websiteId";

		$storeTableName = Mage::getSingleton('core/resource')->getTableName('core_store');
		$sql = "select * from {$this->tableName} as e left join {$storeTableName} as st ".
				"on e.store_id = st.store_id".
				" where e.customer_id = $customerId {$websiteSql}";
		/** @var $db Varien_Db_Adapter_Pdo_Mysql */
		$db = Mage::getSingleton('core/resource')->getConnection('core_write');
		$data = $db->query($sql);
		$data = $data->fetchAll();
		$allowed = array();
		foreach($data as $row){
			$allowed[] = $row['store_id'];
		}

		/** @var $stores Mage_Core_Model_Mysql4_Store_Collection */
		$stores = Mage::getModel('core/store')->getCollection();
		if (!$allWebsites) {
			$stores->addWebsiteFilter($customer->getWebsiteId());
		}

		$result = array();
		if($allowed){
			foreach($stores as $store){
				$result[$store->getId()] = in_array($store->getId(), $allowed);
			}
		}else{
			foreach($stores as $store){
				$result[$store->getId()] = true;
			}
		}

		return $result;
	}

	/**
	 * Save customers data to the db. $data in following format:
	 * $data[<customer_id>][<store_id>] = true|false;
	 * @param $data
	 * @return void
	 */
	public function save($data){
		$toDelete = array();
		$toInsert = array();

		foreach($data as $customerId => $customerData){
			foreach($customerData as $storeId => $hasAccess){
				$customerId = (int) $customerId;
				$storeId = (int) $storeId;
				if($hasAccess){
					$toInsert[] = "($customerId, $storeId)";
				}else{
					$toDelete[] = "(customer_id=$customerId and store_id=$storeId)";
				}
			}
		}

		/** @var $db Varien_Db_Adapter_Pdo_Mysql */
		$db = Mage::getSingleton('core/resource')->getConnection('core_write');
		if(count($toDelete) > 0){
			$sql = "delete from `{$this->tableName}` where ".implode(' or ',$toDelete);
			$db->query($sql);
		}

		if(count($toInsert) > 0){
			$sql = "replace into `{$this->tableName}` (customer_id, store_id) values".implode(',', $toInsert);
			$db->query($sql);
		}
	}

	protected $tableName = 'itoris_storelogincontrol_customer';
}

?>