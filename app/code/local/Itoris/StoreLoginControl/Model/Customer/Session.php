<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_StoreLoginControl_Model_Customer_Session extends Mage_Customer_Model_Session {

	public function login($username, $password) {
		if (!method_exists($this, 'renewSession')) {
			return parent::login($username, $password);
		}
		/** @var $customer Mage_Customer_Model_Customer */
		$customer = Mage::getModel('customer/customer')
			->setWebsiteId(Mage::app()->getStore()->getWebsiteId());

		if ($customer->authenticate($username, $password)) {
			$this->renewSession();
			$this->setCustomerAsLoggedIn($customer);
			return true;
		}
		return false;
	}
}
?>