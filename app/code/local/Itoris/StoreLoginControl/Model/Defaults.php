<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Model_Defaults {

	public function __construct(){
		$this->tableName = Mage::getSingleton('core/resource')->getTableName($this->tableName);
	}

	public function load(){
		if($this->data !== null) {
			return;
		}

		$coreStoreTableName = Mage::getSingleton('core/resource')->getTableName('core_store');
		$coreWebsiteTableName = Mage::getSingleton('core/resource')->getTableName('core_website');

		/** @var $db Varien_Db_Adapter_Pdo_Mysql */
		$db = Mage::getSingleton('core/resource')->getConnection('core_write');
		/** @var $result Zend_Db_Statement_Pdo */
		$queryResult = $db->query(
"select e.website_id as website, w.name as website_name, e2.store_id as end_store_id, e2.name as end_store_name, e.store_id as orig_store_id, e.name as  orig_store_name,
if ((select count(*) from `{$this->tableName}` as d where d.orig_store_id = e.store_id and d.end_store_id=e2.store_id) > 0, 1, 0) as conn

from `$coreStoreTableName` as e

inner join `$coreStoreTableName` as e2 on e2.code <> 'admin'
left join `$coreWebsiteTableName` as w on w.website_id = e.website_id
where e.code<>'admin'");

		$data = $queryResult->fetchAll();
		$this->data = $data;
	}

	public function getWebsiteNames(){
		$this->load();

		$names = array();
		foreach($this->data as $row){
			$names[$row['website']] = $row['website_name'];
		}
		
		return $names;
	}

	public function getStoreNames($withWebsiteName = false){
		$this->load();

		$names = array();
		foreach($this->data as $row){
			$names[$row['orig_store_id']] 	= ($withWebsiteName ? $row['website_name'] . '::' : '') . $row['orig_store_name'];
			$names[$row['end_store_id']]	= ($withWebsiteName ? $row['website_name'] . '::' : '') . $row['end_store_name'];
		}

		return $names;
	}

	public function getHierarchy(){
		$this->load();
		$hierarchy = array();
		foreach($this->data as $row){
			$wid = $row['website'];
			if(empty($hierarchy[$wid])){
				$hierarchy[$wid] = array();
				$hierarchy[$wid]['one_store'] = true;
			}
			
			$osid = $row['orig_store_id'];
			if(empty($hierarchy[$wid][$osid])){
				$hierarchy[$wid][$osid] = array();
			}
			$esid = $row['end_store_id'];
			if($osid != $esid){
				$hierarchy[$wid][$osid][$esid] = (boolean)$row['conn'];
				$hierarchy[$wid]['one_store'] = false;
			}
		}

		return $hierarchy;
	}

	/**
	 * Save data to the database in format
	 * $data[orig_store][end_store] = 1|0
	 * @param $data
	 * @return void
	 */
	public function save($data){
		/** @var $db Varien_Db_Adapter_Pdo_Mysql */
		$db = Mage::getSingleton('core/resource')->getConnection('core_write');

		$toDelete = array();
		$toInsert = array();
		foreach($data as $origStoreId => $endStoresId){
			foreach($endStoresId as $endStoreId => $conn){
				$origStoreId = (int) $origStoreId;
				$endStoreId = (int) $endStoreId;

				if(($conn = (int) $conn) == 0) {
					$toDelete[] = "(`orig_store_id`=$origStoreId and `end_store_id`=$endStoreId)";
				}else if($conn == 1){
					$toInsert[] = "($origStoreId, $endStoreId)";
				}else{
					throw new Exception("Something is wrong here!");
				}
			}
		}

		if(count($toDelete) > 0){
			$sql = "delete from {$this->tableName} where ".implode(' or ',$toDelete);
			$db->query($sql);
		}

		if(count($toInsert) > 0){
			$sql = "replace into {$this->tableName} (`orig_store_id`, `end_store_id`) values".implode(',', $toInsert);
			$db->query($sql);
		}
	}

	public function hasEntries() {
		/** @var $db Varien_Db_Adapter_Pdo_Mysql */
		$db = Mage::getSingleton('core/resource')->getConnection('core_write');
		$entries = $db->fetchAll("select * from {$this->tableName}");
		return count($entries);
	}

	public function deleteAndSave($data) {
		$toInsert = array();
		if (is_array($data)) {
			foreach($data as $origStoreId => $endStoreIds) {
				foreach ($endStoreIds as $endStoreId => $value) {
					$toInsert[] = "($origStoreId, $endStoreId)";
				}
			}
		}
		/** @var $db Varien_Db_Adapter_Pdo_Mysql */
		$db = Mage::getSingleton('core/resource')->getConnection('core_write');

		$sql = "delete from {$this->tableName} where orig_store_id > 0";
		$db->query($sql);

		if(count($toInsert) > 0){
			$sql = "insert into {$this->tableName} (`orig_store_id`, `end_store_id`) values".implode(',', $toInsert);
			$db->query($sql);
		}
	}

	protected $data;
	protected $tableName = 'itoris_storelogincontrol_defaults';
}

?>