<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Model_Entity_Customer_Collection extends Mage_Customer_Model_Entity_Customer_Collection{

	/**
	 * Overrider default behaviour. If attrbute to filter starts with 'store_' then
	 * simply add condition to the select object
	 * @param $attribute
	 * @param null $condition
	 * @param string $joinType
	 * @return Itoris_StoreLoginControl_Model_Entity_Customer_Collection|Mage_Eav_Model_Entity_Collection_Abstract
	 */
	public function addAttributeToFilter($attribute, $condition = null, $joinType = 'inner') {
		if(substr($attribute, 0, 6) == 'store_') {
			$this->_select->having($this->_getConditionSql($attribute, $condition), null, 'TYPE_CONDITION');
			return $this;
		}
		return parent::addAttributeToFilter($attribute, $condition, $joinType);
	}

	/**
	 * Get all selected ids
	 *
	 * @return array
	 */
	public function getAllIdsSql() {
		$idsSelect = clone $this->getSelect();
		$idsSelect->reset(Zend_Db_Select::ORDER);
		$idsSelect->reset(Zend_Db_Select::LIMIT_COUNT);
		$idsSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
		$idsSelect->reset(Zend_Db_Select::COLUMNS);
		$idsSelect->reset(Zend_Db_Select::HAVING);
		$idsSelect->columns(
			'main_table.' . $this->getResource()->getIdFieldName()
		);
		return $this->getConnection()->fetchCol($idsSelect, $this->_bindParams);
	}

	protected function setTotalRecords() {
		$countSelect = clone $this->getSelect();
		$countSelect->reset(Zend_Db_Select::LIMIT_COUNT);
		$countSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
		$this->_totalRecords = count($this->_fetchAll($countSelect));
	}

	/**
	 * Get collection size
	 *
	 * @return int
	 */
	public function getSize() {
		$this->setTotalRecords();
		return $this->_totalRecords;
	}
}

?>