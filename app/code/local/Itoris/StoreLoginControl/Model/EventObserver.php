<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Model_EventObserver{

	/**
	 * Event handler for 'adminhtml_customer_save_after'
	 *
	 * @param Varien_Event_Observer $observer
	 * @return mixed
	 */
	public function adminSaveCustomer(Varien_Event_Observer $observer){
		if(!$this->getDataHelper()->isAdminRegistered()) return;
		
		/** @var $customer Mage_Customer_Model_Customer */
		$customer = $observer->getCustomer();
		$customerId = (int)$customer->getId();
		/** @var $request Mage_Core_Controller_Request_Http */
		$request = Mage::app()->getRequest();

		$account = $request->getPost('account');
		if(isset($account['access'])){
			$access = $account['access'];
			$data = array($customerId => array());
			foreach($access as $storeId => $storeData){
				if($storeData['ex'] != 1) continue;
				$storeId = (int) $storeId;
				$data[$customerId][$storeId] = (isset($storeData['conn']) && $storeData['conn'] == 1);
			}
			/** @var $model Itoris_StoreLoginControl_Model_Customer */
			$model = Mage::getModel('itoris_stlc/customer');
			$model->save($data);
		}
	}

	/**
	 * Event handler for 'customer_save_before'
	 *
	 * @param Varien_Event_Observer $observer
	 * @return mixed
	 */
	public function customerSaveBefore(Varien_Event_Observer $observer){
		if(!$this->getDataHelper()->isActive()) return;
		/** @var $customer Mage_Customer_Model_Customer */
		$customer = $observer->getCustomer();
		if(!$customer->getId()){
			$customer->setItorisStoreLoginControlNewlyCreated(true);
		}
	}

	/**
	 * Event handler for 'customerSaveAfter'
	 *
	 * @param Varien_Event_Observer $observer
	 * @return mixed
	 */
	public function customerSaveAfter(Varien_Event_Observer $observer){
		if(!$this->getDataHelper()->isActive()) return;
		/** @var $customer Mage_Customer_Model_Customer */
		$customer = $observer->getCustomer();
		if($customer->getItorisStoreLoginControlNewlyCreated()){
			/** @var $defaults Itoris_StoreLoginControl_Model_Defaults */
			$defaults = Mage::getModel('itoris_stlc/defaults');
			$defaults->load();
			$hierarchy = $defaults->getHierarchy();
			$customerId = $customer->getId();

			$storeId = (int)$customer->getStoreId();
			$websiteId = (int)$customer->getWebsiteId();
			$access = $hierarchy[$websiteId][$storeId];
			$data = array($customerId => array());
			$data[$customerId][$storeId] = true;
			foreach($access as $allowedStoreId => $isAllowed){
				$data[$customerId][$allowedStoreId] = (bool) $isAllowed;
			}

			/** @var $model Itoris_StoreLoginControl_Model_Customer */
			$model = Mage::getModel('itoris_stlc/customer');
			$model->save($data);
		}
	}

	/**
	 * Event handler for 'controller_action_predispatch'
	 *
	 * @param Varien_Event_Observer $observer
	 * @return mixed
	 */
	public function preDispatch(Varien_Event_Observer $observer){
		if(!$this->getDataHelper()->isActive()) return;
		if ($this->isAllowedUrl()) return;
		if ($this->isSecureCheck()) {
			Mage::app()->getResponse()->setHeader('HTTP/1.1','404 Not Found');
			Mage::app()->getResponse()->setHeader('Status','404 File not found');
		}
		if($this->getCustomerSession()->isLoggedIn()){
			/** @var $model Itoris_StoreLoginControl_Model_Customer */
			$model = Mage::getModel('itoris_stlc/customer');
			$data = $model->loadCustomer($this->getCustomerSession()->getCustomer(), true);
			$storeId = Mage::app()->getStore()->getId();
			if(!$data[$storeId] || (!$this->getDataHelper()->isGlobalShare() && $this->getCustomerSession()->getCustomer()->getWebsiteId() != Mage::app()->getWebsite()->getId())) {
				$this->getCustomerSession()->logout();
				session_regenerate_id(true);
				if(method_exists($this->getCustomerSession(), 'regenerateSessionId')){
					$this->getCustomerSession()->regenerateSessionId();
				}else{
					session_regenerate_id(true);
				}

				$this->getCustomerSession()->addError($this->getDataHelper()->__("You are trying to access the private area of the store your account is not permitted for. Please enter a valid account information."));
				Mage::app()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
				Mage::app()->getResponse()->sendHeaders();
				exit;
			}
		}else{
			if($this->getDataHelper()->isMustLogin()) {
				if ($this->isStickerExtensionRequest() || $this->isCaptchaRequest()) {
					return;
				}
				/** @var $controller Mage_Core_Controller_Front_Action */
				$controller = $observer->getData('controller_action');
				if ($controller instanceof Mage_Customer_AccountController && (Mage::app()->getRequest()->getActionName() == 'confirm' || Mage::app()->getRequest()->getActionName() == 'createPost')) {
					if (Mage::app()->getRequest()->getActionName() == 'confirm') {
						$this->getCustomerSession()->setUseConfirmAction(true);
					}
					return;
				}
				if ($controller instanceof Itoris_RegFields_CaptchaController) {
					return;
				}
				if(Mage::app()->getRequest()->getPathInfo() != '/itoris_storelogincontrol/login/'){
					if(Mage::app()->getRequest()->getPathInfo() == '/customer/account/logoutSuccess/'){
						Mage::getSingleton('core/session')->setPrevUrl(Mage::getBaseUrl());
					}else{
						Mage::getSingleton('core/session')->setPrevUrl(Mage::app()->getRequest()->getOriginalRequest()->getRequestUri());
					}
				}

				if( get_class($controller) != 'Itoris_StoreLoginControl_LoginController'){
					if(Mage::app()->getRequest()->getPathInfo() != '/customer/account/logoutSuccess/'){
						$this->showLoginForm($controller);
					}
				}else{
					if(Mage::app()->getRequest()->getPathInfo() == '/itoris_storelogincontrol/login/'){
						$this->showLoginForm($controller);
					}
				}
			}
		}
	}

	public function isAllowedUrl() {
		$allowedUrls = $this->getDataHelper()->getSettings()->getAllowedUrlsAsArray();
		if (!empty($allowedUrls)) {
			$currentUrl = Mage::helper('core/url')->getCurrentUrl();
			foreach ($allowedUrls as $url) {
				if (strpos($currentUrl, $url) === 0) {
					return true;
				}
			}
		}
		return false;
	}

	public function isSecureCheck() {
		return Mage::app()->getRequest()->getPathInfo() == '/app/etc/local.xml';
	}

	private function isStickerExtensionRequest() {
		$request = Mage::app()->getRequest();
		return $request->getModuleName() == 'stickers' && $request->getControllerName() == 'index' && $request->getActionName() == 'stickerImage';
	}

	private function isCaptchaRequest() {
		$request = Mage::app()->getRequest();
		return $request->getModuleName() == 'captcha' && $request->getControllerName() == 'refresh';
	}

	private function showLoginForm($controller){
		$storesSwitcher = array();

			if(Mage::app()->getWebsite()->getStoresCount() > 1){
				$stores = Mage::app()->getWebsite()->getStores();
				/** @var $store Mage_Core_Model_Store  */
				foreach($stores as $store){
					if($store->getIsActive()){
						$params = array(
							'_query' => array()
						);
						$params['_query']['___store'] = $store->getCode();
						$currentStore = (Mage::app()->getStore()->getId() == $store->getId()) ? 1 : 0;
						$baseUrl = $store->getUrl('', $params);
						$storesSwitcher[] = array('name' => $store->getName(),
												'url' => $baseUrl,
												'active' => $currentStore,
											);
					}
				}
				if(count($storesSwitcher) == 1){
					$storesSwitcher = array();
				}
			}

		Mage::register('storesSwitcher', $storesSwitcher);

		$controller->getLayout()->getUpdate()->resetHandles();
		$controller->getLayout()->getUpdate()->addHandle('itoris_store_login_control');

		$controller->loadLayoutUpdates();
		$controller->generateLayoutXml();
		$controller->generateLayoutBlocks();

		/** @var $head Mage_Page_Block_Html_Head */
		$head = $controller->getLayout()->getBlock('head');
		if ($head) {
			$items = $head->getData('items');
			if (is_array($items)) {
				foreach ($items as $key => $item) {
					if ($item['type'] = 'js') {
						if (strpos($key, 'itoris') === false && !$this->isDefaultJs($item['name'])) {
							$head->removeItem($item['type'], $item['name']);
						}
					}
				}
			}
		}

		$controller->renderLayout();
		Mage::app()->getResponse()->sendResponse();
		exit;
	}

	/**
	 * Event handler for 'customer_customer_authenticated'
	 *
	 * @param \Varien_Event_Observer $observer
	 * @return
	 * @internal param $controller
	 */
	public function authenticateCustomer(Varien_Event_Observer $observer){
		if(!$this->getDataHelper()->isActive()) return;

		$model = Mage::getModel('itoris_stlc/customer');
		$customer = $observer->getModel();
		$data = $model->loadCustomer($customer, true);
		$storeId = Mage::app()->getStore()->getId();
		if(!$data[$storeId] || (!$this->getDataHelper()->isGlobalShare() && $this->getCustomerSession()->getCustomer()->getWebsiteId() != Mage::app()->getWebsite()->getId())) {
			throw new Mage_Core_Exception($this->getDataHelper()->__("Your account is not permitted to access the private area of the store."));
		}
	}

	/**
	 * @return Mage_Customer_Model_Session
	 */
	protected function getCustomerSession(){
		return Mage::getSingleton('customer/session');
	}

	/**
	 * @return Itoris_StoreLoginControl_Helper_Data
	 */
	protected function getDataHelper(){
		return Mage::helper('itoris_stlc');
	}

	private function isDefaultJs($name) {
		$defaultJs = array(
			'prototype/prototype.js',
			'lib/ccard.js',
			'prototype/validation.js',
			'scriptaculous/builder.js',
			'scriptaculous/effects.js',
			'scriptaculous/dragdrop.js',
			'scriptaculous/controls.js',
			'scriptaculous/slider.js',
			'varien/js.js',
			'varien/form.js',
			'varien/menu.js',
			'mage/translate.js',
			'mage/cookies.js',
			'calendar/calendar.js',
			'calendar/calendar-setup.js',
			'mage/captcha.js',
		);

		return in_array($name, $defaultJs);
	}

	public function redirectAfterLogin($obj) {
		$customer = $obj->getCustomer();
		$redirect = Mage::getModel('itoris_stlc/redirect')->load(Mage::app()->getStore()->getId());
		if ($redirect->getId() && !$redirect->isDefault() && $customer->getId()) {
			Mage::getSingleton('customer/session')->setBeforeAuthUrl($redirect->getRedirectUrl($customer->getStoreId()))
				->setAfterAuthUrl($redirect->getRedirectUrl($customer->getStoreId()));
		}
	}
}

?>