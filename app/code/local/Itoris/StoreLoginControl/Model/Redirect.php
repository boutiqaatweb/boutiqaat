<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_StoreLoginControl_Model_Redirect extends Mage_Core_Model_Abstract {

	const REDIRECT_DEFAULT = 1;
	const REDIRECT_CURRENT_STORE = 2;
	const REDIRECT_NATIVE_STORE = 3;
	const REDIRECT_HOMECURRENT_STORE = 4;
	const REDIRECT_HOMENATIVE_STORE = 5;
	const REDIRECT_CUSTOM_URL = 6;

	public function __construct() {
		$this->_init('itoris_stlc/redirect');
	}

	public function isCustomUrl() {
		return $this->getType() == self::REDIRECT_CUSTOM_URL;
	}

	public function isDefault() {
		return $this->getType() == self::REDIRECT_DEFAULT;
	}

	public function getRedirectUrl($storeId) {
		switch ($this->getType()) {
			case self::REDIRECT_CURRENT_STORE:
				return Mage::app()->getStore()->getUrl('customer/account');
			case self::REDIRECT_NATIVE_STORE:
				return Mage::app()->getStore($storeId)->getUrl('customer/account', array('_store_to_url' => true));
			case self::REDIRECT_HOMECURRENT_STORE:
				return Mage::app()->getStore()->getBaseUrl();
			case self::REDIRECT_HOMENATIVE_STORE:
				return Mage::app()->getStore($storeId)->getUrl('',  array('_store_to_url' => true));
			case self::REDIRECT_CUSTOM_URL:
				return $this->getCustomUrl();
			default:
				return Mage::getSingleton('core/session')->getPrevUrl();
		}
	}
}
?>