<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Model_Settings extends Varien_Object {

	protected $tableName = 'itoris_storelogincontrol_settings';
	protected $storeAccessTable = 'itoris_storelogincontrol_store_access';
	protected $textSettings = array('allowed_urls');

	public function __construct(){
		$this->tableName = Mage::getSingleton('core/resource')->getTableName($this->tableName);
		$this->storeAccessTable = Mage::getSingleton('core/resource')->getTableName($this->storeAccessTable);
	}

	public function load(){

		/** @var $db Varien_Db_Adapter_Pdo_Mysql */
		$db = Mage::getSingleton('core/resource')->getConnection('core_write');

		$sql = "select * from {$this->tableName}";
		/** @var $data Zend_Db_Statement_Pdo  */
		$data = $db->query($sql);
		$data = $data->fetchAll();
		foreach($data as $row){
			$this->setData($row['key'], in_array($row['key'], $this->textSettings) ? $row['value_text'] : $row['value']);
		}

		$this->loadStoreAccess();

		return $this;
	}

	public function loadStoreAccess($storeId = null){

		/** @var $db Varien_Db_Adapter_Pdo_Mysql */
		$db = Mage::getSingleton('core/resource')->getConnection('core_write');

		$sql = "select * from {$this->storeAccessTable}";

		if($storeId){
			$sql = "select * from {$this->storeAccessTable} where `store_id`=" . (int)$storeId . "";
		}

		/** @var $data Zend_Db_Statement_Pdo  */
		$data = $db->query($sql);
		$data = $data->fetchAll();

		if($storeId){
			if(empty($data)){
				return null;
			}else{
				return $data[0];
			}
		}

		$stores = $this->prepareStoreAccessArray($data);
		$this->setStoreAccess($stores);
	}

	private function prepareStoreAccessArray($data){
		$stores = array();
		if(!empty($data)){
			foreach($data as $store){
				$stores[$store['store_id']]['must_login'] = (int)$store['must_login'];
				$stores[$store['store_id']]['allow_registration'] = (int)$store['allow_registration'];
				$stores[$store['store_id']]['use_custom_reg_form'] = (int)$store['use_custom_reg_form'];
			}
		}

		return $stores;
	}

	public function save($data){
		$toInsert = array();

		/** @var $db Varien_Db_Adapter_Pdo_Mysql */
		$db = Mage::getSingleton('core/resource')->getConnection('core_write');
		foreach($data as $key => $value){
			$quotedKey = $db->quote($key);
			$value = $db->quote($value);
			$toInsert[] = in_array($key, $this->textSettings) ? "($quotedKey, null, $value)" : "($quotedKey, $value, null)";
		}

		$sql = "replace into {$this->tableName} (`key`, `value`, `value_text`) values ".implode(',',$toInsert);
		$db->query($sql);
		
		return $this;
	}

	public function saveStoreAccess($data) {
		$toInsert = array();

		foreach($data as $storeId => $storeAccess){
			$mustLogin = 0;
			$allowRegistration = 0;
			$useCustomRegForm = 0;
			if(is_array($storeAccess)){
				$mustLogin = isset($storeAccess['must_login']) ? 1 : 0;
				$allowRegistration = isset($storeAccess['allow_registration']) ? 1 : 0;
				$useCustomRegForm = isset($storeAccess['use_custom_reg_form']) ? 1 : 0;
			}
			$toInsert[] = "(" . (int)$storeId . ", $mustLogin, $allowRegistration, $useCustomRegForm)";
		}

		$sql = "replace into {$this->storeAccessTable} (`store_id`, `must_login`, `allow_registration`, `use_custom_reg_form`)
				values " . implode(',', $toInsert) . "
				";
		/** @var $db Varien_Db_Adapter_Pdo_Mysql */
		$db = Mage::getSingleton('core/resource')->getConnection('core_write');
		$db->query($sql);
	}

	public function getAllowedUrlsAsArray() {
		$urls = array();
		$savedUrls = $this->getAllowedUrls();
		$savedUrls = explode(PHP_EOL, $savedUrls);
		foreach ($savedUrls as $url) {
			$url = trim($url);
			if (strlen($url)) {
				$urls[] = $url;
			}
		}
		return $urls;
	}
}

?>