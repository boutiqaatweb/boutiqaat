<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Admin_CustomerStoreAccessController extends Itoris_StoreLoginControl_Controller_Admin_Controller{

	/**
	 * Show access grid
	 */
	public function indexAction(){
		$this->loadLayout();
		$this->_setActiveMenu('customer/itoris_storelogincontrol_customer_store_access');
		$this->_addContent($this->getLayout()->createBlock('itoris_stlc/admin_customerStoreAccess'));
		$this->renderLayout();
	}

	/**
	 * Save grid data
	 */
	public function saveAction(){

		try {
			$access = $this->getRequest()->getPost("access");
			$data = array();
			foreach($access as $customerId => $customerData){
				$customerId = (int) $customerId;
				foreach($customerData as $storeId => $accessData){
					$storeId = (int) $storeId;
					if(isset($accessData['ex']) && $accessData['ex'] == 1){
						$hasAccess = (bool)(isset($accessData['conn']) && $accessData['conn'] == 1);
						if(empty($data[$customerId])){
							$data[$customerId] = array();
						}
						$data[$customerId][$storeId] = $hasAccess;
					}
				}
			}
			/** @var $model Itoris_StoreLoginControl_Model_Customer */
			$model = Mage::getModel('itoris_stlc/customer');
			$model->save($data);
			$this->_getSession()->addSuccess($this->__('Changes have been applied to the customers’ access configuration.'));
		}catch(Exception $e){
			$this->_getSession()->addError($e->getMessage());
		}

		$this->_redirect('*/*/index');

	}

	/**
	 * Return grid post url
	 */
	public function gridAction(){
		$this->loadLayout();
		$this->getResponse()->setBody($this->getLayout()->createBlock('itoris_stlc/admin_customerStoreAccess_grid')->toHtml());
	}

	protected function _isAllowed(){
		return Mage::getSingleton('admin/session')->isAllowed('system/itoris_extensions/itoris_storelogincontrol');
	}

}

?>