<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_StoreLoginControl_Admin_IndexController extends Itoris_StoreLoginControl_Controller_Admin_Controller {

	/**
	 * Show settings form
	 */
	public function indexAction(){
		$this->loadLayout();

		$this->_setActiveMenu('system/itoris_extensions/itoris_storelogincontrol');
		/** @var $model Itoris_StoreLoginControl_Model_Settings */
		$model = Mage::getModel('itoris_stlc/settings');
		/** @var $form Itoris_StoreLoginControl_Block_Admin_Index_SettingsForm */
		$form = $this->getLayout()->getBlock('admin_index')->getChild('form');
		$form->setModel($model->load());
		
		$this->renderLayout();
	}

	/**
	 * Save settings
	 */
	public function saveSettingsAction(){
		if(!$this->getRequest()->isPost()){
			$this->_redirect('*/*/index');
		}

		try {
			/** @var $model Itoris_StoreLoginControl_Model_Settings */
			$model = Mage::getModel('itoris_stlc/settings');
			$postData = $this->getRequest()->getPost('settings');
			$model->save($postData);

			$storeAccessData = $this->getRequest()->getPost('store_access');
			$model->saveStoreAccess($storeAccessData);

			$storeRedirects = $this->getRequest()->getParam('store_redirects');
			if (is_array($storeRedirects)) {
				foreach ($storeRedirects as $storeRedirect) {
					$redirect = Mage::getModel('itoris_stlc/redirect')->load($storeRedirect['store_id']);
					$redirect->addData($storeRedirect);
					$redirect->save();
				}
			}

			/** @var $access Itoris_StoreLoginControl_Model_Defaults */
			$access = Mage::getModel('itoris_stlc/defaults');
			$accesses = $this->getRequest()->getPost('access');
			$access->deleteAndSave($accesses);

			$this->_getSession()->addSuccess($this->__("Configuration saved successfully."));
		} catch(Exception $e) {
			$this->_getSession()->addError($e->getMessage());
		}

		$this->_redirect('*/*/index');
	}

	/**
	 * Save access table
	 */
	public function saveAccessTableAction(){
		$response = new Varien_Object(array('error'=>false));
		Mage::register('response', $response);
		try{
			/** @var $model Itoris_StoreLoginControl_Model_Defaults */
			$model = Mage::getModel('itoris_stlc/defaults');
			$postData = $this->getRequest()->getPost('access');
			$model->save($postData);
			$response->setWid($this->getRequest()->getPost('wid', null));
			$response->setMessage($this->__("Website configuration saved successfully."));
		}catch(Exception $e){
			$response->setError(true);
			$response->setMessage($e->getMessage());
		}

		$this->getResponse()->setHeader('Content-type', 'application/json');
		$this->getResponse()->setBody($response->toJson());
	}

	protected function _isAllowed(){
		return Mage::getSingleton('admin/session')->isAllowed('customer/itoris_storelogincontrol_customer_store_access');
	}
}
?>