<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

require_once Mage::getModuleDir(null, 'Itoris_RegFields') . DS . 'controllers' . DS . 'AccountController.php';

class Itoris_StoreLoginControl_CustomRegistrationController extends Itoris_RegFields_AccountController {

	public function createPostAction() {
		$session = $this->_getSession();
		$session->setCustomerIsLoggedIn(false);
		$session->getMessages(true);
		$customer = parent::createPostAction();
		$messages = $session->getMessages()->getItems();
		$result = array();
		if (!$session->getCustomerIsLoggedIn() && count($messages)) {
			$errors = array();
			foreach ($messages as $message) {
				$errors[] = $message->getText();
			}
			$result['error'] = implode(' ', $errors);
		}
		$html = '';
		if ($customer instanceof Mage_Customer_Model_Customer && $customer->getPendingMessage()) {
			if (!isset($result['error'])) {
				$result['error'] = '';
			}
			$result['error'] = $customer->getPendingMessage();
			$html .= '<div id="registered"></div>';
		} else {
			$customer = $session->getCustomer();
			if ($customer && $customer->getId() && $customer->isConfirmationRequired()) {
				$html .= '<div id="registered"></div>';
			}
		}
		if (isset($result['error'])) {
			$html .= '<div id="error">'. $result['error'] .'</div>';
		} else {
			$html .= '<div id="success"></div>';
		}
		$this->getResponse()->clearHeaders()->setHttpResponseCode(200);
		$this->getResponse()->setBody($html);
	}
}
?>