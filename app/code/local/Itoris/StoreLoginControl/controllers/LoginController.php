<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_StoreLoginControl_LoginController extends Mage_Core_Controller_Front_Action
{
	public function indexAction(){
		$this->_redirectUrl(Mage::getBaseUrl());
	}

	public function loginAction(){
		$this->checkLogin();
		/** @var $session Mage_Customer_Model_Session */
        $session = Mage::getSingleton('customer/session');
		$session->getMessages(true);
		$result = array();
        if ($this->getRequest()->isPost()) {
            $login = array('username' => $this->getRequest()->getPost('email'),
							'password' => $this->getRequest()->getPost('password')
						  );
            if (!empty($login['username']) && !empty($login['password'])) {
                try {
                    $session->login($login['username'], $login['password']);
					if (!$session->isLoggedIn()) {
						$errors = $session->getMessages()->getItems('error');
						$result['error'] = '';
						foreach ($errors as $error) {
							$result['error'] .= $error->getText();
						}
					} else {
						$redirect = Mage::getModel('itoris_stlc/redirect')->load(Mage::app()->getStore()->getId());
						if ($redirect->getId() && !$redirect->isDefault()) {
							$result['redirect_url'] = $redirect->getRedirectUrl($session->getCustomer()->getStoreId());
						}
						$result['error'] = false;
					}
                } catch (Exception $e) {
              		$result['error'] = $this->__('Email and password do not match');
                }
            } else {
                $result['error'] = $this->__('Login and password are required.');
            }
		}
		$this->getResponse()->setBody(Zend_Json::encode($result));
    }

	 public function forgotPasswordAction(){
		$this->checkLogin();
        $email = $this->getRequest()->getPost('email');

        if ($email) {
            $customer = Mage::getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByEmail($email);

            if ($customer->getId()) {
                try {
                    $newPassword = $customer->generatePassword();
                    $customer->changePassword($newPassword, false);
                    $customer->sendPasswordReminderEmail();

					$this->getResponse()->setBody($this->__('A new password has been sent.'));
                }
                catch (Exception $e){
					$this->getResponse()->setBody('error');
				}
            } else {
				$this->getResponse()->setBody($this->__('This email address was not found in our records.'));
            }
        }
    }

	public function checkLogin(){
		if ($this->getCustomerSession()->isLoggedIn() || !Mage::helper('itoris_stlc')->isMustLogin()) {
			$pageId = Mage::getStoreConfig('web/default/cms_home_page');
			if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
				$this->_forward('defaultIndex');
			}
		}
	}

	/**
	 * Create customer account action
	 */
	public function createAccountAction() {
		$this->checkLogin();
		$session = $this->getCustomerSession();
		$session->setEscapeMessages(true); // prevent XSS injection in user input
		$result = array();
		if ($this->getRequest()->isPost()) {
			$errors = array();

			if (!$customer = Mage::registry('current_customer')) {
				$customer = Mage::getModel('customer/customer')->setId(null);
			}

			foreach (Mage::getConfig()->getFieldset('customer_account') as $code=>$node) {
				if ($node->is('create') && ($value = $this->getRequest()->getParam($code)) !== null) {
					$customer->setData($code, $value);
				}
			}

			if ($this->getRequest()->getParam('is_subscribed', false)) {
				$customer->setIsSubscribed(1);
			}

			/**
			 * Initialize customer group id
			 */
			$customer->getGroupId();

			try {
				$customer->setPassword($this->getRequest()->getPost('password'));
				$customer->setConfirmation($this->getRequest()->getPost('confirmation'));
				$customer->setPasswordConfirmation($this->getRequest()->getPost('confirmation'));
				$customerErrors = $customer->validate();
				if (is_array($customerErrors)) {
					$errors = array_merge($customerErrors, $errors);
				}

				if (@class_exists('Mage_Captcha_Helper_Data')) {
					$formId = 'user_create';
					$captchaModel = Mage::helper('captcha')->getCaptcha($formId);
					if ($captchaModel->isRequired()) {
						$captchaParams = $this->getRequest()->getParam(Mage_Captcha_Helper_Data::INPUT_NAME_FIELD_VALUE);
						$captchaString = '';
						if (isset($captchaParams[$formId])) {
							$captchaString = $captchaParams[$formId];
						}
						if (!$captchaModel->isCorrect($captchaString)) {
							$errors[] = Mage::helper('captcha')->__('Incorrect CAPTCHA.');
						}
					}
				}

				$validationResult = count($errors) == 0;

				if (true === $validationResult) {
					$customer->save();

					Mage::dispatchEvent('customer_register_success',
						array('account_controller' => $this, 'customer' => $customer)
					);
					if ($customer->getPendingMessage()) {
						$result['error'] = $customer->getPendingMessage();
						$result['registered'] = true;
					} else {
						if ($customer->isConfirmationRequired()) {
							$customer->sendNewAccountEmail('confirmation', $session->getBeforeAuthUrl(), Mage::app()->getStore()->getId());
							$result['error'] = Mage::helper('customer')->__('Account confirmation is required. Please, check your email for the confirmation link.');
							$result['registered'] = true;
						} else {
							$session->setCustomerAsLoggedIn($customer);
							$customer->sendNewAccountEmail('registered', '', Mage::app()->getStore()->getId());
						}
					}
				} else {
					if (is_array($errors)) {
						$result['error'] = '';
						foreach ($errors as $errorMessage) {
							$result['error'] .= $errorMessage;
						}
					} else {
						$result['error'] = $this->__('Invalid customer data');
					}
				}
			} catch (Mage_Core_Exception $e) {
				if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
					$message = $this->__('There is already an account with this email address.');
					$session->setEscapeMessages(false);
				} else {
					$message = $e->getMessage();
				}
				$result['error'] = $message;
			} catch (Exception $e) {
				$result['error'] = $this->__('Cannot save the customer.');
				Mage::logException($e);
			}
		}

		$this->getResponse()->setBody(Zend_Json::encode($result));
	}

	/**
	 * Retrieve customer session model object
	 *
	 * @return Mage_Customer_Model_Session
	 */
	protected function getCustomerSession() {
		return Mage::getSingleton('customer/session');
	}
}
 
?>