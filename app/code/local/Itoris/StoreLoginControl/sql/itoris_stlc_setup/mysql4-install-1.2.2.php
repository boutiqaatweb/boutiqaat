<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_STORELOGINCONTROL
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

/** @var $this Mage_Core_Model_Resource_Setup */

$this->startSetup();

$query = '';

if (!$this->tableExists($this->getTable('itoris_storelogincontrol_store_access'))) {
	$query .= "CREATE TABLE " . $this->getTable('itoris_storelogincontrol_store_access') . " (
			`store_id` smallint(5) unsigned DEFAULT '0' primary key,
			`must_login` bool NOT NULL,
			`allow_registration` bool NOT NULL,
			`use_custom_reg_form` bool null,
			CONSTRAINT `fk_itoris_storelogincontrol_store_access_store_id` FOREIGN KEY (`store_id`) REFERENCES " . $this->getTable('core_store') . " (`store_id`) ON DELETE cascade ON UPDATE CASCADE
			) ENGINE = InnoDB;";
} else {
	$query .= "ALTER TABLE " . $this->getTable('itoris_storelogincontrol_store_access') . " ADD `use_custom_reg_form` BOOLEAN NULL;";
}

if (!$this->tableExists($this->getTable('itoris_storelogincontrol_defaults'))) {
	$query .= "CREATE TABLE " . $this->getTable('itoris_storelogincontrol_defaults') . " (
  			`orig_store_id` smallint(5) unsigned DEFAULT '0',
  			`end_store_id` smallint(5) unsigned DEFAULT '0',
  			PRIMARY KEY (`orig_store_id`, `end_store_id`),
  			CONSTRAINT `fk_itoris_storelogincontrol_orig_core_store_storeid` FOREIGN KEY (`orig_store_id`) REFERENCES " . $this->getTable('core_store') . " (`store_id`) ON DELETE cascade ON UPDATE CASCADE,
  			CONSTRAINT `fk_itoris_storelogincontrol_end_core_store_storeid` FOREIGN KEY (`end_store_id`) REFERENCES " . $this->getTable('core_store') . " (`store_id`) ON DELETE cascade ON UPDATE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='IToris StoreLoginControl Defaults for newly created customers';";
}

if (!$this->tableExists($this->getTable('itoris_storelogincontrol_settings'))) {
	$query .= "CREATE TABLE " . $this->getTable('itoris_storelogincontrol_settings') . " (
			`key` VARCHAR( 255 ) NOT NULL primary key,
			`value` VARCHAR( 255 ) NOT NULL
			) ENGINE = InnoDB;
			insert into " . $this->getTable('itoris_storelogincontrol_settings') . " (`key`, `value`) values ('enabled', 1);";
}

if (!$this->tableExists($this->getTable('itoris_storelogincontrol_customer'))) {
	$query .= "CREATE TABLE " . $this->getTable('itoris_storelogincontrol_customer') . " (
				`customer_id` int(10) unsigned NOT NULL,
				`store_id` smallint(5) unsigned DEFAULT '0',
				PRIMARY KEY (`customer_id` , `store_id`),
				CONSTRAINT `fk_itoris_storelogincontrol_customer_customer_id` FOREIGN KEY (`customer_id`) REFERENCES " . $this->getTable('customer_entity') . " (`entity_id`) ON DELETE cascade ON UPDATE CASCADE,
				CONSTRAINT `fk_itoris_storelogincontrol_customer_store_id` FOREIGN KEY (`store_id`) REFERENCES " . $this->getTable('core_store') . " (`store_id`) ON DELETE cascade ON UPDATE CASCADE
				) ENGINE = InnoDB;";
}

$this->run($query);

$this->endSetup();
?>