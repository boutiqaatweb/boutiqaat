<?php
class Mage_Adminhtml_Block_Sales_Order_Totals extends Mage_Adminhtml_Block_Sales_Totals//Mage_Adminhtml_Block_Sales_Order_Abstract
{
    /**
     * Initialize order totals array
     *
     * @return Mage_Sales_Block_Order_Totals
     */
    protected function _initTotals()
    {
        parent::_initTotals();
        $this->_totals['paid'] = new Varien_Object(array(
            'code'      => 'paid',
            'strong'    => true,
            'value'     => $this->getSource()->getTotalPaid(),
            'base_value'=> $this->getSource()->getBaseTotalPaid(),
            'label'     => $this->helper('sales')->__('Total Paid'),
            'area'      => 'footer'
        ));
        $this->_totals['refunded'] = new Varien_Object(array(
            'code'      => 'refunded',
            'strong'    => true,
            'value'     => $this->getSource()->getTotalRefunded(),
            'base_value'=> $this->getSource()->getBaseTotalRefunded(),
            'label'     => $this->helper('sales')->__('Total Refunded'),
            'area'      => 'footer'
        ));
        $this->_totals['due'] = new Varien_Object(array(
            'code'      => 'due',
            'strong'    => true,
            'value'     => $this->getSource()->getTotalDue(),
            'base_value'=> $this->getSource()->getBaseTotalDue(),
            'label'     => $this->helper('sales')->__('Total Due'),
            'area'      => 'footer'
        ));

        $celebrities = array();
        foreach ($this->getSource()->getAllItems() as $item) {
            if($item->getCategoryId()){
                if(isset($celebrities[$item->getCategoryId()])){
                    $celebrities[$item->getCategoryId()] = $celebrities[$item->getCategoryId()] +  $item->getCommisionAmount();
                } 
                else{
                    $celebrities[$item->getCategoryId()] = $item->getCommisionAmount();
                }
            }
        }

        foreach ($celebrities as $celebrity => $commision_amount) {
            $this->_totals[$celebrity] = new Varien_Object(array(
                'code'      => $celebrity,
                'strong'    => true,
                'value'     => $commision_amount,
                'base_value'=> $commision_amount,
                'label'     => $this->helper('sales')->__('%s (Commision)', Mage::getModel('catalog/category')->load($celebrity)->getName()),
                'area'      => 'footer'
            ));
        }

        return $this;
    }
}
