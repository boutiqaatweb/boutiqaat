<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Block_Product_List extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Default toolbar block name
     *
     * @var string
     */
    protected $_defaultToolbarBlock = 'catalog/product_list_toolbar';

    /**
     * Product Collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_productCollection;

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }

            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }

            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                    $this->addModelTags($category);
                }
            }
            $this->_productCollection = $layer->getProductCollection();            
            $this->_productCollection->getSelect()->joinLeft('sales_flat_order_item','e.entity_id = sales_flat_order_item.product_id','SUM(sales_flat_order_item.qty_ordered) AS ordered_qty')->group('e.entity_id');
            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }


            $customer_celebrity = $this->getCelebrityCategories();
            if(Mage::registry('current_category')){            
                $category_id = Mage::registry('current_category')->getId();
                if ($category_id && in_array($category_id, $customer_celebrity)) {
                    /*$this->_productCollection->clear();
                    
                    $layer = $this->getLayer();
                    $category = Mage::getModel('catalog/category')->load($category_id);
                    if ($category_id) {
                        $origCategory = $layer->getCurrentCategory();
                        $layer->setCurrentCategory($category);
                        $this->addModelTags($category);
                    }*/

                    $collection = Mage::getModel('catalog/category')
                                ->load($category_id)
                                ->getProductCollection()
                                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                                ->addMinimalPrice()
                                ->addFinalPrice()
                                ->addTaxPercents()
                                ->addUrlRewrite($category_id);
                    foreach ($collection as $product) {                 
                        $date_data = unserialize($product->getCelebrityAdDate());               
                        if(isset($date_data[$category_id])){
                            $product->setCelebrityDatetime(Mage::app()->getLocale()->date($date_data[$category_id], Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT), null, false)->getTimestamp());                  
                            $product->getResource()->saveAttribute($product, 'celebrity_datetime'); 
                            Mage::getModel("catalog/product_action")->updateAttributes(array($product->getId()), array('celebrity_datetime' => Mage::app()->getLocale()->date($date_data[$category_id], Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT), null, false)->getTimestamp()),0);
                        }   
                        else{
                            $product->setCelebrityDatetime(0);
                            $product->getResource()->saveAttribute($product, 'celebrity_datetime'); 
                            Mage::getModel("catalog/product_action")->updateAttributes(array($product->getId()), array('celebrity_datetime' => 0),0);
                        }                   
                    }

                    $collection = Mage::getModel('catalog/category')
                                ->load($category_id)
                                ->getProductCollection()
                                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                                ->addMinimalPrice()
                                ->addFinalPrice()
                                ->addTaxPercents()
                                ->addUrlRewrite($category_id);

                    if($search_number = Mage::app()->getRequest()->getParam('ad_number')) {
                        $search_product_ids = array(); 
                        foreach ($collection as $product) {
                            $number_data = unserialize($product->getCelebrityAdNumber());               
                            if(isset($number_data[$category_id]) && ($number_data[$category_id] == $search_number)) {
                                $search_product_ids[] = $product->getId(); 
                            }   
                        }

                        $collection = Mage::getModel('catalog/category')
                                ->load($category_id)
                                ->getProductCollection()
                                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                                ->addMinimalPrice()
                                ->addFinalPrice()
                                ->addTaxPercents()
                                ->addUrlRewrite($category_id)
                                ->addAttributeToFilter('entity_id', array('in' => $search_product_ids));

                        foreach ($collection as $product) {                 
                            $date_data = unserialize($product->getCelebrityAdDate());               
                            if(isset($date_data[$category_id])){
                                $product->setCelebrityDatetime(Mage::app()->getLocale()->date($date_data[$category_id], Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT), null, false)->getTimestamp());                  
                                $product->getResource()->saveAttribute($product, 'celebrity_datetime'); 
                                Mage::getModel("catalog/product_action")->updateAttributes(array($product->getId()), array('celebrity_datetime' => Mage::app()->getLocale()->date($date_data[$category_id], Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT), null, false)->getTimestamp()),0);
                            }   
                            else{
                                $product->setCelebrityDatetime(0);
                                $product->getResource()->saveAttribute($product, 'celebrity_datetime'); 
                                Mage::getModel("catalog/product_action")->updateAttributes(array($product->getId()), array('celebrity_datetime' => 0),0);
                            }                   
                        }

                        $collection = Mage::getModel('catalog/category')
                                ->load($category_id)
                                ->getProductCollection()
                                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                                ->addMinimalPrice()
                                ->addFinalPrice()
                                ->addTaxPercents()
                                ->addUrlRewrite($category_id)
                                ->addAttributeToFilter('entity_id', array('in' => $search_product_ids));
                    } 
                     
                    Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
                    Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

                    $this->_productCollection = $collection; //->addAttributeToSort('celebrity_datetime', 'desc'); 
                }
            }
        }

        return $this->_productCollection;
    }

    /**
     * Get catalog layer model
     *
     * @return Mage_Catalog_Model_Layer
     */
    public function getLayer()
    {
        $layer = Mage::registry('current_layer');
        if ($layer) {
            return $layer;
        }
        return Mage::getSingleton('catalog/layer');
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getLoadedProductCollection()
    {
        return $this->_getProductCollection();
    }

    /**
     * Retrieve current view mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->getChild('toolbar')->getCurrentMode();
    }

    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     */
    protected function _beforeToHtml()
    {
        $toolbar = $this->getToolbarBlock();
	   $customer_celebrity = $this->getCelebrityCategories();

        // called prepare sortable parameters
        $collection = $this->_getProductCollection();

        // use sortable parameters
        if ($orders = $this->getAvailableOrders()) {
	    
            if(Mage::registry('current_category')){            
                if (!in_array(Mage::registry('current_category')->getId(), $customer_celebrity)) {
			        unset($orders['celebrity_datetime']);
		        }
				else {					
					unset($orders['name']);
					unset($orders['price']);
					unset($orders['position']);
				}
    	    }
    	    else{
    		    unset($orders['celebrity_datetime']);
    	    }
            $toolbar->setAvailableOrders($orders);
        }
        if ($sort = $this->getSortBy()) {
            if(Mage::registry('current_category')){
                if (in_array(Mage::registry('current_category')->getId(), $customer_celebrity)) {
                    $sort = 'celebrity_datetime';
                }
            }
            $toolbar->setDefaultOrder($sort);
        }
        if ($dir = $this->getDefaultDirection()) {
            if(Mage::registry('current_category')){
                if (in_array(Mage::registry('current_category')->getId(), $customer_celebrity)) {
                    $dir = 'desc';
                }
            }
            $toolbar->setDefaultDirection($dir);
        }
        else{
            if(Mage::registry('current_category')){
                if (in_array(Mage::registry('current_category')->getId(), $customer_celebrity)) {
                    $dir = 'desc';
                }
            }
            $toolbar->setDefaultDirection($dir);
        }

        if ($modes = $this->getModes()) {
            $toolbar->setModes($modes);
        }

        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);
        Mage::dispatchEvent('catalog_block_product_list_collection', array(
            'collection' => $this->_getProductCollection()
        ));

        $this->_getProductCollection()->load();

        return parent::_beforeToHtml();
    }

    /**
     * Retrieve Toolbar block
     *
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function getToolbarBlock()
    {
        if ($blockName = $this->getToolbarBlockName()) {
            if ($block = $this->getLayout()->getBlock($blockName)) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, microtime());
        return $block;
    }

    /**
     * Retrieve additional blocks html
     *
     * @return string
     */
    public function getAdditionalHtml()
    {
        return $this->getChildHtml('additional');
    }

    /**
     * Retrieve list toolbar HTML
     *
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }

    public function setCollection($collection)
    {
        $this->_productCollection = $collection;
        return $this;
    }

    public function addAttribute($code)
    {
        $this->_getProductCollection()->addAttributeToSelect($code);
        return $this;
    }

    public function getPriceBlockTemplate()
    {
        return $this->_getData('price_block_template');
    }

    /**
     * Retrieve Catalog Config object
     *
     * @return Mage_Catalog_Model_Config
     */
    protected function _getConfig()
    {
        return Mage::getSingleton('catalog/config');
    }

    /**
     * Prepare Sort By fields from Category Data
     *
     * @param Mage_Catalog_Model_Category $category
     * @return Mage_Catalog_Block_Product_List
     */
    public function prepareSortableFieldsByCategory($category) {
        if (!$this->getAvailableOrders()) {
            $this->setAvailableOrders($category->getAvailableSortByOptions());
        }
        $availableOrders = $this->getAvailableOrders();
        if (!$this->getSortBy()) {
            if ($categorySortBy = $category->getDefaultSortBy()) {
                if (!$availableOrders) {
                    $availableOrders = $this->_getConfig()->getAttributeUsedForSortByArray();
                }
                if (isset($availableOrders[$categorySortBy])) {
                    $this->setSortBy($categorySortBy);
                }
            }
        }

        return $this;
    }

    /**
     * Retrieve block cache tags based on product collection
     *
     * @return array
     */
    public function getCacheTags()
    {
        return array_merge(
            parent::getCacheTags(),
            $this->getItemsTags($this->_getProductCollection())
        );
    }

    public function getCelebrityCategories()
    {
        $collection = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('celebrity')->addAttributeToFilter('is_celebrity', true);
        $_celebrity = array();
        foreach ($collection as $customer) {
            $_celebrity[] = $customer->getCelebrity();
        }
        return $_celebrity;
    }

    public function getAdNumber($product)
    {
        if(Mage::registry('current_category')){            
            $category_id = Mage::registry('current_category')->getId();
            $customer_celebrity = $this->getCelebrityCategories();
            if(in_array($category_id, $customer_celebrity)){
                $number_data = unserialize($product->getCelebrityAdNumber());               
                if(isset($number_data[$category_id])) {
                    return $number_data[$category_id];
                }
            }
        }

        return false;
    }
}
