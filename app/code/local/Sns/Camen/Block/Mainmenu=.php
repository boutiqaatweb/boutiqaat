<?php
class Sns_Camen_Block_Mainmenu extends Mage_Catalog_Block_Navigation {
    protected $_groupitemwidth = 3;

	protected $_cacheKeyArray = NULL;

    protected function _construct()
    {
        parent::_construct();
		$this->addData(array(
			'cache_lifetime'    => 99999999,
			'cache_tags'        => array(Mage_Catalog_Model_Product::CACHE_TAG),
		));
    }
	public function getCacheKeyInfo()
	{
		if (NULL === $this->_cacheKeyArray)
		{
			$this->_cacheKeyArray = array(
				'SNS_CAMEN_BLOCK_MAINMENU',
				Mage::app()->getStore()->getId(),
				Mage::getDesign()->getPackageName(),
				Mage::getDesign()->getTheme('template'),
				Mage::getSingleton('customer/session')->getCustomerGroupId(),
				'template' => $this->getTemplate(),
				'name' => $this->getNameInLayout(),
				(int)Mage::app()->getStore()->isCurrentlySecure(),
			);
		}
		return $this->_cacheKeyArray;
	}
    protected function _getMoMenuHtml($category, $level = 0, $isLast = false, $isFirst = false) {
        if (!$category->getIsActive()) {
            return '';
        }
        $html = array();
        // get all children
        if (Mage::helper('catalog/category_flat')->isEnabled()) {
            $children = (array)$category->getChildrenNodes();
            $childrenCount = count($children);
        } else {
            $children = $category->getChildren();
            $childrenCount = $children->count();
        }
        $hasChildren = ($children && $childrenCount);

        // select active children
        $activeChildren = array();
        $sort = array();
        foreach ($children as $child) {
            if ($child->getIsActive()) {
                $activeChildren[] = $child;
                if($category->getId() == 95){
                    $sort[] = strtolower($child->getName());
                }
            }
        }
        $activeChildrenCount = count($activeChildren);
        $hasActiveChildren = ($activeChildrenCount > 0);
        
        // render children
        $li = '';
        $j = 0;
        if($category->getId() == 95){
            array_multisort($sort, SORT_ASC, $activeChildren);
        }
        if($category->getId()==6){
            $sorted = $this->_record_sort($activeChildren, "name");
            foreach ($sorted as $child) {
                $li .= $this->_getMoMenuHtml(
                    $child,
                    ($level + 1),
                    ($j == $activeChildrenCount - 1),
                    ($j == 0)
                );
                $j++;
            }
        }else{
            foreach ($activeChildren as $child) {
                $li .= $this->_getMoMenuHtml(
                    $child,
                    ($level + 1),
                    ($j == $activeChildrenCount - 1),
                    ($j == 0)
                );
                $j++;
            }
        }
        

        // prepare list item html classes
        $itemposition = $this->_getItemPosition($level);
        
        $linkClass = '';
        $classes = array();
        $classes[] = 'level' . $level;

        $classes[] = 'nav-' . $itemposition;
        if ($this->isCategoryActive($category)) {
            $classes[] = 'active';
            $linkClass = ' active';
        }
        
        
        $linkClass .= ' menu-title-lv'.$level;

		if ($isFirst) $classes[] = 'first';
        if ($isLast) $classes[] = 'last';
        if ($hasActiveChildren) $classes[] = 'parent';
		
        $liclass = implode(' ', $classes);
		
		$html[] = '<li class="'.$liclass.'">';
        $html[] = '<a href="'.$this->getCategoryUrl($category).'" class="'.$linkClass.'">';
        $html[] = '<span>' . $this->escapeHtml($category->getName()) . '</span>';
        $html[] = '</a>';
        
        if (!empty($li) && $hasActiveChildren) {
            $html[] = '<ul class="level' . $level . '">';
            $html[] = $li;
            $html[] = '</ul>';
        }

        $html[] = '</li>';

        $html = implode("\n", $html);
        return $html;

    }
    protected function _getMenuItemHtml($category, $level = 0, $isLast = false, $isFirst = false) {
    	//if($level >= 1) return;
        if (!$category->getIsActive()) {
            return '';
        }
        $html = array();
		
        // get all children
        if (Mage::helper('catalog/category_flat')->isEnabled()) {
            $children = (array)$category->getChildrenNodes();
            $childrenCount = count($children);
        } else {
            $children = $category->getChildren();
            $childrenCount = $children->count();
        }
        $hasChildren = ($children && $childrenCount);

        // select active children
        $activeChildren = array();
        $sort = array();
        foreach ($children as $child) {
            if ($child->getIsActive()) {
                $activeChildren[] = $child;
                if($category->getId() == 95){
                    $sort[] = strtolower($child->getName());
                }
            }
        }
        $activeChildrenCount = count($activeChildren);
        $hasActiveChildren = ($activeChildrenCount > 0);
        if($level==0){
       		$catdetail = Mage::getModel('catalog/category')->load($category->getId());
	        if($catdetail->getData('camen_groupsubcat')) {
	        	$this->_isgroup = true;
	        	$this->_groupitemwidth = $catdetail->getData('camen_subcat_colw');
	        } else {
	        	$this->_isgroup = false;
	        }
        }
        // render children
        $li = '';
        $j = 0;
        if($category->getId() == 95){
            array_multisort($sort, SORT_ASC, $activeChildren);
        }
        foreach ($activeChildren as $child) {
            $li .= $this->_getMenuItemHtml(
                $child,
                ($level + 1),
                ($j == $activeChildrenCount - 1),
                ($j == 0)
            );
            $j++;
        }
        
		$showblock = false;
        $showsubmenu = false;
        if(!empty($li) && $hasActiveChildren) $showsubmenu = true;
        
        if($level==0){
	        $menutypes = $catdetail->getData('camen_menutype');
	        $rightblock = $this->_getCatBlock($catdetail, 'camen_block_r');
	        $topblock = $this->_getCatBlock($catdetail, 'camen_block_t');
	        $bottomblock = $this->_getCatBlock($catdetail, 'camen_block_b');
	        $hidelink = $this->_getCatBlock($catdetail, 'camen_menulink');

		    if(($rightblock || $topblock || $bottomblock) && $menutypes != 0) $showblock = true;
        }
        // prepare list item html classes
        $itemposition = $this->_getItemPosition($level);
        
        $classes = array();
        $classes[] = 'level' . $level;

        $classes[] = 'nav-' . $itemposition;
        if ($this->isCategoryActive($category)) {
            $classes[] = 'active';
        }
        $linkClass = '';
        
        $linkClass .= ' menu-title-lv'.$level;
        
        if($level==0){
        	if($this->_isgroup) {
        		$classes[] = 'group-item';
        	} else {
        		$classes[] = 'no-group';
        	}
        	if($menutypes == 0){
	            $classes[] = 'drop-submenu';
	            $showblock = false;
	        }
        	if($menutypes == 1){
	            $classes[] = 'drop-blocks';
	            $showsubmenu = false;
	        }
        	if($menutypes == 2){
	            $classes[] = 'drop-submenu-blocks';
	        }
        }
		if ($isFirst) $classes[] = 'first';
        if ($isLast) $classes[] = 'last';
        if ($hasActiveChildren) $classes[] = 'parent';
		
		
        if ($level == 0) {
        	$submenuwidth = $catdetail->getData('camen_subcat_w');
			
        	if(!$rightblock) {
        		$submenuwidth = 12;
        	}
            if($submenuwidth == 12 || $showsubmenu == false){
            	$rightwidth = 12;
            } else {
            	$rightwidth = 12 - $submenuwidth;
            }
        }
		
        if($level==1 && $this->_isgroup){
            $classes[] = 'group-block col-sm-'.$this->_groupitemwidth;
        }
        
        $liclass = implode(' ', $classes);

        if($level == 0){
            if($hidelink) {
                $url = 'javascript:void(0)';
            } else {
                $url = $this->getCategoryUrl($category);
            }
            if($catdetail->getData('kunstore_title_alias')) {
                $catTitle = $this->escapeHtml($catdetail->getData('kunstore_title_alias'));
            } else {
                $catTitle = $this->escapeHtml($category->getName());
            }
            $html[] = '<li class="'.$liclass.'">';
            $html[] = '<a href="'.$url.'" class="'.$linkClass.'">';

            $html[] = '<span class="title">';
            if($catdetail->getData('kunstore_label')) $html[] = '<span class="label">' . $catdetail->getData('kunstore_label') . '</span>';
            $html[] = $catTitle;
            $html[] = '</span>';
            $html[] = '</a>';
        } elseif($level == 1) {
			$html[] = '<li class="'.$liclass.'">';
	        $html[] = '<a href="'.$this->getCategoryUrl($category).'" class="'.$linkClass.'">';
	        $html[] = '<span class="title">' . $this->escapeHtml($category->getName()) . '</span>';
	        $html[] = '</a>';
		} else {
			$html[] = '<li class="'.$liclass.'">';
	        $html[] = '<a href="'.$this->getCategoryUrl($category).'" class="'.$linkClass.'">';
	        $html[] = '<span class="title">' . $this->escapeHtml($category->getName()) . '</span>';
	        $html[] = '</a>';
		}

		if($level == 0 && $showblock) {
			$html[] = '<div class="wrap_dropdown fullwidth">';
			$html[] = '<div class="row">';
			
			// top block
			if($topblock){
				$html[] = '<div class="col-sm-12">';
				$html[] = '<div class="wrap_topblock">';
				$html[] = $topblock;
				$html[] = '</div>';
				$html[] = '</div>';
			}
		}
        if (!empty($li) && $hasActiveChildren && $showsubmenu == true) {
            if($level == 0){
            	
            	if($this->_isgroup) {
            		if($showblock) $html[] = '<div class="wrap_group  col-sm-'.$submenuwidth.'">';
            		else $html[] = '<div class="wrap_group fullwidth">';
            		
	                $html[] = '<ul class="row level' . $level . '">';
	                $html[] = $li;
	                $html[] = '</ul>';
	                $html[] = '</div>';
            	} else {
            		if($showblock) $html[] = '<div class="wrap_submenu col-sm-'.$submenuwidth.'">';
            		else $html[] = '<div class="wrap_submenu">';
            		
	                $html[] = '<ul class="level' . $level . '">';
	                $html[] = $li;
	                $html[] = '</ul>';
	                $html[] = '</div>';
            	}
            } else {
            	$html[] = '<div class="wrap_submenu">';
                $html[] = '<ul class="level' . $level . '">';
                $html[] = $li;
                $html[] = '</ul>';
                $html[] = '</div>';
            }
        }
		if($level == 0 && $showblock) {

			// right block
			if($rightblock){
				$html[] = '<div class="col-sm-'.$rightwidth.'">';
				$html[] = '<div class="wrap_rightblock">';
				$html[] = $rightblock;
				$html[] = '</div>';
				$html[] = '</div>';
			}

			// bottom block
			if($bottomblock){
				$html[] = '<div class="col-sm-12">';
				$html[] = '<div class="wrap_bottomblock">';
				$html[] = $bottomblock;
				$html[] = '</div>';
				$html[] = '</div>';
			}
			
			$html[] = '</div>'; // end row
			$html[] = '</div>';
		}

        $html[] = '</li>';

        $html = implode("\n", $html);
        return $html;

    }

    public function getMenuHtml($momenu = false, $level = 0) {
    	
        $this->_isMomenu = $momenu;
        $activeCategories = array();
        foreach ($this->getStoreCategories() as $child) {
            if ($child->getIsActive()) {
                $activeCategories[] = $child;
            }
        }
        $activeCategoriesCount = count($activeCategories);
        $hasActiveCategoriesCount = ($activeCategoriesCount > 0);

        if (!$hasActiveCategoriesCount) {
            return '';
        }

        $html = '';
        $j = 0;
        
        
        $customitems	=	Mage::helper('camen/data')->getField('menu_customItems');
		$array_customitems = unserialize($customitems);

		$collect_customitems = array();
		foreach($array_customitems as $key=>$customitem){
			$customitem['link'] = Mage::helper('cms')->getBlockTemplateProcessor()->filter($customitem['link']);
			$collect_customitems[] = $customitem;
		}
		if($this->_isMomenu) $html .= $this->_getCustomItems(true, $collect_customitems, 'first');
		else $html .= $this->_getCustomItems(false, $collect_customitems, 'first');
        foreach ($activeCategories as $category) {
            if($this->_isMomenu){
                if($category->getId() == 6){
                    $html .= $this->_getBrandMoMenuHtml(
                        $category,
                        $level,
                        ($j == $activeCategoriesCount - 1),
                        ($j == 0)
                    );
                }
                else{
                    $html .= $this->_getMoMenuHtml(
                        $category,
                        $level,
                        ($j == $activeCategoriesCount - 1),
                        ($j == 0)
                    );
                }
            } else {
//		        $catdetail = Mage::getModel('catalog/category')->load($category->getId());
//		        if($catdetail->getData('camen_groupsubcat')) {
//		        	$this->_isgroup = true;
//		        	$this->_groupitemwidth = $catdetail->getData('camen_subcat_colw');
//		        } else {
//		        	$this->_isgroup = false;
//		        }
                if($category->getId() == 6){
                    $html .= $this->_getBrandMenuItemHtml(
                        $category,
                        $level,
                        ($j == $activeCategoriesCount - 1),
                        ($j == 0)
                    );
                }
                else{    
                    $html .= $this->_getMenuItemHtml(
                        $category,
                        $level,
                        ($j == $activeCategoriesCount - 1),
                        ($j == 0)
                    );
                }
                $html .= $this->_getCustomItems(false, $collect_customitems, $category->getId());
            }
            $j++;
        }
		if($this->_isMomenu) $html .= $this->_getCustomItems(true, $collect_customitems, 'last');
		else $html .= $this->_getCustomItems(false, $collect_customitems, 'last');
        
        return $html;
    } 
    protected function _getCustomItems($momenu = false, $items, $position){
    	$curUrl = Mage::helper('core/url')->getCurrentUrl();
    	$html = '';
		foreach($items as $menuitem){
			$liClass = 'level0 custom-item';
			$lickClass = 'menu-title-lv0';
			if($menuitem['status'] && $momenu == false) $liClass .= "drop-staticblock";
			if(strtolower($curUrl) == strtolower($menuitem['link'])) {
				$liClass .= ' active';
				$lickClass .= ' active';
			}
			$drophtml = '';
			if($menuitem['status'] && $momenu == false){
				$drophtml .= '<div class="wrap_dropdown fullwidth">';
				$drophtml .= $this->getLayout()->createBlock('cms/block')->setBlockId($menuitem['block_id'])->toHtml();
				$drophtml .= '</div>';
			}
			if($menuitem['position'] == $position){
				$html .= '<li class="'.$liClass.'">';
				$html .= 	'<a class="'.$lickClass.'" target="'.$menuitem['target'].'" href="'.$menuitem['link'].'">';
				$html .= 		'<span class="title">'.$menuitem['title'].'</span>';
				$html .= 	'</a>';
				$html .= 	$drophtml;
				$html .= '</li>';
			}
		}
		return $html;
    }
    protected function _getCatBlock($category, $block){
        if (!$this->_tplProcessor){
            $this->_tplProcessor = Mage::helper('cms')->getBlockTemplateProcessor();
        }
        return $this->_tplProcessor->filter( trim($category->getData($block)) );
    }
	protected function _toHtml(){
		if (!$this->getTemplate()){
			$this->setTemplate('sns/blocks/mainmenu.phtml');
		}
		return parent::_toHtml();
	}

    protected function _record_sort($records, $field, $reverse=false)
    {
        $hash = array();
       
        foreach($records as $record)
        {
            $hash[$record[$field]] = $record;
        }
       
        ($reverse)? krsort($hash) : ksort($hash,SORT_STRING | SORT_FLAG_CASE);
       
        $records = array();
       
        foreach($hash as $record)
        {
            $records []= $record;
        }
       
        return $records;
    }



    protected function _getBrandMenuItemHtml($category, $level = 0, $isLast = false, $isFirst = false) {
        //if($level >= 1) return;
        if (!$category->getIsActive()) {
            return '';
        }
        $html = array();
        
        // get all children
        if (Mage::helper('catalog/category_flat')->isEnabled()) {
            $children = (array)$category->getChildrenNodes();
            $childrenCount = count($children);
        } else {
            $children = $category->getChildren();
            $childrenCount = $children->count();
        }
        $collection = Mage::getModel('shopbybrand/brand')->getCollection()->addFieldToFilter('status',1);
        $options = $collection->getData();
        $hasChildren = ($options && $childrenCount);

        // select active children
        $activeChildren = array();

        

        foreach ($options as $child) {
            $activeChildren[(is_numeric(substr($child["name"], 0, 1)) ? 'num' : substr($child["name"], 0, 1) )][] = $child;
        }
        $activeChildrenCount = count($activeChildren);        
        $hasActiveChildren = ($activeChildrenCount > 0);
        if($level==0){
            $catdetail = Mage::getModel('catalog/category')->load($category->getId());
            if($catdetail->getData('camen_groupsubcat')) {
                $this->_isgroup = true;
                $this->_groupitemwidth = $catdetail->getData('camen_subcat_colw');
            } else {
                $this->_isgroup = false;
            }
        }
        // render children
        $li = '';
        $sorting_option = '';
        $alphabets =  array(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, 'num');
        $j = 0;
        foreach ($alphabets as $alphabet) {                    
            foreach ($activeChildren[$alphabet] as $child) {

                $li .= '<li><a href='.Mage::getBaseUrl().$child["url_key"].'>'.$child["name"].'</a></li>';
                      
            }
            $sorting_option .= '<li class="brands" style="float: left; width: 40px; text-align: center; font-weight: normal; font-size: 25px;"><a onclick="OnBrandLetterClicked('.$alphabet.');" href="#" tabindex="'.$j.'">'.$alphabet.'</a></li>';
            $j++;  
        }

        $showblock = false;
        $showsubmenu = false;
        if(!empty($li) && $hasActiveChildren) $showsubmenu = true;
        
        if($level==0){
            $menutypes = $catdetail->getData('camen_menutype');
            $rightblock = $this->_getCatBlock($catdetail, 'camen_block_r');
            $topblock = $this->_getCatBlock($catdetail, 'camen_block_t');
            $bottomblock = $this->_getCatBlock($catdetail, 'camen_block_b');
            $hidelink = $this->_getCatBlock($catdetail, 'camen_menulink');

            if(($rightblock || $topblock || $bottomblock) && $menutypes != 0) $showblock = true;
        }
        // prepare list item html classes
        $itemposition = $this->_getItemPosition($level);
        
        $classes = array();
        $classes[] = 'level' . $level;

        $classes[] = 'nav-' . $itemposition;
        if ($this->isCategoryActive($category)) {
            $classes[] = 'active';
        }
        $linkClass = '';
        
        $linkClass .= ' menu-title-lv'.$level;
        
        if($level==0){
            if($this->_isgroup) {
                $classes[] = 'group-item';
            } else {
                $classes[] = 'no-group';
            }
            if($menutypes == 0){
                $classes[] = 'drop-submenu';
                $showblock = false;
            }
            if($menutypes == 1){
                $classes[] = 'drop-blocks';
                $showsubmenu = false;
            }
            if($menutypes == 2){
                $classes[] = 'drop-submenu-blocks';
            }
        }
        if ($isFirst) $classes[] = 'first';
        if ($isLast) $classes[] = 'last';
        if ($hasActiveChildren) $classes[] = 'parent';
        
        
        if ($level == 0) {
            $submenuwidth = $catdetail->getData('camen_subcat_w');
            
            if(!$rightblock) {
                $submenuwidth = 12;
            }
            if($submenuwidth == 12 || $showsubmenu == false){
                $rightwidth = 12;
            } else {
                $rightwidth = 12 - $submenuwidth;
            }
        }
        
        if($level==1 && $this->_isgroup){
            $classes[] = 'group-block col-sm-'.$this->_groupitemwidth;
        }
        
        $liclass = implode(' ', $classes);

        if($level == 0){
            if($hidelink) {
                $url = 'javascript:void(0)';
            } 
            elseif($category->getId()==6){
                $setlink = Mage::getStoreConfig('shopbybrand/general/router');
                $url = $this->getUrl($setlink, array());
            }
            else {
                $url = $this->getCategoryUrl($category);
            }
            if($catdetail->getData('kunstore_title_alias')) {
                $catTitle = $this->escapeHtml($catdetail->getData('kunstore_title_alias'));
            } else {
                $catTitle = $this->escapeHtml($category->getName());
            }
            $html[] = '<li class="'.$liclass.'">';
            $html[] = '<a href="'.$url.'" class="'.$linkClass.'">';

            $html[] = '<span class="title">';
            if($catdetail->getData('kunstore_label')) $html[] = '<span class="label">' . $catdetail->getData('kunstore_label') . '</span>';
            $html[] = $catTitle;
            $html[] = '</span>';
            $html[] = '</a>';
        } elseif($level == 1) {
            $html[] = '<li class="'.$liclass.'">';
            $html[] = '<a href="'.$this->getCategoryUrl($category).'" class="'.$linkClass.'">';
            $html[] = '<span class="title">' . $this->escapeHtml($category->getName()) . '</span>';
            $html[] = '</a>';
        } else {
            $html[] = '<li class="'.$liclass.'">';
            $html[] = '<a href="'.$this->getCategoryUrl($category).'" class="'.$linkClass.'">';
            $html[] = '<span class="title">' . $this->escapeHtml($category->getName()) . '</span>';
            $html[] = '</a>';
        }

        if($level == 0 && $showblock) {
            $html[] = '<div class="wrap_dropdown fullwidth">';
            $html[] = '<div class="row">';
            
            // top block
            if($topblock){
                $html[] = '<div class="col-sm-12">';
                $html[] = '<div class="wrap_topblock">';
                $html[] = $topblock;
                $html[] = '</div>';
                $html[] = '</div>';
            }
                //Sorting
                $html[] = '<div class="col-sm-12">';
                $html[] = '<ol class="row level' . $level . '" style="list-style-type: none; padding : 0px;margin : 0px;">';
                $m = 0;
                foreach ($alphabets as $alphabet) { 
                    $class_active = ($m==0) ? 'activebrand' : '';
                    $margin = ($m==0) ? '0 0 5px' : '0 5px';
                    $param_object = "'".$alphabet."'";
                    $alphabet_letter = ($alphabet == 'num') ? '#' : $alphabet; 
                    if(isset($activeChildren[$alphabet])){
                        $html[] =  '<li class="brands '.$class_active.'" style="text-align: center !important; margin: '.$margin.';"><a onclick="OnBrandLetterClicked('.$param_object.', this);" href="javascript:void(0)" tabindex="'.$j.'">'.$alphabet_letter.'</a></li>';
                    }else{
                        $html[] =  '<li class="brands '.$class_active.' disable" style="text-align: center !important; margin: '.$margin.';"><a tabindex="'.$j.'" href="javascript:void(0)">'.$alphabet_letter.'</a></li>';  
                    }
                    $m++;
                }
                //$html[] = $sorting_option;
                $html[] = '</ol>';
                $html[] = '</div>';
        }
        if (!empty($li) && $hasActiveChildren && $showsubmenu == true) {
            if($level == 0){
                
                if($this->_isgroup) {
                    if($showblock) $html[] = '<div class="wrap_group  col-sm-'.$submenuwidth.'">';
                    else $html[] = '<div class="wrap_group fullwidth">';
                    
                    
                    $j = 0;
                    foreach ($alphabets as $alphabet) { 
                        $style = ($j==0) ? 'display:block;' : 'display:none;'; 
                        $html[] = '<div id="'.$alphabet.'" class="row level' . $level . ' brand" style="margin-top:20px;'.$style.'"><ul class="row custom-brand-split level' . $level . ' col-sm-3" style="margin:0px;">';
                        $s = 0;
                        $coun = count($activeChildren[$alphabet]);
                        // $attribute_code = "manufacturer"; 
                        // $attribute_details = Mage::getSingleton("eav/config")->getAttribute("catalog_product", $attribute_code); 
                        // $options = $attribute_details->getSource()->getAllOptions(false);
                        $sorted = $this->_record_sort($activeChildren[$alphabet], "name");
                        if($alphabet=='num'){
                            $html[] = '<li class="col-sm-12"><h2>'.'#'.'</h2></li>';
                        }
                        else{
                            $html[] = '<li class="col-sm-12"><h2>'.$alphabet.'</h2></li>';
                        }
                        //$sort_options = $this->_record_sort($options[$alphabet], "label");
                        foreach($sorted as $option){
                            //echo $option;
                            $html[] = '<li class="col-sm-12"><a href='.Mage::getBaseUrl().$option["url_key"].'>'.$option["name"].'</a></li>';
                            if($s==4){
                                $html[] = '</ul><ul class="row custom-brand-split level' . $level . ' col-sm-3" style="margin:0px;">';
                            }
                            if($s==10){
                                $html[] = '</ul><ul class="row custom-brand-split level' . $level . ' col-sm-3" style="margin:0px;">';
                            }
                            if($s==16){
                                $html[] = '</ul><ul class="row custom-brand-split level' . $level . ' col-sm-3" style="margin:0px;">';
                            }
                            $s++;
                        }
                        // foreach ($sorted as $child) {
                        //     if($s==0){
                        //         $html[] = '<li class="col-sm-12"><h2>'.$alphabet.'</h2></li>';
                        //     }
                            
                            // $html[] = $this->_getMenuItemHtml(
                            //     $child,
                            //     ($level + 1),
                            //     ($j == $activeChildrenCount - 1),
                            //     ($j == 0)
                            // );
                               
                        // }
                        if($coun<=4){
                            $html[] = '</ul><ul class="row custom-brand-split level' . $level . ' col-sm-3" style="margin:0px;">';
                            $html[] = '</ul><ul class="row custom-brand-split level' . $level . ' col-sm-3" style="margin:0px;">';
                            $html[] = '</ul><ul class="row custom-brand-split level' . $level . ' col-sm-3" style="margin:0px;">';
                        }
                        if($coun>4 && $coun<=10){
                            $html[] = '</ul><ul class="row custom-brand-split level' . $level . ' col-sm-3" style="margin:0px;">';
                            $html[] = '</ul><ul class="row custom-brand-split level' . $level . ' col-sm-3" style="margin:0px;">';
                        }
                        if($coun>10 && $coun<=16){
                            $html[] = '</ul><ul class="row custom-brand-split level' . $level . ' col-sm-3" style="margin:0px;">';
                            $html[] = '</ul><ul class="row custom-brand-split level' . $level . ' col-sm-3" style="margin:0px;">';
                        }
                        $html[] = '</ul></div>';
                        $j++;                         
                    }
                    $html[] = '</div>';
                } else {
                    if($showblock) $html[] = '<div class="wrap_submenu col-sm-'.$submenuwidth.'">';
                    else $html[] = '<div class="wrap_submenu">';
                    
                    $html[] = '<ul class="level' . $level . '">';
                    $html[] = $li;
                    $html[] = '</ul>';
                    $html[] = '</div>';
                }
            } else {
                $html[] = '<div class="wrap_submenu">';
                $html[] = '<ul class="level' . $level . '">';
                $html[] = $li;
                $html[] = '</ul>';
                $html[] = '</div>';
            }
        }
        if($level == 0 && $showblock) {

            // right block
            if($rightblock){
                $html[] = '<div class="col-sm-'.$rightwidth.' barnd-right-box">';
                $html[] = '<div class="wrap_rightblock">';
                $html[] = $rightblock;
                $html[] = '</div>';
                $html[] = '</div>';
            }

            // bottom block
            if($bottomblock){
                $html[] = '<div class="col-sm-12">';
                $html[] = '<div class="wrap_bottomblock brands-only">';
                $html[] = $bottomblock;
                $html[] = '</div>';
                $html[] = '</div>';
            }
            
            $html[] = '</div>'; // end row
            $html[] = '</div>';
        }

        $html[] = '</li>';

        $html = implode("\n", $html);
        return $html;

    }


    protected function _getBrandMoMenuHtml($category, $level = 0, $isLast = false, $isFirst = false) {
        if (!$category->getIsActive()) {
            return '';
        }
        $html = array();
        // get all children
        if (Mage::helper('catalog/category_flat')->isEnabled()) {
            $children = (array)$category->getChildrenNodes();
            $childrenCount = count($children);
        } else {
            $children = $category->getChildren();
            $childrenCount = $children->count();
        }
        $collection = Mage::getModel('shopbybrand/brand')->getCollection()->addFieldToFilter('status',1);
        $options = $collection->getData();
        $hasChildren = ($options && $childrenCount);

        // select active children
        $activeChildren = array();
        foreach ($options as $child) {
            $activeChildren[(is_numeric(substr($child["name"], 0, 1)) ? 'num' : substr($child["name"], 0, 1) )][] = $child;
        }
        $activeChildrenCount = count($activeChildren);
        $hasActiveChildren = ($activeChildrenCount > 0);
        
        // render children
        
        $li = '';
        $sorting_option = '';
        $alphabets =  array(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, 'num');
        $j = 0;
        foreach ($alphabets as $alphabet) {                    
            foreach ($activeChildren[$alphabet] as $child) {

                $li .= '<li><a href='.Mage::getBaseUrl().$child["url_key"].'>'.$child["name"].'</a></li>';
                      
            }
            $sorting_option .= '<li class="brands" style="float: left; width: 40px; text-align: center; font-weight: normal; font-size: 25px;"><a onclick="OnBrandLetterClicked('.$alphabet.');" href="#" tabindex="'.$j.'">'.$alphabet.'</a></li>';
            $j++;  
        }
        

        // prepare list item html classes
        $itemposition = $this->_getItemPosition($level);
        
        $linkClass = '';
        $classes = array();
        $classes[] = 'level' . $level;

        $classes[] = 'nav-' . $itemposition;
        if ($this->isCategoryActive($category)) {
            $classes[] = 'active';
            $linkClass = ' active';
        }
        
        
        $linkClass .= ' menu-title-lv'.$level;

        if ($isFirst) $classes[] = 'first';
        if ($isLast) $classes[] = 'last';
        if ($hasActiveChildren) $classes[] = 'parent';
        
        $liclass = implode(' ', $classes);
        $setlink = Mage::getStoreConfig('shopbybrand/general/router');
        $url = $this->getUrl($setlink, array());
        $html[] = '<li class="'.$liclass.'">';
        $html[] = '<a href="'.$url.'" class="'.$linkClass.'">';
        $html[] = '<span>' . $this->escapeHtml($category->getName()) . '</span>';
        $html[] = '</a>';
        
        if (!empty($li) && $hasActiveChildren) {
            $html[] = '<ul class="level' . $level . '">';
            $html[] = $li;
            $html[] = '</ul>';
        }

        $html[] = '</li>';

        $html = implode("\n", $html);
        return $html;

    }
}