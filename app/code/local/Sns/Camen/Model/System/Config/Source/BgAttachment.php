<?php

class Sns_Camen_Model_System_Config_Source_BgAttachment
{
	public function toOptionArray()
	{
		return array(
			array('value'=>'fixed', 'label'=>Mage::helper('camen')->__('fixed')),
			array('value'=>'scroll', 'label'=>Mage::helper('camen')->__('scroll'))
		);
	}
}
