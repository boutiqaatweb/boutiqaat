<?php

class Sns_Camen_Model_System_Config_Source_BgPosition
{
	public function toOptionArray()
	{
		return array(
			array('value'=>'left top', 'label'=>Mage::helper('camen')->__('left top')),
			array('value'=>'left center', 'label'=>Mage::helper('camen')->__('left center')),
			array('value'=>'left bottom', 'label'=>Mage::helper('camen')->__('left bottom')),
			array('value'=>'right top', 'label'=>Mage::helper('camen')->__('right top')),
			array('value'=>'right center', 'label'=>Mage::helper('camen')->__('right center')),
			array('value'=>'right bottom', 'label'=>Mage::helper('camen')->__('right bottom')),
			array('value'=>'center top', 'label'=>Mage::helper('camen')->__('center top')),
			array('value'=>'center center', 'label'=>Mage::helper('camen')->__('center center')),
			array('value'=>'center bottom', 'label'=>Mage::helper('camen')->__('center bottom'))
		);
	}
}
