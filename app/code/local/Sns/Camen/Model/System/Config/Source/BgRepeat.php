<?php

class Sns_Camen_Model_System_Config_Source_BgRepeat
{
	public function toOptionArray()
	{
		return array(
			array('value'=>'no-repeat', 'label'=>Mage::helper('camen')->__('no-repeat')),
			array('value'=>'repeat', 'label'=>Mage::helper('camen')->__('repeat')),
			array('value'=>'repeat-x', 'label'=>Mage::helper('camen')->__('repeat-x')),
			array('value'=>'repeat-y', 'label'=>Mage::helper('camen')->__('repeat-y'))
		);
	}
}
