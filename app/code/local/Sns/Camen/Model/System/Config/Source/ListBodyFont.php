<?php
class Sns_Camen_Model_System_Config_Source_ListBodyFont
{
	public function toOptionArray()
	{
		return array(
			array('value'=>'arial', 'label'=>Mage::helper('camen')->__('Arial')),
			array('value'=>'arial-black', 'label'=>Mage::helper('camen')->__('Arial black')),
			array('value'=>'courier new', 'label'=>Mage::helper('camen')->__('courier New')),
			array('value'=>'georgia', 'label'=>Mage::helper('camen')->__('Georgia')),
			array('value'=>'impact', 'label'=>Mage::helper('camen')->__('Impact')),
			array('value'=>'lucida-console', 'label'=>Mage::helper('camen')->__('Lucida Console')),
			array('value'=>'lucida-grande', 'label'=>Mage::helper('camen')->__('Lucida-grande')),
			array('value'=>'palatino', 'label'=>Mage::helper('camen')->__('Palatino')),
			array('value'=>'tahoma', 'label'=>Mage::helper('camen')->__('Tahoma')),
			array('value'=>'times new roman', 'label'=>Mage::helper('camen')->__('Times')),
			array('value'=>'Trebuchet', 'label'=>Mage::helper('camen')->__('Trebuchet')),
			array('value'=>'Verdana', 'label'=>Mage::helper('camen')->__('Verdana')),
			array('value'=>'segoe ui', 'label'=>Mage::helper('camen')->__('Segoe UI'))
		);
	}
}
