<?php

class Sns_Camen_Model_System_Config_Source_ListColor
{
	public function toOptionArray()
	{
		return array(
			array('value'=>'red', 'label'=>Mage::helper('camen')->__('Red')),
			array('value'=>'brown', 'label'=>Mage::helper('camen')->__('Brown')),
			array('value'=>'purple', 'label'=>Mage::helper('camen')->__('Purple')),
			array('value'=>'yellow', 'label'=>Mage::helper('camen')->__('Yellow')),
			array('value'=>'blue', 'label'=>Mage::helper('camen')->__('Blue')),
			array('value'=>'green', 'label'=>Mage::helper('camen')->__('Green')),
			array('value'=>'chocolate', 'label'=>Mage::helper('camen')->__('Chocolate')),
			array('value'=>'slateblue', 'label'=>Mage::helper('camen')->__('Slateblue')),
		);
	}
}
