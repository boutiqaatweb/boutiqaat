<?php

class Sns_Camen_Model_System_Config_Source_ListEffectImage
{
	public function toOptionArray()
	{
		return array(
			array('value'=>'', 'label'=>Mage::helper('camen')->__('None')),
			array('value'=>'preserve-3d', 'label'=>Mage::helper('camen')->__('Preserve 3D')),
			array('value'=>'translatex', 'label'=>Mage::helper('camen')->__('Translate X')),
			array('value'=>'translatey', 'label'=>Mage::helper('camen')->__('Translate Y')),
		);
	}
}
