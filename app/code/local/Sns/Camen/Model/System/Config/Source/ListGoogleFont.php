<?php

class Sns_Camen_Model_System_Config_Source_ListGoogleFont
{
	public function toOptionArray()
	{
		return array(
			array('value'=>'', 'label'=>Mage::helper('camen')->__('No select')),
			array('value'=>'Anton', 'label'=>Mage::helper('camen')->__('Anton')),
			array('value'=>'Roboto+Condensed', 'label'=>Mage::helper('camen')->__('Roboto Condensed')),
			array('value'=>'Port+Lligat+Slab', 'label'=>Mage::helper('camen')->__('Port Lligat Slab')),
			array('value'=>'Questrial', 'label'=>Mage::helper('camen')->__('Questrial')),
			array('value'=>'Kameron', 'label'=>Mage::helper('camen')->__('Kameron')),
			array('value'=>'Oswald:300,400,700', 'label'=>Mage::helper('camen')->__('Oswald')),
			array('value'=>'Open+Sans:300,400,600,700', 'label'=>Mage::helper('camen')->__('Open Sans')),
			array('value'=>'Open+Sans+Condensed:300,300italic,700', 'label'=>Mage::helper('camen')->__('Open Sans Condensed')),
			array('value'=>'BenchNine', 'label'=>Mage::helper('camen')->__('BenchNine')),
			array('value'=>'Droid Sans', 'label'=>Mage::helper('camen')->__('Droid Sans')),
			array('value'=>'Droid Serif', 'label'=>Mage::helper('camen')->__('Droid Serif')),
			array('value'=>'PT Sans', 'label'=>Mage::helper('camen')->__('PT Sans')),
			array('value'=>'Vollkorn', 'label'=>Mage::helper('camen')->__('Vollkorn')),
			array('value'=>'Ubuntu', 'label'=>Mage::helper('camen')->__('Ubuntu')),
			array('value'=>'Neucha', 'label'=>Mage::helper('camen')->__('Neucha')),
			array('value'=>'Cuprum', 'label'=>Mage::helper('camen')->__('Cuprum')),
			array('value'=>'Passion+One:400,700', 'label'=>Mage::helper('camen')->__('Passion One')),
			array('value'=>'Roboto+Slab:400,700', 'label'=>Mage::helper('camen')->__('Roboto Slab'))
		);
	}
}
