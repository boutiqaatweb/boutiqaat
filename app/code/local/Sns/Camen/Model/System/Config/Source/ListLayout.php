<?php

class Sns_Camen_Model_System_Config_Source_ListLayout
{
	public function toOptionArray()
	{
		return array(
			array('value'=>'1', 'label'=>Mage::helper('camen')->__('Full width')),
			array('value'=>'2', 'label'=>Mage::helper('camen')->__('Boxed')),
		);
	}
}
