<?php

class Sns_Camen_Model_System_Config_Source_ListMenuStyles
{
	public function toOptionArray()
	{
		return array(
			//array('value'=>'base', 'label'=>Mage::helper('camen')->__('Base')),
			array('value'=>'mega', 'label'=>Mage::helper('camen')->__('Mega'))
		);
	}
}
