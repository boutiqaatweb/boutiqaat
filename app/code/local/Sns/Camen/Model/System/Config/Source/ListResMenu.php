<?php

class Sns_Camen_Model_System_Config_Source_ListResMenu
{
	public function toOptionArray()
	{
		return array(
			array('value'=>'sidebar', 'label'=>Mage::helper('camen')->__('SideBar')),
			array('value'=>'collapse', 'label'=>Mage::helper('camen')->__('Collapse'))
		);
	}
}
