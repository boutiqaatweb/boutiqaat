<?php

class Sns_Camen_Model_System_Config_Source_StyleHeader
{
	public function toOptionArray()
	{
		return array(
			array('value'=>'transparent', 'label'=>Mage::helper('camen')->__('Transparent')),
			array('value'=>'solid', 'label'=>Mage::helper('camen')->__('Solid'))
		);
	}
}