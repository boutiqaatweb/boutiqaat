<?php 
    class Sns_Camen_Adminhtml_ImportController extends Mage_Adminhtml_Controller_Action{
        public function indexAction() {
            $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit/section/sns_camen_cfg/"));
        }
        public function blocksAction() {
            $isoverwrite = Mage::helper('camen')->getCfg('install/overwrite_blocks');
            Mage::getSingleton('camen/import_cms')->importCms('cms/block', 'blocks', $isoverwrite);
            $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit/section/sns_camen_cfg/"));
        }
        public function pagesAction() {
            $isoverwrite = Mage::helper('camen')->getCfg('install/overwrite_pages');
            Mage::getSingleton('camen/import_cms')->importCms('cms/page', 'pages', $isoverwrite);
            $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit/section/sns_camen_cfg/"));
        }
    }
    //$config = Mage::getStoreConfig('section_name/group/field'); //value
?>