<?php

class Sns_Products_Block_Deals extends Sns_Products_Block_List {
	protected $defaultTemplate = 'sns/products/deals.phtml';
	protected $filter = 'deals';

	public function getMobileDetect(){
		require_once(Mage::getBaseDir('lib').'/Mobile_detect/Mobile_Detect.php'); 
		return $detect = new Mobile_Detect;
			
	}
}
