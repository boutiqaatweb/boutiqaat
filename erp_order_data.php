<?php
/**
 * @author      MagePsycho <info@magepsycho.com>
 * @website     http://www.magepsycho.com
 * @category    Export / Import
 */
$mageFilename = 'app/Mage.php';
require_once $mageFilename;
Mage::setIsDeveloperMode(true);
ini_set('display_errors', 1);
umask(0);
Mage::app();
Mage::register('isSecureArea', 1);
// Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
 
set_time_limit(0);
ini_set('memory_limit','1024M');
 
$id = 1744;

$order = Mage::getModel('sales/order')->load($id);
$payment_method_code = $order->getPayment()->getMethodInstance()->getCode();
$status = $order->getStatus();
/*if($payment_method_code =='cashondelivery' && $status == 'pending'){
    return;
}*/
$shipping_address = $order->getShippingAddress();
$remarks = $shipping_address->getStreet();
$extra_remarks = $remarks[0];
$ddate = Mage::getModel('ddate/ddate_store')->load($order->getId(),'sales_order_id')->getDdateId();
$dtime = Mage::getModel('ddate/ddate')->load($ddate);
$order_data = array();
$order_lines = array();
foreach ($order->getAllItems() as $order_item) {
	$order_items['sku'] = $order_item['sku'];
    $order_items['product_name'] = $order_item['name'];
    $order_items['price'] = number_format($order_item['price'],4);
    $order_items['qty'] = number_format($order_item['qty_ordered'],4);
    $order_items['total'] = number_format($order_item['row_total'],4);
    $order_items['discount'] = number_format($order_item['discount_amount'],4);
    
    /* Celebrity Value*/
    $order_items['category_id'] = $order_item['category_id'];
    $order_items['special_name'] = $order_item['special_name'];
    $order_items['celebrity_price'] = $order_item['celebrity_special_price'];
    $order_items['celebrity_commision'] = $order_item['commision_amount'];
    $order_items['special_name'] = $order_item['special_name'];
    $order_items['ad_date'] = $order_item['celebrity_ad_date'];
    $order_lines[] = $order_items;
}
$order_data['order_date'] = $order->getCreatedAt();
$order_data['order_lines'] = $order_lines;
$order_data['order_number'] = $order->getIncrementId();
if($order->getCustomerIsGuest()){
    $order_data['customer_idspecified'] = '0';
    $order_data['customer_id'] = '';
}else{
    $order_data['customer_idspecified'] = '1';
    $order_data['customer_id'] = $order->getCustomerId();
}
$order_data['store_id'] = Mage::getModel('core/store')->load($order->getStoreId())->getCode();//$order->getStoreName();
$order_data['shipping_firstname'] = $shipping_address->getFirstname();
$order_data['shipping_lastname'] = $shipping_address->getLastname();
$order_data['customer_email'] = $order->getCustomerEmail();
$order_data['discount'] = number_format($order->getDiscountAmount(),4);
$order_data['payment_method_id'] = $order->getPayment()->getMethodInstance()->getCode();
$order_data['shipping_method'] = $order->getShippingMethod();
$order_data['delivery_date'] = $dtime['ddate'];//'0000-00-00';
$order_data['sales_center'] = '1';
$order_data['city'] = $shipping_address->getCity();
$order_data['street_address'] = $shipping_address->getAddrStreet();
$order_data['phone_number'] = $shipping_address->getTelephone();
$order_data['order_remarks'] = $extra_remarks;
$order_data['time_slot'] = $dtime->getDtimetext();//'00:00';
if($shipping_address->getCountryId() == 'KW'){
    $order_data['order_area'] = $shipping_address->getCity();
    $order_data['order_block'] = $shipping_address->getAddrBlock();
    $order_data['order_mobile'] = $shipping_address->getTelephone();
    $order_data['order_buildingnumber'] = $shipping_address->getAddrVilla();
    $order_data['order_flatenumber'] = $shipping_address->getAddrFlatenumber();
    //$order_data['order_floornumber'] = $shipping_address->getAddrFloornumber();
    //$order_data['order_landline'] = $shipping_address->getAddrLandline();
}
echo '<pre>'; 
print_r($order_data);
?>