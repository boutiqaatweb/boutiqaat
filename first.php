<?php
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors',1);

include 'app/Mage.php';
Mage::setIsDeveloperMode(true);
Mage::app();

$api_url_v2 = "http://37.34.186.95/ERPwebservice/n_erpwebservice.asmx?WSDL";
$username = Mage::getStoreConfig('erp/erp_config/username');
$password = Mage::getStoreConfig('erp/erp_config/password'); 

// $cli = new SoapClient($api_url_v2, array('cache_wsdl' => WSDL_CACHE_NONE));
$cli = new SoapClient($api_url_v2, array('cache_wsdl' => WSDL_CACHE_NONE,'trace' => 1,'exceptions'=> 1,'connection_timeout' => 30));
$login = array('as_userid'=>$username,'as_pwd'=>$password);
$login_object = (object)$login;
$session_id_function = $cli->login($login_object);
$session_id_user = array('a_session_id'=>$session_id_function);
$session = (object)$session_id_user;

echo "<pre>";
print_r($session);
exit;