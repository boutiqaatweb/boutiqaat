<?php
Mage::app();
$geoIP = Mage::getSingleton('geoip/country');
$country = $geoIP->getCountry();

if(strcmp($country,'US') == 0) {
    $mageRunType = 'website';
    $mageRunCode = 'US';
}
else
{
    $mageRunType = 'website';
    $mageRunCode = 'CA';
}
Mage::reset();