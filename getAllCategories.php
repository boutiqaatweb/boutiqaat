<?php
set_time_limit(0);
require_once 'app/Mage.php';
Mage::app();

   $rootCatId = Mage::app()->getStore()->getRootCategoryId();
   $catlistHtml = getTreeCategories($rootCatId, false);
   echo $catlistHtml;

function getTreeCategories($parentId, $isChild){
    $allCats = Mage::getModel('catalog/category')->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('is_active','1')
                ->addAttributeToFilter('include_in_menu','1')
                ->addAttributeToFilter('parent_id',array('eq' => $parentId))
                ->addAttributeToSort('position', 'asc'); 
                
    $class = ($isChild) ? "sub-cat-list" : "cat-list";
    $html .= '<ul class="'.$class.'">';
    foreach($allCats as $category)
    {
        $html .= '<li><span>'.$category->getName()." (".$category->getID().")</span>";
        $subcats = $category->getChildren();
        if($subcats != ''){ 
            $html .= getTreeCategories($category->getId(), true);
        }
        $html .= '</li>';
    }
    $html .= '</ul>';
    return $html;
}

?>