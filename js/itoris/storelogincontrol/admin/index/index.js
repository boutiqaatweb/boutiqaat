var StoreLoginControl = {

	accessTable : {
		init : function(){
			$(StoreLoginControl.WEBSITE_SELECT_ID).observe('change',this.events.onWebsiteSelectChangedHandler);
			$$('.website_config').each(function(webSiteNode){
				var websiteId = parseInt(webSiteNode.id.substr(17));
				webSiteNode.select('input').each(function(input){
					input.observe('change', function(){
						StoreLoginControl.accessTable.changed(websiteId);
					});
				});

				try{
					webSiteNode.select('button.save_changes_button')[0].observe('click', function(ev){
						StoreLoginControl.accessTable.saveWebsiteChanges(websiteId);
						ev.stop();
						return false;
					});
				}catch(e){/*ignored*/}
			});
		},

		changed : function(wid){
			$('save_changes_container_' + wid).addClassName('active');
		},

		saveWebsiteChanges : function(wid){
			var params = {'wid': wid};
			//params['website_id'] = wid;
			$('access_table_tab_' + wid).select('input[type=checkbox]').each(function(input){
				params[input.name] = input.checked ? input.value : 0;
			});
			new Ajax.Request(StoreLoginControl.accessTable.ACTION_SAVE_ACCESS_TABLE,{
				parameters : params,
				//onComplete : this.saveWebsiteChangesCallback,

				onSuccess : this.saveWebsiteChangesCallback.bind(this),
				onFailure : StoreLoginControl.ajaxFailure
			});
		},

		saveWebsiteChangesCallback : function(transport, resp){
			if(!transport.responseJSON || transport.responseJSON.error == true){
				return StoreLoginControl.ajaxFailure(transport);
			}

			alert(transport.responseJSON.message);
			$('save_changes_container_' + transport.responseJSON.wid).removeClassName('active');
		},

		events : {
			onWebsiteSelectChangedHandler : function(ev){
				$$('.website_config.active')[0].removeClassName('active');
				$('access_table_tab_' + ev.target.value).addClassName('active');
			}
		},

		ACTION_SAVE_ACCESS_TABLE : null
	},

	window : {
		message : function(msg){
			
		}
	},

	ajaxFailure : function(transport){
		if(transport.responseJSON && transport.responseJSON.message){
			alert(transport.responseJSON.message);
			return;
		}

		alert("Error while connecting to the server. Please try again.");
	},

	

	WEBSITE_SELECT_ID : 'website_select'
};