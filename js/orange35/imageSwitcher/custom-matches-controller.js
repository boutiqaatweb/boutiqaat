(function () {
    document.observe('dom:loaded', function () {
        if (typeof document.Orange35.ImageSwitcherMatchesData === 'undefined') {
            throw 'imageSwitcherMatchesData module not found';
        }
        var module = document.Orange35.ImageSwitcherMatchesData;

        if (!Object.keys(module.optionsInfo).length) {
            $$('.custom-matches-custom-options-list-empty-notice').first().style.display = 'block';
            $$('.custom-matches-new-form').first().style.display = 'none';
            return true;
        }

        if (!module.imagesCount) {
            $$('.custom-matches-images-list-empty-notice').first().style.display = 'block';
            $$('.custom-matches-new-form').first().style.display = 'none';
            return true;
        }

        var optionsCollection = {};

        for (var optionId in module.optionsInfo) {
            if (module.optionsInfo.hasOwnProperty(optionId)) {
                optionsCollection[optionId] = module.optionFactory(optionId);
            }
        }

        var imageSelector = new module.imageSelector($('gallery-image-selector'));
        var matchesManager = new module.matchesManager(module.matches, $('matches-preview-container'));

        module.imageSelectorObj = imageSelector;
        module.optionsCollection = optionsCollection;

        var applyButton = $('match-apply');

        var timeoutId = 0;
        applyButton.observe('click', function (event) {
            event.preventDefault();
            var values = {};
            for (optionId in optionsCollection) {
                if (optionsCollection.hasOwnProperty(optionId)) {
                    var optionObj = optionsCollection[optionId];
                    if($('checkbox_allvalue_'+optionId).checked){
                        values[optionId] = $('checkbox_allvalue_'+optionId).getValue();
                    }
                    else{
                        if (optionObj.hasValue()) {
                            values[optionId] = optionObj.getValue();
                        }
                    }
                }
            }
            if (!Object.keys(values).length) {
                $$('.matches-action-block .error').first().appear({duration: 0});
                if (timeoutId) {
                    window.clearTimeout(timeoutId);
                    timeoutId = 0;
                }
                timeoutId = window.setTimeout(function () {
                    $$('.matches-action-block .error').first().fade({duration: 0.2, from: 0.9});
                    timeoutId = 0;
                }, 4000);
            } else {
                var match = {
                    productId : module.productId,
                    imageId   : imageSelector.getImageId(),
                    imagesId  : imageSelector.getImagesId().slice(0),
                    values    : values
                };
                if (imageSelector.hasImagesId()) {
                    matchesManager.addMatch(match, true);
                    imageSelector.imageId = false;
                    imageSelector.imagesId = [];
                    imageSelector.updateView();
                } else {
                    $$('.matches-action-block .error').first().appear({duration: 0});
                    timeoutId = window.setTimeout(function () {
                        $$('.matches-action-block .error').first().fade({duration: 0.2, from: 0.9});
                        timeoutId = 0;
                    }, 4000);
                    matchesManager.removeMatch(match, true);
                }
            }
        });
        matchesManager.setMatches(module.matches, true);
        return true;
    });
})();