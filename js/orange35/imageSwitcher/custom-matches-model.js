(function() {
    var extend = function (dest, source) {
        var F = function () {};
        F.prototype = source.prototype;
        dest.prototype = new F();
        dest.prototype.constructor = dest;
    };

    /**
     * Abstract option class
     * @param optionId
     * @param optionBoxElement root element of option DOM
     * @param valuesList accept values
     * @returns {CustomOptionAbstract}
     * @constructor
     */
    var CustomOptionAbstract = function(optionId, optionBoxElement, valuesList) {
        this.multiple = false;
        this.id = optionId;
        this.optionBoxElement = optionBoxElement;
        this.valuesList = valuesList;
        this.value = false;
        this.init();
        return this;
    };

    CustomOptionAbstract.fn = CustomOptionAbstract.prototype;

    CustomOptionAbstract.fn.init = function() {
        this.root = this.optionBoxElement.select('.input-box').first();
        this.attachObservers();
    };

    /**
     * Attach observers to DOM objects
     */
    CustomOptionAbstract.fn.attachObservers = function () {
        throw 'Method must be overridden';
    };

    /**
     * Retrieve current selected value
     * @returns {int | Array}
     */
    CustomOptionAbstract.fn.getValue = function () {
        return this.value;
    };

    /**
     * Retrieve list of valid values
     * @returns {Array}
     */
    CustomOptionAbstract.fn.getValuesList = function () {
        return this.valuesList;
    };

    /**
     * Set option's current value(s) (with view updating)
     * @param value {int | boolean | Array}
     * @param updateView {boolean}
     * @returns {CustomOptionAbstract.fn}
     */
    CustomOptionAbstract.fn.setValue = function (value, updateView) {
        var acceptValues = this.getValuesList();
        acceptValues.push("%all%");
        if (!value) {
            value = false;
        } else if (this.isMultiple()) {
            for (var innerValue in value) {
                if (value.hasOwnProperty(innerValue)) {
                    var valid = false;
                    for (var validValue in acceptValues) {
                        if (acceptValues.hasOwnProperty(validValue)
                            && value[innerValue] == acceptValues[validValue]
                        ) {
                            valid = true;
                            break;
                        }
                    }
                    if (!valid) {
                        if(value != "%all%"){
                            throw 'Invalid values list given';
                        }
                    }
                }
            }
        } else {
            valid = false;
            for (validValue in acceptValues) {
                if (acceptValues.hasOwnProperty(validValue)
                    && value == acceptValues[validValue]
                ) {
                    valid = true;
                    break;
                }
            }
            if (!valid) {
                if(value != "%all%"){
                    throw 'Invalid value given';
                }
            }
        }
        if(value == "%all%"){
            $("checkbox_allvalue_"+this.id).checked = true;
            $$("#select_"+this.id+", [id^='checkbox_value_"+this.id+"'], [id^='radio_value_"+this.id+"']").each(function(e){
                if(e.type == "checkbox" || e.type == "radio"){
                    e.parentNode.hide();
                }
                else{
                    e.hide();
                }
            });
        }
        else{
            $("checkbox_allvalue_"+this.id).checked = false;
            $$("#select_"+this.id+", [id^='checkbox_value_"+this.id+"'], [id^='radio_value_"+this.id+"']").each(function(e){
                if(e.type == "checkbox" || e.type == "radio"){
                    e.parentNode.show();
                }
                else{
                    e.show();
                }
            });
            this.value = value;
        }

        if (true === updateView) {
            this.updateView();
        }
        return this;
    };

    /**
     * Update view according to the current values (method must be overridden in child classes)
     * @return {CustomOptionAbstract.fn}
     */
    CustomOptionAbstract.fn.updateView = function () {
        throw 'Method must be overridden';
    };

    /**
     * For options that may not have values
     * @returns {boolean}
     */
    CustomOptionAbstract.fn.hasValue = function () {
        return this.value !== false;
    };

    /**
     * Does the option possibility to have multiple values
     * @returns {boolean}
     */
    CustomOptionAbstract.fn.isMultiple = function () {
        return this.multiple;
    };

    /**
     * Retrieve ID of associated option
     * @returns {int}
     */
    CustomOptionAbstract.fn.getId = function () {
        return this.id;
    };

    /**
     * Checkboxes option
     * @param optionId
     * @param optionBoxElement
     * @param valuesList
     * @returns {CustomOptionCheckBoxes}
     * @constructor
     */
    var CustomOptionCheckBoxes = function(optionId, optionBoxElement, valuesList) {
        CustomOptionAbstract.call(this, optionId, optionBoxElement, valuesList);
        this.multiple = true;
        return this;
    };

    extend(CustomOptionCheckBoxes, CustomOptionAbstract);

    CustomOptionCheckBoxes.fn = CustomOptionCheckBoxes.prototype;

    CustomOptionCheckBoxes.fn.updateValue = function () {
        var values = [];
        this.root.select('input[type="checkbox"]').each(function (el) {
            if (el.checked) {
                values.push(el.value);
            }
        });
        if (!values.length) {
            values = false;
        }
        this.setValue(values, true);
    };

    CustomOptionCheckBoxes.fn.attachObservers = function () {
        var checkboxesCount = 0;
        var self = this;
        this.root.select('input[type="checkbox"]').each(function (el) {
            checkboxesCount++;
            el.observe('click', function () {
                self.updateValue();
            });
        });
        if (checkboxesCount !== this.valuesList.length) {
            throw 'Invalid HTML markup';
        }
    };

    CustomOptionCheckBoxes.fn.updateView = function () {
        this.root.select('input[type="checkbox"]').each(function (el) {
            el.checked = false;
        });
        if (this.hasValue()) {
            var values = this.getValue();
            for (var i in values) {
                if (values.hasOwnProperty(i)) {
                    this.root.select('input[type="checkbox"][value="' + values[i] + '"]').first().checked = true;
                }
            }
        }
    };

    /**
     * MultiSelect option
     * @param optionId
     * @param optionBoxElement
     * @param valuesList
     * @returns {CustomOptionMulti}
     * @constructor
     */
    var CustomOptionMulti = function(optionId, optionBoxElement, valuesList) {
        CustomOptionAbstract.call(this, optionId, optionBoxElement, valuesList);
        this.multiple = true;
        return this;
    };

    extend(CustomOptionMulti, CustomOptionAbstract);

    CustomOptionMulti.fn = CustomOptionMulti.prototype;

    CustomOptionMulti.fn.updateView = function () {
        this.root.select('option').each(function (el) {
            el.selected = false;
        });
        if (this.hasValue()) {
            var values = this.getValue();
            for (var i in values) {
                if (values.hasOwnProperty(i)) {
                    this.root.select('option[value="' + values[i] + '"]').first().selected = true;
                }
            }
        }
    };

    CustomOptionMulti.fn.updateValue = function () {
        var values = [];
        this.root.select('option').each(function (el) {
            if (el.selected) {
                values.push(el.value);
            }
        });
        if (!values.length) {
            values = false;
        }
        this.setValue(values, true);
    };

    CustomOptionMulti.fn.attachObservers = function () {
        var self = this;
        var element = this.root.select('select').first();
        if (!element) {
            throw 'Invalid HTML markup'
        }
        element.observe('change', function () {
            self.updateValue();
        });
    };

    /**
     * Radio option
     * @param optionId
     * @param optionBoxElement
     * @param valuesList
     * @returns {CustomOptionRadio}
     * @constructor
     */
    var CustomOptionRadio = function(optionId, optionBoxElement, valuesList) {
        CustomOptionAbstract.call(this, optionId, optionBoxElement, valuesList);
        this.multiple = false;
        return this;
    };

    extend(CustomOptionRadio, CustomOptionAbstract);

    CustomOptionRadio.fn = CustomOptionRadio.prototype;

    CustomOptionRadio.fn.updateView = function() {
        var currentSelectionId = 'radio_value_' + this.id + '_' + (this.hasValue() ? this.value : 'none');
        this.root.select('input[type="radio"]').each(function(el) {
            el.checked = el.id === currentSelectionId;
        });
    };

    CustomOptionRadio.fn.attachObservers = function () {
        var radioButtonsCount = 0;
        var self = this;
        this.root.select('input[type="radio"]').each(function (el) {
            radioButtonsCount++;
            el.observe('click', function () {
                self.setValue(parseInt(el.getValue()), true);
            });
        });
        if (radioButtonsCount !== this.valuesList.length + 1) {
            throw 'Invalid HTML markup';
        }
    };

    /**
     * DropDown option
     * @param optionId
     * @param optionBoxElement
     * @param valuesList
     * @returns {CustomOptionDropDown}
     * @constructor
     */
    var CustomOptionDropDown = function(optionId, optionBoxElement, valuesList) {
        CustomOptionAbstract.call(this, optionId, optionBoxElement, valuesList);
        return this;
    };

    extend(CustomOptionDropDown, CustomOptionAbstract);

    CustomOptionDropDown.fn = CustomOptionDropDown.prototype;

    CustomOptionDropDown.fn.updateView = function () {
        this.root.select('select').first().value = this.hasValue() ? this.getValue() : '';
    };

    CustomOptionDropDown.fn.attachObservers = function () {
        var self = this;
        var element = this.root.select('select').first();
        if (!element) {
            throw 'Invalid HTML markup'
        }
        element.observe('change', function () {
            self.setValue(parseInt(element.getValue()), true);
        });
    };


    /**
     * Retrieve option object
     * @param optionId
     * @returns {CustomOptionAbstract}
     */
    var customOptionFactory = function(optionId) {
        optionId = parseInt(optionId);
        var options = module.optionsInfo;
        if (typeof options === 'undefined') {
            throw 'Options info not initialized';
        }
        if (!options[optionId]) {
            throw 'Option with ID ' + optionId + ' not founded in option info';
        }
        var optionType   = options[optionId]['type'];
        var optionValues = options[optionId]['values'];
        var optionBoxElement = $('match_option_' + optionId);
        if (!optionBoxElement) {
            throw 'Can\'t find option root element';
        }
        var optionObj = null;
        switch (optionType) {
            case 'drop_down':
                optionObj = new CustomOptionDropDown(optionId, optionBoxElement, optionValues);
                break;
            case 'radio':
                optionObj = new CustomOptionRadio(optionId, optionBoxElement, optionValues);
                break;
            case 'multiple':
                optionObj = new CustomOptionMulti(optionId, optionBoxElement, optionValues);
                break;
            case 'checkbox':
                optionObj = new CustomOptionCheckBoxes(optionId, optionBoxElement, optionValues);
                break;
            default:
                throw 'Undefined option type';
        }
        return optionObj;
    };

    /**
     * Image selector element
     * @param galleryRoot
     * @returns {MatchesImageSelector}
     * @constructor
     */
    var MatchesImageSelector = function (galleryRoot) {
        if (!galleryRoot || !galleryRoot.select) {
            throw 'Invalid argument given';
        }
        this.imageId = false;
        this.imagesId = [];
        this.root = galleryRoot;
        this.images = [];
        this.init();
        return this;
    };

    MatchesImageSelector.fn = MatchesImageSelector.prototype;

    MatchesImageSelector.fn.init = function () {
        var self = this;
        var images = [];
        var elementIdPrefixLength = ('gallery_image_').length;
        this.root.select('div.[id^="gallery_image_"]').each(function (el) {
            var elementId = el.id;
            var imageId = parseInt(elementId.substr(elementIdPrefixLength));
            if (!imageId) {
                throw 'Invalid element id';
            }
            images.push(imageId);
            el.observe('click', function (event) {
                if (!event.ctrlKey) {
                    self.setImageId(imageId, true);
                } else {
                    self.setImageId(false, true);
                }
            });
        });
        if (!images.length) {
            throw 'Images not found';
        }
        this.images = images;
    };

    /**
     * Retrieve selected image id
     * @returns {int}
     */
    MatchesImageSelector.fn.getImageId = function () {
        return this.imageId;
    };

    MatchesImageSelector.fn.getImagesId = function () {
        return this.imagesId;
    };

    /**
     * Retrieve list of images owned by current product
     * @returns {Array}
     */
    MatchesImageSelector.fn.getImagesList = function () {
        return this.images;
    };

    MatchesImageSelector.fn.hasImageId = function () {
        return false !== this.imageId;
    };
    MatchesImageSelector.fn.hasImagesId = function () {
        if(!this.imagesId.length){
            return false;
        }
        else{
            return true;
        }
    };

    /**
     * Set selected image
     * @param imageId {int | boolean}
     * @param updateView {boolean}
     * @returns {MatchesImageSelector.fn}
     */
    MatchesImageSelector.fn.setImageId = function (imageId, updateView) {
        if (!imageId) {
            imageId = false;
        } else {
            var images = this.getImagesList();
            var valid = false;
            for (var i in images) {
                if (images.hasOwnProperty(i)) {
                    if (imageId == images[i]) {
                        valid = true;
                        break;
                    }
                }
            }
            if (!valid) {
                throw 'Invalid image id';
            }
        }
        if(this.imagesId.indexOf(imageId) >= 0){
            this.imagesId.splice(this.imagesId.indexOf(imageId), 1);
        }
        else{
            this.imagesId.push(imageId);
        }
        this.imageId = imageId;
        if (true === updateView) {
            this.updateView();
        }
        return this;
    };

    MatchesImageSelector.fn.updateView = function () {
        this.root.select('div.[id^="gallery_image_"].selected').each(function (el) {
            el.removeClassName('selected');
        });
        /*if (this.hasImageId()) {
            $('gallery_image_' + this.getImageId()).addClassName('selected');
        }*/
        if(this.imagesId.length){
            this.imagesId.each(function(image_id){
                $('gallery_image_' + image_id).addClassName('selected');
            });
        }
        return this;
    };

    /**
     * Matches manager
     * @param matches {Object} array of available matches assigned to current product
     * @param container
     * @returns {MatchesManager}
     * @constructor
     */
    var MatchesManager = function (matches, container) {
        if (!container || !container.select) {
            throw 'Invalid container';
        }
        if ('object' !== typeof matches) {
            throw 'Invalid matches given';
        }
        this.matches = [];
        for (var index in matches) {
            if (matches.hasOwnProperty(index)) {
                this.matches.push(matches[index]);
            }
        }
        this.container = container;
        this.init();
        return this;
    };

    MatchesManager.fn = MatchesManager.prototype;

    MatchesManager.fn.init = function () {

        return this;
    };

    MatchesManager.fn.getMatches = function () {
        return this.matches;
    };

    MatchesManager.fn.addMatch = function (match, updateView) {
        var matches = this.getMatches();
        var loc = module.localization;
        var newMatches = [];
        var added = false;
        for (var i = 0, length = matches.length; i < length; i++) {
            if (this.compareMatches(match, matches[i])) {
                newMatches.push(match);
                added = true;
            } else {
                newMatches.push(matches[i]);
            }
        }
        if (!added) {
            newMatches.push(match);
            this.setMatches(newMatches, updateView);
            document.getElementById("co_combination").scrollIntoView();
        }
        else{
            var k = confirm(loc["You're about to change an image for an existing combination, would you like to proceed anyway?"]);
            if(k){
                this.setMatches(newMatches, updateView);
                document.getElementById("co_combination").scrollIntoView();
            }
        }

    };

    MatchesManager.fn.removeMatch = function (match, updateView) {
        var matches = this.getMatches();
        var newMatches = [];
        for (var i = 0, length = matches.length; i < length; i++) {
            if (!this.compareMatches(match, matches[i])) {
                newMatches.push(matches[i]);
            }
        }
        this.setMatches(newMatches, updateView);
    };

    MatchesManager.fn.compareMatches = function (leftMatch, rightMatch) {
        if (!leftMatch || !leftMatch.values) {
            throw 'Invalid left argument';
        }
        if (!rightMatch || !rightMatch.values) {
            throw 'Invalid right argument';
        }
        var leftValues = leftMatch.values;
        var rightValues = rightMatch.values;
        if (Object.keys(leftValues).length !== Object.keys(rightValues).length) {
            return false;
        }
        for (var optionId in leftValues) {
            if (leftValues.hasOwnProperty(optionId)) {
                if (leftValues[optionId] instanceof Array && rightValues[optionId] instanceof Array) {
                    if (leftValues[optionId].length === rightValues[optionId].length) {
                        for (var i = 0, length = leftValues[optionId].length; i < length; i++) {
                            if (leftValues[optionId][i] !== rightValues[optionId][i]) {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                } else if (leftValues[optionId] !== rightValues[optionId]) {
                    return false;
                }
            }
        }
        return true;
    };

    MatchesManager.fn.setMatches = function (matches, updateView) {
        this.matches = matches;
        if (true == updateView) {
            this.updateView();
        }
        return this;
    };

    /**
     * Update matches table
     * @returns {MatchesManager.fn}
     */
    MatchesManager.fn.updateView = function () {
        var table = '<table class="grid" cellspacing="0">';

        var matches = this.getMatches();
        if (!matches.length) {
            table = module.notMatchesNotice;
        } else {
            var optionsLabels = module.optionsLabels;
            var optionsInfo = module.optionsInfo;
            var optionsOrder = module.optionsOrder;

            var loc = module.localization;

            table += '<thead><tr class="headings">';

            for (var index in optionsOrder) {
                if (optionsOrder.hasOwnProperty(index)) {
                    var optionId = optionsOrder[index];
                    table += '<th>' + optionsLabels.options[optionId] + '</th>';
                }
            }
            table += '<th>' + loc['Image'] + '</th><th>' + loc['Actions'] + '</th>';

            table += '</tr></thead>';

            var images = module.images;
            for (var i = matches.length-1; i >= 0; i--) {
                /** @type {Object} */
                var values = matches[i].values;
                var imageSrc = images[matches[i].imageId];

                table += '<tr>';
                for (index in optionsOrder) {
                    if (optionsOrder.hasOwnProperty(index)) {
                        optionId = optionsOrder[index];
                        table += '<td>';
                        if (values.hasOwnProperty(optionId)) {
                            if (values[optionId] instanceof Array) {
                                var first = true;
                                for (var k = 0, l = values[optionId].length; k < l; k++) {
                                    if (!first) {
                                        table += ', ';
                                    }
                                    table += optionsLabels.values[values[optionId][k]];
                                    first = false;
                                }
                            } else {
                                if(values[optionId]=="%all%"){
                                    table += "--All Values--";
                                }
                                else{
                                    table += optionsLabels.values[values[optionId]];
                                }
                            }
                        } else {
                            table += '--Not Selected--'
                        }
                        table += '</td>';
                    }
                }


                table += '<td class="image">';
                matches[i].imagesId.each(function(image_id){
                    var image_src = images[image_id];
                    table+='<img src="' + image_src + '">';
                });
                table += '</td>';

                //table += '<td class="image"><img src="' + imageSrc + '"></td>';

                table += '<td class="actions">'
                    + '<a class="delete" title="'+loc['Delete this Combination']+'" href="#" onclick="return false;" value="' + i
                    + '">' + loc['Delete'] + '</a>'
                    + '<br /><a class="edit" title="'+loc['Edit this Combination']+'" href="#" value="' + i + '">' + loc['Edit'] + '</a>';

                //Add hidden input for save matches on db
                table += '<input type="hidden" name="customOptionsMatches[]" value=\''
                    + JSON.stringify(matches[i]) + '\' /> ';

                table += '</td></tr>';
            }

            table += '</table>';
        }

        this.container.innerHTML
            ? this.container.innerHTML = table
            : this.textContent = table;

        this.bindTableObservers();
        return this;
    };

    MatchesManager.fn.bindTableObservers = function () {
        var self = this;
        this.container.select('a.delete').each(function (el) {
            el.observe('click', function (event) {
                event.preventDefault();
                var matches = self.getMatches();
                var newMatches = [];
                var index = el.getAttribute('value');
                for (var i = 0, length = matches.length; i < length; i++) {
                    if (i != index) {
                        newMatches.push(matches[i]);
                    }
                }
                self.setMatches(newMatches, true);
            });
        });
        this.container.select('a.edit').each(function (el) {
            el.observe('click', function (event) {
                event.preventDefault();
                var matches = self.getMatches();
                var index = el.getAttribute('value');
                var optionsCollection = module.optionsCollection;
                var imageSelectorObj = module.imageSelectorObj;
                if (optionsCollection && imageSelectorObj && matches[index]) {
                    /** @type {Object} */
                    var values = matches[index].values;
                    var imageId = matches[index].imageId;
                    for (var optionId in optionsCollection) {
                        if (optionsCollection.hasOwnProperty(optionId)) {
                            if (values.hasOwnProperty(optionId)) {
                                optionsCollection[optionId].setValue(values[optionId], true);
                            } else {
                                optionsCollection[optionId].setValue(false, true);
                            }
                        }
                    }
                    imageSelectorObj.imagesId = [];
                    matches[index].imagesId.each(function(image_id){
                        imageSelectorObj.setImageId(image_id, true);
                    });
                    //imageSelectorObj.setImageId(imageId, true);
                    document.getElementById("co_main_title").scrollIntoView();
                    $$("#co_title .msg").first().show();
                    $("loading-mask").show();
                    var timeoutId = 0;
                    if (timeoutId) {
                        window.clearTimeout(timeoutId);
                        timeoutId = 0;
                    }
                    timeoutId = window.setTimeout(function () {
                        $("loading-mask").hide();
                        timeoutId = 0;
                    }, 500);
                    var timeoutId = 0;
                    if (timeoutId) {
                        window.clearTimeout(timeoutId);
                        timeoutId = 0;
                    }
                    timeoutId = window.setTimeout(function () {
                        $$("#co_title .msg").first().fade({duration: 0.2, from: 0.9});
                        timeoutId = 0;
                    }, 5000);
                }
            });
        });
    };

    /**
     * Global export
     */
    if (typeof document.Orange35 === 'undefined') {
        document.Orange35 = {};
    }
    if (typeof document.Orange35.ImageSwitcherMatchesData === 'undefined') {
        document.Orange35.ImageSwitcherMatchesData = {};
    }
    var module = document.Orange35.ImageSwitcherMatchesData;

    /**
     * Options descriptor (must be initialized in php code)
     * @type {Object}
     */
    module.optionsInfo;

    /**
     * Options order
     * @type {Object}
     */
    module.optionsOrder;

    /**
     * Options labels (must be initialized in php code)
     * @type {Object}
     */
    module.optionsLabels;

    /**
     * Count of product images (must be initialized in php code)
     * @type {int}
     */
    module.imagesCount;

    /**
     * Current matches (must be initialized in php code)
     * @type {Object}
     */
    module.matches;

    /**
     * (must be initialized in php code)
     * @type {string}
     */
    module.notMatchesNotice;

    /**
     * Product ID (must be initialized in php code)
     * @type {int}
     */
    module.productId;

    /**
     * Object with localization strings (must be initialized in php code)
     * @type {Object}
     */
    module.localization;

    /**
     * List of images owned by current product (must be initialized in php code)
     * @type {Object}
     */
    module.images;

    module.optionFactory    = customOptionFactory;
    module.imageSelector    = MatchesImageSelector;
    module.matchesManager   = MatchesManager;
})();