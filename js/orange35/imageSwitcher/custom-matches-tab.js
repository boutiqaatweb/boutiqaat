document.observe('dom:loaded', function() {
    $('product_info_tabs_orange35_imageSwitcher_optionsMatches').observe('click', function() {
        var customOptionsTab = $('product_info_tabs_customer_options');
        if (customOptionsTab.hasClassName('changed')) {
            $$('.custom-matches-content').first().style.display = 'none';
            $$('.custom-matches-notice').first().style.display = 'block';
        }
    });
});