<?php 
require_once 'app/Mage.php';
Mage::app();
$paymentid = $_GET['PaymentID']; // Reads the value of the Payment ID passed by GET request by knet.
$presult = $_GET['Result']; // Reads the value of the Result passed by GET request by knet.
$postdate = $_GET['PostDate']; // Reads the value of the PostDate passed by GET request byknet.
$tranid = $_GET['TranID']; // Reads the value of the TranID passed by GET request by knet.
$auth = $_GET['Auth']; // Reads the value of the Auth passed by GET request by the user.
$ref = $_GET['Ref']; // Reads the value of the Ref passed by GET request by knet.
$trackid = $_GET['TrackID'];  // Reads the value of the TrackID passed by GET request by knet.
$udf1 = $_GET['UDF1'];  // Reads the value of the UDF1 passed by GET request by knet.
$udf2 = $_GET['UDF2'];  // Reads the value of the UDF1 passed by GET request by knet.
$udf3 = $_GET['UDF3'];  // Reads the value of the UDF1 passed by GET request by knet.
$udf4 = $_GET['UDF4'];  // Reads the value of the UDF1 passed by GET request by knet.
$udf5 = $_GET['UDF5'];
$order_id = $udf1;
if ($order_id  == "") {
    $msgerror = Mage::helper('customer')->__('The payment has been declined. Kindly contact the site administrator.');
    //echo $msgerror;
    exit();
}
	try{
        //Write the response to the table
        $result = Mage::getModel('burgan/burgan')->load($order_id, 'order_id');
        $result->setData('paymentid',$paymentid);
        $result->setData('result',$presult);
        $result->setData('postdate',$postdate);
        $result->setData('tranid',$tranid);
        $result->setData('auth',$auth);
        $result->setData('ref',$ref);
        $result->setData('trackid',$trackid);
        $result->setData('udf1',$udf1);
        $result->setData('udf2',$udf2);
        $result->setData('udf3',$udf3);
        $result->setData('udf4',$udf4);
        $result->setData('udf5',$udf5);
        $result->save();
        $order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
        if($presult == 'CAPTURED'){
            if($order->canInvoice()) {
                //Start Handle Invoice
                $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                $invoice->register();
 
                $invoice->getOrder()->setCustomerNoteNotify(true);          
                $invoice->getOrder()->setIsInProcess(true);
                $order->addStatusHistoryComment('The payment has been registered successfully.', false);
 
                $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());
 
                $transactionSave->save();
                $order->sendNewOrderEmail();
                //End Handle Invoice
            }
            $msgsuccess = Mage::helper('customer')->__('The payment has been updated successfully');
            //echo $msgsuccess;
            exit();
        }else{
            $order->cancel();
            $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true)->save();
            $msgerror = Mage::helper('customer')->__('The order has been cancelled. Kindly contact the site administrator.');
            //echo $msgerror;
            exit();
        }  
    }catch(Exception $e){
    	$msgerror = $e->getMessage();
    	//echo $msgerror;
        exit();
    }
?>