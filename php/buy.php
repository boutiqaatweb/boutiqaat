<?php 
	require_once "com/aciworldwide/commerce/gateway/plugins/e24PaymentPipe.inc.php" ;
	$Pipe = new e24PaymentPipe;

   $Pipe->setAction(1);
   $Pipe->setCurrency(414);
   $Pipe->setLanguage("ARA"); //change it to "ARA" for arabic language
   $Pipe->setResponseURL("http://boutiqaat.com/php/response.php"); // set your respone page URL
   $Pipe->setErrorURL("http://boutiqaat.com/php/error.php"); //set your error page URL
   $Pipe->setAmt("10"); //set the amount for the transaction
   //$Pipe->setResourcePath("/Applications/MAMP/htdocs/php-toolkit/resource/");
   $Pipe->setResourcePath("/var/www/vhosts/boutiqaat.com/httpdocs/knet/"); //change the path where your resource file is
   $Pipe->setAlias("bout"); //set your alias name here
   $trackid_rand = rand(11111111,99999999);
   $Pipe->setTrackId($trackid_rand);//generate the random number here
 
   $Pipe->setUdf1("UDF 1"); //set User defined value
   $Pipe->setUdf2("UDF 2"); //set User defined value
   $Pipe->setUdf3("UDF 3"); //set User defined value
   $Pipe->setUdf4("UDF 4"); //set User defined value
   $Pipe->setUdf5("UDF 5"); //set User defined value
		if($Pipe->performPaymentInitialization()!=$Pipe->SUCCESS){
				echo "Result=".$Pipe->SUCCESS;
				echo "<br>".$Pipe->getErrorMsg();
				echo "<br>".$Pipe->getDebugMsg();
			//header("location: https://www.yourURL.com/error.php");
		}else {
			$payID = $Pipe->getPaymentId();
          $payURL = $Pipe->getPaymentPage();
			 //$Pipe->getDebugMsg();
			 header("location:".$payURL."?PaymentID=".$payID);
		}
?>