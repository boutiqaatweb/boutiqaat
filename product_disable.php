<?php
set_time_limit(0);
require_once 'app/Mage.php';
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$storeid = Mage_Core_Model_App::ADMIN_STORE_ID;
$row = 1;
Mage::log('product disable script started', Zend_Log::DEBUG, 'product_disable.log');
if (($handle = fopen("product_disable.csv", "r")) !== FALSE) {
    $skuArray = array();
    $descArray = array();
    while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
        $num = count($data);
        if($row == 1){
            for ($c=0; $c < $num; $c++) {
                if(strtolower($data[$c]) == 'sku'){
                    $skuIndex = $c;
                }
                echo $data[$c] . "<br />\n";
            }
            $row++;
        }else{
            $skuArray[] = $data[$skuIndex];
        }
    }
    fclose($handle);
}
echo count($skuArray);
Mage::log('product disable Count'.count($skuArray), Zend_Log::DEBUG, 'product_disable.log');
echo "<br />";
$products = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('sku', array('in' => $skuArray));
$products->addAttributeToFilter('status',1);

echo $products->count();
echo "<br />";
foreach($products as $product)
{
    $sku = $product->getSku();
    $product_id = $product->getId();
    //$storeid=Mage::app()->getStoreId();
    Mage::getModel('catalog/product_status')->updateProductStatus($product_id, $storeId, Mage_Catalog_Model_Product_Status::STATUS_DISABLED);

    Mage::log($sku, Zend_Log::DEBUG, 'product_disable.log');
}
Mage::log('product disable script Finished', Zend_Log::DEBUG, 'product_disable.log');
?>