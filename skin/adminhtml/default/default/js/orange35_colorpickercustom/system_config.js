Colorpickercustom_Config = Class.create();

Colorpickercustom_Config.prototype = {
    initialize:function(form){
        var showSliderSelect = $('colorpicker_section_custom_group_product_show_slider');
        var showTooltipSelect = $('colorpicker_section_custom_group_product_show_tooltip');
        this.toggleSlider();
        this.toggleTooltip();
        Event.observe(showSliderSelect, 'change', this.toggleSlider);
        Event.observe(showTooltipSelect, 'change', this.toggleTooltip);
    },

    toggleSlider:function(){
        var showSliderSelect = $('colorpicker_section_custom_group_product_show_slider');
        var showSlider = showSliderSelect.options[showSliderSelect.selectedIndex].value;
        if(showSlider == 0){
            $('row_colorpicker_section_custom_group_product_options_per_page').addClassName('no-display');
        }
        else{
            $('row_colorpicker_section_custom_group_product_options_per_page').removeClassName('no-display');
        }
    },

    toggleTooltip:function(){
        var showTooltipSelect = $('colorpicker_section_custom_group_product_show_tooltip');
        var showTooltip = showTooltipSelect.options[showTooltipSelect.selectedIndex].value;
        if(showTooltip == 0){
            $('row_colorpicker_section_custom_group_product_tooltip_width').addClassName('no-display');
            $('row_colorpicker_section_custom_group_product_tooltip_height').addClassName('no-display');
        }
        else{
            $('row_colorpicker_section_custom_group_product_tooltip_width').removeClassName('no-display');
            $('row_colorpicker_section_custom_group_product_tooltip_height').removeClassName('no-display');
        }
    }
}

document.observe('dom:loaded', function () {
        var cp_config = new Colorpickercustom_Config($('config_edit_form').serialize(true));
    }
);

function flushAttributeCacheImages(flushUrl, targetButton)
{
    if (targetButton.className.indexOf('disabled') != -1) {
        return false;
    }
    if (confirm('Do you want flush attribute cache images?')) {
        new Ajax.Request(flushUrl, {
            method: 'post',
            parameters: {},
            onSuccess: function(data){
                var response = data['responseText'].evalJSON();
                if (response.status == 'success') {
                    targetButton.className = targetButton.className + " disabled";
                }
            }
        });
    }
    return false;
}
