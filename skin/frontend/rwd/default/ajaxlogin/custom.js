
var awAjaxLoginUpdater = {
    prepareTopLinksBlock : function() {
        var skipContents = $j('.skip-content');
        var skipLinks = $j('.skip-link');

        skipLinks.on('click', function (e) {
            e.preventDefault();

            var self = $j(this);
            var target = self.attr('href');

            // Get target element
            var elem = $j(target);

            // Check if stub is open
            var isSkipContentOpen = elem.hasClass('skip-active') ? 1 : 0;

            // Hide all stubs
            skipLinks.removeClass('skip-active');
            skipContents.removeClass('skip-active');

            // Toggle stubs
            if (isSkipContentOpen) {
                self.removeClass('skip-active');
            } else {
                self.addClass('skip-active');
                elem.addClass('skip-active');
            }
        });
    }
};