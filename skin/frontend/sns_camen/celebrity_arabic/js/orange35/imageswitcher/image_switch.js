if (typeof Orange35 == 'undefined') {
    var Orange35 = {};
}
Orange35.ImageSwitcher = Class.create();
Orange35.ImageSwitcher.prototype = {
    defaultGallery: "",
    match: null,
    matchType: 'none',
    test: 1,
    originalGallery: "",
    defaultImg: false,      //todo: чому не ''?
    defaultZoomImg: false,  //todo: чому не ''?
    originalZoomImg: false, //todo: чому не ''?
    defaultGalleryMain: "",
    originalGalleryMain: "",
    productImageGallerySelector: ".product-image-gallery",
    /** from block config */
    customMatches: [],
    imageSelector: '',
    galleryImageTemplate: '',
    gallerySelector: '',
    galleryImageSelector: '',
    zoomImageSelector: '',
    typeStrict: 'strict',
    typeUnStrict: 'unstrict',
    typeAll: 'all',
    typeDefault: 'default',
    typeNone: 'none',
    initialize: function (config) {
        this.initConfig(config);
        this.prepareImages4Zoom();
        this.setDefaults();
        this.bindProcess();
    },
    initConfig: function(config) {
        this.customMatches        = config.customMatches;
        this.imageSelector        = config.imageSelector;
        this.galleryImageTemplate = config.galleryImageTemplate;
        this.gallerySelector      = config.gallerySelector;
        this.galleryImageSelector = config.galleryImageSelector;
        this.zoomImageSelector    = config.zoomImageSelector;
    },
    prepareImages4Zoom: function () {
        if ('undefined' == typeof document.images2ZoomImagesDictonary) {//todo: якось би уникнути глобальних об'єктів
            document.images2ZoomImagesDictonary = {};
        }
        this.customMatches.each(function (match) {
            var image = match.images[0];
            if (image['imageUrl'] && image['zoomImageUrl']) {
                document.images2ZoomImagesDictonary[image.imageUrl] = image.zoomImageUrl;
            }
        });
    },
    setDefaults: function () {
        if ($$(this.imageSelector).length) {
            this.defaultImg = $$(this.imageSelector).first().src;
        }
        this.originalImage = this.defaultImg;
        var thumbnails = $$(this.gallerySelector);
        if (thumbnails.length) {
            this.defaultGallery = thumbnails.first().innerHTML;
        }
        if ($$(this.productImageGallerySelector).length) {
            this.defaultGalleryMain = $$(this.productImageGallerySelector).first().innerHTML;
        }
        this.originalGallery = this.defaultGallery;
        this.originalGalleryMain = this.defaultGalleryMain;

        Event.observe(window, 'load', (function () {
            if (this.zoomImageSelector && $$(this.zoomImageSelector).length) {
                var zoomImage = $$(this.zoomImageSelector);
                if (zoomImage.length) {
                    var img = zoomImage.first();
                    switch (img.tagName.toLowerCase()) {
                        case 'img':
                            this.defaultZoomImg = img.src;
                            break;
                        case 'a':
                            this.defaultZoomImg = img.href;
                            break;
                        default:
                            this.defaultZoomImg = img.style.backgroundImage;
                            this.defaultZoomImg = this.defaultZoomImg.substring(5, this.defaultZoomImg.length - 2);
                            break;
                    }
                }
            } else {
                this.defaultZoomImg = false;
            }
            this.originalZoomImg = this.defaultZoomImg;
        }).bind(this));
    },
    initCloudZoom: function () {
        if (typeof jQuery != "undefined" && typeof jQuery().CloudZoom != "undefined") {
            if ($$('.product-image .mousetrap').length) {
                $$('.product-image .mousetrap').first().remove();
            }
            jQuery(this.zoomImageSelector + ', .cloud-zoom-gallery').CloudZoom();
        }
    },
    updateZoomImage: function (zoomImageUrl) {
        if (this.zoomImageSelector && zoomImageUrl) {
            $$(this.zoomImageSelector).each((function (zoomImage) {
                //var zoomImage = zoomImage;
                //if (zoomImage.length) {
                    var img = zoomImage; //.first();
                    switch (img.tagName.toLowerCase()) {
                        case 'img':
                            img.src = zoomImageUrl;
                            break;
                        case 'a':
                            img.href = zoomImageUrl;
                            break;
                        default:
                            img.style.backgroundImage = "url('" + zoomImageUrl + "')";
                    }
                //}
                
            }).bind(this));
        }        
    },
    initProductMediaManager: function (images) {
        if (typeof ProductMediaManager == "object") {
            $$(this.galleryImageSelector).each(function (e) {
                e.removeAttribute("onclick");
            });
            $$(".product-image-gallery .gallery-image").each(function (e) {
                if (e.id != "image-main") {
                    e.remove();
                }
            });
            if ($$(this.productImageGallerySelector).length) {
                if (images != undefined && images.length) {
                    var i = 0;
                    images.each((function (image) {
                        var item = '<img id="image-%index%" class="gallery-image" data-zoom-image="%image%" src="%image%" alt="%alt%" title="%title%">';
                        item = this.replace_template(item, image.imageUrl, image.imageUrl, image.imageUrl, i);
                        $$(this.productImageGallerySelector)[0].insert(item);
                        i++
                    }).bind(this));
                } else {
                    $$(this.productImageGallerySelector).first().update(this.defaultGalleryMain);
                }
                $$(".product-image .product-image-gallery .gallery-image").each(function (element) {
                    element.removeClassName("visible");
                });
                $$(this.imageSelector).each(function (element) {
                    element.addClassName("visible");
                });
            }
            ProductMediaManager.init();
        }
    },
    bindProcess: function () {
        if (typeof opConfig == "undefined") {
            return false;
        }
        opConfig.reloadPrice = opConfig.reloadPrice.wrap((function (parentMethod) {
            parentMethod();
            if ($$(this.imageSelector).length) {
                var selectedOptions = this.getSelectedOptions();
                this.initCombination(selectedOptions);
                if(this.matchType == this.typeDefault || this.matchType == this.typeNone){
                    this.switchDefaultImages();
                }
                else{
                    this.changeImages(this.match.images);
                }
            }

        }).bind(this));

        //Reload image on page load
        Event.observe(window, 'load', function () {
            opConfig.reloadPrice();
        });
    },
    initCombination: function (selectedOptions) {
        this.matchType = this.typeNone;
        this.match = null;
        if(JSON.stringify(selectedOptions) != "{}"){
            this.customMatches.each((function (match) {
                //якщо СТРОГА пара (1-го типу) співпала
                if (JSON.stringify(match.values) == JSON.stringify(selectedOptions)) {
                    this.matchType = this.typeStrict;
                    this.match = match;
                }
            }).bind(this));
            var allMatch = null;
            if (this.match == null) {

                this.customMatches.each((function (match) {
                    var detectedAllCombination = true;
                    var key;
                    var tempValues = this.cloneObj(selectedOptions);
                    for (key in match.values) {
                        if (match.values.hasOwnProperty(key) && match.values[key] == "%all%") {
                            tempValues[key] = match.values[key];
                        }
                        else{
                            detectedAllCombination = false;
                        }
                    }
                    if(!detectedAllCombination && JSON.stringify(match.values) == JSON.stringify(tempValues)){
                        this.matchType = this.typeUnStrict;
                        this.match = match;
                    }
                    if(detectedAllCombination){
                        allMatch = match;
                    }
                }).bind(this));
            }
            if(this.matchType == this.typeNone && this.match == null && allMatch != null){
                this.matchType = this.typeAll;
                this.match = allMatch;
            }
        }
        else{
            this.matchType = this.typeDefault;
        }
    },

    switchDefaultImages: function () {
        if (this.defaultImg) {
            $$(this.imageSelector).each((function (image) {
                image.src = this.defaultImg;
            }).bind(this));
        }
        this.updateZoomImage(this.defaultZoomImg);
        if (this.gallerySelector && this.defaultGallery) {
            $$(this.gallerySelector).first().update(this.defaultGallery);
        }  
        //this.initCloudZoom();
        //this.initProductMediaManager();
        this.rwdFix();
        couresel_init();
    },

    getSelectedOptions: function () {
        var options = {};
        $$('body .product-custom-option').each((function (element) {
            var optionId = 0;
            if (['checkbox', 'radio', 'select-one', 'select-multiple'].indexOf(element.type) === -1 ) {
                return false;
            }

            element.name.sub(/[0-9]+/, function (match) {
                optionId = parseInt(match[0], 10);
            });

            if (element.type == 'checkbox' || element.type == 'select-multiple') {
                if (typeof options[optionId] == "undefined") {
                    options[optionId] = [];
                }
            }

            if ((element.type == 'checkbox' || element.type == 'radio') && element.checked) {
                if (element.type == 'checkbox') {
                    options[optionId].push(element.getValue());
                } else {
                    options[optionId] = parseInt(element.getValue()) > -1 ? parseInt(element.getValue()) : undefined;
                }
            } else if ((element.type == 'select-one' || element.type == 'select-multiple') && 'options' in element) {
                $A(element.options).each((function (selectOption) {
                    if ('selected' in selectOption && selectOption.selected) {
                        if (element.type == 'select-multiple') {
                            options[optionId].push(selectOption.value);
                        } else {
                            options[optionId] = parseInt(selectOption.value) > -1 ? parseInt(selectOption.value) : undefined;
                        }
                    }
                }));
            }
            if (typeof options[optionId] != "undefined" && options[optionId].length <= 0) {
                delete options[optionId];
            }
        }));
        for (var option in options) {
            if (options.hasOwnProperty(option) && options[option] == undefined) {
                delete options[option];
            }
        }
        return options;
    },
    changeImages: function (images) {
        function updateGallery() {
            elements = $$(this.gallerySelector);
            if (elements.length) {
                var gallery = elements.first();
                if (images.length) {
                    gallery.update("");
                    var i = 0;
                    images.each((function (image) {
                        var item = this.replace_template(this.galleryImageTemplate, image.imageUrl, image.zoomImageUrl, image.thumbImageUrl, i);
                        gallery.insert(item);
                        i++
                    }).bind(this));
                } else {
                    gallery.update(this.defaultGallery);
                }
            }
        }
        if (images[0].imageUrl) {
            var elements = $$(this.imageSelector);
            elements.each(function (image) {
                image.src = images[0].imageUrl;
                image.setAttribute('data-zoom-image', images[0].zoomImageUrl);
            });
            this.updateZoomImage(images[0].zoomImageUrl);
            updateGallery.call(this);            
            //this.initCloudZoom();
            //this.initProductMediaManager(images);            
            this.rwdFix();
            couresel_init();
        }
    },
    cloneObj: function (obj) {
        var target = {};
        for (var i in obj) {
            if (obj.hasOwnProperty(i)) {
                target[i] = obj[i];
            }
        }
        return target;
    },
    rwdFix: function () {
        $$(".product-image .product-image-gallery .gallery-image").each(function (element) {
            element.removeClassName("visible");
        });
        $$(this.imageSelector).each(function (element) {
            element.addClassName("visible");
        });
    },
    replace_template: function (template, imageUrl, zoomUrl, thumbUrl, index) {
        template = template.replace(new RegExp("%image%",'g'), imageUrl);
        template = template.replace(new RegExp("%zoom_image%",'g'), zoomUrl);
        template = template.replace(new RegExp("%thumbnail%",'g'), thumbUrl);
        template = template.replace(new RegExp("%alt%",'g'), '');//todo: передавати тітлу і альт
        template = template.replace(new RegExp("%title%",'g'), '');
        template = template.replace(new RegExp("%index%",'g'), index);

        return template;
    }
};