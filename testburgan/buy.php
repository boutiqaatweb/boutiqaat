<!--
This merchant demo is published by Knet as a demonstration of the process
of Online Knet Payment Gateway Transactions. Note however that this is not
a fully running demo and there are parts that the merchant has to build him self.
Also, this demo is not tested for security or stability, and Knet does not intend to recommend
this for production purposes. Merchants should build their own web pages based on their needs. 
This demo is just a guide as to what the whole process will look like.
/*Developed by saqib 18-08-2009*/
-->
<?php 
ob_start();
// ini_set("display_errors", "1");
// error_reporting(E_ALL);
	$trackid_rand = rand(11111111,99999999);
	require_once "com/aciworldwide/commerce/gateway/plugins/e24PaymentPipe.inc.php" ;
	$Pipe = new e24PaymentPipe;

   $Pipe->setAction(1);
   $Pipe->setCurrency(414);
   $Pipe->setLanguage("ENG"); //change it to "ARA" for arabic language
   $Pipe->setResponseURL("http://www.boutiqaat.com/testburgan/response.php"); // set your respone page URL
   $Pipe->setErrorURL("http://www.boutiqaat.com/testburgan/error.php"); //set your error page URL
   $Pipe->setAmt("1"); //set the amount for the transaction
   //$Pipe->setResourcePath("/Applications/MAMP/htdocs/php-toolkit/resource/");
   $Pipe->setResourcePath("/var/www/vhosts/boutiqaat.com/httpdocs/testburgan/resource/"); //change the path where your resource file is
   //$Pipe->setResourcePath("/var/www/vhosts/boutiqaat.com/httpdocs/testburgan/liveresource/"); //change the path where your resource file is
   $Pipe->setAlias("t2802"); //set your alias name here
   //$Pipe->setAlias("t1862");
   $Pipe->setTrackId($trackid_rand);//generate the random number here
 
   $Pipe->setUdf1("UDF 1"); //set User defined value
   $Pipe->setUdf2("UDF 2"); //set User defined value
   $Pipe->setUdf3("UDF 3"); //set User defined value
   $Pipe->setUdf4("UDF 4"); //set User defined value
   $Pipe->setUdf5("UDF 5"); //set User defined value

            //get results
		if($Pipe->performPaymentInitialization()!=$Pipe->SUCCESS){
				echo "Result=".$Pipe->SUCCESS;
				echo "<br>".$Pipe->getErrorMsg();
				echo "<br>".$Pipe->getDebugMsg();
			//header("location: https://www.yourURL.com/error.php");
		}else {
         // echo "res:";
         //echo "<pre>";
         //print_r($Pipe);
			$payID = $Pipe->getPaymentId();
         $payURL = $Pipe->getPaymentPage();
			//echo $Pipe->getDebugMsg();
         //echo $payURL."?PaymentID=".$payID;
			header("location:".$payURL."?PaymentID=".$payID);
		} 
?>
<!-- <form action="https://pgtest.burgan.com:443/gateway/servlet/PaymentInitHTTPServlet" method="post">
   <input name="id" type="hidden" value="2802" />
   <input name="password" type="hidden" value="admin1234" />
   <input name="currency" type="hidden" value="414" />
   <input name="language" type="hidden" value="ENG" />
   <input name="amt" type="hidden" value="10" />
   <input name="errorURL" type="hidden" value="http://www.boutiqaat.com/testburgan/error.php" />
   <input name="responseURL" type="hidden" value="http://www.boutiqaat.com/testburgan/response.php" />
   <input name="trackId" type="hidden" value="234234243" />   
   <input type="submit" value="submit"/>
</form> -->