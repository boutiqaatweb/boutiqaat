<?php
require_once 'app/Mage.php';
Mage::app();
//$customer = Mage::getSingleton('customer/customer')->load(136);
$productIds = array(1199,4254);
$ordered_qty = array(2,1);
$quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('customer_id', 147)
		->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
$cartarr = array();                                
for($i = 0; $i < count($productIds); $i++){
	$cartarr[$productIds[$i]] = (isset($cartarr[$productIds[$i]]) ? $cartarr[$productIds[$i]] : 0) + $ordered_qty[$i];
}
$newQty = array();
foreach ($cartarr as $key => $value) {
	$_product = Mage::getModel('catalog/product')->load($key);
	$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
	if($stock->getQty() >= $value){
		continue;
	}
	else{
		$newQty[$key] = $stock->getQty();
	}
}

$quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('customer_id', 147)
		->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
$quoteCollection = $quote->getAllVisibleItems();
foreach ($quoteCollection as $_item) {
	$item = $quote->getItemById($_item->getItemId());
	$productId = $item->getProductId();
	//echo $item->getQty();
	if(isset($newQty[$productId])){
		if($newQty[$productId]>=$item->getQty()){
			$newQty[$productId] = $newQty[$productId] - $item->getQty();
		}
		else{
			$item->setQty($newQty[$productId]);
			$newQty[$productId] = 0;
			$quote->collectTotals()->save();
		}
	}
}
$quote->collectTotals()->save();
$quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('customer_id', 147)
		->addFieldToFilter('is_active', 1)->setOrder('entity_id', 'desc')->getFirstItem();
$quoteNewC = $quote->getAllVisibleItems();
foreach ($quoteNewC as $iitem) {
	echo $iitem->getQty();
	echo "<br>";
}